; ndk, libc - [ start.asm ]
;
; Assembler source for start.ndk, the ndk executable stub.
; Currently empty, but used in linking with lcc
;
; (c)2002 dcipher / neuraldk

global start
extern main
extern ndk_exit

[bits 32]
section .data

  ndkBinary:	db 'NDK Binary :)', 0

section .text
start:
  call main
  call ndk_exit
  ret
