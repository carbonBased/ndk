; ndk, libc - [ end.asm ]
;
; NDK shutdown object, used by lcc to create ndk (rdoff)
; binary files.
;
; (c) 2002 dcipher / neuraldk

global ndk_exit

[bits 32]
section .data

  NDKExit:	db 'End of NDK Binary :)',0

section .text

ndk_exit:
  ret
