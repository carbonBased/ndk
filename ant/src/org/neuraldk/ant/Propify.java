package org.neuraldk.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import java.util.Vector;
import java.util.Iterator;
import java.io.*;

public class Propify extends Task
{
   private static final String propFilename = "build.properties";
   public void execute()
   {
      String destDir = getProject().getProperty("path.generated");
      String propDir = getProject().getProperty("path.props");
      StringBuffer headerBuffer = new StringBuffer();
      StringBuffer asmBuffer = new StringBuffer();
      Vector properties = new Vector();
      boolean useArrays = "1".equals(getProject().getProperty("propdb.useArrays"));

      readProps(propDir + "/" + propFilename, properties);
      readProps(propDir + "/" + getProject().getProperty("platform.architecture") + "/" + propFilename, properties);

      if( useArrays )
      {
         headerBuffer.append("#include <types.h>\n\n");

         headerBuffer.append("#define PROPERTY_MAX_NAME_LENGTH   (32)\n");
         headerBuffer.append("#define PROPERTY_MAX_VALUE_LENGTH  (32)\n");

         headerBuffer.append("typedef struct _PropertyRecord\n");
         headerBuffer.append("{\n");
         headerBuffer.append("   const Character propertyName[PROPERTY_MAX_NAME_LENGTH];\n");
         headerBuffer.append("   const Character propertyValue[PROPERTY_MAX_VALUE_LENGTH];\n");
         headerBuffer.append("} PropertyRecord;\n\n");

         headerBuffer.append("extern PropertyRecord properties[];\n\n");
      }

      writePropCHeader(destDir + "/properties.h", headerBuffer, properties);
      writePropAsmHeader(destDir + "/properties.inc", asmBuffer, properties);

      if( useArrays )
      {
         StringBuffer dbBuffer = new StringBuffer();

         dbBuffer.append("#include \"properties.h\"\n\n");

         writePropDatabase(destDir + "/properties.c", dbBuffer, properties);
      }
      else
      {
         File file = new File(destDir + "/properties.c");
         file.delete();
      }
   }

   protected void readProps(String propFile, Vector props)
   {
      BufferedReader input = null;
      String line;

      try
      {
         input = new BufferedReader( new FileReader( new File( propFile ) ) );
         while (( line = input.readLine()) != null)
         {
            if(line.indexOf("#") == -1)
            {
               props.add( line );
            }
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      finally
      {
         try
         {
            if (input != null)
            {
               input.close();
            }
         }
         catch (IOException ex)
         {
            ex.printStackTrace();
         }
      }
   }

   void writePropCHeader(String filename, StringBuffer buffer, Vector props)
   {
      Iterator it;

      it = props.iterator();
      while(it.hasNext())
      {
         String line = (String)it.next();

         // transform the line (rembering that replaceAll accepts a regex, where .
         // represents any character, therefore \. must be used to change a period
         String[] tokens = line.split("=");

         if(tokens.length == 2)
         {
            String name  = tokens[0];
            String value = tokens[1];
            String quotedValue = "\"" + value + "\"";

            name = name.replaceAll("\\.", "_");

            buffer.append("#define Property_" + name + " " + value + "\n");
            buffer.append("#define PropertyQuoted_" + name + " " + quotedValue + "\n");
         }
      }

      writeFile( filename, buffer );
   }

   void writePropAsmHeader(String filename, StringBuffer buffer, Vector props)
   {
      Iterator it;

      it = props.iterator();
      while(it.hasNext())
      {
         String line = (String)it.next();

         // transform the line (rembering that replaceAll accepts a regex, where .
         // represents any character, therefore \. must be used to change a period
         String[] tokens = line.split("=");

         if(tokens.length == 2)
         {
            String name  = tokens[0];
            String value = tokens[1];
            String quotedValue = "\"" + value + "\"";

            name = name.replaceAll("\\.", "_");

            buffer.append("%define Property_" + name + " " + value + "\n");
            buffer.append("%define PropertyQuoted_" + name + " " + quotedValue + "\n");
         }
      }

      writeFile( filename, buffer );
   }

   void writePropDatabase(String filename, StringBuffer buffer, Vector props)
   {
      Iterator it;

      buffer.append("PropertyRecord properties[] = {\n");

      it = props.iterator();
      while(it.hasNext())
      {
         String line = (String)it.next();

         String[] tokens = line.split("=");

         if(tokens.length == 2)
         {
            String name  = tokens[0];
            String value = tokens[1];

            name = name.replaceAll("\\.", "_");
            buffer.append("   { \"" + name + "\", \"" + value + "\" }, \n");
         }
      }


      buffer.append("};\n\n");

      writeFile( filename, buffer );
   }

   void writeFile(String filename, StringBuffer buffer)
   {
      Writer output = null;
      try
      {
         output = new BufferedWriter( new FileWriter( new File( filename ) ) );
         output.write( buffer.toString() );
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      finally
      {
         if (output != null)
         {
            try
            {
               output.close();
            }
            catch( Exception e )
            {
               e.printStackTrace();
            }
         }
      }
   }
}