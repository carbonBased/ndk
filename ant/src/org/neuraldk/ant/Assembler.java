package org.neuraldk.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline;
import org.apache.tools.ant.types.DirSet;
import org.neuraldk.ant.Flag;

import java.util.Vector;
import java.io.File;

public class Assembler extends Task
{
   String asm = "nasm";
   String outputDir = "built/obj";
   Vector flags;
   Vector includes;
   Vector sourceFileSets;
   Executer exec;

   public Assembler()
   {
      flags = new Vector();
      sourceFileSets = new Vector();
      includes = new Vector();
   }

   // The method executing the task
   public void execute() throws BuildException
   {
      int i, c;
      Commandline.Argument arg;
      Commandline.Argument src, dest;

      exec = new Executer();

      exec.setProject( getProject() );
      exec.setFailonerror( false );
      exec.setExecutable( asm );

      // generate all the include directories
      for(i = 0; i < includes.size(); i++)
      {
         DirectoryScanner ds = ((DirSet)includes.get(i)).getDirectoryScanner(getProject());
         String[] dirs = ds.getIncludedDirectories();
         String baseDir = ds.getBasedir().getName();

         /* add the base dir in */
         arg = exec.createArg();
         arg.setValue( "-I" +  baseDir + "/");

         /* and all included sub dirs */
         for (c = 0; c < dirs.length; c++)
         {
            arg = exec.createArg();
            arg.setValue( "-I" +  baseDir + "/" + dirs[c] + "/");
         }
      }

      // generate the argument line from all the flags
      for(i = 0; i < flags.size(); i++)
      {
         arg = exec.createArg();
         arg.setValue( ((Flag)flags.get(i)).getName() );
      }

      arg = exec.createArg();
      arg.setValue("-o");
      dest = exec.createArg();
      src  = exec.createArg();

      // generate the full command line by iterating through all the
      // source files to be compiled
      for(i = 0; i < sourceFileSets.size(); i++)
      {
         DirectoryScanner ds = ((FileSet)sourceFileSets.get(i)).getDirectoryScanner(getProject());
         String[] files = ds.getIncludedFiles();
         String baseDir = ds.getBasedir().getName();
         for (c = 0; c < files.length; c++)
         {
            String srcFileString = baseDir + "/" + files[c];
            String dstFileString = outputDir + "/" + files[c].replaceAll(".asm$", ".o");
            File   srcFile = new File(srcFileString), dstFile = new File(dstFileString);

            /* only assemble files that actually need to be... */
            if(!dstFile.exists() || ( srcFile.lastModified() > dstFile.lastModified()))
            {
               src.setValue(srcFileString);
               dest.setValue(dstFileString);

               System.out.println(files[c]);
               System.out.println(exec.getCommandLine());
               exec.execute();
            }
         }
      }
   }

   public void setAssembler(String val)
   {
      asm = val;
   }

   public void setOutput(String val)
   {
      outputDir = val;
   }

   public void addConfiguredFlag(Flag flag)
   {
      flags.add(flag);
   }

   public void addConfiguredFileSet(FileSet fs)
   {
      sourceFileSets.add(fs);
   }

   /* and dirset's for include files */
   public void addConfiguredDirSet(DirSet incs)
   {
      includes.add(incs);
   }
}
