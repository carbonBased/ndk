package org.neuraldk.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline;
import org.apache.tools.ant.types.DirSet;
import org.neuraldk.ant.Flag;

import java.util.Vector;
import java.io.*;

public class CCompiler extends Task
{
   String compiler = "gcc";
   String outputDir = "built/obj";
   String lstDir = "built/lst";
   Vector flags;
   Vector sourceFileSets;
   Vector includes;
   Executer exec;

   public CCompiler()
   {
      flags = new Vector();
      sourceFileSets = new Vector();
      includes = new Vector();
   }

   // The method executing the task
   public void execute() throws BuildException
   {
      int i, c;
      Commandline.Argument arg;
      Commandline.Argument src, dest, lst = null;
      //String argumentLine = "";

      exec = new Executer();

      exec.setProject( getProject() );
      exec.setFailonerror( true );
      exec.setExecutable( compiler );
      exec.setVMLauncher( false );

      arg = exec.createArg();
      arg.setValue("-c");

      // generate the argument line from all the flags
      for(i = 0; i < flags.size(); i++)
      {
         //argumentLine += ((Flag)flags.get(i)).getName();
         arg = exec.createArg();
         arg.setValue( ((Flag)flags.get(i)).getName() );
      }

      // generate all the include directories
      for(i = 0; i < includes.size(); i++)
      {
         DirectoryScanner ds = ((DirSet)includes.get(i)).getDirectoryScanner(getProject());
         String[] dirs = ds.getIncludedDirectories();
         String baseDir = ds.getBasedir().getName();

         /* add the base dir in */
         arg = exec.createArg();
         arg.setValue( "-I" +  baseDir);

         /* and all included sub dirs */
         for (c = 0; c < dirs.length; c++)
         {
            arg = exec.createArg();
            arg.setValue( "-I" +  baseDir + "/" + dirs[c]);
         }
      }

      if( compiler.equals("gcc") )
      {
         arg = exec.createArg();
         arg.setValue("-Wa,-ahslmndc");
         lst = exec.createArg();
      }

      src  = exec.createArg();
      arg = exec.createArg();
      arg.setValue("-o");
      dest = exec.createArg();

      // generate the full command line by iterating through all the
      // source files to be compiled
      for(i = 0; i < sourceFileSets.size(); i++)
      {
         DirectoryScanner ds = ((FileSet)sourceFileSets.get(i)).getDirectoryScanner(getProject());
         String[] files = ds.getIncludedFiles();
         String baseDir = ds.getBasedir().getName();
         for (c = 0; c < files.length; c++)
         {
            String srcFileString = baseDir + "/" + files[c];
            String dstFileString = outputDir + "/" + files[c].replaceAll(".c$", ".o");
            String lstFileString = lstDir + "/" + files[c].replaceAll(".c$", ".lst" );
            File   srcFile = new File(srcFileString), dstFile = new File(dstFileString);
            //File   lstFile = new File(lstFileString);

            /* only compile files that actually need to be... */
            if(!dstFile.exists() || ( srcFile.lastModified() > dstFile.lastModified()))
            {
               src.setValue(srcFileString);
               dest.setValue(dstFileString);
               if( compiler.equals("gcc") )
               {
                  lst.setValue( "-Wa,-a=" + lstFileString );
               }

               System.out.println(files[c]);
               System.out.println(exec.getCommandLine());
               exec.execute();
            }

            //System.out.println(commandLine);

            /*
            try
            {
               //String proc_str;
               Process proc = Runtime.getRuntime().exec(commandLine);
               DataInputStream in = new DataInputStream(proc.getInputStream());

               // the process above has it's output redirected... lets make sure it
               // goes to STDOUT
               while(true) System.out.println(in.readChar());
            }
            catch (EOFException e)
            {
               System.out.println("EOFException!");
            }
            catch (IOException e)
            {
               System.out.println("IOException!");
               System.exit(0);
            }
            */
         }
      }
   }

   // The setter for the "op1" attribute
   public void setCompiler(String val)
   {
      compiler = val;
   }

   public void setOutput(String val)
   {
      outputDir = val;
   }

   public void addConfiguredFlag(Flag flag)
   {
      flags.add(flag);
   }

   /* use fileset's for c source */
   public void addConfiguredFileSet(FileSet fs)
   {
      sourceFileSets.add(fs);
   }

   /* and dirset's for include files */
   public void addConfiguredDirSet(DirSet incs)
   {
      includes.add(incs);
   }
}
