package org.neuraldk.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.types.Commandline;
import org.neuraldk.ant.Flag;

import java.util.Vector;
import java.io.*;

public class Indent extends Task
{
	Vector flags;
	Vector sourceFileSets;
	Executer exec;

	public Indent()
	{
		flags = new Vector();
		sourceFileSets = new Vector();
	}

	// The method executing the task
	public void execute() throws BuildException
	{
		int i, c;
		Commandline.Argument arg;
		Commandline.Argument src, dest;

		exec = new Executer();

		exec.setProject( getProject() );
		exec.setFailonerror( true );
		exec.setExecutable( "indent" );
		exec.setVMLauncher( false );

		// generate the argument line from all the flags
		for(i = 0; i < flags.size(); i++)
		{
			arg = exec.createArg();
			arg.setValue( ((Flag)flags.get(i)).getName() );
		}

		src  = exec.createArg();
		arg = exec.createArg();
		arg.setValue("-o");
		dest = exec.createArg();

		for(i = 0; i < sourceFileSets.size(); i++)
		{
			DirectoryScanner ds = ((FileSet)sourceFileSets.get(i)).getDirectoryScanner(getProject());
			String[] files = ds.getIncludedFiles();
			String baseDir = ds.getBasedir().getName();
			for (c = 0; c < files.length; c++)
			{
				String srcFileString = baseDir + "/" + files[c];
				String dstFileString = getProject().getProperty("path.built") + "/indent/" + files[c];

				/* outputs *.ci files, for c indented source */

				src.setValue(srcFileString);
				dest.setValue(dstFileString);

				System.out.println(files[c]);
				System.out.println(exec.getCommandLine());
				exec.execute();
			}
		}
	}

	public void addConfiguredFlag(Flag flag)
	{
		flags.add(flag);
	}

	public void addConfiguredFileSet(FileSet fs)
	{
		sourceFileSets.add(fs);
	}
}
