package org.neuraldk.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline;
import org.neuraldk.ant.Flag;

import java.util.Vector;
import java.io.*;

public class Linker extends Task
{
	String linker = "ld";
	String outputDir = "built/bin";
	String outputFile = "/ndk.kernel";
	String script;// = "src/ldscript";
	Vector flags;
	Vector sourceFileSets;
	Executer exec;

	public Linker()
	{
		flags = new Vector();
		sourceFileSets = new Vector();
		script = null;
	}

	// The method executing the task
	public void execute() throws BuildException
	{
		int i, c;
		Commandline.Argument arg;
		Commandline.Argument src, dest;
		File linkScript;
		Writer linkScriptWriter;

		exec = new Executer();

		exec.setProject( getProject() );
		exec.setFailonerror( true );
		exec.setExecutable( linker );
		exec.setVMLauncher( false );

		// generate the argument line from all the flags
		for(i = 0; i < flags.size(); i++)
		{
			arg = exec.createArg();
			arg.setValue( ((Flag)flags.get(i)).getName() );
		}

		if(script != null)
		{
			arg = exec.createArg();
			arg.setValue("-T");
			arg = exec.createArg();
			arg.setValue(outputDir + "/ldscript");
			//arg.setValue("src/ldscript");
		}

		arg = exec.createArg();
		arg.setValue("-o");
		arg = exec.createArg();
		arg.setValue(outputDir + "/" + outputFile);

		try
		{
			if(script != null)
			{
				System.out.println("Creating linker script: " + outputDir + "/ldscript");
				linkScript = new File(outputDir + "/ldscript");
				linkScriptWriter = new BufferedWriter( new FileWriter(linkScript) );
			}
			else linkScriptWriter = null;

			// generate a linker script...
			for(i = 0; i < sourceFileSets.size(); i++)
			{
				DirectoryScanner ds = ((FileSet)sourceFileSets.get(i)).getDirectoryScanner(getProject());
				String[] files = ds.getIncludedFiles();
				String baseDir = ds.getBasedir().getName();
				for (c = 0; c < files.length; c++)
				{
					if(script != null)
					{
						linkScriptWriter.write( "INPUT(" + baseDir + "/" + files[c] + ")\n");
					}
					else
					{
						arg = exec.createArg();
						arg.setValue(baseDir + "/" + files[c]);
					}
				}
			}

			if(script != null)
			{
				// now append the standard linker script afterwards...
				FileReader scriptFile = new FileReader(script);
				BufferedReader scriptReader = new BufferedReader(scriptFile);
				while((c = scriptReader.read()) != -1)
				{
					linkScriptWriter.write(c);
				}
				scriptReader.close();
				linkScriptWriter.close();
			}
		}
		catch(IOException e)
		{
			System.out.println("Couldn't create linker script");
			System.exit(-1);
		}

		System.out.println(exec.getCommandLine());
		exec.execute();

	}

	// The setter for the "op1" attribute
	public void setLinker(String val)
	{
		linker = val;
	}

	public void setOutputDir(String val)
	{
		outputDir = val;
	}

	public void setOutputFile(String val)
	{
		outputFile = val;
	}

	public void setScript(String val)
	{
		script = val;
	}

	public void addConfiguredFlag(Flag flag)
	{
		flags.add(flag);
	}

	public void addConfiguredFileSet(FileSet fs)
	{
		sourceFileSets.add(fs);
	}
}
