package org.neuraldk.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class Flag extends Task
{
	String flag;

	public Flag()
	{
	}

	public void setName(String name)
	{
		flag = name;
	}

	public String getName()
	{
		return flag;
	}
}