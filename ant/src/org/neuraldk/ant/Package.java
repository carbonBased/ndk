package org.neuraldk.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline;
import org.apache.tools.ant.types.DirSet;

import java.util.Vector;
import java.io.*;

public class Package extends Task
{
   String tar = "tar";
   String gzip = "gzip";
   String directory, includes;
   Executer exec;

   public Package()
   {
      directory = "distrib";
   }

   // The method executing the task
   public void execute() throws BuildException
   {
      tarDistrib();
      gzipDistrib();
   }

   // The setter for the "op1" attribute
   public void setDir(String val)
   {
      directory = val;
   }

   public void setIncludes(String inc)
   {
      includes = inc;
   }

   private void tarDistrib()
   {
      Commandline.Argument arg;

      exec = new Executer();

      exec.setProject( getProject() );
      exec.setFailonerror( true );
      exec.setExecutable( tar );
      exec.setVMLauncher( false );
      exec.setDir( new File(directory) );

      arg = exec.createArg();
      arg.setValue("-c");

      /*
      arg = exec.createArg();
      arg.setValue("-C");

      arg = exec.createArg();
      arg.setValue(directory);
      */

      arg = exec.createArg();
      arg.setValue("-f");

      arg = exec.createArg();
      arg.setValue(includes + ".tar");


      /*
      arg = exec.createArg();
      arg.setValue("-K");

      arg = exec.createArg();
      arg.setValue(includes);
      */

      arg = exec.createArg();
      arg.setValue(includes);
      //arg.setValue(directory + "/" + includes);

      System.out.println(exec.getCommandLine());
      exec.execute();
   }

   private void gzipDistrib()
   {
      Commandline.Argument arg;

      exec = new Executer();

      exec.setProject( getProject() );
      exec.setFailonerror( true );
      exec.setExecutable( gzip );
      exec.setVMLauncher( false );
      exec.setDir( new File(directory) );

      arg = exec.createArg();
      arg.setValue("-9");

      arg = exec.createArg();
      arg.setValue(includes + ".tar");

      System.out.println(exec.getCommandLine());
      exec.execute();
   }
}
