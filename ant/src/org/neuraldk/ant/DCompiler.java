package org.neuraldk.ant;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline;
import org.neuraldk.ant.Flag;
import org.neuraldk.ddfc.*;

import java.util.Vector;
import java.io.*;

public class DCompiler extends Task
{
	Vector ddfFileSets;
	String outputDir;
	Executer exec;

	public DCompiler()
	{
		ddfFileSets = new Vector();
	}

	// The method executing the task
	public void execute() throws BuildException
	{
		DriverDefinition ddf;
		int i, c;
		
		for(i = 0; i < ddfFileSets.size(); i++)
		{
			DirectoryScanner ds = ((FileSet)ddfFileSets.get(i)).getDirectoryScanner(getProject());
			String[] files = ds.getIncludedFiles();
			String baseDir = ds.getBasedir().getName();
			for (c = 0; c < files.length; c++)
			{
				// execute ddfc on  baseDir + "/" + files[c]
				ddf = new DriverDefinition(baseDir + "/" + files[c]);
				ddf.compile();
				ddf.write(outputDir + "/");
			}
		}
	}

	public void setOutput(String val)
	{
		outputDir = val;
	}
	
	public void addConfiguredFileSet(FileSet fs)
	{
		ddfFileSets.add(fs);
	}	
}
