/* ndk - [ testsTaskSpecificHandle.c ]
 *
 * Code to test the task specific handle module
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>
#include <core/arch/i386/taskSpecificHandle.h>

static ErrorCode testsTaskSpecificHandleCreate(TestCase me);
static ErrorCode testsTaskSpecificHandleSet(TestCase me);
static ErrorCode testsTaskSpecificHandleGet(TestCase me);

static TaskSpecificHandle handle;
static TaskSpecificHandle handle2;

ErrorCode testsTaskSpecificHandleInit(TestGroup * group)
{
   TestCase tc;
   
   testGroupCreate(group, "taskSpecificHandle");

   testCaseCreate(&tc, "taskSpecificHandleCreate", testsTaskSpecificHandleCreate);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "taskSpecificHandleSet", testsTaskSpecificHandleSet);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "taskSpecificHandleGet", testsTaskSpecificHandleGet);
   testGroupAddTestCase(*group, tc);

   return ErrorNoError;   
}

static ErrorCode testsTaskSpecificHandleCreate(TestCase me)
{
   ErrorCode ec;
   
   ec = taskSpecificHandleCreate( &handle );
   
   printf("Created task specific handle %d\n", handle );

   ec = taskSpecificHandleCreate( &handle2 );
   
   printf("Created second task specific handle %d\n", handle2 );

   
   return ec;
}

static ErrorCode testsTaskSpecificHandleSet(TestCase me)
{
   ErrorCode ec;
   uint32 data = 0x12340001;
   
   ec = taskSpecificHandleSet( handle, data );
   printf("set data to 0x%x\n", data );
   
   return ec;
}

static ErrorCode testsTaskSpecificHandleGet(TestCase me)
{
   ErrorCode ec;
   uint32 data = 0x21;
   
   ec = taskSpecificHandleGet( handle, &data );
   printf("received data of 0x%x\n", data );
   
   return ec;
}
