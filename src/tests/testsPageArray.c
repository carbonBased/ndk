/* ndk - [ testsPageAlloc.c ]
 *
 * Code to test the page allocator
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>
#include <pageBasedArray.h>

static ErrorCode testsPageArrayCreate(TestCase me);
static ErrorCode testsPageArraySet(TestCase me);
static ErrorCode testsPageArrayGet(TestCase me);
static ErrorCode testsPageArraySetLong(TestCase me);
static ErrorCode testsPageArrayGetLong(TestCase me);
static ErrorCode testsPageArrayGetFaulty(TestCase me);

static PageArray pa;

typedef struct _PageArrayElement
{
   uint32 num;
   char *name;
   Boolean flag;
   uint32 num2;
} PageArrayElement;

PageArrayElement sampleElement = { 0x12345678, "testing", True, 0x11011E11 };

ErrorCode testsPageArrayInit(TestGroup * group)
{
   TestCase tc;

   testGroupCreate(group, "pageArray");

   testCaseCreate(&tc, "pageArrayCreate", testsPageArrayCreate);
   testGroupAddTestCase(*group, tc);

   testCaseCreate(&tc, "pageArraySet", testsPageArraySet);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageArrayGet", testsPageArrayGet);
   testGroupAddTestCase(*group, tc);
   
   
   /*
   testCaseCreate(&tc, "pageArraySetLong", testsPageArraySetLong);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageArrayGetLong", testsPageArrayGetLong);
   testGroupAddTestCase(*group, tc);
   */

   testCaseCreate(&tc, "pageArrayGetFaulty", testsPageArrayGetFaulty);
   testGroupAddTestCase(*group, tc);

}

static ErrorCode testsPageArrayCreate(TestCase me)
{
   ErrorCode err;
   
   err = pageArrayCreate( &pa, sizeof( PageArrayElement ), 0, True );
   
   return err;
}

static ErrorCode testsPageArraySet(TestCase me)
{
   ErrorCode err;
   
   err = pageArraySetItem( pa, 2, &sampleElement );
   
   return err;
}

static ErrorCode testsPageArrayGet(TestCase me)
{
   ErrorCode err = ErrorNoError;
   PageArrayElement receivedElement;
   
   err = pageArrayGetItem( pa, 2, (Pointer*)&receivedElement );
   if( err == ErrorNoError )
   {
      printf("orig: %d %s %d %d\n", sampleElement.num, sampleElement.name, sampleElement.flag, sampleElement.num2 );
      printf("recv: %d %s %d %d\n", receivedElement.num, receivedElement.name, receivedElement.flag, receivedElement.num2 );
      if( memcmp( &receivedElement, &sampleElement, sizeof( PageArrayElement ) ) )
      {
         err = ErrorBadParam;
      }
   }
   
   return err;
}


static ErrorCode testsPageArraySetLong(TestCase me)
{
   ErrorCode err;
   
   err = pageArraySetItem( pa, 567, &sampleElement );
   
   return err;
}

static ErrorCode testsPageArrayGetLong(TestCase me)
{
   ErrorCode err = ErrorNoError;
   PageArrayElement receivedElement;
   
   err = pageArrayGetItem( pa, 567, (Pointer*)&receivedElement );
   if( err == ErrorNoError )
   {
      if( memcpy( &receivedElement, &sampleElement, sizeof( PageArrayElement ) ) )
      {
         err = ErrorBadParam;
      }
   }
   
   return err;
}

static ErrorCode testsPageArrayGetFaulty(TestCase me)
{
   ErrorCode err = ErrorNoError;
   PageArrayElement receivedElement;
   
   /* 1 hasn't been set */
   err = pageArrayGetItem( pa, 1, (Pointer*)&receivedElement );
   if( err == ErrorNotFound )
   {
      err = ErrorNoError;
   }
   
   return err;
}
