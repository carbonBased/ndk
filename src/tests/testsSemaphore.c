/* ndk - [ testsSemaphore.c ]
 *
 * Code to test the ndk semaphore implementation
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>
#include <core/semaphore.h>

static SemaphoreId semId;
static Semaphore sem;

static ErrorCode testsSemaphoreCreate(TestCase me);
static ErrorCode testsSemaphoreMap(TestCase me);
static ErrorCode testsSemaphoreUp(TestCase me);
static ErrorCode testsSemaphoreDown(TestCase me);

ErrorCode testsSemaphoreInit(TestGroup * group)
{
   TestCase tc;

   testGroupCreate(group, "semaphore");

   testCaseCreate(&tc, "semaphoreCreate", testsSemaphoreCreate);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "semaphoreMap", testsSemaphoreMap);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "semaphoreUp", testsSemaphoreUp);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "semaphoreUp", testsSemaphoreUp);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "semaphoreDown", testsSemaphoreDown);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "semaphoreDown", testsSemaphoreDown);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "semaphoreDown", testsSemaphoreDown);
   testGroupAddTestCase(*group, tc);
   
   return ErrorNoError;
}

static ErrorCode testsSemaphoreCreate(TestCase me)
{
   ErrorCode ec;
   
   ec = semaphoreCreate( &semId, SemaphoreTypeFIFO, 0 );
   printf("created semaphoreId 0x%8x\n", semId );
   
   return ec;
}

static ErrorCode testsSemaphoreMap(TestCase me)
{
   ErrorCode ec;
   
   ec = semaphoreMap( semId, &sem );
   printf("mapped semaphore to 0x%8x\n", sem );
   
   return ec;
}

static ErrorCode testsSemaphoreUp(TestCase me)
{
   ErrorCode ec;
   
   ec = semaphoreUp( &sem );
   
   return ec;
}

static ErrorCode testsSemaphoreDown(TestCase me)
{
   ErrorCode ec;
   
   ec = semaphoreDown( &sem, 2000 );
   
   return ec;
}