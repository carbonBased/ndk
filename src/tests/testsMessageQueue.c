/* ndk - [ testsMessageQueue.c ]
 *
 * Code to test the task specific handle module
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>
#include <core/messageQueue.h>

static ErrorCode testsMessageQueueCreate(TestCase me);
static ErrorCode testsMessageQueueMap(TestCase me);
static ErrorCode testsMessageQueueSend(TestCase me);
static ErrorCode testsMessageQueueReceive(TestCase me);

static MessageQueue queue;
static MessageQueueId queueId;

ErrorCode testsMessageQueueInit(TestGroup * group)
{
   TestCase tc;
   
   testGroupCreate(group, "messageQueueTests");

   testCaseCreate(&tc, "messageQueueCreate", testsMessageQueueCreate);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "messageQueueMap", testsMessageQueueMap);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "messageQueueSend", testsMessageQueueSend);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "messageQueueReceive", testsMessageQueueReceive);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "messageQueueReceive", testsMessageQueueReceive);
   testGroupAddTestCase(*group, tc);

   return ErrorNoError;
}

static ErrorCode testsMessageQueueCreate(TestCase me)
{
   ErrorCode ec = ErrorNoError;
   
   ec = messageQueueCreate( &queueId, 0 );
   
   return ec;
}

static ErrorCode testsMessageQueueMap(TestCase me)
{
   ErrorCode ec = ErrorNoError;
   
   ec = messageQueueMap( queueId, &queue );
   
   return ec;
}

static ErrorCode testsMessageQueueSend(TestCase me)
{
   ErrorCode ec = ErrorNoError;
   uint32 msg = 0x12346660;
   
   ec = messageQueueSend( &queue, msg );
   
   return ec;
}

static ErrorCode testsMessageQueueReceive(TestCase me)
{
   ErrorCode ec = ErrorNoError;
   uint32 msg;
   
   ec = messageQueueReceive( &queue, 1000, &msg );
   
   printf("received msg 0x%8x\n", msg );
   
   return ec;
}
