/* ndk - [ testsDriverManager.c ]
 *
 * Code to test the ndk BTree implementation
 *
 * (c)2005 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>

#include <driver.h>

static ErrorCode testDriverManagerAdd(TestCase me);

static DriverItem driverRoot;

ErrorCode testsDriverManagerInit(TestGroup * group)
{
   TestCase tc;

   testGroupCreate(group, "driverManager");

   testCaseCreate(&tc, "add", testDriverManagerAdd);
   testGroupAddTestCase(*group, tc);
}

static ErrorCode testDriverManagerAdd(TestCase me)
{
   Driver driver;

   driverCreate(&driver);
   driverSetRoot(driver, "", driverRoot);
   driverManagerAdd(driver);

   driverManagerShowTree();
}
