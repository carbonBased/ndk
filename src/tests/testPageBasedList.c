/* ndk - [ testsPageBasedList.c ]
 *
 * Code to test the page-based linked list
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>
#include <core/pageBasedList.h>
#include <core/physicalMemoryDescriptor.h>

static PageListId listId = NULL;
static PageList list = NULL;
static PhysicalMemoryDescriptor packetDesc;
static uint8 packet[1024*10]; // 10kb message?

static ErrorCode testsPageListCreate(TestCase me);
static ErrorCode testsPageListMap(TestCase me);
static ErrorCode testsPageListAdd(TestCase me);
static ErrorCode testsPageListRemoveFirst(TestCase me);
static ErrorCode testsPageListRemoveLast(TestCase me);
static ErrorCode testsPageListRemoveNext(TestCase me);
static ErrorCode testsPageListRemove(TestCase me);

static void dumpList(void);

ErrorCode testsPageListInit(TestGroup * group)
{
   TestCase tc;
   uint32 i;

   testGroupCreate(group, "pageBasedList");
   
   /* create a packet to be used with listAdd */
   for( i = 0; i < sizeof( packet ); i++ )
   {
      packet[i] = i;
   }
   physicalMemoryDescriptorCreate( &packetDesc, packet, sizeof(packet) );

   testCaseCreate(&tc, "pageListCreate", testsPageListCreate);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageListMap", testsPageListMap);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageListAdd", testsPageListAdd);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageListRemoveFirst", testsPageListRemoveFirst);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageListAdd", testsPageListAdd);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageListRemoveLast", testsPageListRemoveLast);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageListAdd", testsPageListAdd);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageListRemoveNext", testsPageListRemoveNext);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "pageListRemove", testsPageListRemove);
   testGroupAddTestCase(*group, tc);
   
   return ErrorNoError;
}

static ErrorCode testsPageListCreate(TestCase me)
{
   ErrorCode err;
   
   err = pageListCreate( &listId );
   
   printf( "created PageList with Id 0x%x\n", listId );
   
   return err;
}

static ErrorCode testsPageListMap(TestCase me)
{
   ErrorCode err = ErrorOutOfState;
   
   if( listId )
   {
      err = pageListMap( listId, &list );

      pageListDump( list );
   }
   printf("pageListMap returned list pointer 0x%8x\n", list);
   
   return err;
}

static ErrorCode testsPageListAdd(TestCase me)
{
   ErrorCode err = ErrorOutOfState;
   uint32 i;
   
   if( list )
   {
      err = pageListAdd( &list, packetDesc );
      
      for( i = 0; i < 1030; i++ )
      {
         err = pageListAdd( &list, (Pointer)i );
         printf("added message of %d to 0x%8x err=0x%8x\n", i, list, err);
      }
      
      //dumpList();
      pageListDump( list );
   }
   
   return err;
}

static ErrorCode testsPageListRemoveFirst(TestCase me)
{
   ErrorCode err = ErrorOutOfState;
   Pointer data;
   uint32 i = 0;
   
   if( list )
   {
      err = ErrorNoError;
      while( pageListRemoveFirst( list, &data ) == ErrorNoError )
      {
         printf( "Received %d element of 0x%8x\n", i++, data );
      }
      pageListDump( list );
   }
   
   return err;
}

static ErrorCode testsPageListRemoveLast(TestCase me)
{
   ErrorCode err = ErrorOutOfState;
   Pointer data;
   uint32 i = 0;
   
   if( list )
   {
      err = ErrorNoError;
      while( pageListRemoveLast( list, &data ) == ErrorNoError )
      {
         printf( "Received %d element of 0x%8x\n", i++, data );
      }
      pageListDump( list );
   }
   
   return err;
}

/* removes every other item in list using an iterator... */
static ErrorCode testsPageListRemoveNext(TestCase me)
{
   ErrorCode err = ErrorOutOfState;
   Pointer data;
   PageListNode node;
   uint32 i = 0;
   
   if( list )
   {
      err = ErrorNoError;
      
      pageListGetFirst( list, &node );
      do
      {
         if( pageListRemoveNext( list, node, &data ) == ErrorNoError )
         {
            printf( "Received %d element of 0x%8x\n", i++, data );
         }
         else
         {
            printf( "Error getting next\n" );
            break;
         }
      }
      while( pageListGetNext( list, &node ) == ErrorNoError );
      pageListDump( list );
   }
   
   return err;
}

static ErrorCode testsPageListRemove(TestCase me)
{
   ErrorCode ec;
   
   ec = pageListRemove( &list, (Pointer)51 );
   
   pageListDump( list );
   
   return ec;
}

static void dumpList(void)
{
   PageListNode iter;
   Pointer data;
   uint32 i = 0;
   
   printf("Dumping list:\n");
   
   if( pageListGetFirst( list, &iter ) == ErrorNoError )
   {
      do
      {
         pageListGetData( list, iter, &data );
         printf("%d -> 0x%8x\n", i++, data );
      } while( pageListGetNext( list, &iter ) == ErrorNoError );
   }
}
