 /* ndk - [ tests.h ]
  *
  * Code to test the ndk kernel framework
  *
  * Please see:
  *
  * (c)2005 dcipher / neuraldk
  *           www.neuraldk.org
  */

#include <errorCodes.h>
#include <types.h>

typedef struct _TestCase *TestCase;

typedef struct _TestGroup *TestGroup;

typedef ErrorCode(*TestCaseFunction) (TestCase me);

ErrorCode testsInit(void);
ErrorCode testsStart(void);
ErrorCode testCaseCreate(TestCase * tc, String name, TestCaseFunction func);
ErrorCode testGroupCreate(TestGroup * group, String name);
ErrorCode testGroupAddTestCase(TestGroup group, TestCase tc);
