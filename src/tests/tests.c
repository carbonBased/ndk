/* ndk - [ tests.c ]
 *
 * Code to test the ndk kernel framework
 *
 * Please see:
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <brink/list.h>
#include <timer.h>
#include <console.h>

struct _TestCase
{
   String name;
   TestCaseFunction exec;
};

struct _TestGroup
{
   String name;
   List testCases;
};

static List testGroups;
static ListIterator testGroupIterator;

ErrorCode testsInit(void)
{
   TestGroup group;

   // something in here causes a crash... not sure just yet...
   printf("Initializing test cases\n");

   listCreate(&testGroups);
   listIteratorCreate(&testGroupIterator, testGroups);

   //testsBTreeInit(&group);
   //listAddBack(testGroups, group);
   
   testsDriverManagerInit(&group);
   listAddBack(testGroups, group);
   
   //testsPageAllocInit(&group);
   //listAddBack(testGroups, group);
   
   //testsPageArrayInit(&group);
   //listAddBack(testGroups, group);
   
   //testsPhysDescInit( &group );
   //listAddBack(testGroups, group);
   
   //testsPageListInit( &group );
   //listAddBack( testGroups, group );
   
   //testsTaskSpecificHandleInit( &group );
   //listAddBack( testGroups, group );
   
   //testsMessageQueueInit( &group );
   //listAddBack( testGroups, group );

#if !STANDALONE
   //testsMutexInit(&group);
   //listAddBack(testGroups, group);
#endif

   //testsSemaphoreInit(&group);
   //listAddBack(testGroups, group);

   return ErrorNoError;
}

ErrorCode testsFinal(void)
{
   listDestroy(&testGroups);
   listIteratorDestroy(&testGroupIterator);
   return ErrorNoError;
}

ErrorCode testsStart(void)
{
   TestGroup testGroup;
   TestCase testCase;
   ListIterator testCaseIterator;
   ErrorCode ec;
   uint32 length;

   listGetLength(testGroups, &length);
   printf("There are %d test groups\n", length);

   listIteratorReset(testGroupIterator);
   while((ec = listIteratorGetNext(testGroupIterator, (void **)&testGroup)) == ErrorNoError)
   {
      printf("executing test group %s\n", testGroup->name);
      listIteratorCreate(&testCaseIterator, testGroup->testCases);

      //timerDelay(2000);

      while(listIteratorGetNext(testCaseIterator, (void **)&testCase) == ErrorNoError)
      {
         printf("  executing test case %s\n", testCase->name);
         ec = testCase->exec(testCase);
         printf("  test case %s: ", testCase->name );
         if( ec == ErrorNoError )
         {
            printf(" PASSED\n");
         }
         else
         {
            printf(" FAILED 0x%x\n", ec);
         }
         //timerDelay(2000);
      }

      listIteratorDestroy(&testCaseIterator);
   }
   printf("done testing %d %x\n", ec, testGroup);
   return ErrorNoError;
}

ErrorCode testCaseCreate(TestCase * tc, String name, TestCaseFunction func)
{
   TestCase tCase = (TestCase) malloc(sizeof(struct _TestCase));

   tCase->name = name;
   tCase->exec = func;

   *tc = tCase;

   return ErrorNoError;
}

ErrorCode testGroupCreate(TestGroup * group, String name)
{
   TestGroup tg;

   tg = (TestGroup) malloc(sizeof(struct _TestGroup));
   tg->name = name;

   listCreate(&tg->testCases);

   *group = tg;

   return ErrorNoError;
}

ErrorCode testGroupAddTestCase(TestGroup group, TestCase tc)
{
   listAddBack(group->testCases, (void *)tc);
   return ErrorNoError;
}
