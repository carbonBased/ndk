/* ndk - [ testsBTree.c ]
 *
 * Code to test the ndk BTree implementation
 *
 * (c)2005 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>

#include <brink/binaryTree.h>

static ErrorCode testBTreeAdd(TestCase me);
static int32 btreeNumberComparator(Boolean keyComparison, void *data1, void *data2);

ErrorCode testsBTreeInit(TestGroup * group)
{
   TestCase tc;

   testGroupCreate(group, "btree");

   testCaseCreate(&tc, "add", testBTreeAdd);
   testGroupAddTestCase(*group, tc);
}

static ErrorCode testBTreeAdd(TestCase me)
{
   BinaryTree btree;
   ErrorCode ec;
   int i;

   binaryTreeCreate(&btree, btreeNumberComparator);

   // 255 works... 256 doesn't...
   for(i = 1; i <= 256; i++)
   {
      printf("Adding %d\n", i);
      binaryTreeAdd(btree, (void *)i);
   }

   for(i = 1; i <= 256; i++)
   {
      ec = binaryTreeFind(btree, (void *)i, (void **)&i);
      printf("Got back %d %x\n", i, ec);
   }
}

static int32 btreeNumberComparator(Boolean keyComparison, void *data1, void *data2)
{
   uint32 int1, int2;

   int1 = (uint32)data1;
   int2 = (uint32)data2;

   switch (keyComparison)
   {
   case True:
   case False:
      return data1 - data2;
   }
}
