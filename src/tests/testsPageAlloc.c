/* ndk - [ testsPageAlloc.c ]
 *
 * Code to test the page allocator
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>
#include <arch/i386/pager.h>

static ErrorCode testsPageAllocOne(TestCase me);
static ErrorCode testsPageFreeOne(TestCase me);
static ErrorCode testsPageAllocAndFreeLarge(TestCase me);
static ErrorCode testsPageAllocAndFreeHuge(TestCase me);

static LinearAddress globalPointer;

ErrorCode testsPageAllocInit(TestGroup * group)
{
   TestCase tc;

   testGroupCreate(group, "pageAllocator");

   testCaseCreate(&tc, "allocOne", testsPageAllocOne);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "freeOne", testsPageFreeOne);
   testGroupAddTestCase(*group, tc);
   /* run the same tests again... */
   testCaseCreate(&tc, "allocOneAgain", testsPageAllocOne);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "freeOneAgain", testsPageFreeOne);
   testGroupAddTestCase(*group, tc);

   testCaseCreate(&tc, "allocAndFreeLarge", testsPageAllocAndFreeLarge);
   testGroupAddTestCase(*group, tc);

   testCaseCreate(&tc, "allocAndFreeHuge", testsPageAllocAndFreeHuge);
   testGroupAddTestCase(*group, tc);
}

static ErrorCode testsPageAllocOne(TestCase me)
{
   ErrorCode err;
   
   err = pagerAllocate( pagerHeapKernel, 1, &globalPointer);
   printf("received page 0x%x err=0x%x\n", globalPointer, err);
   
   return err;
}

static ErrorCode testsPageFreeOne(TestCase me)
{
   ErrorCode err;
   
   err = pagerFree( pagerHeapKernel, 1, globalPointer);
   printf("freed page 0x%x err=0x%x\n", globalPointer, err);
   
   return err;
}

static ErrorCode testsPageAllocAndFreeLarge(TestCase me)
{
   ErrorCode err;
   
   /* gauaranteed to need to create a new page table... */
   err = pagerAllocate( pagerHeapKernel, 4097, &globalPointer);
   printf("received page 0x%x err=0x%x\n", globalPointer, err);

   err = pagerFree( pagerHeapKernel, 4097, globalPointer);
   printf("freed page 0x%x err=0x%x\n", globalPointer, err);
   
   return err;
}

static ErrorCode testsPageAllocAndFreeHuge(TestCase me)
{
   ErrorCode err;
   
   /* gauaranteed to need to create two new page table... */
   err = pagerAllocate( pagerHeapKernel, 8200, &globalPointer);
   printf("received page 0x%x err=0x%x\n", globalPointer, err);

   err = pagerFree( pagerHeapKernel, 8200, globalPointer);
   printf("freed page 0x%x err=0x%x\n", globalPointer, err);
   
   return err;
}
