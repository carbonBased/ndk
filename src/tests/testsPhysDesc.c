/* ndk - [ testsPageAlloc.c ]
 *
 * Code to test the page allocator
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>
#include <core/physicalMemoryDescriptor.h>
#include <core/arch/i386/pager.h>

static ErrorCode testsPhysDesc(TestCase me);

// @TODO: SOMEHOW KERNEL DOESN'T LOAD BECAUSE OF THIS: 
//static char payload[ 4096 * 2048 ];
static char *payload;
//static char payload[ 1 ];
static uint32 payloadSize[] = { 150, 4096, 5000, 4096 * 1023, 4096 * 2000 }; 
static uint32 payloadIndex = 0;

static ErrorCode testsPhysDescMain(TestCase me);

ErrorCode testsPhysDescInit(TestGroup * group)
{
   TestCase tc;
   uint32 i;
   uint32 payloadSize = 4096*2048;

   testGroupCreate(group, "physicalMemoryDescriptor");
   
   /* setup payload */
   payload = (char*)malloc(payloadSize);
   for( i = 0; i < payloadSize; i++ )
   {
      payload[i] = (char)i;
   }

   /** @TODO need way to accept parameters, for now use global array */
   testCaseCreate(&tc, "< 4096", testsPhysDescMain);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "= 4096", testsPhysDescMain);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "> 4096", testsPhysDescMain);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "> 4096 w/ last inlined", testsPhysDescMain);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "> 4096 w/ multiple desc pages", testsPhysDescMain);
   testGroupAddTestCase(*group, tc);

}

static ErrorCode testsPhysDescMain(TestCase me)
{
   ErrorCode ec = ErrorNoError, ret = ErrorNoError;
   PhysicalMemoryDescriptor desc;
   LinearAddress linearAddr;
   uint32 size, numPages;
   
   /* round up number of pages */
   numPages = payloadSize[payloadIndex];
   if(numPages & PAGE_SIZE_MASK) numPages += PAGE_SIZE;
   numPages /= PAGE_SIZE;
   
   ec = physicalMemoryDescriptorCreate( &desc, payload, payloadSize[payloadIndex] );
   if( ec == ErrorNoError )
   {
      ec = physicalMemoryDescriptorMap( desc, &linearAddr );
      if( ec == ErrorNoError )
      {
         PhysicalAddress tempPhys1, tempPhys2;
         
         printf("Mapped %x to %x\n", payload, linearAddr );
         
         ec = physicalMemoryDescriptorGetSize( desc, &size );
         if( ec || size != payloadSize[payloadIndex] )
         {
            printf("Data is not correct size, got %d expecting %d ec=%x\n", 
               size, payloadSize[payloadIndex],ec);
               
            ret = ErrorUnknown;
         }
         
         pagerGetPhysical( linearAddr, &tempPhys2 );
         printf( "after map linear %x is %x\n", linearAddr, tempPhys2);
         
         printf( "comparing %x to %x\n", payload, linearAddr );
         printf( "payloadSize %x index %d\n", payloadSize, payloadIndex );
         printf( "payloadSize[payloadIndex] %d\n",  payloadSize[payloadIndex] );
         if( memcmp( payload, linearAddr, payloadSize[payloadIndex] ) )
         {
            uint32 i;
            
            printf("Data does not match!\n");
            
            printf("Expected:\n");
            for(i = 0; i < payloadSize[payloadIndex] / sizeof(uint32); i++ )
            {
               if( (i + 1) % 8 == 0 )
               {
                  printf("%8x\n", ((uint32*)payload)[i] );
               }
               else
               {
                  printf("%8x  ", ((uint32*)payload)[i] );
               }
            }

            printf("\nReceived:\n");
            for(i = 0; i < payloadSize[payloadIndex] / sizeof(uint32); i++ )
            {
               if( (i + 1) % 8 == 0 )
               {
                  printf("%8x\n", ((uint32*)linearAddr)[i] );
               }
               else
               {
                  printf("%8x  ", ((uint32*)linearAddr)[i] );
               }
            }
            
            ret = ErrorUnknown;
         }
         
         printf("attempting to unmap range of %d pages\n", numPages );
         pagerUnmapRange( linearAddr, numPages );
         
         printf("attempting to destroy descriptor\n");
         ec = physicalMemoryDescriptorDestroy( desc );
         if( ec != ErrorNoError )
         {
            printf("Could not destroy descriptor!");
         }
      }
      else
      {
         printf("Couldn't map descriptor\n");
      }
   }
   else
   {
      printf("Couldn't create descriptor\n");
   }   
   
   payloadIndex++;
   
   if( ec ) ret = ec;
      
   return ret;
}

