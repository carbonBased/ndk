/* ndk - [ testsMutex.c ]
 *
 * Code to test the ndk mutex implementation
 *
 * (c)2005-2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "tests.h"
#include <console.h>
#include <core/mutex.h>

static MutexId mutexId;
static Mutex mutex;

static ErrorCode testsMutexCreate(TestCase me);
static ErrorCode testsMutexMap(TestCase me);
static ErrorCode testsMutexLock(TestCase me);
static ErrorCode testsMutexUnlock(TestCase me);

ErrorCode testsMutexInit(TestGroup * group)
{
   TestCase tc;

   testGroupCreate(group, "mutex");

   testCaseCreate(&tc, "mutexCreate", testsMutexCreate);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "mutexMap", testsMutexMap);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "mutexLock", testsMutexLock);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "mutexUnlock", testsMutexUnlock);
   testGroupAddTestCase(*group, tc);
   testCaseCreate(&tc, "mutexUnlock", testsMutexUnlock);
   testGroupAddTestCase(*group, tc);
   
   return ErrorNoError;
}

static ErrorCode testsMutexCreate(TestCase me)
{
   ErrorCode ec;
   
   ec = mutexCreate( &mutexId, MutexTypeRecursive | MutexTypeFIFO );
   printf("created mutexId 0x%8x\n", mutexId );
   
   return ec;
}

static ErrorCode testsMutexMap(TestCase me)
{
   ErrorCode ec;
   
   ec = mutexMap( mutexId, &mutex );
   printf("mapped mutexId 0x%8x to 0x%8x\n", mutexId, mutex );
   
   return ec;
}

static ErrorCode testsMutexLock(TestCase me)
{
   return mutexLock( &mutex, TimeoutInfinite );
}

static ErrorCode testsMutexUnlock(TestCase me)
{
   return mutexUnlock( &mutex );
}
