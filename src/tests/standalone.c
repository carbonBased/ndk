#include "tests.h"

#include <driver.h>

/*
gcc -o built/bin/standalone built/obj/tests/standalone.o built/obj/debug.o built/obj/array.o built/obj/binaryTree.o built/obj/expandingQueue.o built/obj/list.o built/obj/queue.o built/obj/stack.o built/obj/tests/tests.o built/obj/tests/testsBTree.o
*/

int main(void)
{
   /* TODO: there should be a global systemInitialize() function or something... so standalone
    *       and true OS runs can share init and final code
    */
   driverManagerInit();
   testsInit();
   testsStart();
   driverManagerFinal();
}
