/* ndk - [ binaryTree.c ]
 *
 * Routines for maintaining a balanced tree of items
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <brink/binaryTree.h>

struct _BTreeNode
{
   struct _BTreeNode *left, *right;
   void *data;
   int weight;
};

typedef struct _BTreeNode *BTreeNode;

struct _BinaryTree
{
   BTreeNode root;
   uint32 averagePathLength;
   BinaryTreeComparator comparator;
   uint32 length;
};

struct _BinaryTreeIterator
{
   BinaryTree tree;
   BTreeNode current;
};

void _addWeightedNode(BinaryTree bTree, BTreeNode node, BTreeNode parent, BTreeNode item);
void _checkRotations(BinaryTree bTree, BTreeNode node, BTreeNode parent);
BTreeNode _rotateLeft(BTreeNode fulcrum);
BTreeNode _rotateRight(BTreeNode fulcrum);
int _calcWeight(BTreeNode node);
ErrorCode _binaryTreeRemove(BinaryTree bTree, void *key, Boolean isKey);
BTreeNode _findLeafNode(BTreeNode node, BTreeNode parent, BTreeNode item,
                        BinaryTreeComparator comparator);
void _computeAveragePathLength(BinaryTree bTree);
void _computeDepths(BTreeNode node, uint32 *depth, uint32 *sum, uint32 *numNodes);

ErrorCode binaryTreeCreate(BinaryTree *bTree, BinaryTreeComparator comparator)
{
   BinaryTree tree = (BinaryTree)malloc(sizeof(struct _BinaryTree));

   tree->root = NULL;
   tree->averagePathLength = 0;
   tree->comparator = comparator;
   tree->length = 0;

   *bTree = tree;

   return ErrorNoError;
}

ErrorCode binaryTreeDestroy(BinaryTree *bTree)
{
   free(*bTree);

   return ErrorNoError;
}

ErrorCode binaryTreeAdd(BinaryTree bTree, void *data)
{
   BTreeNode node = (BTreeNode) malloc(sizeof(struct _BTreeNode));

   node->left = NULL;
   node->right = NULL;
   node->weight = 2;            // two null children equate to a weight of 2
   node->data = data;

   if(bTree->root == NULL)
   {
      bTree->root = node;
   }
   else
   {
      /*
         BTreeNode leafNode = _findLeafNode(bTree->root, bTree->root, node, bTree->comparator);

         if(bTree->comparator(False, node->data, leafNode->data) < 0)
         {
         leafNode->left = node;
         }
         else
         {
         leafNode->right = node;
         }
       */
      _addWeightedNode(bTree, bTree->root, NULL, node);
   }
   bTree->length++;

   return ErrorNoError;
}

ErrorCode binaryTreeRemove(BinaryTree bTree, void *data)
{
   return _binaryTreeRemove(bTree, data, False);
}

ErrorCode binaryTreeRemoveByKey(BinaryTree bTree, void *key)
{
   return _binaryTreeRemove(bTree, key, True);
}

/**
 * Find a specific item in the tree
 */
ErrorCode binaryTreeFind(BinaryTree bTree, void *key, void **data)
{
   BTreeNode currentItem;
   int32 comparison;
   ErrorCode err = ErrorNoError;

   currentItem = bTree->root;

   // find the item we want to delete and store it in currentItem
   while(currentItem && ((comparison = bTree->comparator(True, key, currentItem->data)) != 0))
   {
      printf("Find comparison\n");
      if(comparison < 0)
      {
         currentItem = currentItem->left;
      }
      else
      {
         currentItem = currentItem->right;
      }
   }

   if(currentItem)
   {
      *data = currentItem->data;
   }
   else
   {
      *data = NULL;
      err = ErrorNotFound;
   }

   return err;
}

ErrorCode binaryTreeGetLength(BinaryTree bTree, uint32 *length)
{
   *length = bTree->length;

   return ErrorNoError;
}

ErrorCode binaryTreeGetFirst(BinaryTree bTree, void **data)
{
   return ErrorNotSupported;
}

ErrorCode binaryTreeGetLast(BinaryTree bTree, void **data)
{
   return ErrorNotSupported;
}

ErrorCode binaryTreeIteratorCreate(BinaryTreeIterator *bti, BinaryTree bTree)
{
   return ErrorNotSupported;
}

ErrorCode binaryTreeIteratorReset(BinaryTreeIterator bti)
{
   return ErrorNotSupported;
}

ErrorCode binaryTreeIteratorGetNext(BinaryTreeIterator bti, void **data)
{
   return ErrorNotSupported;
}

ErrorCode binaryTreeIteratorGetPrevious(BinaryTreeIterator bti, void **data)
{
   return ErrorNotSupported;
}

ErrorCode binaryTreeIteratorDestroy(BinaryTreeIterator *bti)
{
   return ErrorNotSupported;
}

// private functions

void _addWeightedNode(BinaryTree bTree, BTreeNode node, BTreeNode parent, BTreeNode item)
{
   if(node == NULL)
   {
      if(bTree->comparator(False, item->data, parent->data) < 0)
      {
         parent->left = item;
      }
      else
      {
         parent->right = item;
      }
   }
   else
   {
      if(bTree->comparator(False, item->data, node->data) < 0)
      {
         _addWeightedNode(bTree, node->left, node, item);
      }
      else
      {
         _addWeightedNode(bTree, node->right, node, item);
      }
      node->weight = _calcWeight(node->right) + _calcWeight(node->left);
      _checkRotations(bTree, node, parent);
   }
}

void _checkRotations(BinaryTree bTree, BTreeNode node, BTreeNode parent)
{
   int wl, wr;
   BTreeNode res1, res2;

   if(node != NULL)
   {
      wl = _calcWeight(node->left);
      wr = _calcWeight(node->right);
      if(wr > wl)
      {
         if(_calcWeight(node->right->right) > wl)
         {
            res1 = _rotateLeft(node);
            if(parent != NULL)
            {
               if(bTree->comparator(False, res1->data, parent->data) < 0)
               {
                  parent->left = res1;
               }
               else
               {
                  parent->right = res1;
               }
            }
            else
            {
               bTree->root = res1;
            }
            _checkRotations(bTree, node->left, node);
         }
         else if(_calcWeight(node->right->left) > wl)
         {
            res1 = _rotateRight(node->right);
            node->right = res1;
            res2 = _rotateLeft(node);
            if(parent != NULL)
            {
               if(bTree->comparator(False, res1->data, parent->data) < 0)
               {
                  parent->left = res2;
               }
               else
               {
                  parent->right = res2;
               }
            }
            else
            {
               bTree->root = res2;
            }
            _checkRotations(bTree, node->left, node);
            _checkRotations(bTree, node->right, node);
         }
      }
      else if(wl > wr)
      {
         if(_calcWeight(node->left->left) > wr)
         {
            res1 = _rotateRight(node);
            if(parent != NULL)
            {
               if(bTree->comparator(False, res1->data, parent->data) < 0)
               {
                  parent->left = res1;
               }
               else
               {
                  parent->right = res1;
               }
            }
            else
            {
               bTree->root = res1;
            }
            _checkRotations(bTree, node->right, node);
         }
         else if(_calcWeight(node->left->right) > wr)
         {
            res1 = _rotateLeft(node->left);
            node->left = res1;
            res2 = _rotateRight(node);
            if(parent != NULL)
            {
               if(bTree->comparator(False, res2->data, parent->data) < 0)
               {
                  parent->left = res2;
               }
               else
               {
                  parent->right = res2;
               }
            }
            else
            {
               bTree->root = res2;
            }
            _checkRotations(bTree, node->left, node);
            _checkRotations(bTree, node->right, node);
         }
      }
   }
}

// TODO: evaluate the purpose of temp -- eiffel appears to pass by value as well...
BTreeNode _rotateLeft(BTreeNode fulcrum)
{
   BTreeNode temp, newRoot;

   // note: this may crash... for testing only, as I know fulrum->data is an integer!
   printf("performing left rotation around %d\n", fulcrum->data);

   temp = fulcrum;
   newRoot = fulcrum->right;
   temp->right = newRoot->left;
   newRoot->left = temp;
   newRoot->weight = temp->weight;
   temp->weight = (_calcWeight(temp->left) + _calcWeight(temp->right));

   return newRoot;
}

BTreeNode _rotateRight(BTreeNode fulcrum)
{
   BTreeNode temp, newRoot;

   // note: this may crash... for testing only, as I know fulrum->data is an integer!
   printf("performing right rotation around %d\n", fulcrum->data);

   temp = fulcrum;
   newRoot = fulcrum->left;
   temp->left = newRoot->right;
   newRoot->right = temp;
   newRoot->weight = temp->weight;
   temp->weight = (_calcWeight(temp->right) + _calcWeight(temp->left));

   return newRoot;
}

int _calcWeight(BTreeNode node)
{
   int weight;

   if(node == NULL)
   {
      weight = 1;
   }
   else
   {
      weight = node->weight;
   }

   return weight;
}

/**
 * TODO: check over this entire code block!!!
 */
ErrorCode _binaryTreeRemove(BinaryTree bTree, void *data, Boolean isKey)
{
   BTreeNode parent, previous, currentItem, replace, replaceLeft;
   int32 comparison;

   previous = NULL;
   currentItem = bTree->root;

   // find the item we want to delete and store it in currentItem
   while((comparison = bTree->comparator(isKey, data, currentItem->data)) != 0)
   {
      previous = currentItem;

      if(comparison < 0)
      {
         currentItem = currentItem->left;
      }
      else
      {
         currentItem = currentItem->right;
      }
   }

   if(currentItem->left == NULL)
   {
      replace = currentItem->right;
   }
   else if(currentItem->right == NULL)
   {
      replace = currentItem->left;
   }
   else
   {
      // if the new being removed has both a right and a left link, replace the
      // node to be removed with it's in-order successor (which can never have
      // more then one child)

      // find the in-order successor...
      parent = currentItem;
      replace = currentItem->right;
      replaceLeft = replace->left;
      while(replaceLeft != NULL)
      {
         parent = replace;
         replace = replaceLeft;
         replaceLeft = replace->left;
      }

      // and perform the actual replacement
      if(parent != currentItem)
      {
         parent->left = replace->right;
         replace->right = currentItem->right;
      }

      replace->left = currentItem->left;
   }

   if(previous == NULL)
   {
      bTree->root = replace;
   }
   else
   {
      if(currentItem == previous->left)
      {
         previous->left = replace;
      }
      else
      {
         previous->right = replace;
      }
   }

   bTree->length--;

   // TODO!  How to rebalance the tree at this point!?

   return ErrorNoError;
}

BTreeNode _findLeafNode(BTreeNode node, BTreeNode parent, BTreeNode item,
                        BinaryTreeComparator comparator)
{
   BTreeNode leafNode = NULL;

   if(node != NULL)
   {
      if(comparator(False, item, node) < 0)
      {
         leafNode = _findLeafNode(node->left, node, item, comparator);
      }
      else if(comparator(False, item, node) > 0)
      {
         leafNode = _findLeafNode(node->right, node, item, comparator);
      }
      else
      {
         printf("This item already exists in the btree!\n");
         //assert(-1);
      }
   }
   else
   {
      leafNode = parent;
   }

   return leafNode;
}

void _computeAveragePathLength(BinaryTree bTree)
{
   uint32 depth = 1, sum = 0, numNodes = 0;

   _computeDepths(bTree->root, &depth, &sum, &numNodes);

   // note, technically this is a floating point number, but... I hate floating
   // point, so I'll keep it in a form of fixed point
   bTree->averagePathLength = (sum * 256) / numNodes;
}

void _computeDepths(BTreeNode node, uint32 *depth, uint32 *sum, uint32 *numNodes)
{
   if(node != NULL)
   {
      *sum += *depth;
      (*numNodes)++;
      (*depth)++;

      _computeDepths(node->left, depth, sum, numNodes);
      _computeDepths(node->right, depth, sum, numNodes);
   }
}
