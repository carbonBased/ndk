/* ndk - [ list.h ]
 *
 * Routines for maintaining a list of items
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup list list.h: linked list routines
 * @ingroup core
 *
 * Defines interfaces for working with lists of data
 */
/** @{ */

// TODO: convert void* to Pointer!

#ifndef __ndk_list_h__
#define __ndk_list_h__

#include <errorCodes.h>
#include <types.h>

typedef struct _List *List;
typedef struct _ListIterator *ListIterator;

typedef Boolean (*ListComparator) (Pointer key, Pointer data);

/**
 * Creates a doubly-linked list
 */
ErrorCode listCreate(List *list);

/**
 * Delete a doubly-linked list
 */
ErrorCode listDestroy(List *list);

/**
 * Add an item to the linked list at the front/head
 */
ErrorCode listAddFront(List list, Pointer data);

/**
 * Add an item to the linked list at the back/tail
 */
ErrorCode listAddBack(List list, Pointer data);

/**
 * Remove an item from the list
 */
ErrorCode listRemove(List list, Pointer data);

/**
 * Get the total number of items in this list
 */
ErrorCode listGetLength(List list, uint32 *length);

/**
 * Get the first node in the list
 */
ErrorCode listGetFirst(List list, Pointer *data);

/**
 * Get the last node in the list
 */
ErrorCode listGetLast(List list, Pointer *data);

/**
 * Attempt to find an item in the list
 */
ErrorCode listFind(List list, ListComparator comparator, Pointer key, Pointer *data);

/**
 * Create an iterator for a list
 */
ErrorCode listIteratorCreate(ListIterator * li, List list);

/**
 * Reset the iterator to point to the beginning on the list.
 * Can be called at any time, even if the list has changed
 * after the iterator was created
 */
ErrorCode listIteratorReset(ListIterator li);

/**
 * Get the next node in the list
 */
ErrorCode listIteratorGetNext(ListIterator li, Pointer *data);

/**
 * Get the previous node in the list
 */
ErrorCode listIteratorGetPrevious(ListIterator li, Pointer *data);

/**
 * Destroy a list iterator
 */
ErrorCode listIteratorDestroy(ListIterator * li);

#endif

/** @} */
