/* ndk - [ queue.h ]
 *
 * Basic abstract queue class
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup Queue queue.h: Abstract queue class
 * @ingroup core
 *
 * This file defines a interface for working with simple queues
 */
/** @{ */

#include <errorCodes.h>
#include <types.h>

// TODO: add "exandingQueue" class... will actually be a linked list of
// these queue classes, used to mimick an infinitely large queue (no
// max size)... advantage being, then able to reuse this code

typedef struct _Queue *Queue;

ErrorCode queueCreate(Queue *q, uint32 size);
ErrorCode queueAdd(Queue q, Pointer item);
ErrorCode queueGet(Queue q, Pointer *item);
ErrorCode queuePeek(Queue q, Pointer *item);
ErrorCode queueGetSize(Queue q, uint32 *size);
ErrorCode queueGetRemaining(Queue q, uint32 *rem);
ErrorCode queueDestroy(Queue *q);

/** @} */
