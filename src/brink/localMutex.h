/* ndk - [ mutex.h ]
 *
 * Routines for implementing mutual exclusion
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup mutex mutex.h: mutex routines
 * @ingroup core
 *
 * Defines interfaces for implementing mutual exclusion
 */
/** @{ */

#include <errorCodes.h>
#include <types.h>

#ifndef __ndk_local_mutex_h__
#define __ndk_local_mutex_h__

typedef struct _LocalMutex *LocalMutex;

// TODO: implement all these correctly! (and rename to LocalMutexType, or keep?)
typedef enum _MutexType
{
   MutexTypeRecursive = 0,
   MutexTypeNonRecursive = 1,
   MutexTypeFIFO = 0,
   MutexTypePriority = 2
} MutexType;

/**
 * @param mutex Pointer to a mutex handle, which will contain
 *              a valid mutex upon completion.
 * @param type  The type of mutex to be created
 */
ErrorCode localMutexCreate(LocalMutex *mutex, MutexType type);

/**
 * @param mutex   The mutex to lock
 * @param timeout The time to wait for the mutex to be available
 *                for locking.
 */
ErrorCode localMutexLock(LocalMutex mutex, Timeout timeout);

/**
 * @param mutex The mutex to unlock
 */
ErrorCode localMutexUnlock(LocalMutex mutex);

/**
 * @param mutex The mutex to destroy
 */
ErrorCode localMutexDestroy(LocalMutex *mutex);

#endif

/** @} */
