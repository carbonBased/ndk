/* ndk - [ hashTable.h ]
 *
 * Routines for maintaining a generic hash table
 *
 * (c)2005 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup hashTable hashTable.h: hash table routines
 * @ingroup core
 *
 * Defines interfaces for working with hash tables
 */
/** @{ */

#ifndef __ndk_hashTable_h__
#define __ndk_hashTable_h__

#include <errorCodes.h>
#include <types.h>

/**
 * The default hash table size (large prime numbers are the best sizes when 
 * using the modulus method of clamping hash values into table range)
 */
const uint32 HashTableDefaultSize = 97;

typedef uint32 (*HashFunction) (void *key, uint32 keySize);

typedef struct _HashTable *HashTable;

ErrorCode hashTableCreate(HashTable *table, uint32 size, HashFunction hashFunction);

ErrorCode hashTableAdd(HashTable table, void *key, uint32 keySize, Pointer object);

ErrorCode hashTableAddString(HashTable table, String key, Pointer object);

ErrorCode hashTableGet(HashTable table, void *key, uint32 keySize, Pointer *object);

ErrorCode hashTableGetString(HashTable table, String key, Pointer *object);

ErrorCode hashTableDestroy(HashTable *table);

#endif

/** @} */
