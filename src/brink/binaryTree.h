/* ndk - [ binaryTree.h ]
 *
 * Routines for maintaining a balanced tree of items
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup binaryTree binaryTree.h: binary tree routines
 * @ingroup core
 *
 * Defines interfaces for working with weight balanced trees of data
 */
/** @{ */

#ifndef __ndk_binaryTree_h__
#define __ndk_binaryTree_h__

#include <errorCodes.h>
#include <types.h>

typedef struct _BinaryTree *BinaryTree;
typedef struct _BinaryTreeIterator *BinaryTreeIterator;

/**
 * @param keyComparison if true, data1 contains a key value, and not an actual node
 *                      otherwise, both data1 and data2 are nodes in the tree
 *
 * Returns:
 *  +1 if data1 > data2
 *  -1 if data1 < data2
 *   0 if data1 = data2 (this shouldn't happen in a bTree)
 */
typedef int32 (*BinaryTreeComparator) (Boolean keyComparison, void *data1, void *data2);

/**
 * Creates an empty binary tree
 */
ErrorCode binaryTreeCreate(BinaryTree *bTree, BinaryTreeComparator comparator);

/**
 * Delete a binary tree
 */
ErrorCode binaryTreeDestroy(BinaryTree *bTree);

/**
 * Add an item to the binary tree
 */
ErrorCode binaryTreeAdd(BinaryTree bTree, void *data);

/**
 * Remove an item from the bTree
 */
ErrorCode binaryTreeRemove(BinaryTree bTree, void *data);

/**
 * Remove an item from the bTree, using a key, not a data pointer
 */
ErrorCode binaryTreeRemoveByKey(BinaryTree bTree, void *key);

/**
 * Find a specific item in the tree
 */
ErrorCode binaryTreeFind(BinaryTree bTree, void *key, void **data);

/**
 * Get the total number of items in this btree
 */
ErrorCode binaryTreeGetLength(BinaryTree bTree, uint32 *length);

/**
 * Get the first/lowest node in the tree
 */
ErrorCode binaryTreeGetFirst(BinaryTree bTree, void **data);

/**
 * Get the last/highest node in the list
 */
ErrorCode binaryTreeGetLast(BinaryTree bTree, void **data);

/**
 * Create an iterator for a list
 */
ErrorCode binaryTreeIteratorCreate(BinaryTreeIterator *bti, BinaryTree bTree);

/**
 * Reset the iterator to point to the beginning on the list.
 * Can be called at any time, even if the list has changed
 * after the iterator was created
 */
ErrorCode binaryTreeIteratorReset(BinaryTreeIterator bti);

/**
 * Get the next node in the list
 */
ErrorCode binaryTreeIteratorGetNext(BinaryTreeIterator bti, void **data);

/**
 * Get the previous node in the list
 */
ErrorCode binaryTreeIteratorGetPrevious(BinaryTreeIterator bti, void **data);

/**
 * Destroy a list iterator
 */
ErrorCode binaryTreeIteratorDestroy(BinaryTreeIterator *bti);

#endif

/** @} */
