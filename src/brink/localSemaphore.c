/* ndk - [ localSemaphore.c ]
 *
 * Routines for implementing semaphores
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <brink/localSemaphore.h>
#include <memory.h>
#include <task.h>
#include <brink/localMutex.h>
#include <brink/list.h>

struct _LocalSemaphore
{
   uint32 value;
   List waitingTasks;
   LocalMutex mutex;                 // to syncronize operations on this semaphore
};

ErrorCode localSemaphoreCreate(LocalSemaphore *semaphore, SemaphoreType type, uint32 initial)
{
   LocalSemaphore sem = (LocalSemaphore)malloc(sizeof(struct _LocalSemaphore));

   sem->value = initial;
   listCreate(&(sem->waitingTasks));
   localMutexCreate(&(sem->mutex), MutexTypeFIFO);

   *semaphore = sem;
   return ErrorNoError;
}

ErrorCode localSemaphoreUp(LocalSemaphore semaphore)
{
   uint32 length;

   semaphore->value++;
   printf("semaphore up %d\n", semaphore->value);
   //mutexLock(semaphore->mutex, TimeoutInfinite);

   // semaphore has been raised... check to see if someone wants it!
   listGetLength(semaphore->waitingTasks, &length);
   if(length != 0)
   {
      Task task;

      /* TODO: implement these?
         switch(mutex->type) {
         case MutexTypeFIFO:
         case MutexTypePriority:
         }
       */
      if(listGetFirst(semaphore->waitingTasks, (void **)&task) == ErrorNoError)
      {
         // remove the task from the waiting state, and activate it
         listRemove(semaphore->waitingTasks, (void *)task);
         taskSetState(task, TaskStateAlive);
         printf("marked task active\n");
      }
      else
      {
         printf("No one waiting on this semaphore? %x\n", semaphore);
      }
   }
   else
   {
      printf("localSem No waiting tasks\n");
   }

   //mutexUnlock(semaphore->mutex);

   return ErrorNoError;
}

ErrorCode localSemaphoreDown(LocalSemaphore semaphore, Timeout timeout)
{
   ErrorCode err = ErrorNoError;
   Task task;

   printf("semaphore down: ");

   //mutexLock(semaphore->mutex, TimeoutInfinite);

   if(semaphore->value)
   {
      semaphore->value--;
      printf("down %d\n", semaphore->value);
      //mutexUnlock(semaphore->mutex);
   }
   else
   {
      TaskState state;

      taskGetCurrent(&task);
      listAddBack(semaphore->waitingTasks, task);

      /*
       * this will tell the task to wait the specified timeout...
       * if the other thread unlocks the mutex, and this task is
       * waiting on it, it will set the task into an Alive state,
       * otherwise, we'll enter the TimedOut state
       */
      taskSetTimeout(task, timeout);

      // unlock first, otherwise a waiting task holds this
      // semaphore's mutex, and will not allow anything to use it (ie, 'Up' it)
      //mutexUnlock(semaphore->mutex);

    reswitch:
      taskForceSwitch();

      //mutexLock(semaphore->mutex, TimeoutInfinite);
      printf("Semaphore, back from switch\n");

      taskGetState(task, &state);
      if(state != TaskStateWaiting)
      {
         listRemove(semaphore->waitingTasks, task);
      }

      switch (state)
      {
      case TaskStateTimedOut:
         taskSetState(task, TaskStateAlive);
         printf("semaphore timed out\n");
         err = ErrorTimedOut;
         break;
      case TaskStateAlive:
         semaphore->value--;
         printf("down delayed %d\n", semaphore->value);
         break;
      default:
         printf("***************************unknown state %d!\n", state);
         goto reswitch;
      }
      //mutexUnlock(semaphore->mutex);
   }

   //mutexUnlock(semaphore->mutex);

   return err;
}

ErrorCode localSemaphoreDestroy(LocalSemaphore *semaphore)
{
   listDestroy(&((*semaphore)->waitingTasks));
   localMutexDestroy(&((*semaphore)->mutex));
   free(*semaphore);

   *semaphore = 0;
   return ErrorNoError;
}
