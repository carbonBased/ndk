/* ndk - [ list.h ]
 *
 * Routines for maintaining a list of items
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <brink/list.h>
#include <stddef.h>

// TODO: ensure head/tail maintained properly across all operations!!!

struct _ListNode
{
   struct _ListNode *prev, *next;
   Pointer data;
};

typedef struct _ListNode *ListNode;

struct _List
{
   ListNode head;
   ListNode tail;
   uint32 totalItems;
};

struct _ListIterator
{
   List list;
   ListNode current;
};

ErrorCode listCreate(List *list)
{
   List ret;

   ret = (List)malloc(sizeof(struct _List));
   ret->head = NULL;
   ret->tail = NULL;
   ret->totalItems = 0;

   *list = ret;
   return ErrorNoError;
}

ErrorCode listDestroy(List *list)
{
   free(*list);
   *list = NULL;

   return ErrorNoError;
}

ErrorCode listAddFront(List list, Pointer data)
{
   ListNode node;

   node = (ListNode) malloc(sizeof(struct _ListNode));
   node->prev = NULL;
   node->next = list->head;
   node->data = data;

   if(list->head)
   {
      list->head->prev = node;
   }
   list->head = node;

   // if this is the first, update both head and tail
   if(list->tail == NULL)
   {
      list->tail = list->head;
   }
   list->totalItems++;

   return ErrorNoError;
}

ErrorCode listAddBack(List list, Pointer data)
{
   ListNode node;

   node = (ListNode) malloc(sizeof(struct _ListNode));
   node->prev = list->tail;
   node->next = NULL;
   node->data = data;

   if(list->tail)
   {
      list->tail->next = node;
   }
   list->tail = node;

   // if this is the first, update both head and tail
   if(list->head == NULL)
   {
      list->head = list->tail;
   }
   list->totalItems++;

   return ErrorNoError;
}

ErrorCode listRemove(List list, Pointer data)
{
   ListNode node = list->head;

   if(list->totalItems)
   {
      while(node != NULL)
      {
         if(node->data == data)
         {
            if(node->prev)
               node->prev->next = node->next;
            if(node->next)
               node->next->prev = node->prev;

            if(node == list->head)
               list->head = node->next;

            if(node == list->tail)
               list->tail = node->prev;

            free(node);
            list->totalItems--;
            break;
         }
         node = node->next;
      }
   }

   if(node == NULL)
      return ErrorNotFound;
   else
      return ErrorNoError;
}

ErrorCode listGetLength(List list, uint32 *length)
{
   *length = list->totalItems;

   return ErrorNoError;
}

ErrorCode listGetFirst(List list, Pointer *data)
{
   if(list->head)
   {
      *data = list->head->data;
      return ErrorNoError;
   }
   else
      return ErrorNotFound;
}

ErrorCode listGetLast(List list, Pointer *data)
{
   if(list->tail)
   {
      *data = list->tail->data;
      return ErrorNoError;
   }
   else
      return ErrorNotFound;
}

ErrorCode listFind(List list, ListComparator comparator, Pointer key, Pointer *data)
{
   ErrorCode err = ErrorNotFound;
   ListNode node = list->head;

   *data = NULL;
   while(node)
   {
      if(comparator(key, node->data))
      {
         *data = node->data;
         err = ErrorNoError;
         break;
      }
      node = node->next;
   }

   return err;
}

ErrorCode listIteratorCreate(ListIterator * li, List list)
{
   ListIterator listIt;

   listIt = (ListIterator) malloc(sizeof(struct _ListIterator));
   listIt->list = list;
   listIt->current = NULL;

   *li = listIt;

   return ErrorNoError;
}

ErrorCode listIteratorReset(ListIterator li)
{
   li->current = NULL;
}

ErrorCode listIteratorGetNext(ListIterator li, Pointer *data)
{
   if(li->current == NULL)
   {
      if(li->list->head == NULL)
      {
         return ErrorNotFound;
      }
      else
      {
         li->current = li->list->head;
      }
   }
   else
   {
      li->current = li->current->next;
   }

   if(li->current)
   {
      *data = li->current->data;
      return ErrorNoError;
   }
   else
   {
      return ErrorNotFound;
   }
}

ErrorCode listIteratorGetPrevious(ListIterator li, Pointer *data)
{
   if(li->current == NULL)
   {
      if(li->list->tail == NULL)
         return ErrorNotFound;
      else
         li->current = li->list->tail;
   }
   else
   {
      li->current = li->current->prev;
   }

   if(li->current)
   {
      *data = li->current->data;
      return ErrorNoError;
   }
   else
      return ErrorNotFound;
}

ErrorCode listIteratorDestroy(ListIterator * li)
{
   free(*li);
   *li = NULL;

   return ErrorNoError;
}
