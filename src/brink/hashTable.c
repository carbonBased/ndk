/* ndk - [ hashTable.c ]
 *
 * Routines for maintaining a generic hash table
 *
 * (c)2005 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <brink/hashTable.h>
#include <brink/list.h>

struct _HashTable
{
   List *table;
   uint32 tableSize;
   uint32 numItems;
   HashFunction hashFunction;
};

static uint32 hashTableDefaultFunc(void *key, uint32 keySize);
static uint32 getBytes(void *from, uint32 size);

static uint32 hashTableDefaultFunc(void *key, uint32 keySize)
{
   uint32 index = 0;
   uint32 *key4Byte = (uint32 *)key;
   uint32 num4Bytes = keySize / 4;
   uint32 remaining = keySize - num4Bytes * 4;
   uint32 currentKey = 0;
   uint32 temp;

   if(keySize < 4)
   {
      /*
       * This hashing function isn't too great with hashes less then
       * 4 bytes... the result is identical to the input, in uint32
       * form.
       */
      currentKey = getBytes(key, keySize);
   }
   else
   {
      /* chop the key into 4-byte chunks, and shift and 'or' them together iteratively */
      currentKey = getBytes(key, 4);
      index++;
      num4Bytes--;

      while(num4Bytes)
      {
         temp = getBytes(&key4Byte[index], 4);

         temp <<= 1;
         currentKey |= temp;

         index++;
         num4Bytes--;
      }

      /* remember to get the remaining (sub 4 pixel) chunk if there is any */
      if(remaining)
      {
         temp = getBytes(&key4Byte[index], remaining);
         temp <<= 1;
         currentKey |= temp;
      }
   }

   return currentKey;
}

static uint32 getBytes(void *from, uint32 size)
{
   uint32 result = 0;

   switch (size)
   {
   case 1:
      result = ((uint8 *)from)[0];
      break;
   case 2:
      result = ((uint16 *)from)[0];
      break;
   case 3:
      /*
       * we're effectively making a 3-byte integer here out of a 16-bit 
       * and an 8-bit integer
       */
      result = (((uint16 *)from)[0] << 8) + ((uint8 *)from)[3];
      break;
   case 4:
      result = ((uint32 *)from)[0];
   }

   return result;
}

ErrorCode hashTableCreate(HashTable *table, uint32 size, HashFunction hashFunction)
{
   ErrorCode err = ErrorNoError;
   HashTable tbl = (HashTable)malloc(sizeof(struct _HashTable));

   if(size == 0)
   {
      size = HashTableDefaultSize;
   }

   if(hashFunction == NULL)
   {
      hashFunction = hashTableDefaultFunc;
   }

   if(tbl)
   {
      tbl->tableSize = size;
      tbl->numItems = 0;
      tbl->hashFunction = hashFunction;

      /* create pointers for each of the linked lists that will serve as 
       * hashtable indices, and null them out, for now
       */
      tbl->table = (List *)malloc(tbl->tableSize * sizeof(List));
      memset(tbl->table, 0, tbl->tableSize * sizeof(List));

      *table = tbl;
   }
   else
   {
      *table = NULL;
      err = ErrorOutOfMemory;
   }

   return err;
}

ErrorCode hashTableAdd(HashTable table, void *key, uint32 keySize, Pointer object)
{
   ErrorCode err = ErrorNoError;
   uint32 hash, tableIndex;

   if(table)
   {
      hash = table->hashFunction(key, keySize);
      tableIndex = hash % table->tableSize;

      if(table->table[tableIndex] == 0)
      {
         listCreate(&table->table[tableIndex]);
      }

      listAddBack(table->table[tableIndex], object);
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode hashTableAddString(HashTable table, String key, Pointer object)
{
   ErrorCode err;

   err = hashTableAdd(table, key, strlen(key), object);

   return err;
}

ErrorCode hashTableGet(HashTable table, void *key, uint32 keySize, Pointer *object)
{
   ErrorCode err = ErrorNoError;
   uint32 hash, tableIndex;

   if(table)
   {
      hash = table->hashFunction(key, keySize);
      tableIndex = hash % table->tableSize;

      if(table->table[tableIndex] == 0)
      {
         err = ErrorNotFound;
      }
      else
      {
         /* search through the list at table->table[tableIndex] until the specified key is found */
         // TODO:
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode hashTableGetString(HashTable table, String key, Pointer *object)
{
   ErrorCode err;

   err = hashTableGet(table, key, strlen(key), object);

   return err;
}

ErrorCode hashTableDestroy(HashTable *table)
{
   ErrorCode err = ErrorNoError;
   uint32 i;

   for(i = 0; i < (*table)->tableSize; i++)
   {
      if((*table)->table[i])
      {
         /* TODO: loop through the list and destroy everything in it? */
         listDestroy(&(*table)->table[i]);
      }
   }

   free(*table);
   *table = NULL;

   return err;
}
