/* ndk - [ expandingQueue.h ]
 *
 * Basic abstract expanding queue class
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup ExpandingQueue expandingQueue.h: Abstract queue class
 * @ingroup core
 *
 * This file defines a interface for working with simple queues
 */
/** @{ */

#include <errorCodes.h>
#include <types.h>

typedef struct _ExpandingQueue *ExpandingQueue;

ErrorCode expandingQueueCreate(ExpandingQueue *q, uint32 size);
ErrorCode expandingQueueAdd(ExpandingQueue q, Pointer item);
ErrorCode expandingQueueGet(ExpandingQueue q, Pointer *item);
ErrorCode expandingQueuePeek(ExpandingQueue q, Pointer *item);

//ErrorCode expandingQueueGetSize(ExpandingQueue q, uint32 *size);
//ErrorCode expandingQueueGetRemaining(ExpandingQueue q, uint32 *rem);
ErrorCode expandingQueueDestroy(ExpandingQueue *q);

/** @} */
