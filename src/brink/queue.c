/* ndk - [ queue.c ]
 *
 * Routines for managing queues
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <brink/queue.h>

#include <console.h>

struct _Queue
{
   Pointer *start;
   uint32 size;
   uint32 used;
   uint32 startIndex;
   uint32 currentIndex;
};

ErrorCode queueCreate(Queue *q, uint32 size)
{
   Queue queue;

   queue = (Queue)malloc(sizeof(struct _Queue));
   queue->start = (Pointer *)malloc(sizeof(Pointer) * size);
   queue->size = size;
   queue->used = 0;
   queue->startIndex = 0;
   queue->currentIndex = 0;

   *q = queue;
   return ErrorNoError;
}

ErrorCode queueAdd(Queue q, Pointer item)
{
   if(q->used == q->size)
      return ErrorOutOfBounds;

   q->used++;
   q->start[q->currentIndex++] = item;

   // increment ring buffer indexes
   if(q->currentIndex >= q->size)
      q->currentIndex = 0;
   if(q->currentIndex == q->startIndex)
   {
      q->startIndex++;
      if(q->startIndex >= q->size)
         q->startIndex = 0;
   }

   return ErrorNoError;
}

ErrorCode queueGet(Queue q, Pointer *item)
{
   if(q->used == 0)
      return ErrorOutOfBounds;

   q->used--;
   *item = (Pointer)q->start[q->startIndex++];

   return ErrorNoError;
}

ErrorCode queuePeek(Queue q, Pointer *item)
{
   if(q->used == 0)
      return ErrorOutOfBounds;

   printf("start %x startIndex %d\n", q->start, q->startIndex);

   *item = (Pointer)q->start[q->startIndex];
   return ErrorNoError;
}

ErrorCode queueGetSize(Queue q, uint32 *size)
{
   *size = q->size;
   return ErrorNoError;
}

ErrorCode queueGetRemaining(Queue q, uint32 *rem)
{
   *rem = q->size - q->used;
   return ErrorNoError;
}

ErrorCode queueDestroy(Queue *q)
{
   free((*q)->start);
   free(*q);
   *q = NULL;

   return ErrorNoError;
}
