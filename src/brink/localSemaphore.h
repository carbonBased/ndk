/* ndk - [ localSemaphore.h ]
 *
 * Routines for implementing semaphores
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
�* @defgroup semaphore semaphore.h: semaphore routines
�* @ingroup core
�*
 * Defines interfaces for semaphores
�*/
/** @{ */

#ifndef __ndk_local_semaphore__
#define __ndk_local_semaphore__

#include <errorCodes.h>
#include <types.h>

typedef struct _LocalSemaphore *LocalSemaphore;

typedef enum _SemaphoreType
{
   SemaphoreTypeFIFO,
   SemaphoreTypePriority
} SemaphoreType;

/**
 * Create a new semaphore
 *
 * @param semaphore The semaphore to create
 * @param type The type of semaphore to create
 * @param initial The initial count of the semaphore
 */
ErrorCode localSemaphoreCreate(LocalSemaphore *semaphore, SemaphoreType type, uint32 initial);

/**
 * Increment the semaphore count
 *
 * @param semaphore The semaphore whose count will be incremented
 */
ErrorCode localSemaphoreUp(LocalSemaphore semaphore);

/**
 * Decrement the semaphore count
 *
 * @param semaphore The semaphore whose count will be decremented
 * @param timeout The ammount of time this function will wait to 
 *                succeed if the count is already 0
 */
ErrorCode localSemaphoreDown(LocalSemaphore semaphore, Timeout timeout);

/**
 * Destroy a semaphore
 *
 * @param semaphore The semaphore to destroy
 */
ErrorCode localSemaphoreDestroy(LocalSemaphore *semaphore);

#endif

/** @} */
