/* ndk - [ messageQueue.h ]
 *
 * Basic message queue class for IPC
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup MessageQueue messageQueue.h: Basic message queue class
* @ingroup core
*
* This file defines a interface for working with message queues
*/
/** @{ */

#ifndef __ndk_localMessageQueue_h__
#define __ndk_localMessageQueue_h__

#include <core/messageQueue.h>
#include <errorCodes.h>
#include <types.h>

typedef struct _LocalMessageQueue *LocalMessageQueue;

ErrorCode localMessageQueueCreate(LocalMessageQueue *msgq, uint32 size);
ErrorCode localMessageQueueSend(LocalMessageQueue mq, Pointer message);
ErrorCode localMessageQueuePeek(LocalMessageQueue mq, Pointer *message);
ErrorCode localMessageQueueReceive(LocalMessageQueue mq, Timeout to, Pointer *message);
ErrorCode localMessageQueueDestroy(LocalMessageQueue *mq);

#endif

/** @} */
