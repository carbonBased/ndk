/* ndk - [ expandingQueue.c ]
 *
 * Basic abstract expanding queue class
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <brink/expandingQueue.h>
#include <brink/list.h>
#include <brink/queue.h>

struct _ExpandingQueue
{
   List queues;
   Queue head;
   Queue tail;
   uint32 queueSize;
   uint32 totalSize;
};

ErrorCode expandingQueueCreate(ExpandingQueue *q, uint32 size)
{
   ExpandingQueue queue;
   Queue firstQueue;

   queue = (ExpandingQueue)malloc(sizeof(struct _ExpandingQueue));
   queue->queueSize = size;
   queueCreate(&firstQueue, size);

   queue->head = firstQueue;
   queue->tail = firstQueue;

   listCreate(&queue->queues);
   listAddBack(queue->queues, firstQueue);

   *q = queue;
   return ErrorNoError;
}

ErrorCode expandingQueueAdd(ExpandingQueue q, Pointer item)
{
   uint32 remaining;

   queueAdd(q->tail, item);
   queueGetRemaining(q->tail, &remaining);

   // if we're run out of queue space, add a new one in...
   if(remaining == 0)
   {
      Queue next;

      queueCreate(&next, q->queueSize);
      listAddBack(q->queues, next);

      q->tail = next;
   }

   return ErrorNoError;
}

ErrorCode expandingQueueGet(ExpandingQueue q, Pointer *item)
{
   uint32 remaining;

   queueGet(q->head, item);
   queueGetRemaining(q->head, &remaining);

   // if we've gotten rid of everything in the head, advance to the next,
   // unless there are none left
   if(remaining == q->queueSize && (q->head != q->tail))
   {
      listRemove(q->queues, q->head);

      // deallocate it
      queueDestroy(&q->head);

      // and get the next one
      listGetFirst(q->queues, (void *)(&(q->head)));
      if(q->head == NULL)
         q->head = q->tail;
   }

   return ErrorNoError;
}

ErrorCode expandingQueuePeek(ExpandingQueue q, Pointer *item)
{
   uint32 remaining;

   queueGetRemaining(q->head, &remaining);
   if(remaining)
   {
      queuePeek(q->head, item);
      return ErrorNoError;
   }

   return ErrorNotAvailable;
}

// returns what!?
ErrorCode expandingQueueGetSize(ExpandingQueue q, uint32 *size)
{
   *size = 0;
   return ErrorNotAvailable;
}

ErrorCode expandingQueueDestroy(ExpandingQueue *q)
{
   // destroy all queues in the list

   // destroy the list
   listDestroy(&((*q)->queues));

   // and this queue
   free(*q);
   *q = NULL;

   return ErrorNoError;
}
