/* ndk - [ messageQueue.c ]
 *
 * Basic message queue class for IPC
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "localMessageQueue.h"
#include "expandingQueue.h"
#include <brink/localSemaphore.h>
#include <brink/localMutex.h>
#include "queue.h"
#include <console.h>
#include <stdarg.h>
#include <core/arch/i386/pager.h>
#include <klibc/assert.h>

struct _LocalMessageQueue
{
   union
   {
      ExpandingQueue expanding;
      Queue limited;
      void *queue;
   } queue;

   uint32 size;

   // general purpose get/put routines that *should* allow an
   // object oriented approach to the queues (ie, allow us to
   // transparently use either a return or expanding queue)
   ErrorCode(*get) (void *queue, Pointer *message);
   ErrorCode(*put) (void *queue, Pointer message);
   ErrorCode(*peek) (void *queue, Pointer *message);
   LocalMutex mutex;

   LocalSemaphore sem;
};

ErrorCode localMessageQueueCreate(LocalMessageQueue *msgq, uint32 size)
{
   LocalMessageQueue mq = (LocalMessageQueue)malloc(sizeof(struct _LocalMessageQueue));

   if(size)
   {
      // create queue
      mq->get = (ErrorCode(*)(void *, Pointer *))queueGet;
      mq->put = (ErrorCode(*)(void *, Pointer))queueAdd;
      mq->peek = (ErrorCode(*)(void *, Pointer *))queuePeek;
      queueCreate(&mq->queue.limited, size);
   }
   else
   {
      // create expanding queue (ie, has no size)
      mq->get = (ErrorCode(*)(void *, Pointer *))expandingQueueGet;
      mq->put = (ErrorCode(*)(void *, Pointer))expandingQueueAdd;
      mq->peek = (ErrorCode(*)(void *, Pointer *))expandingQueuePeek;
      // todo: expanding queue to accept initial size?  Or no...?
      expandingQueueCreate(&mq->queue.expanding, 100);
   }
   mq->size = size;

   printf("Using queue of %x\n", mq->queue);

   localSemaphoreCreate(&(mq->sem), SemaphoreTypeFIFO, 0);
   localMutexCreate(&(mq->mutex), MutexTypeFIFO);

   *msgq = mq;

   return ErrorNoError;
}

ErrorCode localMessageQueueSend(LocalMessageQueue mq, Pointer message)
{
   ErrorCode ec = ErrorNoError;

   //mutexLock(mq->mutex, TimeoutInfinite);

   ec = mq->put((void *)mq->queue.queue, message);

   if(ec == ErrorNoError)
   {
      ec = localSemaphoreUp(mq->sem);
   }
   else
   {
      printf("Error sending message!\n");
   }

   //mutexUnlock(mq->mutex);

   return ec;
}

ErrorCode localMessageQueuePeek(LocalMessageQueue mq, Pointer *message)
{
   ErrorCode ec;

   //mutexLock(mq->mutex, TimeoutInfinite);

   ec = mq->peek((void *)mq->queue.queue, message);

   //mutexUnlock(mq->mutex);

   return ec;
}

ErrorCode localMessageQueueReceive(LocalMessageQueue mq, Timeout to, Pointer *message)
{
   ErrorCode ec;

   //mutexLock(mq->mutex, TimeoutInfinite);

   ec = localSemaphoreDown(mq->sem, to);
   if(ec == ErrorNoError)
   {
      ec = mq->get((void *)mq->queue.queue, message);
   }
   else
   {
      printf("error receiving message\n");
   }

   //mutexUnlock(mq->mutex);

   return ec;
}

ErrorCode localMessageQueueDestroy(LocalMessageQueue *mq)
{
   if((*mq)->size)
   {
      queueDestroy(&((*mq)->queue.limited));
   }
   else
   {
      expandingQueueDestroy(&((*mq)->queue.expanding));
   }
   localSemaphoreDestroy(&((*mq)->sem));

   free(*mq);
   *mq = 0;

   return ErrorNoError;
}
