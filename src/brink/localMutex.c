/* ndk - [ list.h ]
 *
 * Routines for maintaining a list of items
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <brink/localMutex.h>
#include <brink/list.h>
#include <task.h>

struct _LocalMutex
{
   List waitingTasks;
   MutexType type;
   Boolean locked;
   Task owner;
};

ErrorCode localMutexCreate(LocalMutex *mutex, MutexType type)
{
   LocalMutex mu;

   mu = (LocalMutex)malloc(sizeof(struct _LocalMutex));
   listCreate(&(mu->waitingTasks));
   mu->type = type;
   mu->locked = False;

   *mutex = mu;

   return ErrorNoError;
}

// TODO: SERIALIZE this, and handle timeout :)
ErrorCode localMutexLock(LocalMutex mutex, Timeout timeout)
{
   Task task;
   ErrorCode err = ErrorNoError;

   //printf("Locking LocalMutex %x\n", mutex);

   taskGetCurrent(&task);
   if(mutex->locked == True)
   {
      TaskState state;

      listAddBack(mutex->waitingTasks, task);

      /*
       * this will tell the task to wait the specified timeout...
       * if the other thread unlocks the mutex, and this task is
       * waiting on it, it will set the task into an Alive state,
       * otherwise, we'll enter the TimedOut state
       */
      taskSetTimeout(task, timeout);
      taskForceSwitch();
      taskGetState(task, &state);
      listRemove(mutex->waitingTasks, task);
      switch (state)
      {
      case TaskStateTimedOut:
         taskSetState(task, TaskStateAlive);
         err = ErrorTimedOut;
         break;
      case TaskStateAlive:
         mutex->owner = task;
         mutex->locked = True;
         break;
      }
   }
   else
   {
      //printf("Locking unlocked mutex!\n");
      mutex->owner = task;
      mutex->locked = True;
   }

   return err;
}

ErrorCode localMutexUnlock(LocalMutex mutex)
{
   Task task;
   uint32 length;

   //printf("Unlocking LocalMutex %x\n", mutex);
   taskGetCurrent(&task);
   if(mutex->owner == task)
   {
      // LocalMutex has been unlocked... check to see if someone wants it!
      listGetLength(mutex->waitingTasks, &length);
      if(length != 0)
      {
         /* TODO: implement these?
            switch(mutex->type) {
            case MutexTypeFIFO:
            case MutexTypePriority:
            }
          */
         if(listGetFirst(mutex->waitingTasks, (void **)&task) == ErrorNoError)
         {
            listRemove(mutex->waitingTasks, (void *)task);
            mutex->owner = task;

            taskSetState(task, TaskStateAlive);
         }
         else
         {
            printf("No one waiting on this mutex? %x\n", mutex);
            mutex->owner = NULL;
            mutex->locked = False;
         }
         //taskSetTimeout(task, 0);
      }
      else
      {
         mutex->locked = False;
      }

      return ErrorNoError;
   }
   else
   {
      return ErrorNotOwner;
   }
}

ErrorCode localMutexDestroy(LocalMutex *mutex)
{
   listDestroy(&((*mutex)->waitingTasks));
   free(*mutex);

   *mutex = NULL;

   return ErrorNoError;
}
