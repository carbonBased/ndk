/* ndk - [ string.c ]
 *
 * Contains functions similar to the Ansi C
 * implementations in memory.h and string.h
 * Many of these have been borrowed from
 * the NetBSD C Library.  Not only is development
 * quicker when you can rely on these open source
 * projects, it's also incredibly stable (how many
 * thousands of users do you think have "beta
 * tested" those NetBSD routines?)
 *
 * Since I haven't decided upon how I will license
 * ndk, using source code from BSD (under the BSD
 * license) is probably the best way to go.  Leaves
 * the most number of doors open.
 *
 * Please see:
 *   http://www.ajk.tele.fi/libc/code.html [ NetBSD C Library Site ]
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "string.h"

/*
 * sizeof(word) MUST BE A POWER OF TWO
 * SO THAT wmask BELOW IS ALL ONES
 */
typedef long word;              /* "word" used for optimal copy speed */

#define wsize   sizeof(word)
#define wmask   (wsize - 1)

int strncmp(char *s1, char *s2, long n)
{

   if(n == 0)
      return 0;

   do
   {
      if(*s1 != *s2++)
         return (*(unsigned char *)s1 - *(unsigned char *)--s2);

      if(*s1++ == 0)
         break;
   }
   while(--n != 0);

   return 0;
}

int strcmp(char *s1, char *s2)
{
   while(*s1 == *s2++)
      if(*s1++ == 0)
         return 0;

   return (*(char *)s1 - *(char *)--s2);
}

// not from BSD...
char *strcpy(char *dest, const char *src)
{
   int i = 0;

   while(src[i])
      dest[i] = src[i++];

   dest[i] = 0;
   return dest;
}

/** TODO: n should be size_t */
char *strncpy(char *s1, const char *s2, int n)
{
   char *rc = s1;

   while((n > 0) && (*s1++ = *s2++))
   {
      /* Cannot do "n--" in the conditional as size_t is unsigned and we have
       * to check it again for >0 in the next loop.
       */
      --n;
   }
   while(n--)
   {
      *s1++ = '\0';
   }

   return rc;
}

// not from BSD
size_t strlen(const char *s)
{
   int i = 0;

   while(s[i])
   {
      i++;
   }

   return i;
}

char *strchr(const char *s, int c)
{
   do
   {
      if(*s == (char)c)
      {
         return (char *)s;
      }
   }
   while(*s++);

   return 0;
}

void *memcpy(void *dst0, void *src0, long length)
{
//        void *dst0;
//        const void *src0;
//        register size_t length;

   char *dst = dst0;
   const char *src = src0;
   long t;

   if(length == 0 || dst == src) /* nothing to do */
      goto done;

   /*
    * Macros: loop-t-times; and loop-t-times, t>0
    */
#define TLOOP(s) if (t) TLOOP1(s)
#define TLOOP1(s) do { s; } while (--t)

   if((unsigned long)dst < (unsigned long)src)
   {
      /*
       * Copy forward.
       */
      t = (long)src;            /* only need low bits */
      if((t | (long)dst) & wmask)
      {
         /*
          * Try to align operands.  This cannot be done
          * unless the low bits match.
          */
         if((t ^ (long)dst) & wmask || length < wsize)
            t = length;
         else
            t = wsize - (t & wmask);
         length -= t;
         TLOOP1(*dst++ = *src++);
      }
      /*
       * Copy whole words, then mop up any trailing bytes.
       */
      t = length / wsize;
      TLOOP(*(word *) dst = *(word *) src;
            src += wsize;
            dst += wsize);
      t = length & wmask;
      TLOOP(*dst++ = *src++);
   }
   else
   {
      /*
       * Copy backwards.  Otherwise essentially the same.
       * Alignment works as before, except that it takes
       * (t&wmask) bytes to align, not wsize-(t&wmask).
       */
      src += length;
      dst += length;
      t = (long)src;
      if((t | (long)dst) & wmask)
      {
         if((t ^ (long)dst) & wmask || length <= wsize)
            t = length;
         else
            t &= wmask;
         length -= t;
         TLOOP1(*--dst = *--src);
      }
      t = length / wsize;
      TLOOP(src -= wsize;
            dst -= wsize;
            *(word *) dst = *(word *) src);
      t = length & wmask;
      TLOOP(*--dst = *--src);
   }
 done:
   return (dst0);
}

void *memset(void *dst, int c, int n)
{
   if(n != 0)
   {
      char *d = dst;

      do
      {
         *d++ = c;
      }
      while(--n != 0);
   }
   return (dst);
}

int memcmp(const void *s1, const void *s2, int n)
{
   const unsigned char *p1 = (const unsigned char *)s1;
   const unsigned char *p2 = (const unsigned char *)s2;

   while(n--)
   {
      if(*p1 != *p2)
      {
         return *p2 - *p1;
      }
      ++p1;
      ++p2;
   }
   return 0;
}
