#ifndef __ndk_stddef_h__
#define __ndk_stddef_h__

#include <core/types.h>

typedef PointerType size_t;
typedef PointerType ptr_t;

#endif
