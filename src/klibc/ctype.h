/* ndk - [ ctype.h ]
 *
 * Contains code for the libc ctype function
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#ifndef __ndk_ctype_h__
#define __ndk_ctype_h__

int isalnum(int c);
int isalpha(int c);
int isascii(int c);
int isblank(int c);
int iscntrl(int c);
int isdigit(int c);
int isgraph(int c);
int islower(int c);
int isprint(int c);
int ispunct(int c);
int isspace(int c);
int isupper(int c);
int isxdigit(int c);

#endif
