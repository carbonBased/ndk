/* ndk - [ printf.h ]
 *
 * Contains code for the libc printf function
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#ifndef __ndk_printf_h__
#define __ndk_printf_h__

void printf(const char *format, ...);
void putchar(int c);
void itoa(char *buf, int base, int d, signed int length);

#endif
