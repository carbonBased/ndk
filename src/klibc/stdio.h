#ifndef _ndk_stdio_h__
#define _ndk_stdio_h__

//#define FILE int
//typedef struct _IO_FILE FILE;
typedef long FILE;

#define stdin  ((FILE*)(0))
#define stdout ((FILE*)(1))
#define stderr ((FILE*)(2))

int fprintf(FILE * stream, const char *format, ...);

#endif
