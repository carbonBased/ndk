#ifndef __ndk_sysconf_h__
#define __ndk_sysconf_h__

#include <unistd.h>

long sysconf(int name);

#endif
