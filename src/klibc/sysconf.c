#include <sysconf.h>
#include <pager.h>

long sysconf(int name)
{
   uint32 value = 0;

   switch (name)
   {
   case PAGESIZE:
      pagerGetPageSize(&value);
      break;
   }

   return value;
}
