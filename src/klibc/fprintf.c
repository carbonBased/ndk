#include <stdio.h>

int fprintf(FILE * stream, const char *format, ...)
{
   switch ((int)stream)
   {
   case (int)stdin:
      break;
   case (int)stderr:
      printf("[err] ");
   case (int)stdout:
      printf(format);
      break;
   }
}
