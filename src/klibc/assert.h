#ifndef __ndk_assert_h__
#define __ndk_assert_h__

#include <types.h>

#ifndef NDEBUG

#define assert(n) \
	_assert(n, __FILE__, __LINE__);

#else

#define assert(n) ;

#endif

void _assert(uint32 cond, char *fname, uint32 location);

#endif
