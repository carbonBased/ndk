/**
 * \brief basic memory management routine
 *
 * \param increment
 *    if positive, the number of bytes to allocate
 *    if negative, the number of bytes to deallocate
 *    if zero, return the current top of heap
 * \return
 *    if memory was allocated, return the base of the new block
 *    if memory was deallocated, return the new top of heap
 */

// the below is defined in errno.h!
//#define ENOMEM (-1)

#include <errno.h>
#include <console.h>
#include <pager.h>
#include <propDb.h>

void *sbrk(signed int increment);

// the following are temporary... should use sysconf()!!!
#define SB_PAGE_SIZE (4096)
#define SB_PAGE_SIZE_MASK (4095)

/*
 * start off at 4MB, above kernel, for now.  NOTE: this is a hard coded value and 
 * assumes the kernel (and autoloaded boot modules!) will never be larger then 
 * 4MB.  This is definitely the case right now, but may not be in the past.
 */
const uint32 initialBrk = propertyGetAsInteger(kernel_base_linear) + 0x00400000;
uint32 currentBrk = propertyGetAsInteger(kernel_base_linear) + 0x00400000;   // These two must be the same!
uint32 availableInPage = 0x0;

/** TEMPORARY!!! @TODO: FIX THIS */
//long availableMemory = 0xffffffff;
long availableMemory = 0x00A00000;

// TODO: get page size from pagerGetPageSize()

void *sbrk(signed int increment)
{
   void *allocBase;
   void *pageAddr;

   //    void *linearAddr = allocBase;
   uint32 linearAddr, physAddr;

   //console() << "sbrk(" << (long)increment << ");\n";

   printf("sbrk(0x%x)\n", increment);

   if(increment > 0)
   {
      // allocate more memory, on top of brk...
      allocBase = (void *)currentBrk;
      //console() << "inc > 0, inPage " << availableInPage << "\t";

      if((unsigned)availableMemory >= (unsigned)increment)
      {
         // increase brk -- not yet!
         currentBrk += increment;

         // map more pages, if need be
         if(availableInPage > increment)
         {
            //console() << "Alloc less then a page\t";
            availableInPage -= increment;
         }
         else
         {
            /*
               void *pageAddr;
               void *linearAddr = allocBase;
             */
            linearAddr = (uint32)allocBase;

            //console() << "Alloc more then a page\t";

            // use up the rest of the top page
            increment -= availableInPage;

            // and figure out how much we'll have left over...
            availableInPage = SB_PAGE_SIZE - (increment & SB_PAGE_SIZE_MASK);
            if(availableInPage == SB_PAGE_SIZE)
               availableInPage = 0;

            // finally, perform all the necessary page mappings
            while(increment > 0)
            {
               // map a new page here
               // JW: should be pagerGetPage()!!!
               //pageAddr = pages().AllocPage();
               //pager().MapToLinear(pageAddr, (void *)linearAddr);
               pagerGetNewPhysical(&pageAddr);  //memoryAllocPage();
               pagerMapPhysicalToLinear(pageAddr, (LinearAddress) linearAddr);
               //pageAddr = memoryAllocUnmappedPage();
               //pagerMapToLinear(pageAddr, linearAddr);

               //linearAddr = (void *)( (long)linearAddr + PAGE_SIZE );
               linearAddr += SB_PAGE_SIZE;

               increment -= SB_PAGE_SIZE;
            }
         }
         printf("sbrk allocBase %x\n", allocBase);
         return allocBase;
      }
      else
      {
         printf("sbrk returning -2\n");
         return (void *)(-2);
      }
   }
   else if(increment < 0)
   {
      printf("+++ NEGATIVE SBRK... UNTESTED!\n");
      // lets make this more intuitive here... subtracting negatives confused
      // people :)
      increment = -increment;

      if((currentBrk - increment) >= initialBrk)
      {
         // deallocate memory from top of heap...
         //currentBrk -= increment;

         if((SB_PAGE_SIZE - availableInPage) > increment)
         {
            availableInPage -= increment;
            currentBrk -= increment;
         }
         else
         {
            linearAddr = currentBrk - (SB_PAGE_SIZE - availableInPage);
            currentBrk -= increment;

            while(increment >= SB_PAGE_SIZE)
            {
               // unmap top page
               // JW: NOTE: todo!!!
               //physAddr = (long)pager().LinearToPhysical((void *)linearAddr);

               //pager().UnMap((void *)linearAddr);
               //pages().FreePage((void *)physAddr);

               increment -= SB_PAGE_SIZE;
               linearAddr -= SB_PAGE_SIZE;
            }

            // adjust the ammount left in the top page
            availableInPage = SB_PAGE_SIZE - increment;
            if(availableInPage == SB_PAGE_SIZE)
               availableInPage = 0;
         }
      }
      else
      {
         printf("sbrk returning -3\n");
         return (void *)(-3);
      }
   }
   else
   {
      printf("increment == 0, not effecting currentBrk\n");
   }

   printf("sbrk returning %x\n", currentBrk);
   return (void *)currentBrk;
}
