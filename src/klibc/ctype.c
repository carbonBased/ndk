/* ndk - [ ctype.c ]
 *
 * Contains code for the libc ctype function
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "ctype.h"

int isalnum(int c)
{
   return isalpha(c) || isdigit(c);
}

int isalpha(int c)
{
   /**
	 * NOTE: According to linux man page:
	 * In some locales, there may be additional characters for which isalpha()
	 * is true—letters which are neither upper case nor lower case.
	 */
   return (isupper(c) || islower(c));
}

int isascii(int c)
{
   return !(c & 0x80);
}

int isblank(int c)
{
   return ((c == ' ') || (c == '\t'));
}

int iscntrl(int c)
{
   return 0;
}

int isdigit(int c)
{
   return ((c >= '0') && (c <= '9'));
}

int isgraph(int c)
{
   /* TODO: implement: checks for any printable character except space. */
   return !isblank(c);
}

int islower(int c)
{
   return ((c >= 'a') && (c <= 'z'));
}

int isprint(int c)
{
   return 1;
}

int ispunct(int c)
{
   /* TODO: implement: checks  for  any printable character which is not a space or an
    * alphanumeric character.
    */
   return 0;
}

int isspace(int c)
{
   /* checks for white-space characters.   In  the  "C"  and  "POSIX"
    * locales,  these  are:  space, form-feed ('\f'), newline ('\n'),
    * carriage return ('\r'), horizontal tab ('\t'), and vertical tab
    * ('\v').
    */
   return ((c == ' ') || (c == '\f') || (c == '\n') || (c == '\r') || (c == '\t') || (c == '\v'));

}

int isupper(int c)
{
   return ((c >= 'A') && (c <= 'Z'));
}

int isxdigit(int c)
{
   return (((c >= 'A') && (c <= 'F')) || ((c >= 'a') && (c <= 'f')) || (isdigit(c)));
}
