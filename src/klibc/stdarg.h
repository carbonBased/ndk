/* ndk - [ stdarg.h ]
 *
 * Contains partial libc stdarg implementation
 *
 * (c)2005 dcipher / neuraldk
 *           www.neuraldk.org
 */

#ifndef __ndk_stdarg_h__
#define __ndk_stdarg_h__

typedef void **va_list;

#define va_start(ap, last) \
	ap = (void**)&last;		\
	ap++;

#define va_arg(ap, type) \
	(type)*ap++;

#define va_end(ap) \
	ap = 0;

#define va_copy(dest, src) \
	dest = src;

#endif
