/* ndk - [ string.h ]
 *
 * libc string functions used by kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#ifndef __ndk_string_h__
#define __ndk_string_h__

#include <stddef.h>

int strncmp(char *s1, char *s2, long n);
int strcmp(char *s1, char *s2);
char *strcpy(char *dest, const char *src);
char *strncpy(char *s1, const char *s2, int n);
size_t strlen(const char *s);
char *strchr(const char *s, int c);

void *memcpy(void *dst0, void *src0, long size);
void *memset(void *dst, int c, int n);

//int memcmp( const void * s1, const void * s2, size_t n );
int memcmp(const void *s1, const void *s2, int n);

#endif
