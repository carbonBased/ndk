/* ndk - [ klibc.h ]
 *
 * Basic libc implementation for ndk
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * \defgroup klibc platform independant libc implementation
 */
/** @{ */

#ifndef __ndk_klibc_h__
#define __ndk_klibc_h__

#endif

/** @} */
