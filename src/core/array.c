/* ndk - [ array.c ]
 *
 * A simple, object oriented, array implementation.
 * Used for the idt and gdt
 *
 * Please see:
 *     /src/idt.c
 *     /src/gdt.c
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "array.h"

void *arrayElementAt(Array *array, long eNum)
{
   return (void *)(array->baseAddress + (eNum * array->sizeOfElement));
}

long arrayFindNthEmptyElement(Array *array, int n)
{
   int i;
   void *currentElement;

   for(i = 0; i < array->numElements; i++)
   {
      n -= arrayIsElementEmpty(array, i);
      if(n == 1)
         return i;
   }
   return -1;
}

long arrayIsElementEmpty(Array *array, long eNum)
{
   char *element = (char *)(array->baseAddress + (eNum * array->sizeOfElement));
   int i = 0;

   while(i < array->sizeOfElement && element[i] == 0)
      i++;

   if(i <= array->sizeOfElement && element[i] == 0)
      return 1;
   else
      return 0;
}
