/* ndk - [ descgate.c ]
 *
 * Routines to create descriptors and gates withen
 * arrays (ie, the idt and/or gdt).
 *
 * Please see:
 *   /src/gdt.c
 *   /src/idt.c
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */
#include "descgate.h"

void createDescriptor(Array *table, short descNum, long base, long limit, long control)
{
   Descriptor *desc;

   desc = (Descriptor *) arrayElementAt(table, descNum);
   //(Descriptor *)(table + desc_num * 8);

   desc->limit_low = limit & 0xffff;
   desc->base_low = base & 0xffff;
   desc->base_med = (base >> 16) & 0xff;
   desc->access = (control + D_PRESENT) >> 8;
   desc->limit_high = limit >> 16;
   desc->granularity = (control & 0xff) >> 4;
   desc->base_high = base >> 24;

   return;
}

void createGate(Array *table, short descNum, long offset, short selector, long control)
{
   Gate *gate;

   gate = (Gate *) arrayElementAt(table, descNum);
   //(Gate *)(table + desc_num * 8);

   gate->offset_low = offset & 0xffff;
   gate->selector = selector;
   gate->access = control + D_PRESENT;
   gate->offset_high = offset >> 16;

   return;
}
