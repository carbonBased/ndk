/* ndk - [ isr.h ]
 *
 * Default interrupts for x86
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup isr isr.h: Interrupt Service Routines
 * @ingroup i386
 *
 * Contains the interrupt service routines
 */
/** @{ */

#ifndef __ndk_isr_h__
#define __ndk_isr_h__

extern void int00h_wrapper(void);
extern void int01h_wrapper(void);
extern void int02h_wrapper(void);
extern void int03h_wrapper(void);
extern void int04h_wrapper(void);
extern void int05h_wrapper(void);
extern void int06h_wrapper(void);
extern void int07h_wrapper(void);
extern void int08h_wrapper(void);
extern void int09h_wrapper(void);
extern void int0Ah_wrapper(void);
extern void int0Bh_wrapper(void);
extern void int0Ch_wrapper(void);
extern void int0Dh_wrapper(void);
extern void int0Eh_wrapper(void);
extern void int0Fh_wrapper(void);
extern void int10h_wrapper(void);
extern void int11h_wrapper(void);
extern void int12h_wrapper(void);
extern void int13h_wrapper(void);
extern void int14h_wrapper(void);
extern void int15h_wrapper(void);
extern void int16h_wrapper(void);
extern void int17h_wrapper(void);
extern void int18h_wrapper(void);
extern void int19h_wrapper(void);
extern void int1Ah_wrapper(void);
extern void int1Bh_wrapper(void);
extern void int1Ch_wrapper(void);
extern void int1Dh_wrapper(void);
extern void int1Eh_wrapper(void);
extern void int1Fh_wrapper(void);

extern void int20h_wrapper(void);   // timer
extern void int21h_wrapper(void);   // keyboard

extern char exceptionHasErrorCode[0x20];

#endif

/** @} */
