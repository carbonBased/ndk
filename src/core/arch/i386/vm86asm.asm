; ndk - [ vm86.asm ]
;
; The assembly language portion of the vm86
; task handler.  Currently being revamped
; (does not work).
;
; Please see:
;   /src/vm86.{c,h}
;
; (c)2002 dcipher / neuraldk
;           www.neuraldk.org

global vm86IntHandler
global vm86Test
global iretInstruction

;extern kprintf

[bits 32]

;section .data
  intMsg db 'back from int',0

;section .code

; top 16 bits of eax = real mode interrupt
; all other registers must be preserved and sent to real mode int
vm86IntHandler:
  ; create new vm86 stack frame
  push dword 0 ;gs
  push dword 0 ;fs
  push dword 0 ;ds
  push dword 0 ;es
  push dword 0x7000 ; vm86 stack space (ss)
  push dword 0xffff ; esp
  pushfd
  ; ** new
  pop edx
  or edx, 0x20000
  push edx
  ; enable vm86 bit
  ;or dword [ss:esp+4], 0x20000
  ; ** end new -> clobbered edx!!!

  ; save eax and ebx below where vm86 stack will end
  mov [ss:esp-12],  eax
  mov [ss:esp-16], ebx

  ; calculate offset of real mode idt vector
  shr eax, 16
  shl eax, 2
  add eax, 0x00001000 ; real mode idt is stored at 1kb (boot loader put it there)
  ; save the offset in ebx
  mov ebx, eax
  ; and read in the real mode code segment
  xor eax, eax
  ;** mov ax, [ebx+2]
  mov eax, 0x0000c000
  and eax, 0x0000ffff
  ; and put it to the stack
  push eax

  ; read in the real mode ip
  ;** mov ax, [ebx]
  mov eax, 0x00000d98
  and eax, 0x0000ffff
  ; and put it on the stack
  push eax


  ; now restore eax and ebx (remember, we've push 8 bytes, so they're
  ; 8 bytes closer to esp)
  mov eax, [ss:esp-4]
  mov ebx, [ss:esp-8]

  ; remove the high order bits from eax (no longer needed)
  and eax, 0x0000ffff

  ; iret to the real mode interrupt, in vm86 mode!!!
iretInstruction:
  ;jmp iretInstruction
  iret
  ;push intMsg
  ;call printf
  ;iret

vm86Test:
;  mov eax, 0x10
;  shl eax, 16
;  mov ax, 0x13
  mov eax, 0x16
  shl eax, 16
  mov ax, 0x0
  mov ebx, 0x11111111
  mov ecx, 0x22222222
  mov edx, 0x33333333
  int 0x81
  ret
