/* ndk - [ array.h ]
 *
 * Interface for allowing x86 processors in pmode to
 * run rmode apps (vm86 mode)
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup vm86 vm86.h: VM86 mode functions
 * @ingroup i386
 *
 * Contains code for running real mode apps in protected mode
 */
/** @{ */

#ifndef __ndk_vm86_h__
#define __ndk_vm86_h__

#include "ndk.h"
#include "task.h"
#include "pager.h"

typedef struct
{
   long eax, ebx, ecx, edx;
   long cs, ds, es, fs, gs;
   long eip, esp, ebp;
   long esi, edi;
} regs;

/*
void vm86Init(void);
void vm86CreatePagingInfo(void);
void vm86FillTSS(TSS *tss, long eip, long p0_esp, long p3_esp);
void vm86Int(long int);
*/

extern void vm86IntHandler(void);
extern void vm86Test(void);
extern long iretInstruction;

//extern pageDirectory *vm86PageDir;

#endif

/** @} */
