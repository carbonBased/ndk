/* ndk - [ gdt.h ]
 *
 * Defines the GDT
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup gdt gdt.h: GDT functions
* @ingroup i386
*
 * Contains the global descriptor table
*/
/** @{ */

#ifndef __ndk_gdt_h__
#define __ndk_gdt_h__

#include "array.h"

extern Array GDT;

void gdtInit(void);

#endif

/** @} */
