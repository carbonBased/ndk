/* ndk - [ arch.c ]
 *
 * Defines the architecture dependant initialization
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <arch.h>
#include <console.i386.h>
#include <timer.i386.h>
#include <core/task.h>  /* must be included before task.i386.h */
#include <task.i386.h>
#include <taskSpecificHandle.h>
#include <pager.h>
#include <idt.h>
#include <pic.h>
#include <rZero.h>

ErrorCode architectureInit(unsigned long magic, unsigned long addr)
{
   //assert( sizeof(MessageQueue) == 4096 );
   
   consoleInit();
   multibootInit(magic, addr);
   gdtInit();
   pagerInit();
   idtInit();
   /* setup the PIC */
   picInit();
   rZeroInit();
   timerInit();
   //vm86Init();
   //memoryInit();
   taskInit();
   taskSpecificHandleInit();

   return ErrorNoError;
}
