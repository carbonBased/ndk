/* ndk - [ task.i386.h ]
 *
 * Interface for creating/controlling tasks
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup i386Task task.h: Task/Process operations
 * @ingroup i386
 *
 * This file defines the portable interface to the platforms timer hardware.
 */
/** @{ */

#ifndef __ndk_task_i386_h__
#define __ndk_task_i386_h__

typedef enum
{
   TaskSpecificHandles = 0,
   TaskOsDependentHandlesMax
} TaskOsDependentHandle;

// \TODO just a test
ErrorCode taskInit(void);
ErrorCode taskSetOsDependantHandle( Task task, TaskOsDependentHandle handle, Pointer value );
ErrorCode taskGetOsDependantHandle( Task task, TaskOsDependentHandle handle, Pointer *value );

/* probably temporary... */
ErrorCode taskGetSystemTask( Task *task );

#endif

/** @} */
