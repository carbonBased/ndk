/* ndk - [ memory.h ]
 *
 * Basic kernel memory management
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup memory memory.h: kernel memory routines
 * @ingroup core
 *
 * Contains basic memory placement routines
 */
/** @{ */

#ifndef __ndk_memory_h__
#define __ndk_memory_h__

#include "multiboot.h"
#include "console.h"
#include "pmode.h"
#include "pager.h"
#include "stack.h"
#include <types.h>

ErrorCode memoryGetKernelBase(Pointer *base);
ErrorCode memoryGetKernelSize(uint32 *size);
ErrorCode memoryGetModulesBase(Pointer *base);
ErrorCode memoryGetModulesSize(uint32 *size);
ErrorCode memoryGetModulesCount(uint32 *count);

#endif

/** @} */
