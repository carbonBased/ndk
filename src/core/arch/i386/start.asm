; ndk - [ start.asm ]
;
; The assembly language stub which defines its
; sections to the multiboot loader, and sets up
; the environment so that a transfer to the C
; source code can be made.
;
; Please see:
;   /src/ndk.c
;
; (c)2002-2008 dcipher / neuraldk
;           www.neuraldk.org

%include "generated/properties.inc"

bits 32
section .text

; defined in the linker script
extern textEnd
extern dataEnd
extern bssEnd
extern textEndPhys
extern dataEndPhys
extern bssEndPhys
extern printf
   

; defined in the C code
extern ndkStart

; multi boot header defines
MBOOT_PAGE_ALIGN   equ 1 << 0
MBOOT_MEM_INFO     equ 1 << 1
MBOOT_AOUT_KLUDGE  equ 1 << 16

MBOOT_MAGIC equ 0x1BADB002
MBOOT_FLAGS equ MBOOT_PAGE_ALIGN | MBOOT_MEM_INFO | MBOOT_AOUT_KLUDGE
CHECKSUM    equ -(MBOOT_MAGIC + MBOOT_FLAGS)

; this is technically 0x1,0000,0000 - base_linear + base_phsyical
UPPER_HALF_BASE equ (0xffffffff - Property_kernel_base_linear + 1 + Property_kernel_base_physical)
LIN_PHYS_DIFF   equ (Property_kernel_base_linear - Property_kernel_base_physical)

STACK_SIZE  equ 0x1000

global _start
global start
global gdtBase
global idtBase
global realModeIdtBase
global pageDirectoryBase
global pageTableBase
global pageTableConventionalMemBase
global systemStack

entry:
    jmp start

    ; The Multiboot header
align 4, db 0
mBootHeader:

HEADER_PHYS equ mBootHeader - LIN_PHYS_DIFF
ENTRY_PHYS  equ entry - LIN_PHYS_DIFF

    dd MBOOT_MAGIC
    dd MBOOT_FLAGS
    dd CHECKSUM
    ; fields used if MBOOT_AOUT_KLUDGE is set in
    ; MBOOT_HEADER_FLAGS
    dd HEADER_PHYS        ; these are PHYSICAL addresses
    dd ENTRY_PHYS         ; start of kernel .text (code) section
    dd dataEndPhys        ; end of kernel .data section
    dd bssEndPhys         ; end of kernel BSS
    dd ENTRY_PHYS         ; kernel entry point (initial EIP)

 start:
_start:
    ; make sure we're going in the right direction!
    cld

    ; seems grub gives us an acceptable stack, so I'll use it, for now...
    push eax
    push ebx

    ; map the multiboot structure
    mov eax, ebx
    ;mov ecx, 1471 ;sizeof(multiboot_info_t) - TESTING FOR NOW, NOT CORRECT
    mov ecx, 13*4
    call pager_ensure_mapped

    ;mov esi, -1
    ;mov ebx, [esi]

    ; map the array of module_t structures
    mov ecx, [eax+5*4]          ; number of modules
    push ecx
    imul ecx, 4*4               ; sizeof (module_t)
    mov eax, [eax+6*4]
    call pager_ensure_mapped    ; ecx is clobbered

    ; loop through all the modules and map their strings
    pop edi                     ;  num modules to loop through
    dec edi
    mov ebx, eax                ; base of modules
map_module_name_loop:
    ; get the string
    ; eax = info->mods[n].
    mov eax, edi
    imul eax, 4*4               ; sizeof(module_t)
    add eax, ebx                ; &multiboot_info_t->mods[i]
   
    ;; @TODO save eax and map the module data:  (mod_end - mod_start) bytes @ mod_start
    push eax
    mov ecx, [eax+4]            ; module_t->mod_end
    mov eax, [eax]              ; module_t->mod_start
    sub ecx, eax                ; mod_end - mod_start + 1
    inc ecx
   ;;  reset
   ;;  mov ebx, -1
   ;;  mov ebx, [ebx]
    call pager_ensure_mapped
    pop eax

   ;; add eax, 2*4                ; offset of string in module_t
   ;;  JJW: add eax, ebx
    mov eax, [eax + 2*4]        ; offset of string in module_t
    
    ; calc length of string and put into ecx (seems to work correctly)
    xor ecx, ecx
  string_length:
    mov dl, [eax+ecx]
    inc ecx
    cmp dl, 0
    jne string_length

    call pager_ensure_mapped

    dec edi
    jns map_module_name_loop
    
    ; map video memory
    mov eax, 0x000a0000
    mov ecx, 0x00020000
    call pager_ensure_mapped

    ; map the kernel
    mov eax, ENTRY_PHYS
    mov ecx, bssEndPhys
    sub ecx, ENTRY_PHYS
    call pager_ensure_mapped

    ; map all 4mb, as a test...
    ;mov eax, 0
    ;mov ecx, 0x003fffff
    ;call pager_ensure_mapped

    ;mov edi, -1
    ;mov edi, [edi]


    ; Do not clobber eax or ebx!
;;; JJW UNTESTED
    ; calculate the range of pages which need to be mapped (from kernel
    ; physical start, to physical end of modules
;    mov edx, [esp+4]             ;  prob'ly not right...
    ;mov edx, ebx
    ;mov ebx, [edx+6*4]          ;  multiboot_info_t->mods_addr
    ;mov eax, [edx+5*4]          ;  multiboot_info_t->mods_count
        ;;  the above code works correct (eax and ebx are accurate)

    ;printf_test_fmt db 'multiboot 0x%8x mods 0x%8x count %d\n'
    ;push eax
    ;push ebx
    ;push edx
    ;mov eax, printf_test_fmt
    ;push eax
    ;call printf
    ;add esp, 16
    ;jmp $

    ; reset
;   mov ecx, -1
;   mov edx, [ecx]

    ;dec eax
    ;imul eax, dword 4*4         ;  *= sizeof( module_t )
    ;add eax, ebx                ;  &modules[ mods_count ]
    ;mov edx, [eax+4]            ;  mod_end

        ;;  I believe the above accurately calculates the end of the last module, as well

    ; reset
    ;mov ecx, -1
    ;mov ebx, [ecx]

    ;add edx, 4096               ;  add one page (round up)
    ;and edx, 0xfffff000

    ;or edx, 1 | 2 | 4           ; add in the access bits

    ;mov edx, 0x11223344

;;; ----

    ;mov eax, Property_kernel_base_physical

        ;; map the pages
        ;; map in any other required pages (video memory, etc)
        ;; what about multiboot structures?
        
;;; JJW UNTESTED
    ;jmp $

    pop ebx
    pop eax

    ; copy the current real mode idt + bios info
    xor esi, esi
    mov edi, realModeIdtBase - LIN_PHYS_DIFF
    mov ecx, 0x1000
    rep movsb

    mov ecx, pGDT - LIN_PHYS_DIFF
    lgdt    [ecx]                  ; load the GDT

    mov ecx, pIDT - LIN_PHYS_DIFF
    lidt    [ecx]                  ; load the IDT

    mov ecx, pLDT - LIN_PHYS_DIFF
    lldt    [ecx]                  ; load the LDT (0)

    mov     dx, 0x08        ; 0x08 - kernel data segment
    mov     ds, dx
    mov     es, dx
    mov     fs, dx
    mov     gs, dx
    mov     dx, 0x18        ; 0x18 - kernel stack segment
    mov     ss, dx

    mov    esp, systemStack - LIN_PHYS_DIFF + STACK_SIZE

    ; push the multiboot info structure, and magic 
    push ebx
    push eax

    ; load cs with new selector
    ; jmp 0x10:new_gdt
    jmp 0x10:(new_gdt - LIN_PHYS_DIFF)

  new_gdt:
    ; time for some C!

    ; enable the alignment check bit, for fun :)
    ; Personally, I think every single program should be
    ; properly aligned, but too many people don't know about
    ; the performance penalties!  With this bit enabled, the
    ; processor will interrupt on any unaligned memory
    ; access (assuming eflags also contains the same bit)
    ; In time, I'll probably just contain a running count
    ; of unaligned access per task... just for show (it'll
    ; slow unaligned programs even more... but... tough!)
    mov eax, cr0
    or eax, 262144
    mov cr0, eax

    mov edi, 0x000b8000
    mov ax, 0x6565
    mov ecx, 2000
    rep stosw

    ; create identity map for first 1mb + kernel size
    ; for(i = 0; i < 1024; i++) pageTable1mb[i] = i * 4096 | BITS
    ; pTable1mb->pageAddr[i] = (i * 4096) | PAGE_PRESENT | PAGE_READ_WRITE | PAGE_P3_ACCESS;
    ; @TODO this does full 4MB!
; JJW REMOVED FOR NOW
;    mov ecx, 4096
;    mov eax, (1023 * 4096) | (1) | (2) | (4)
;    mov ebx, pageTableConventionalMemBase - LIN_PHYS_DIFF
;  fill_conv_page_table:
;  ;  cmp ecx, 0x280 ; index of 0xa0000 in first page table
;  ;  jl fill_conv_next
;  ;  cmp ecx, 0x2fc ; index of 0xbf000 in first page table
;  ;  jg fill_conv_next
;    mov dword [ebx + ecx - 4], eax
;  fill_conv_next:
;    sub eax, 4096
;    sub ecx, 4
;    jnz fill_conv_page_table

    mov edi, 0x000b8000
    mov ax, 0x6666
    mov ecx, 2000
    rep stosw

    ; set the first page table @TODO remove all the P3 access points!
    mov eax, pageDirectoryBase - LIN_PHYS_DIFF
    mov ebx, pageTableConventionalMemBase - LIN_PHYS_DIFF
    or  ebx, 1 | 2 | 4  ; page present, read/write, p3 access
    mov dword [eax], ebx

    mov edi, 0x000b8000
    mov ax, 0x6767
    mov ecx, 2000
    rep stosw

    ; calculate linear page table index
    ; mov edx, (Property_kernel_base_linear / 4096) & (4096 * 1024 - 1)

    ; create linear mapped kernel page table (@todo: finish)
    mov ecx, 4096
    mov eax, (1023 * 4096) + (1024*1024) | (1) | (2) | (4)
    mov ebx, pageTableBase - LIN_PHYS_DIFF
  fill_linear_page_table:
    mov [ebx + ecx - 4], eax
    sub eax, 4096
    sub ecx, 4
    jnz fill_linear_page_table

    ; calculate linear page directory index, and set linear page table
    mov edx, ( Property_kernel_base_linear / (1024 * 4096) ) * 4
    mov eax, pageDirectoryBase - LIN_PHYS_DIFF
    mov ebx, pageTableBase - LIN_PHYS_DIFF
    or  ebx, 1 | 2 | 4
    mov [eax + edx], ebx

    ; now map the page directory into itself, as the last element
    mov eax, pageDirectoryBase - LIN_PHYS_DIFF
    mov ebx, eax
    or  ebx, 1 | 2 | 4
    mov edx, 1023 * 4
    mov [eax + edx], ebx

    mov edi, 0x000b8000
    mov ax, 0x6868
    mov ecx, 2000
    rep stosw

    ; set page dir
    ; setCR3(((getCR3() & 0xfff) | (long)temp));
    mov eax, cr3
    and eax, 0xfff
    mov ebx, pageDirectoryBase - LIN_PHYS_DIFF
    or  eax, ebx
    mov cr3, eax

    mov edi, 0x000b8000
    mov ax, 0x6969
    mov ecx, 2000
    rep stosw

    ;jmp short $

    ; enable paging
    ; setCR0((getCR0() | CR0_PAGING_ENABLE));
    mov eax, cr0
    or  eax, 0x80000000
    mov cr0, eax

    mov edi, 0x000b8000
    mov ax, 0x7070
    mov ecx, 2000
    rep stosw

    ; jump to linear mapped kernel
    lea eax, [linear_mapped_kernel]
    jmp eax

  linear_mapped_kernel:

    ; reload GDT, IDT and LDT to be within linear kernel space
    mov ecx, pGDT
    lgdt    [ecx]                  ; load the GDT

    mov ecx, pIDT
    lidt    [ecx]                  ; load the IDT

    mov ecx, pLDT
    lldt    [ecx]                  ; load the LDT (0)

    ; @TODO remove identity mapped kernel
    mov edi, 0x000b8000
    mov ax, 0x7171
    mov ecx, 2000
    rep stosw

    call ndkStart
    ;call mb_main
    jmp short $

; Ensures that a certain region of memory is identity mapped into the first page
; table.  This function assumes the address range is within the first 4MB.
;
; on entry:
;  eax address
;  ecx length
;
; clobbered:
;  ecx
pager_ensure_mapped:
    push eax
    push edx
    push ebx
    push edi                    ; note, this register is only saved for debug reasons, for now...
    push esi

    mov edi, 0
    mov esi, eax  ; save incrementing eax here...

  pager_ensure_mapped_loop:
    ;push eax
    mov eax, esi

    inc edi

    and eax, 0xfffff000
    mov ebx, eax
    or  ebx, 1 | 2 | 4

    shr eax, 10 ; (addr / 4096) * 4
    mov edx, pageTableConventionalMemBase - LIN_PHYS_DIFF
    mov [edx+eax], ebx

    ; check if we need to map more pages
    ;pop eax
    mov eax, esi
    and eax, 0x00000fff
    add eax, ecx
    and eax, 0xfffff000
    jz pager_ensure_mapped_done
    
    ; if( ecx >= 4096 ) { ecx -= 4096 } else { ecx = 0 }
    cmp ecx, 4096
    jge decrease_by_full_page
    mov ecx, 0
    jmp already_decreased_count
  decrease_by_full_page:
    sub ecx, 4096
  already_decreased_count:

    ;add eax, 4096
    ;mov esi, eax
    add esi, 4096
    jmp pager_ensure_mapped_loop
  pager_ensure_mapped_done:
    pop esi
    pop edi
    pop ebx
    pop edx
    pop eax
    ret

; note that the idt and gdt below are still in the .text section, not the .data section
; bad things happened at bootup when this was in the .data section, but I'm lazy and didn't
; bother to investigate.  It's fine in .text, and aesthetically pleasing (to myself) to have
; these structures at the beginning of the kernel, anyway

; page align from here down, this will become our idt and gdt
align 4096

idtBase:
        ; zero fill the idt
        times 800h db 0

; begin the gdt
gdtBase:
        ; Null descriptor
        ;   base : 0x00000000
        ;   limit: 0x00000000 ( 0.0 MB)
        dd 0
        dd 0

        ; 0x08 descriptor - Kernel data segment
        ;   base : 0x00000000
        ;   limit: 0xfffff pages (4 GB)
        ;   DPL  : 0
        ;   32 bit, present, system, expand-up, writeable
        dd 0x0000ffff
        dd 0x00cf9200

        ; 0x10 descriptor - Kernel code segment
        ;   base : 0x00000000
        ;   limit: 0xfffff (4 GB)
        ;   DPL  : 0
        ;   32 bit, present, system, non-conforming, readable
        dd 0x0000ffff
        dd 0x00cf9A00

        ; 0x18 descriptor - Kernel stack segment
        ;   base : 0x00000000 //0x00080000
        ;   limit: 0xfffff (4 GB)
        ;   DPL  : 0
        ;   32 bit, present, system, expand-up, writeable
        dd 0x0000ffff
        dd 0x00cf9200
        ;dd 0x00cb9200

        ; ----

        ; 0x20 descriptor - Task data segment
        ;   base : 0x00000000
        ;   limit: 0xfffff pages (4 GB)
        ;   DPL  : 3
        ;   32 bit, present, system, expand-up, writeable
        dd 0x0000ffff
        dd 0x00cff200

        ; 0x28 descriptor - Task code segment
        ;   base : 0x00000000
        ;   limit: 0xfffff (4 GB)
        ;   DPL  : 3
        ;   32 bit, present, system, non-conforming, readable
        dd 0x0000ffff
        dd 0x00cffA00

        ; 0x30 descriptor - Task stack segment
        ;   base : 0x00000000 //0x00080000
        ;   limit: 0xfffff (4 GB)
        ;   DPL  : 3
        ;   32 bit, present, system, expand-up, writeable
        dd 0x0000ffff
        dd 0x00cff200


        ; 0x38 descriptor - Upper-Half Kernel code segment (Tim Robinson trick)
        ;   base : UPPER_HALF_BASE
        ;   limit: 0xfffff (4 GB)
        ;   DPL  : 0
        ;   32 bit, present, system, non-conforming, readable
        dd 0x0000ffff + ((UPPER_HALF_BASE & 0x0000ffff) << 16)
        dd 0x00cf9A00 + ((UPPER_HALF_BASE & 0x00ff0000) >> 16) + (UPPER_HALF_BASE & 0xff000000)

        ; 0x40 descriptor - Upper-Half kernel data segment (Tim Robinson trick)
        dd 0x0000ffff + ((UPPER_HALF_BASE & 0x0000ffff) << 16)
        dd 0x00cf9A00 + ((UPPER_HALF_BASE & 0x00ff0000) >> 16) + (UPPER_HALF_BASE & 0xff000000)

        ; 0x48 descriptor - Upper-Half kernel stack segment (Tim Robinson trick)
        dd 0x0000ffff + ((UPPER_HALF_BASE & 0x0000ffff) << 16)
        dd 0x00cf9200 + ((UPPER_HALF_BASE & 0x00ff0000) >> 16) + (UPPER_HALF_BASE & 0xff000000)


        ; and pad it to the end of the section with 0's
        times 800h-($-gdtBase) db 0

; due to the above IDT and GDT spanning one page (and being page aligned), this
; next section is also page aligned (will contain page directory, and first page table
pageDirectoryBase:
        times 0x1000 db 0

pageTableBase:
        times 0x1000 db 0

pageTableConventionalMemBase:
        times 0x1000 db 0

; realModeIdtBase is also page aligned
realModeIdtBase:
        times 0x1000 db 0

section .data

pIDT            dw 800h         ; limit of 256 IDT slots
                dd idtBase - LIN_PHYS_DIFF      ; contained below

pGDT            dw 800h         ; 256 GDT slots
                dd gdtBase - LIN_PHYS_DIFF      ; contained below (after IDT)

pLDT            dw 0            ; no LDT at this time, but must still load
                dd 0            ; with something (else multitasking might fail)

printf_test_fmt db 'multiboot 0x%8x mods 0x%8x count %d\n'

section .bss
    align 4, db 0
    common systemStack 0x4000
    resb 0x4000