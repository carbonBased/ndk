/* ndk - [ idt.h ]
 *
 * Defines the IDT
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
�* @defgroup idt idt.h: IDT functions
�* @ingroup i386
�*
 * Contains the interrupt descriptor table
�*/
/** @{ */

#ifndef __ndk_idt_h__
#define __ndk_idt_h__

#include "descgate.h"
#include "console.h"
#include "array.h"
#include "isr.h"
#include "ndk.h"
#include "task.h"

extern Array IDT;

void idtInit(void);

ErrorCode idtGetTaskHandle( uint32 interrupt, Task *task );
ErrorCode idtSetTaskHandle( uint32 interrupt, Task task );

/*
void stdHandler (short exceptionNum,
                 short cs,
		  long ip,
		 short error,
		 short taskReg,
		  long edi,
		  long esi,
		  long ebp,
		  long esp,
		  long ebx,
		  long edx,
		  long ecx,
		  long eax,
		 short ss,
		 short gs,
		 short fs,
		 short es,
		 short ds);
		 */
#endif

/** @} */
