/* ndk - [ pager.h ]
 *
 * Functions for interacting with the x86 pager
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @TODO Major cleanup!  Interfaces should be platform independant, and 
 *        source should be easier to read (private functions _ and at bottom!)
 * @defgroup pager pager.h: Intel pager functions
 * @ingroup i386
 *
 * Contains functions for working with the pager
 */
/** @{ */

#ifndef __ndk_pager_h__
#define __ndk_pager_h__

#include "console.h"
#include "errorCodes.h"
#include "types.h"

#define CR0_PAGING_ENABLE	(0x80000000)

// available for use in page directories and tables...
#define PAGE_PRESENT			(1)
#define PAGE_READ_WRITE		(2)
#define PAGE_P3_ACCESS		(4)
#define PAGE_WRITE_THROUGH	(8)
#define PAGE_CACHE_DISABLE	(16)
#define PAGE_ACCESSED		(32)

// page directories only
#define PAGE_SIZE_4MB		(128)

// page tables only
#define PAGE_DIRTY			(64)

// TODO: the following defines are from the old memory.c... clean them up!
#define PAGE_SIZE (4096)
#define PAGE_SIZE_MASK (4095)

// start allocating at 2GB (linear)
#define ALLOC_OFFSET     (524288)

#define MULTI_PAGE_ENTRY (1)
#define TOP_OF_DMA_STACK (4*1024*1024)

typedef struct
{
   long pageTable[1024];
} /*__attribute__ ((packed))*/ PageDirectory;

typedef struct
{
   long pageAddr[1024];
} /*__attribute__ ((packed))*/ PageTable;

typedef enum _PageAccess
{
   PageAccessKernel,
   PageAccessApplication
} PageAccess;

typedef enum _PagerHeapDirection
{
   PagerHeapExpandUp,
   PagerHeapExpandDown,
} PagerHeapDirection;

struct _PagerHeap
{
   Pointer base;
   uint32  numPages;
   PagerHeapDirection direction;
};

typedef void *PhysicalAddress;
typedef void *LinearAddress;
typedef struct _PagerHeap *PagerHeap;

/* should there be another kernel heap, occupying the same space, but expanding 
 * in the opposite direction, for temporary allocations (ie, the ones made 
 * while creating mutexes, semaphores and messageQueues?).  Doing so would 
 * prevent fragmentation!
 */
extern PagerHeap pagerHeapKernel;
extern PagerHeap pagerHeapApps;
extern PagerHeap pagerHeapShared;
/** @TODO temporary kernel heap allocating at top, and going down of std kernel heap */

ErrorCode pagerInit(void);
ErrorCode pagerLock(void);
ErrorCode pagerUnlock(void);
ErrorCode pagerInvalidate(void);
ErrorCode pagerGetPageSize(uint32 *page);
ErrorCode pagerGetNewPhysical(PhysicalAddress *addr);
//ErrorCode pagerGetNewLinear(LinearAddress *addr); /* delete ? */
ErrorCode pagerMapPhysicalToLinear(PhysicalAddress phys, LinearAddress lin);
ErrorCode pagerUnmap(LinearAddress lin);
ErrorCode pagerUnmapRange( LinearAddress lin, uint32 numPages);
ErrorCode pagerGetPhysical(LinearAddress lin, PhysicalAddress *addr);
ErrorCode pagerGetLinear(PhysicalAddress phys, LinearAddress *addr);   // possible/needed?

/* simple page allocator */
ErrorCode pagerAllocate( PagerHeap heap, uint32 numPages, Pointer *base );
ErrorCode pagerReserve( PagerHeap heap, uint32 numPages, Pointer *base );
ErrorCode pagerMap( PagerHeap heap, PhysicalAddress phys, Pointer *base );
ErrorCode pagerMapRange( PagerHeap heap, uint32 numPages, PhysicalAddress phys[], Pointer *base );
ErrorCode pagerFree( PagerHeap heap, uint32 numPages, Pointer base );
ErrorCode pagerFreePhysical( PhysicalAddress addr );

// remove/change
//ErrorCode pagerMapPage(unsigned long physAddr);
//void *pagerLinToPhys(void *linAddr);

#endif

/** @} */
