/* ndk - [ console.c ]
 *
 * Contains the minimal ammount of code necessary
 * to have console output for the kernel.
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <console.h>
#include <console.i386.h>

#include <brink/list.h>
#include <io.h>

/* The video memory address. */
#define COLOUR_VIDEO    0xB8000
#define MONO_VIDEO	0xB0000

/* The number of lines and columns (1 based, not 0 based!) */
static long consoleCols = 80;
static long consoleLines = 25;

/* precalculate some commonly used equations */
static long consoleBytesPerLine = 80 * 2;
static long consoleTotalBytes = 4000;

/* The attribute of an character. */
static char consoleAttrib = 15;

/* position of cursor */
static int consoleXPos, consoleYPos;

// to support a new scrollable window... this should be changed,
// as it only supports windows of full width
static unsigned char *windowAddrTop;
static int windowAddrSize;

/* location of video memory. */
static volatile unsigned char *consoleAddr;

/** TODO: implement the list... for now, I'm just keeping track of one... as mentioned in console.h, 
* this is only a hack to get serial output for debugging purposes 
*/
//static List characterListenters;

static ConsoleCharacterListener charListener;

ErrorCode consoleInit(void)
{
   // check for mono adaptor?
   consoleAddr = (unsigned char *)COLOUR_VIDEO;

   windowAddrTop = (unsigned char *)consoleAddr;
   windowAddrSize = consoleTotalBytes;

   charListener = NULL;

   consoleClear();

   return ErrorNoError;
}

/* Clear the screen and initialize VIDEO, XPOS and YPOS. */
ErrorCode consoleClear(void)
{
   int i;

   for(i = 0; i < consoleCols * (consoleLines - 1) * 2; i++)
      *(consoleAddr + i) = 0;

   consoleXPos = 0;
   consoleYPos = 0;

   return ErrorNoError;
}

/* Put the character C on the screen. */
ErrorCode consolePutChar(int c)
{
   if(charListener)
   {
      charListener(c);
   }
   /* bochs debug console output */
   outportb(0xe9, c);

   if(c == '\n' || c == '\r')
   {
    newline:
      consoleXPos = 0;
      consoleYPos++;
      if(consoleYPos >= (consoleLines - 1))
      {
         consoleYPos = consoleLines - 1;
         // scroll up a line
         memcpy(windowAddrTop, windowAddrTop + consoleBytesPerLine,
                windowAddrSize - consoleBytesPerLine);
         // clear the last line
         memset(windowAddrTop + windowAddrSize - consoleBytesPerLine, 0, consoleBytesPerLine);
      }

      /** NOTE: function return in middle, fix */
      return;
   }
   else if(c == '\t')
   {
      consoleXPos += (8 - (consoleXPos & 7));
      /** NOTE: function return in middle, fix */
      return;
   }

   *(consoleAddr + (consoleXPos + consoleYPos * consoleCols) * 2) = c & 0xFF;
   *(consoleAddr + (consoleXPos + consoleYPos * consoleCols) * 2 + 1) = consoleAttrib;

   consoleXPos++;
   if(consoleXPos >= consoleCols)
      goto newline;

   return ErrorNoError;
}

ErrorCode consoleGetSize(uint32 *rows, uint32 *cols)
{
   *rows = consoleLines;        // make lines 1 based!
   *cols = consoleCols;         // cols is already 1 based
}

ErrorCode consoleGetFormat(ConsoleColourFormat * format)
{
   *format = ConsoleColour_TBgIFgI_844;

   return ErrorNoError;
}

ErrorCode consoleGetFrameBuffer(Pointer *buffer)
{
   *buffer = (Pointer)COLOUR_VIDEO;

   return ErrorNoError;
}

ErrorCode consoleEnableCharacterListener(ConsoleCharacterListener characterListener)
{
   ErrorCode err = ErrorNoError;

   charListener = characterListener;

   return err;
}

ErrorCode consoleDisableCharacterListener(ConsoleCharacterListener characterListener)
{
   ErrorCode err = ErrorNoError;

   charListener = NULL;

   return err;
}

ErrorCode consoleLockScrollWindowRect(uint32 row1, uint32 col1, uint32 row2, uint32 col2)
{
   return ErrorNotImplemented;
}

ErrorCode consoleLockScrollWindow(uint32 row1, uint32 row2)
{
   // make zero based!
   row1--;
   row2--;

   windowAddrTop = (unsigned char *)(consoleAddr + (consoleBytesPerLine * row1));
   windowAddrSize = ((row2 - row1) + 1) * consoleBytesPerLine;

   return ErrorNoError;
}
