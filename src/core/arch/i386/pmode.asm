; ndk - [ pmode.asm ]
;
; These are x86 specific assembly language routines for ndk.
; For portability reasons I've tried to encapsulate all the
; assembly language routines in external files.
;
; I've tried to avoid inline assembly at all costs.  I'd like
; the C source to be completely ANSI portable (although there
; are __attribute__ ((packed))'s throughout, for GCC.
;
; Please see:
;   /src/pager.{c,h}
;
; (c)2002 dcipher / neuraldk
;           www.neuraldk.org

[bits 32]

global jumpToTSS
global loadTaskReg
global invalidatePage
global getCR0
global setCR0
global getCR2
global setCR2
global getCR3
global setCR3

segment .text

jumpToTSS:
      push    ebp
      mov     ebp, esp
      jmp far [ss:ebp+4]
      pop     ebp
      ret

loadTaskReg:
      push    ebp
      mov     ebp, esp
      ltr     word [ss:ebp+8]
      pop     ebp
      ret

invalidatePage:
      push    ebp
      mov     ebp, esp
      mov     eax, [ss:ebp+8]
      invlpg  [eax]
      pop ebp
      ret

getCR0:
      mov eax, cr0
      ret

setCR0:
      push ebp
      mov ebp, esp
      push eax
      mov eax, [ss:ebp+8]
      mov cr0, eax
      pop eax
      pop ebp
      ret

getCR2:
      mov eax, cr2
      ret

setCR2:
      push ebp
      mov ebp, esp
      push eax
      mov eax, [ss:ebp+8]
      mov cr2, eax
      pop eax
      pop ebp
      ret

getCR3:
      mov eax, cr3
      ret

setCR3:
      push ebp
      mov ebp, esp
      push eax
      mov eax, [ss:ebp+8]
      mov cr3, eax
      pop eax
      pop ebp
      ret
