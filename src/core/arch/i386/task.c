/* ndk - [ task.c ]
*
* This file encompasses the ndk multitasking
* code.  This code is currently being revamped
* and will, eventually, be ported to an rZero
* module to boot-time interchangeability.
*
* Implementation of /src/task.h
*
* Please see:
*   /doc/commonTaskStructure.kwd
*
* (c)2002 dcipher / neuraldk
*           www.neuraldk.org
*/

#include "task.h"
#include "gdt.h"
#include "idt.h"
#include "pager.h"
#include "descgate.h"
#include <klibc/malloc.h>
#include <core/timer.h>
#include <brink/localMessageQueue.h>
#include <core/propDb.h>
#include <core/objectKernel.h>
#include <core/console.h>
#include <core/arch/i386/pmode.h>
#include <core/arch/i386/task.i386.h>

#include <string.h>

#define TASK_FLAG_IOMAP  (1)

#define TASK_TYPE_KERNEL (0)
#define TASK_TYPE_USER   (1)
#define TASK_TYPE DAEMON (2)

#define TASK_EXECUTING   (0)
#define TASK_READY       (1)
#define TASK_BUSY        (2)
#define TASK_WAITING     (3)

// Possible Flags for taskNew
#define TASK_P0          (1)
#define TASK_IOMAP       (1 << 1)
//                       (1 << 2)
//                       (1 << 3)
//                       (1 << 4)
//                       (1 << 5)
//                       (1 << 6)
//                       (1 << 7)
//                       (1 << 8)

/* TSS definition */
typedef struct
{                               /* TSS for 386+ */
   uint32 link;
   uint32 esp0;
   uint32 ss0;
   uint32 esp1;
   uint32 ss1;
   uint32 esp2;
   uint32 ss2;
   uint32 cr3;
   uint32 eip;
   uint32 eflags;
   uint32 eax;
   uint32 ecx;
   uint32 edx;
   uint32 ebx;
   uint32 esp;
   uint32 ebp;
   uint32 esi;
   uint32 edi;
   uint32 es;
   uint32 cs;
   uint32 ss;
   uint32 ds;
   uint32 fs;
   uint32 gs;
   uint32 ldtr;
   uint16 trace;
   uint16 io_map_addr;
   Pointer osDependent[TaskOsDependentHandlesMax];
} __attribute__ ((packed)) TSS;

typedef struct
{                               /* TSS for 386+ */
   uint32 link;
   uint32 esp0;
   uint32 ss0;
   uint32 esp1;
   uint32 ss1;
   uint32 esp2;
   uint32 ss2;
   uint32 cr3;
   uint32 eip;
   uint32 eflags;
   uint32 eax;
   uint32 ecx;
   uint32 edx;
   uint32 ebx;
   uint32 esp;
   uint32 ebp;
   uint32 esi;
   uint32 edi;
   uint32 es;
   uint32 cs;
   uint32 ss;
   uint32 ds;
   uint32 fs;
   uint32 gs;
   uint32 ldtr;
   uint16 trace;
   uint16 io_map_addr;
   Pointer osDependent[TaskOsDependentHandlesMax];
   uint8  io_map[8192];
} __attribute__ ((packed)) TSSWithIOMap;

typedef struct
{
   uint8 port[8192];
} __attribute__ ((packed)) IOMap;

struct _Task
{
   //long  driverSpecific[4];
   //union {
   //  TSSWithIOMap *withIO;
   //  TSS *withoutIO;
   //} tss;
   TSS* tss;
   short selector;
   short null;
   long uid;
   long gid;
   char flags;
   long startingTick;
   long totalBusyTicks;
   long totalWaitTicks;
   long totalExecTicks;
   long totalTimesActive;
   // stuff above here may not be populated by taskCreate() yet!
   long next;
   void (*entry) (void *);
   void *argument;
   TaskState state;
   TaskType type;
   Timeout timeout;
   uint32 stackSize;
   Pointer p0Stack;
   Pointer p3Stack;
   uint8 priority;
   char name[64];
   MessageQueueId queueId;
   Object object; /* if set, contains imports and exports for this task, as well as section locations */
} /*_Task __attribute__ ((packed))*/ ;

// \TODO replace with two lists -- one for active (requiring attention) tasks,
// one for waiting/blocked tasks
Task currentTask = NULL;
Task firstTask = NULL;

TSS mainTss;
struct _Task mainTask;

// private functions...
static void taskCommonEntry(void);
static void taskScheduler(void *arg);
static ErrorCode taskGetEntry(Task task, void (**entry) (void *));
static ErrorCode taskGetArgument(Task task, void **argument);
static Boolean taskIsBusy(Task task);
static Task taskGetNext(Task from);
static short taskGetSelector(Task from);
static void taskFillTSSApp(TSS * tss, long eip, long p0_esp, long p3_esp);
static void taskFillTSSOS(TSS * tss, long eip, long p0_esp);
static void taskAdd(Task task, TSS * tss);

static Timer taskTimer;

// declared in start.asm!
extern uint32 systemStack;

// public functions...
ErrorCode taskInit(void)
{
   mainTss.ldtr = 0;
   mainTss.fs = mainTss.gs = 0;
   mainTss.ds = mainTss.es = SEL_P0_DATA;
   mainTss.ss = SEL_P0_STACK;
   mainTss.cs = SEL_P0_CODE;
   mainTss.eflags = 0x202L | 1 << 18;  // IOPL0, STI, Alignment Check
   mainTss.esp = 0;
   mainTss.eip = 0;
   mainTss.cr3 = getCR3();

   printf("adding main task\n");
   mainTask.type = TaskTypeOS;
   mainTask.state = TaskStateAlive;
   mainTask.priority = 0;
   mainTask.timeout = 0;
   mainTask.stackSize = 0x1000;
   mainTask.p0Stack = (Pointer)&systemStack;
   mainTask.p3Stack = (Pointer)0xdeeffeed;
   mainTask.object = objectKernel;
   strcpy(mainTask.name, "main");
   taskAdd(&mainTask, &mainTss);
   //printf("Task Main: 0x%x\n", task[0].selector);

   //printf("CR3: 0x%x\n", getCR3());

   //printf("Loading TR\n");
   loadTaskReg(mainTask.selector);

   // every 10ms, or 100 times per second
   // 5ms == 200 times per second
   timerCreate(&taskTimer, 10, taskScheduler, NULL);
   printf("Got timer %x for task scheduler\n", taskTimer);
   timerSetRepeat(taskTimer, True);
   printf("Start timer\n");
   timerStart(taskTimer);

   return ErrorNoError;
}

/** \TODO stackSize references p0 or p3 or both stacks? */
ErrorCode taskCreate(Task *task, Task parent, String name, void (*entry) (void *), void *argument,
                     TaskType type, uint32 stackSize, uint8 priority)
{
   Task newTask;
   TSS *tss;
   IOMap *iomap;
   void *p0_stack, *p3_stack;
   uint32 i;

   newTask = (Task)malloc(sizeof(struct _Task));
   tss = (TSS *) malloc(sizeof(TSS));

   if(parent != NULL)
   {
      printf("___TODO: create thread, not task!\n");
   }

   newTask->entry = entry;
   newTask->argument = argument;
   newTask->type = type;
   newTask->state = TaskStateAlive;
   newTask->stackSize = stackSize;  // p0 or p3 stack!?
   newTask->priority = priority;
   newTask->timeout = 0;
   newTask->object = NULL;
   
   // concatenate name to 63 characters with a null terminator
   if(strlen(name) > 63)
      name[63] = '\0';
   strcpy(newTask->name, name);

   // p0 stack always 4kb?
   /** @TODO: pager... */
   p0_stack = (void *)malloc(4096);
   newTask->p0Stack = p0_stack;
   switch (type)
   {
   case TaskTypeUser:
      /** @TODO: pager */
      p3_stack = (void *)malloc(stackSize);
      //((long*)p3_stack)[(stackSize/4)-1] = (long)argument;
      newTask->p3Stack = p3_stack;
      taskFillTSSApp(tss, (long)taskCommonEntry, (long)p0_stack + 4096, (long)p3_stack + stackSize);
      break;
   case TaskTypeOS:
      //((long*)p0_stack)[1024-1] = (long)argument;
      newTask->p3Stack = (Pointer)0xbaabbaab;
      memset(p0_stack, 0x11, 4096);
      taskFillTSSOS(tss, (long)taskCommonEntry, (long)p0_stack + 4096);
      break;
   }
   
   for(i = 0; i < TaskOsDependentHandlesMax; i++)
   {
      tss->osDependent[i] = NULL;
   }
   
   messageQueueCreate(&newTask->queueId, 0);

   *task = newTask;
   taskAdd(*task, tss);

   return ErrorNoError;
}

ErrorCode taskDestroy(Task *task)
{
   (*task)->state = TaskStateDead;

   // invalidate
   *task = NULL;

   // wait for this task to no longer be active?  Or simply have the task
   // switch routine note that this task is dead and remove it?
   return ErrorNotImplemented;
}

ErrorCode taskSetObject( Task task, Object object )
{
   ErrorCode ec = ErrorNoError;
   
   task->object = object;
   
   return ec;
}

ErrorCode taskGetCurrent(Task *task)
{
   *task = currentTask;

   return ErrorNoError;
}

ErrorCode taskGetState(Task task, TaskState * state)
{
   *state = task->state;
   return ErrorNoError;
}

ErrorCode taskSetState(Task task, TaskState state)
{
   task->state = state;
   return ErrorNoError;
}

ErrorCode taskGetMessageQueueId(Task task, MessageQueueId *queueId)
{
   ErrorCode err = ErrorNoError;

   if(task)
   {
      *queueId = task->queueId;
   }
   else
   {
      err = ErrorNoError;
      *queueId = NULL;
   }

   return err;
}

ErrorCode taskSetTimeout(Task task, Timeout to)
{
   uint32 currentTicks;
   uint32 frequency;

   //Task task;
   //taskGetCurrent(&task);

   timerGetTicks(&currentTicks);
   timerGetFreq(&frequency);

   // when timerGetTicks() >= this value, the task will become active again
   if(to == TimeoutInfinite)
   {
      task->timeout = TimeoutInfinite;
   }
   else
   {
      task->timeout = currentTicks + (to * frequency / 1000);
   }
   task->state = TaskStateWaiting;
   //task->waitingOn = TaskWaitingDelay;
   //task->delayMs = ms;

   // ideally, force a task switch here!

   return ErrorNoError;
}

ErrorCode taskForceSwitch(void)
{
   Task task;

   //asm("cli");
   taskGetCurrent(&task);
   printf("Forcing task switch from %x!\n", task);

   // calling int 0x20 to force a switch isn't good... will not force a switch,
   // per se, it will force the timer code to be rerun... this will only force a
   // switch if the task switching code has timer has expired!!!
   // Not to mention, explicity calling the timer interrupt will screw up the
   // timer, as it will think the timer delta has expired (ie, 10ms, perhaps)
   //__asm__("sti;\nint $0x20");

   // Instead, call the switching code direction!
   taskScheduler(NULL);
   //asm("sti");
}

extern textEnd;
ErrorCode taskPrintStackTrace(Task task)
{
   //Task task;
   int32 i, stackDirection;
   uint32 stackSize;

   // if used in an interrupt, this wont be true!  This will be the task's
   // p0 stack, but we want the p3 stack, usually!
   uint32 *stackPtr = (uint32 *)&task;
   Pointer stackBase;

   /* uncomment to remove (lengthy) stack output on each trap/exception */
   /*
   while(1)
   {
      printf("fault, pausing\n");
      asm("stop: \n hlt \n jmp stop\n");
   }
   //*/

   /* ensure we use the fullscreen when printing the stack trace */
   consoleLockScrollWindow(1, 25);

   // register print out goes here

   //taskGetCurrent(&task);
   taskGetStackSize(task, &stackSize);
   //implement taskGetStackMaximumSize(Task task, uint32 *size);

   // returns +1 or -1 for the direction the stack expands... since we'll be going backwards,
   // we want to negate it
   taskGetStackDirection(&stackDirection);
   //stackDirection = -stackDirection;
   taskGetStackBase(task, &stackBase);
   // or should offset == stackPtr - stackBase > 0 ? 1 : 0
   taskGetStackPointer(task, (Pointer *)&stackPtr);

   if(stackSize > 128)
      stackSize = 128;           // limit ourselves for now (for some reason, errors in modules have invalid stack sizes!)
   //if(stackSize > 64)
   //   stackSize = 64;           // limit ourselves for now

#if 1
   printf("Stacktrace %x %x %d %d:\n", stackBase, stackPtr, stackSize, stackDirection);
   printf("Stack.Begin:\n");
   for(i = 0; i < stackSize; i++)
   {
      //printf("0x%8x  ", stackBase + stackOffset + (i * stackDirection);
      if(((i + 1) % 8) == 0 && i != 0)
      {
         printf("%8x\n",  *(stackPtr + (i * stackDirection)));
         continue;
      }
      else
      {
         printf("%8x  ", *(stackPtr + (i * stackDirection)));
      }
      //if(((i + 1) % 4) == 0 && i != 0)
      //   printf(" ");
   }
   printf("Stack.End\n");
#endif
   
   /* 
    * if we have symbolic information for this task, we can dump out a probably 
    * call stack by cross-referencing with the .text segment
    */
   if( task->object )
   {
      ObjectSection code;

      printf("task->object %x\n", task->object );
      
      if( objectParserGetSection( task->object, ".text", &code ) == ErrorNoError )
      {
         PointerType codeStart, codeEnd;
         PointerType kernelStart, kernelEnd;

         
         codeStart = code->offset + (PointerType)task->object->base;
         codeEnd   = code->length + (PointerType)task->object->base;
         
         printf(".text = %x - %x", codeStart, codeEnd );

         kernelStart = propertyGetAsInteger(kernel_base_linear);
         kernelEnd = (PointerType)&textEnd;
         
         taskGetStackPointer(task, (Pointer *)&stackPtr);
         
         printf("Probable stack trace based on 0x%8x < .text < 0x%8x\n", codeStart, codeEnd );
         
         /* go through the whole stack again, and cross-ref with .text boundaries */
         for(i = 0; i < stackSize; i++)
         {
            PointerType loc = *(stackPtr + (i * stackDirection));
            
            if( loc > codeStart && loc < codeEnd )
            {
               printf("%8x\n", loc );
            }
            else if( loc > kernelStart && loc < kernelEnd )
            {
               printf("%8x (kernel)\n", loc );
            }
         }
      }
   }

   return ErrorNoError;
}

ErrorCode taskGetStackPointer(Task task, Pointer *ptr)
{
   Task current;
   Boolean isCurrent = False;
   TSS *tss = (TSS *) task->tss;

   taskGetCurrent(&current);
   if(current == task)
      isCurrent = True;

   switch (task->type)
   {
   case TaskTypeOS:
      if(isCurrent)
      {
         // if we're in an OS task, and it's the current one, we'll
         // grab the stack pointer from the current stack pointer
         *ptr = (Pointer)((uint32)&task - 8);   // \TODO: fix/calc this!
      }
      else
      {
         // otherwise, grab it from the TSS, as it's been saved!
         *ptr = (Pointer)tss->esp0;
      }
      break;
   case TaskTypeUser:
      if(isCurrent)
      {
         // at this point we're in the app's p0 stack, but we want the
         // app's p3 stack!  How to get it?  The return to address in
         // p3 stack space should be at the top of this p0 stack!
      }
      else
      {
         // grab from the TSS
         *ptr = (Pointer)tss->esp;
      }
      break;
   }

   return ErrorNoError;
}

ErrorCode taskGetStackBase(Task task, Pointer *ptr)
{
   switch (task->type)
   {
   case TaskTypeOS:
      *ptr = (Pointer)task->p0Stack;
      break;
   case TaskTypeUser:
      *ptr = (Pointer)task->p3Stack;
      break;
   }

   return ErrorNoError;
}

ErrorCode taskGetStackSize(Task task, uint32 *size)
{
   *size = task->stackSize;

   return ErrorNoError;
}

ErrorCode taskGetStackDirection(int32 *direction)
{
   // This may be false!
   *direction = +1;

   return ErrorNoError;
}

ErrorCode taskSetOsDependantHandle( Task task, TaskOsDependentHandle handle, Pointer value )
{
   ErrorCode ec = ErrorNoError;
   
   if( handle < TaskOsDependentHandlesMax )
   {
      task->tss->osDependent[ handle ] = value;
   }
   else
   {
      ec = ErrorBadParam;
   }
   
   return ec;
}

ErrorCode taskGetOsDependantHandle( Task task, TaskOsDependentHandle handle, Pointer *value )
{
   ErrorCode ec = ErrorNoError;
   
   if( handle < TaskOsDependentHandlesMax )
   {
      *value = task->tss->osDependent[ handle ];
   }
   else
   {
      ec = ErrorBadParam;
   }
   
   return ec;
}

ErrorCode taskGetSystemTask( Task *task )
{
   *task = &mainTask;

   return ErrorNoError;
}

// private functions...
static void taskFillTSSApp(TSS * tss, long eip, long p0_esp, long p3_esp)
{
   tss->ldtr = 0;
   tss->fs = tss->gs = SEL_P3_DATA | 3;
   tss->ds = tss->es = SEL_P3_DATA | 3;
   tss->ss = SEL_P3_STACK | 3;
   tss->cs = SEL_P3_CODE | 3;
   /*
      tss->fs = tss->gs = SEL_P0_DATA;
      tss->ds = tss->es = SEL_P0_DATA;
      tss->ss = SEL_P0_STACK;
      tss->cs = SEL_P0_CODE;
    */
   tss->eflags = 0x00000202L;   //| 1 << 18;  // IOPL=0, STI, Alignment Check
   tss->esp = /*p0_esp; */ p3_esp;
   tss->eip = eip;
   tss->cr3 = getCR3();

   tss->esp0 = p0_esp;
   tss->ss0 = SEL_P0_STACK;

   tss->link = 0;
}

static void taskFillTSSOS(TSS * tss, long eip, long p0_esp)
{
   tss->ldtr = 0;
   tss->fs = tss->gs = SEL_P0_DATA;
   tss->ds = tss->es = SEL_P0_DATA;
   tss->ss = SEL_P0_STACK;
   tss->cs = SEL_P0_CODE;
   /*
      tss->fs = tss->gs = SEL_P0_DATA;
      tss->ds = tss->es = SEL_P0_DATA;
      tss->ss = SEL_P0_STACK;
      tss->cs = SEL_P0_CODE;
    */
   tss->eflags = 0x00000202L;   //| 1 << 18;  // IOPL=0, STI, Alignment Check
   tss->esp = p0_esp;
   tss->eip = eip;
   tss->cr3 = getCR3();

   tss->esp0 = p0_esp;
   tss->ss0 = SEL_P0_STACK;

   tss->link = 0;
}

static long taskFindEmptySelector(void)
{
   // find first empty GDT entry (remember, GDT entry 0
   // is zero, but we don't want to use it, hense the 2)
   return arrayFindNthEmptyElement(&GDT, 2);
}

static long taskCreateSelector(Task task)
{
   int selectorIndex = taskFindEmptySelector();

   task->selector = selectorIndex * 8;

   createDescriptor(&GDT, selectorIndex, (uint32)task->tss, sizeof(TSS), D_TSS);
}

static void taskAdd(Task task, TSS * tss)
{
   //Task llTask;

   printf("adding task %x ", task);

   task->tss = tss;
   taskCreateSelector(task);

   printf("with selector %x\n", task->selector);

   asm("cli");
   if(firstTask == NULL)
   {
      printf("firstTask is null\n");
      firstTask = (Task)task;
      currentTask = (Task)task;
   }
   else
   {
      task->next = (long)firstTask;
      firstTask = task;
   }
   asm("sti");
   /*
      llTask = firstTask;
      while(llTask->next != NULL) llTask = (Task)llTask->next;
      llTask->next = (long)task;
    */

   return;
}

static void taskCreateTaskGate(Task task, TSS * tss)
{
   Task llTask;
   int selectorIndex;

   task->tss = tss;
   taskCreateSelector(task);    // create selector in GDT

   //selectorIndex = 128;
   //task->selector = selectorIndex * 8;
   createGate(&IDT, 128, 0x0, task->selector | 3, D_TASK + D_DPL3);

   /* do not add take gate to linked list!!!
      if(firstTask == NULL) {
      firstTask = (Task *)task;
      currentTask = (Task *)task;
      return;
      }

      llTask = firstTask;
      while(llTask->next != NULL) llTask = (Task *)llTask->next;
      llTask->next = (long)task;
    */

   return;
}

static Boolean taskIsBusy(Task task)
{
   /*
      char *desc;
      int temp;
      Boolean isBusy = False;

      // check to see if current task's busy bit is set
      desc  = (char *)arrayElementAt(&GDT, task->selector / 8);
      desc += 5;
      temp  = *desc;
      // if the descriptor says its busy... it's busy, regardless of
      // what the task state is
      if(temp & 2)
      {
      isBusy = True;
      }
      else
      {
      // otherwise check the actual task state
      switch(task->state)
      {
      case TaskStateWaiting:
      case TaskStateDead:
      isBusy = True;
      break;
      }
      }

      return isBusy;
    */
   char *desc;
   int temp;

   /* check to see if current task's busy bit is set */
   desc = (char *)arrayElementAt(&GDT, task->selector / 8);
   desc += 5;
   temp = *desc;
   if(temp & 2)
      return 1;
   else
      return 0;
}

static Task taskGetNext(Task from)
{
   return (Task)from->next;
}

static short taskGetSelector(Task from)
{
   return from->selector;
}

static ErrorCode taskGetEntry(Task task, void (**entry) (void *))
{
   *entry = task->entry;
   return ErrorNoError;
}

static ErrorCode taskGetArgument(Task task, void **argument)
{
   *argument = task->argument;
   return ErrorNoError;
}

static void taskCommonEntry(void)
{
   Task task;
   void (*entry) (void *);
   void *arg;

   taskGetCurrent(&task);
   taskGetEntry(task, &entry);
   taskGetArgument(task, &arg);

   printf("taskCommonEntry: task:0x%8x, entry:0x%8x\n", task, entry);

   entry(arg);

   printf("taskCommonEntry: %8x done\n", task);

   taskDestroy(&task);

   while(1)
   {
      // \TODO: add this when taskScheduler handles dead tasks by removing them!
      //taskForceSwitch();
      asm("hlt");
   }
}

static int switching = 0;

static void taskScheduler(void *arg)
{
   static uint32 x = 0;
   uint32 numTasks;
   Task test, previousTask;

   // use a shitty mutex here 'switching'... can't use a real
   // mutex, as it will cause task switches, which is what this
   // function takes care of! :)
   //printf("ts %d\n", x++);
   //asm("cli");
   if(switching)
   {
      //printf("==================================ns===============================\n");
      //printf("==================================ns===============================\n");
      //printf("==================================ns===============================\n");
      //asm("sti");
      return;
   }
   switching = 1;
   //asm("sti");

   /* if there's 0 or 1 task, no need to cycle -- moved below
   if(firstTask == NULL || taskGetNext(firstTask) == NULL)
   {
      switching = 0;
      printf("************************************** Not enough tasks to switch\n");
      return;
   }
   */

   numTasks = 1;
   test = firstTask;
   while(test->next)
   {
      numTasks++;
      test = (Task)test->next;
   }

   //printf("%d\n", numTasks);

   if(currentTask->state == TaskStateDead)
   {
      Task task;
      // remove the task from memory here
      // remove it from whatever list
      printf("task %s is dead, removing\n", currentTask->name);
      
      if( currentTask == firstTask )
      {
         printf("current is first, easy removal\n");
         firstTask = currentTask->next;
      }
      else
      {
         task = firstTask;
         while( task->next )
         {
            if( task->next == currentTask )
            {
               printf("found prev, removing\n");
               task->next = currentTask->next;
               break;
            }
            task = task->next;
         }
      }
      
      printf("freeing %8x %8x\n", currentTask->tss, currentTask);
      /** @TODO: free property! */
      //free( currentTask->tss );
      //free( currentTask );
   }
   
   if(firstTask == NULL || taskGetNext(firstTask) == NULL)
   {
      switching = 0;
      printf("************************************** Not enough tasks to switch\n");
      /* this would be bad if current task was just deleted! */
      return;
   }
   
   previousTask = currentTask;

   /* standard round robin scheduler */
   //printf("jmp from %x\n", currentTask->selector);
   do
   {
    getNextTask:
      currentTask = taskGetNext(currentTask);   //(Task)currentTask->next;

      if(currentTask == NULL)
         currentTask = (Task)firstTask;

      // if the tasks is in the waiting state, check to see if it
      // can become active now
      if(currentTask->state == TaskStateWaiting)
      {
         uint32 timerTicks;

         timerGetTicks(&timerTicks);

         if(timerTicks >= currentTask->timeout)
         {
            printf("************************Bringing waiting task to life %d %d\n", timerTicks,
                   currentTask->timeout);
            currentTask->state = TaskStateTimedOut;
         }
         else
         {
            goto getNextTask;
         }
      }
      //printf("-");
   }
   while(taskIsBusy(currentTask));
   //printf("+");
   //printf("%x->%d (%x->%d)", currentTask, (int)taskGetSelector(currentTask), &mainTask, (int)taskGetSelector(&mainTask));
   //if(previousTask == currentTask) printf("Same task!\n");

   if(currentTask /*== &mainTask*/ )
   {
      //TSS *tss = (TSS*)currentTask->tss;
      //printf("%x_%x:%x ", currentTask, (int)tss->cs, (int)tss->eip);
   }

   // "unlock" the mutex
   switching = 0;

   // \TODO: make this an assert!
   if((currentTask->state != TaskStateAlive) && (currentTask->state != TaskStateTimedOut))
      printf("Switching to non-active %d task %s!\n", currentTask->state, currentTask->name);
   // this is just a test... regarding my taskGate!!!
   /*
      asm("movl $0xffffffff, %%eax\n"
      "addl $0x1, %%eax\n"
      :
      :
      : "eax");
    */
   //printf("%d\n", taskGetSelector(currentTask));
   jumpToTSS(taskGetSelector(currentTask));
}
