/* ndk - [ pic.h ]
 *
 * Functions for interacting with the programmable
 * interrupt controller
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup pic pic.h: Interrupt Controller functions
 * @ingroup i386
 *
 * Contains code for interfacing with the programmable interrupt
 * controller
 */
/** @{ */

#ifndef __ndk_pic_h__
#define __ndk_pic_h__

void picInit(void);
void picEnableIRQ(unsigned short irqNum);
void picDisableIRQ(unsigned short irqNum);

#define M_PIC  0x20             /* I/O for master PIC              */
#define M_IMR  0x21             /* I/O for master IMR              */
#define S_PIC  0xA0             /* I/O for slave PIC               */
#define S_IMR  0xA1             /* I/O for slace IMR               */

#define EOI    0x20             /* EOI command                     */

#define ICW1   0x11             /* Cascade, Edge triggered         */
                        /* ICW2 is vector                  */
                        /* ICW3 is slave bitmap or number  */
#define ICW4   0x01             /* 8088 mode                       */

#define M_VEC  32               /* Vector for master               */
#define S_VEC  40               /* Vector for slave                */

#define OCW3_IRR  0x0A          /* Read IRR                        */
#define OCW3_ISR  0x0B          /* Read ISR                        */

#endif

/** @} */
