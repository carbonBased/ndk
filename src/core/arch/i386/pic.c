/* ndk - [ pic.c ]
 *
 * Contains simple routines to enable the PIC
 * and remap all IRQs to 0x20 - 0x2F.  Also
 * contians routines to enable/disable certain
 * IRQs.
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "pic.h"
#include "io.h"

unsigned short irq_mask = 0xFFFF;   // All IRQs disabled initially

/* init8259() initialises the 8259 Programmable Interrupt Controller */
void picInit(void)
{
   //message("initializing 8259 PIC...\n");

   /* Initialize the PIC and remap hardware irqs to int 32 and up.
    * In other words, irqs 0-7 (from the first PIC) will be int 32-39,
    * and irqs 8-15 (from the second PIC) will be int 40-47.
    */

   /* initialize the first (master) PIC */
   //outportb(M_PIC, ICW1);                /* ICW1: bit 0 - requires ICW4 */
   //outportb(M_PIC + 1, M_VEC);           /* the new vector for irq 0 */
   //outportb(M_PIC + 1, 0x80);            /* not sure about this (10000000b) */
   //outportb(M_PIC + 1, ICW4);            /* ICW4: 8086 mode */

   /* initialize the second (slave) PIC */
   //outportb(S_PIC, ICW1);
   //outportb(S_PIC + 1, S_VEC);           /* new vector for irq 8 */
   //outportb(S_PIC + 1, 7);               /* not sure about this... */
   //outportb(S_PIC + 1, 1);               /* or this... */

   outportb(M_PIC, 0x11);       /* start 8259 initialization */
   outportb(S_PIC, 0x11);
   outportb(M_PIC + 1, M_VEC);  /* master base interrupt vector */
   outportb(S_PIC + 1, S_VEC);  /* slave base interrupt vector */
   outportb(M_PIC + 1, 1 << 2); /* bitmask for cascade on IRQ2 */
   outportb(S_PIC + 1, 2);      /* cascade on IRQ2 */
   outportb(M_PIC + 1, 1);      /* finish 8259 initialization */
   outportb(S_PIC + 1, 1);

   outportb(M_IMR, 0xff);       /* Mask all interrupts */
   outportb(S_IMR, 0xff);

   asm("sti");
}

/* This is the old PIC initialization code that I borrowed from
 * GazOS because mine wasn't working... this code doesn't work
 * either!!!  And yet GazOS itself does... I don't get it.
 * Anyway, doesn't matter... I got my own code working, but I'll
 * leave this here, just in case I forgot something in my code
 */
  //outportb(M_PIC, ICW1);       /* Start 8259 initialization    */
  //outportb(S_PIC, ICW1);

  //outportb(M_PIC+1, M_VEC);    /* Base interrupt vector        */
  //outportb(S_PIC+1, S_VEC);

  //outportb(M_PIC+1, 1<<2);     /* Bitmask for cascade on IRQ 2 */
  //outportb(S_PIC+1, 2);        /* Cascade on IRQ 2             */

  //outportb(M_PIC+1, ICW4);     /* Finish 8259 initialization   */
  //outportb(S_PIC+1, ICW4);

  //outportb(M_IMR, 0xff);       /* Mask all interrupts          */
  //outportb(S_IMR, 0xff);

/* enables irq irq_no */
void picEnableIRQ(unsigned short irq_no)
{
   irq_mask &= ~(1 << irq_no);
   if(irq_no >= 8)
      irq_mask &= ~(1 << 2);

   outportb(M_PIC + 1, irq_mask & 0xFF);
   outportb(S_PIC + 1, (irq_mask >> 8) & 0xFF);
}

/* disables irq irq_no */
void picDisableIRQ(unsigned short irq_no)
{
   irq_mask |= (1 << irq_no);
   if((irq_mask & 0xFF00) == 0xFF00)
      irq_mask |= (1 << 2);

   outportb(M_PIC + 1, irq_mask & 0xFF);
   outportb(S_PIC + 1, (irq_mask >> 8) & 0xFF);
}
