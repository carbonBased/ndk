/* ndk - [ gdt.c ]
 *
 * Contains an "array" object, which encompases
 * the GDT.
 *
 * Please see:
 *   /src/array.c
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "gdt.h"
#include "types.h"

/* defined in start.asm */
extern uint32 gdtBase;

/* IDT @ 0x800, 256 entries, 8 bytes long */
Array GDT = { 0, 256, 8 };

void gdtInit(void)
{
   printf("gdt base address is %x\n", &gdtBase);
   GDT.baseAddress = &gdtBase;
}
