/* ndk - [ io.c ]
 *
 * Contains simple port io routines
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "io.h"

inline unsigned char inportb(unsigned int port)
{
   unsigned char ret;

   asm volatile ("inb %%dx,%%al":"=a" (ret):"d"(port));

   return ret;
}

inline void outportb(unsigned int port, unsigned char value)
{
   asm volatile ("outb %%al,%%dx"::"d" (port), "a"(value));
}

inline void outportw(unsigned int port, unsigned int value)
{
   asm volatile ("outw %%ax,%%dx"::"d" (port), "a"(value));
}
