/* ndk - [ timer.c ]
 *
 * Contains code to initialize and control
 * the x86's programmable interval timer.
 *
 * Implementation of /src/timer.h
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <timer.h>
#include <timer.i386.h>
#include <brink/list.h>
#include <pic.h>
#include <idt.h>
#include <isr.h>
#include <task.h>
#include <pmode.h>
#include <console.h>
#include <descgate.h>
#include <brink/localMutex.h>

// Port delcarations
#define TIMER0_CNTR (0x40)
#define TIMER1_CNTR (0x41)
#define TIMER2_CNTR (0x42)
#define TIMER_CTRL  (0x43)

// for use with TIMER_CTRL port
#define TIMER0      (0 << 6)
#define TIMER1      (1 << 6)
#define TIMER2      (2 << 6)

#define TIMER_LATCH (0 << 4)
#define TIMER_LSB   (1 << 4)
#define TIMER_MSB   (2 << 4)
#define TIMER_WHOLE (TIMER_LSB + TIMER_MSB)

#define TIMER_MODE0 (0 << 1)
#define TIMER_MODE1 (1 << 1)
#define TIMER_MODE2 (2 << 1)
#define TIMER_MODE3 (3 << 1)
#define TIMER_MODE4 (4 << 1)
#define TIMER_MODE5 (5 << 1)

#define TIMER_BIN16 (0)
#define TIMER_BCD   (1)

#define PIT_FREQ (0x1234DD)

struct _GlobalTimer
{
   uint32 freq;
   volatile uint32 totalTicks;  // TODO: move timerTicks to here!
   LocalMutex timerMutex;
};

struct _Timer
{
   Boolean active;
   Boolean repeat;
   uint32 timeout;
   uint32 msTillCallback;
   TimerFunction callback;
};

static struct _GlobalTimer mainTimer;
static List timers;
static ListIterator li;

// private declarations
void timerHandler(void);

//void timerScheduler(void);
void newTimerHandler(void);

ErrorCode timerInit(void)
{
   printf("timerInit: ");

   mainTimer.freq = 0;
   localMutexCreate(&mainTimer.timerMutex, MutexTypeRecursive);
   listCreate(&timers);
   listIteratorCreate(&li, timers);

   createGate(&IDT, 0x20, (long)int20h_wrapper, SEL_P0_CODE, D_TRAP);
   timerSetFreq(200);
   picEnableIRQ(0);
   printf("pass\n");
   printf("enabling ints: ");
   asm("sti");
   printf("pass\n");

   return ErrorNoError;
}

ErrorCode timerSetFreq(uint32 freq)
{
   long counter = PIT_FREQ / freq;

   mainTimer.freq = freq;

   outportb(TIMER_CTRL, /*0x43 */ TIMER0 + TIMER_WHOLE + TIMER_MODE2);
   outportb(TIMER0_CNTR, counter % 256);  // send LSB
   outportb(TIMER0_CNTR, counter / 256);  // send MSB

   return ErrorNoError;
}

ErrorCode timerGetFreq(uint32 *freq)
{
   *freq = mainTimer.freq;

   return ErrorNoError;
}

ErrorCode timerGetTicks(uint32 *ticks)
{
   *ticks = mainTimer.totalTicks;
   return ErrorNoError;
}

ErrorCode timerDelay(uint32 usec)
{
   uint32 waitFor = mainTimer.totalTicks + (usec * mainTimer.freq / 1000);
   uint32 nop;

   while(mainTimer.totalTicks < waitFor)
   {
      nop++;
   }
   return ErrorNoError;
}

ErrorCode timerCreate(Timer *t, uint32 usec, TimerFunction func, void *data)
{
   Timer timer;

   timer = (Timer)malloc(sizeof(struct _Timer));
   timer->active = False;
   timer->repeat = False;       // TODO: add as argument?
   timer->timeout = usec;
   timer->msTillCallback = usec;
   timer->callback = func;

   printf("Add to list %x\n", timer);
   listAddFront(timers, (void *)timer);
   printf("Add to list 2\n");

   *t = timer;
   return ErrorNoError;
}

ErrorCode timerStart(Timer t)
{
   t->active = True;

   return ErrorNoError;
}

ErrorCode timerEnd(Timer t)
{
   t->active = False;

   return ErrorNoError;
}

ErrorCode timerSetTimeout(Timer t, uint32 usec)
{
   t->timeout = usec;
   return ErrorNoError;
}

ErrorCode timerSetRepeat(Timer t, Boolean repeat)
{
   t->repeat = repeat;

   return ErrorNoError;
}

ErrorCode timerDestroy(Timer *t)
{
   listRemove(timers, *t);

   free(*t);
   *t = NULL;

   return ErrorNoError;
}

/* TODO: generalize this such that any timer function, at any frequency,
 * can be called through here (hense the addition of the timerCreate() functions
 * NOTE: partially done with addition of "newTimerHandler()"... needs to be
 * optimized and renamed
 */
void timerHandler(void)
{
   char *scr = (char *)0x000b8000;

   // VIDINC scr[0]++;
   mainTimer.totalTicks++;

   outportb(M_PIC, EOI);
   newTimerHandler();           // TODO: there's a better way to do this... the majority
   // of this should probably be before the EOI...
   //timerScheduler();
}

/*
void timerScheduler(void) {
  char *desc;

  // if there's 0 or 1 task, no need to cycle
  if(firstTask == NULL ||
    taskGetNext(firstTask) == NULL) {
    return;
  }

  // standard round robin scheduler
  do {
    currentTask = taskGetNext(currentTask);//(Task)currentTask->next;
    if(currentTask == NULL)
      currentTask = (Task)firstTask;
  } while(taskNotBusy(currentTask));

  /* check to see if current task's busy bit is set (just curious... */
  /*
     desc = (char *)arrayElementAt(&GDT, currentTask->selector / 8);
     desc+=5;
     if(*desc & 2) printf("BUSY!\n");
     else printf("N/B\n");
     *\/

     //printf("jmp to %x\n", currentTask->selector);
     // this is just a test... regarding my taskGate!!!
     asm("movl $0xffffffff, %%eax\n"
     "addl $0x1, %%eax\n"
     :
     :
     : "eax");
     jumpToTSS(taskGetSelector(currentTask));
     }
   */

// TODO: make this work! :)
void newTimerHandler(void)
{
   uint32 ms;
   Timer timer;

   //int x = 0;

   //printf("new timer handler\n");
   //if(mutexLock(mainTimer.timerMutex, TimeoutForever)) {
   // this is removed, for now, as timerHandler takes care of this
   // for the time being... they were actually being updated twice!
   //mainTimer.totalTicks++;

   // calculate how many ms went by in that tick
   // based on the current timer frequency:
   // NOTE: possibly loss of precision here!  Keep this in
   // mind if 1000 is not divisable by frequency, or if
   // frequency is above 1000.
   ms = 1000 / mainTimer.freq;
   listIteratorReset(li);
   while(listIteratorGetNext(li, (void **)&timer) == ErrorNoError)
   {
      //printf("tmr - %x!\n", timer);
      if(timer->active == True)
      {
         //printf("tmr + %x!\n", timer);
         if(timer->msTillCallback <= ms)
         {
            // send timer to queue (or pipe), which the callback thread
            // is waiting on, and executes the callback (outside of the
            // thead space of this actual timer interrupt!)
            //MessageQueueSend(timerQueue, timer, TimeoutForever);
            //MessagePipeSend(timerPipe, timer, TimeoutForever);
            //printf("timer callback for %8x\n", timer);
            timer->callback(NULL);  // TODO: this shouldn't be a direct callback,
            // use the queue or pipe above... prob'ly queue.  Also, what do
            // we pass as the argument?  Need a way to set this!!!
            if(timer->repeat == True)
            {
               timer->msTillCallback = timer->timeout;
            }
            else
            {
               timer->active = False;
            }
         }
         else
         {
            timer->msTillCallback -= ms;
         }
      }
   }
   //listIteratorDestroy(&li);

   // mutexUnlock(mainTimer.timerMutex);
   //}

   // signal end of interrupt (NOTE: already done for now!!!)
   //outportb (M_PIC, EOI);
}
