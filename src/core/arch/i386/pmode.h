/**
 * @file pmode.h Functions for handling Intel protected mode
 */
/**
 * @defgroup PMode pmode.h: Protected Mode Operations
 * @ingroup i386
 *
 * This file defines data types and functions that are necessary
 * for interation in protected mode.  These functions are implemented in
 * assembly language by pmode.asm
 */
/** @{ */

#ifndef __ndk_pmode_h__
#define __ndk_pmode_h__

/*! privelege level 0 (kernel) data selector */
#define SEL_P0_DATA	(0x08)
/*! privelege level 0 (kernel) code selector */
#define SEL_P0_CODE	(0x10)
/*! privelege level 0 (kernel) stack selector */
#define SEL_P0_STACK	(0x18)

/*! privelege level 3 (apps) data selector */
#define SEL_P3_DATA	(0x20)
/*! privelege level 3 (apps) code selector */
#define SEL_P3_CODE	(0x28)
/*! privelege level 3 (apps) stack selector */
#define SEL_P3_STACK	(0x30)

/**
 * Execute a far jump to a task switch segment
 *
 * @param sel the selector of the tss to jump to
 */
extern void jumpToTSS(short sel);

/**
 * Loads the task register with a new tss
 *
 * @param sel the selector of the tss to use
 */
extern void loadTaskReg(short sel);

/**
 * Invalidates a page of memory such that it is no longer
 * accessable/cached
 *
 * @param physAddr the physical address of the page to invalidate
 */
extern void invalidatePage(long physAddr);

/**
 * Set the CR0 register
 *
 * @param CR0 value to set register to
 */
extern void setCR0(long CR0);

/**
 * Set the CR2 register
 *
 * @param CR2 value to set register to
 */
extern void setCR2(long CR2);

/**
 * Set the CR3 register
 *
 * @param CR3 value to set register to
 */
extern void setCR3(long CR3);

/**
 * Get the value of the CR0 register
 *
 * @return the value of the CR0 register
 */
extern long getCR0(void);

/**
 * Get the value of the CR2 register
 *
 * @return the value of the CR2 register
 */
extern long getCR2(void);

/**
 * Get the value of the CR3 register
 *
 * @return the value of the CR3 register
 */
extern long getCR3(void);

#endif

/** @} */
