/* ndk - [ system.c ]
 *
 * Basic system routines for the kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <system.h>
#include <errorCodes.h>

ErrorCode systemAssert(uint32 lineNumber, String filename)
{
   int x = 0;
   int y = 1;

   printf("Exception %s Line %d\n", filename, lineNumber);

   // divide by zero exception
   y /= x;

   return ErrorNoError;
}

ErrorCode systemReset(void)
{
   // invalidate page 0 (the IDT)

   // force an interrupt
   asm("int $0x20");

   return ErrorNoError;
}

ErrorCode systemSleep(void)
{
   return ErrorNotImplemented;
}

ErrorCode systemResume(void)
{
   return ErrorNotImplemented;
}
