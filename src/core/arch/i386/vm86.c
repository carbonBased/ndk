/* ndk - [ vm86.c ]
 *
 * Contains code to handle vm86 tasks.
 * Currently being revamped (does not work).
 *
 * Please see:
 *   /src/drivers/video/vesa/vesa.c
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "vm86.h"

// page directory to use when calling bios ints
/*
PageDirectory *vm86PageDir;
TSS vm86TSS;

void vm86Init(void) {
  vm86CreatePagingInfo();
  vm86FillTSS(&vm86TSS, 0, 0, 0);
  createGate(&IDT, 0x81, (long)vm86IntHandler, SEL_P0_CODE, D_INT);
}

void vm86CreatePagingInfo(void) {
  PageTable *pTable;
  long i;

  // find 4kb aligned address for page directory
  vm86PageDir = (PageDirectory *)0x00092000;

  // find 4kb aligned address for 1 page table
  pTable = (PageTable *)0x00093000;

  // assign 1st page directory entry to page table
  vm86PageDir->pageTable[0] = (long)pTable | PAGE_PRESENT
                                           | PAGE_READ_WRITE
                                           | PAGE_P3_ACCESS;

  // clear the rest of the page directory
  for(i = 1; i < 1024; i++)
    vm86PageDir->pageTable[i] = 0;

  // assign the first page to the real mode idt
  pTable->pageAddr[0] = 0x00001000;

  // and map the rest of the pages to their physical
  // equivalents
  for(i = 1; i < 1024; i++) {
    pTable->pageAddr[i] = (i * 4096) | PAGE_PRESENT
                                     | PAGE_READ_WRITE
                                     | PAGE_P3_ACCESS;
  }
}

// the next two might be better placed in task.c
void vm86FillTSS(TSS *tss, long eip, long p0_esp, long p3_esp) {
  tss->ldtr = 0;
  tss->fs = tss->gs = SEL_P3_DATA | 3;
  tss->ds = tss->es = SEL_P3_DATA | 3;
  tss->ss = SEL_P3_STACK | 3;
  tss->cs = SEL_P3_CODE | 3;
  tss->eflags = 0x00023202L | 1 << 18;  // vm86, IOPL=3, STI, Alignment Check
  tss->esp = /*p0_esp;*\/p3_esp;
  tss->eip = eip;
  tss->cr3 = getCR3();

  tss->esp0 = p0_esp;
  tss->ss0 = SEL_P0_STACK;

  tss->link = 0;
}

/*
void vm86Int(long intNum, vm86Regs *regs) {
  // load registers with values in regs

  // push interrupt number onto stack

  // call real mode interrrupt interface

  // copy registers back in regs
}
*/

/*
// Call this interrupt (0x??) with the real mode int you wish to call
// pushed onto the stack (or in the top bits of eax?  real mode ints
// wont use it anyway...).  All registers are set up as if you were
// calling that real mode interrupt.
void vm86IntHandler(void) {
  // at this point:
  //   an iret returns to vm86int function, or P3 app...
  //   interrupt number is on stack (which one?) above cs:eip, eflags
  //     currentTask->p3_stack... what if the kernel called?  It doesn't
  //     have a p3 stack... probably best to use high half of a register (eax?)

  // save stack frame (where?) -- perhaps not needed

  // add to stack frame
  //   point to real mode interrupt, set vm86 = 1

  // iret

  // ....
  // control is passed to real mode int, in vm86 mode...
  // upon vm86 iret, transfer is given to gpf routine:

  // get previous stack frame (again, from where?)
  //   -- not needed... pmode return address is on the stack!

  // change stack frame to point to this, set vm86 = 0
  //   -- again, not needed?

  // iret, returns to pmode function that called int
}
*/
