/* ndk - [ mutex.c ]
 *
 * Routines for implementing mutual exclusion
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <core/mutex.h>
#include <core/task.h>
#include <core/arch/i386/pager.h>
#include <core/pageBasedList.h>
#include <core/task.h>

#define MUTEX_REC_TYPE (0x00000001)

struct _Mutex 
{
   MutexType type;
   Boolean locked;
   Task owner;
   uint32 lockDepth;
   // waiting tasks contained in the list!
   //Task waitingTasks[ (4096 - sizeof(MutexType) - sizeof(Boolean) - sizeof(Task)) / sizeof(Task) ];
};

/* create a new mutex, which is really a list with mutex data embedded into it! */
ErrorCode mutexCreate(MutexId *mutex, MutexType type)
{
   ErrorCode ec = ErrorNoError;
   PageListId listId;
   PageList list;
   
   ec = pageListCreate( &listId );
   if( ec == ErrorNoError )
   {
      ec = pageListMap( listId, &list );
      if( ec == ErrorNoError )
      {
         struct _Mutex mutexData;
         
         mutexData.locked = False;
         mutexData.lockDepth = 0;
         mutexData.owner = NULL;
         mutexData.type = type;
         
         ec = pageListEmbedPrivateData( list, &mutexData, sizeof( mutexData ) );
         
         pageListUnmap( list );
      }
   }
   
   *mutex = (MutexId)listId;
   
   return ec;
}

ErrorCode mutexMap(MutexId mutexId, Mutex *mutex)
{
   return pageListMap( (PageListId)mutexId, (PageList*)mutex );
}

ErrorCode mutexUnmap(Mutex mutex)
{
   return pageListUnmap((PageList)mutex );
}

/* 
 * because lock may add to the list, therefore potentially causes a remapping, we need 
 * to pass in a pointer to the list
 */
ErrorCode mutexLock(Mutex *mutexPtr, Timeout timeout)
{
   Task task;
   Mutex mutex;
   ErrorCode err = ErrorNoError;
   
   pageListGetPrivateData( (PageList)(*mutexPtr), (Pointer*)&mutex );

   taskGetCurrent(&task);
   
   printf("mutexLock(0x%8x,%d) privdata=0x%8x currentTask=0x%8x\n",
      (*mutexPtr), timeout, mutex, task );
   if(mutex->locked == True)
   {
      TaskState state;
      
      if( ((mutex->type & MUTEX_REC_TYPE ) == MutexTypeRecursive) &&
           (mutex->owner == task ) )
      {
         printf("recursiveLock\n");
         mutex->lockDepth++;
      }
      else
      {
         printf("not owner of lock, sleeping\n");
         //listAddBack(mutex->waitingTasks, task);
         pageListAdd( (PageList*)mutexPtr, task );
   
         /*
          * this will tell the task to wait the specified timeout...
          * if the other thread unlocks the mutex, and this task is
          * waiting on it, it will set the task into an Alive state,
          * otherwise, we'll enter the TimedOut state
          */
         taskSetTimeout(task, timeout);
         taskForceSwitch();
         taskGetState(task, &state);
         //listRemove(mutex->waitingTasks, task);
         pageListRemove( (PageList*)mutexPtr, task );
         switch (state)
         {
         case TaskStateTimedOut:
            taskSetState(task, TaskStateAlive);
            err = ErrorTimedOut;
            break;
         case TaskStateAlive:
            mutex->owner = task;
            mutex->locked = True;
            break;
         }
      }
   }
   else
   {
      printf("Locking unlocked mutex!\n");
      mutex->owner = task;
      mutex->locked = True;
   }

   return err;
   
}

ErrorCode mutexUnlock(Mutex *mutexPtr)
{
   Task task;
   uint32 length;
   Mutex mutex;
   ErrorCode ec = ErrorNoError;
   
   pageListGetPrivateData( (PageList)(*mutexPtr), (Pointer*)&mutex );   

   //printf("Unlocking LocalMutex %x\n", mutex);
   taskGetCurrent(&task);
   
   printf("mutexUnlock(0x%8x) privdata=0x%8x owner=0x%8x current=0x%8x\n",
      (*mutexPtr), mutex, mutex->owner, task);
   if(mutex->owner == task)
   {
      if( mutex->lockDepth )
      {
         printf("recursiveUnlock\n");
         mutex->lockDepth--;
      }
      else
      {
         // Mutex has been unlocked... check to see if someone wants it!
         //listGetLength(mutex->waitingTasks, &length);
         pageListGetLength( (PageList)(*mutexPtr), &length );
         if(length != 0)
         {
            /* TODO: implement these?
               switch(mutex->type) {
               case MutexTypeFIFO:
               case MutexTypePriority:
               }
             */
            /*
            if(listGetFirst(mutex->waitingTasks, (void **)&task) == ErrorNoError)
            {
               listRemove(mutex->waitingTasks, (void *)task);
               mutex->owner = task;
   
               taskSetState(task, TaskStateAlive);
            }
            else
            {
               printf("No one waiting on this mutex? %x\n", mutex);
               mutex->owner = NULL;
               mutex->locked = False;
            }
            */
            if( pageListRemoveFirst( (PageList)(*mutexPtr), (Pointer*)&task ) == ErrorNoError )
            {
               mutex->owner = task;
               
               taskSetState( task, TaskStateAlive );
               printf("mutex unlocked, now owned by 0x%8x\n", task );
            }
            //taskSetTimeout(task, 0);
         }
         else
         {
            printf("mutex unlocked, no waiting tasks\n");
            mutex->owner = NULL;
            mutex->locked = False;
         }
      }
   }
   else
   {
      ec = ErrorNotOwner;
   }
   
   return ec;   
}

ErrorCode mutexDestroy(Mutex *mutex)
{
}

ErrorCode mutexDestroyById(MutexId *mutex)
{
}
