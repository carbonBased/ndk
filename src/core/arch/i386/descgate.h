/* ndk - [ descgate.h ]
 *
 * Support defines and function for working with
 * descriptors and gates
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup descgate descdate.h: Descriptor/Gate functions
 * @ingroup i386
 *
 * Functions for working with Intel descriptors and gates
 */
/** @{ */

#ifndef __ndk_descgate_h__
#define __ndk_descgate_h__

#include "array.h"

typedef struct _Descriptor Descriptor;
typedef struct _Gate Gate;

void createDescriptor(Array *table, short desc_num, long base, long limit, long control);
void createGate(Array *table, short gate_num, long offset, short selector, long control);

// Types of descriptors
#define D_LDT          0x200    /* LDT segment                      */
#define D_TASK         0x500    /* Task gate                        */
#define D_TSS          0x900    /* TSS                              */
#define D_CALL         0x0C00   /* 386 call gate                    */
#define D_INT          0x0E00   /* 386 interrupt gate               */
#define D_TRAP         0x0F00   /* 386 trap gate                    */
#define D_DATA         0x1000   /* Data segment                     */
#define D_CODE         0x1800   /* Code segment                     */

// attributes for descriptors
#define D_DPL3         0x6000   /* DPL3 or mask for DPL             */
#define D_DPL2         0x4000   /* DPL2 or mask for DPL             */
#define D_DPL1         0x2000   /* DPL1 or mask for DPL             */
#define D_PRESENT      0x8000   /* Present (included by default)    */
#define D_NOT_PRESENT  0x8000   /* Not Present                      */

// more attributes for segment descriptors (not gates)
#define D_ACC          0x100    /* Accessed (Data or Code)          */
#define D_WRITE        0x200    /* Writable (Data segments only)    */
#define D_READ         0x200    /* Readable (Code segments only)    */
#define D_BUSY         0x200    /* Busy (TSS only)                  */
#define D_EXDOWN       0x400    /* Expand down (Data segments only) */
#define D_CONFORM      0x400    /* Conforming (Code segments only)  */
#define D_BIG          0x40     /* Default to 32 bit mode           */
#define D_BIG_LIM      0x80     /* Limit is in 4K units             */

struct _Descriptor
{
   unsigned short limit_low;    /* limit 0..15    */
   unsigned short base_low;     /* base  0..15    */
   unsigned char base_med;      /* base  16..23   */
   unsigned char access;        /* access byte    */
   unsigned int limit_high:4;   /* limit 16..19   */
   unsigned int granularity:4;  /* granularity    */
   unsigned char base_high;     /* base 24..31    */
} __attribute__ ((packed));

struct _Gate
{
   unsigned short offset_low;   /* offset 0..15    */
   unsigned short selector;     /* selector        */
   unsigned short access;       /* access flags    */
   unsigned short offset_high;  /* offset 16..31   */
} __attribute__ ((packed));

#endif

/** @} */
