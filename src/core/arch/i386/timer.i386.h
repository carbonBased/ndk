/* ndk - [ console.i386.h ]
 *
 * Basic console IO for kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup i386Timer Timer.i386.h: Timer functions
 * @ingroup i386
 *
 * This file defines an interface for working with timers
 */
/** @{ */

#ifndef __ndk_timer_i386_h__
#define __ndk_timer_i386_h__

/**
 * Initialize the timer hardware
 */
ErrorCode timerInit(void);

#endif

/** @} */
