/* ndk - [ pager.c ]
 *
 * Contains code to initialize the pager, and
 * to convert to and from linear and physical
 * address spaces.
 *
 * Please see:
 *   /src/memory.c
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */
 
/**
 * There is an inherit problem here... functions like pagerMap() rely on 
 * _pagerFindContiguousPages() to return a base linear address which will then have 
 * physical pages mapped to it.  However, when mapping those pages, it might be necessary 
 * to create a new page directory -- this will then allocate a new page to the next 
 * available linear address in the kernel heap (ie, where the pagerMap() is probably 
 * about to also map)... UNTRUE, I think... maps to kernel space, user maps to shared
 */

#include "pager.h"

#include <propDb.h>
#include <stack.h>
#include <multiboot.h>
#include <propDb.h>
#include <klibc/assert.h>

#undef USED_STACK

#if Property_trace_pager_verbose == 1
#define VPRINTF(x) printf x 
#else
#define VPRINTF(x)
#endif

// see ndk/docs/memoryStacks.kwd for more details
typedef struct
{
   uint32 total;
   Stack avail;
#ifdef USED_STACK
   Stack used;
#endif
} PageStack;

PageDirectory *sysPageDir;
PageStack pageStack;
uint32 totalMemory, totalPages;

PagerHeap pagerHeapKernel, pagerHeapApps, pagerHeapShared;
/** 
 * @TODO this 0xC0800000 is temporary... is used to ensure mallocs (sbrk's) and pager 
 * allocations in the kernel don't conflict while the kernel continues to use both 
 * the pager and the byte allocator!
 */
struct _PagerHeap pagerHeapKernelData = { (Pointer)0xC0800000, 1024*1024*1, PagerHeapExpandUp };   // 1gb
struct _PagerHeap pagerHeapAppsData   = { (Pointer)0x00000000, 1024*1024*3, PagerHeapExpandUp };   // 3gb
struct _PagerHeap pagerHeapSharedData = { (Pointer)0xBFFFF000, 1024*1024*3, PagerHeapExpandDown }; // 3gb down

// private functions...
static ErrorCode _pagerAllocPages( PagerHeap heap, uint32 numPages, Pointer base );
static ErrorCode _pagerFindContiguousPages( PagerHeap heap, uint32 numPages, Pointer *base );


// fix up these private functions...
void pagerCreatePageStack(void);
Boolean pagerTableExists(unsigned long tableNum);
void pagerCreateTable(unsigned long tableNum);
long pagerMapPage(unsigned long physAddr);   // TODO: remove, merge into new API
void *pagerLinToPhys(void *linearAddr);   // TODO: remove, merge into new API

/* these two are defined in start.asm... perhaps not the most perfect spot
 * to put them logically, but it makes sense for them to be here for
 * space conservation purposes (4kb alignment, etc)
 */
extern uint32 pageDirectoryBase;
extern uint32 pageTableBase;
extern uint32 pageTableConventionalMemBase;

ErrorCode pagerInit(void)
{
   PageTable *pTable;
   //PageTable *pTable1mb;
   Pointer kbase, mbase;
   uint32 ksize, msize;
   uint32 i, addr;
   uint32 pagesToAllocate;
   PhysicalAddress dummyPage;
   uint32 temp;

   VPRINTF(("pager init\n"));
   
   //pagerHeapKernelData.base = (Pointer)0xC0000000;
   //pagerHeapKernelData.numPages = 1024*1024;
   pagerHeapKernel = &pagerHeapKernelData;
   pagerHeapApps = &pagerHeapAppsData;
   pagerHeapShared = &pagerHeapSharedData;

   // find 4kb aligned address of page directory
   sysPageDir = (PageDirectory *)&pageDirectoryBase;

   /**
    * most of the pager initialization has been done inside start.asm
    * where the page directory, and page tables are setup to map the kernel
    * into the virtual address space at kernel_base_linear
    */

   /** 
    * @todo: unmap the memory between 1MB and 4MB (ie, the physical 
    * allocation of the kernel... it should always be accessed at the 
    * new virtual location!
    */

   VPRINTF(("CR0: 0x%x\n", getCR0()));
   VPRINTF(("CR3: 0x%x\n", getCR3()));

   pagerCreatePageStack();

   memoryGetKernelBase(&kbase);
   memoryGetKernelSize(&ksize);
   memoryGetModulesBase(&mbase);
   memoryGetModulesSize(&msize);
   VPRINTF(("kernel base %x size %d\n", (long)kbase, (long)ksize));
   VPRINTF(("modules base %x size %d\n", (long)mbase, (long)msize));

   return ErrorNoError;
}

/**
 * pagerLock() and pagerUnlock() can be used to make a group of 
 * pager methods perform atomicly (all pager functions will 
 * attempt to grab this same mutex)
 */
ErrorCode pagerLock(void)
{
   // mutexLock( pagerMutex, TimeoutInfinite );
   
   return ErrorNoError;
}

ErrorCode pagerUnlock(void)
{
   // mutexUnlock( pagerMutex );
   
   return ErrorNoError;
}

ErrorCode pagerInvalidate(void)
{
   VPRINTF(("Resetting paging hardware\n"));
   
   setCR3( getCR3() );
   
   return ErrorNoError;
}

ErrorCode pagerGetPageSize(uint32 *page)
{
   *page = 4096;
   return ErrorNoError;
}

ErrorCode pagerGetNewPhysical(PhysicalAddress * addr)
{
   if(pageStack.avail.pointer == 0)
   {
      VPRINTF(("Not enough physical memory!\n"));
      return ErrorOutOfMemory;
   }

   // place the physical address onto the used stack, and
   // return it
   *addr = (PhysicalAddress) stackPop(&pageStack.avail);
#ifdef USED_STACK   
   stackPush(&pageStack.used, (long)*addr);
#endif

   return ErrorNoError;
}

// allocates a page anywhere in linear space... useful?  I dunno...
// I have doubts that this really works, and whether it should even be
// implemented... it's basically a really crude malloc...
/*
ErrorCode pagerGetNewLinear(LinearAddress * addr)
{
   PhysicalAddress pAddr;

   printf("pagerGetNewLinear()\n");

   // TODO: cleanup!  pagerMapPage is an old API as well..
   if(pagerGetNewPhysical(&pAddr) == ErrorNoError)
   {
      printf("Got physical %x\n", pAddr);
      *addr = (LinearAddress) pagerMapPage((uint32)pAddr);
      printf("Got linear %x\n", *addr);
      return ErrorNoError;
   }
   else
      return ErrorNotAvailable;
}
*/

// TODO: error check if page exists!
ErrorCode pagerMapPhysicalToLinear(PhysicalAddress physicalAddr, LinearAddress linearAddr)
{
   uint32 *pageMap = (uint32 *)0xffc00000;
   uint32 allocPageOffset = ((uint32)linearAddr / 4096);
   uint32 pointer, value;
   
   invalidatePage( (uint32)linearAddr & 0xfffff000 );

   VPRINTF(("pagerMapPhysicalToLinear %x -> %x\n", (long)physicalAddr, (long)linearAddr));
   pointer = allocPageOffset;

   // make sure to create a page table if it doesn't exist!!!
   if(/*pointer % 1024 == 0 && */ pagerTableExists(pointer / 1024) == False)
   {
      VPRINTF(("pagerMapPhysicalToLinear -> create new table\n"));
      pagerCreateTable(pointer / 1024);
   }

   // put physical page into linear space
   value = (long)physicalAddr | PAGE_PRESENT | PAGE_READ_WRITE | PAGE_P3_ACCESS;
   VPRINTF(("pageMap %x tableNum %d, index %d, value 0x%8x\n", pageMap, pointer / 1024, pointer, value));
   pageMap[pointer] = value;

   invalidatePage( (uint32)linearAddr & 0xfffff000 );
   //pagerInvalidate();
   
   //printf("read back %x\n", pageMap[pointer]);
   
   //physAddr = (PhysicalAddress)(pointer*4096);
   return ErrorNoError;
}

ErrorCode pagerUnmap(LinearAddress lin)
{
   ErrorCode err = ErrorNoError;
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 index;

   index = (uint32)lin / 4096;
   
   if( pages[ index ] != 0 )
   {
      pages[ index ] = 0;
      invalidatePage( index * 4096 );
      //invalidatePage( (PointerType)(pages + index) & 0xfffff000 ); // invalidate the translation page too?
   }
   else
   {
      err = ErrorBadParam;
   }
   
   return err;
}

ErrorCode pagerUnmapRange( LinearAddress lin, uint32 numPages)
{
   while( numPages-- )
   {
      pagerUnmap( lin );
      lin = (LinearAddress)( (uint32)lin + 4096 );
   }
}


ErrorCode pagerGetPhysical(LinearAddress lin, PhysicalAddress * addr)
{
   ErrorCode ec = ErrorNoError;
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 linAddr = (uint32)lin;
   uint32 physAddr;
   uint32 bottom4k;
   uint32 pageTableNum;
   uint32 pageIndex;

   // save the bottom 4k, and remove it from the linear address
   bottom4k = linAddr & 4095;
   linAddr -= bottom4k;

   // debugging info...
   VPRINTF(("page aligned linAddr: 0x%8x\n", linAddr));

   // calculate which page table to look into
   pageTableNum = (unsigned long)linAddr / (4096 * 1024);
   //printf("table %d, index %d\n", pageTableNum, pageTableIndex);
   // Check if this page table exists!
   if(!pagerTableExists(pageTableNum))
   {
      VPRINTF(("Page table doesn't exist for linear address 0x%8x\n", linAddr));
      ec = ErrorNotFound;
   }
   else
   {
      // calculate the offset into the page tables, and get our physical address
      pageIndex = (unsigned long)linAddr >> 12;
      physAddr = pages[(unsigned long)pageIndex] & 0xfffff000;
      
      if( physAddr )
      {
         // add the bottom 4k
         physAddr += bottom4k;
      
         *addr = (PhysicalAddress)physAddr;
      }
      else
      {
         ec = ErrorNotFound;
      }
   }
   
   return ec;
}

ErrorCode pagerGetLinear(PhysicalAddress phys, LinearAddress * addr)
{
   // TODO: implement?
   return ErrorNotImplemented;
}

/**
 * Allocate a contiguous block of pages
 */
ErrorCode pagerAllocate( PagerHeap heap, uint32 numPages, Pointer *base )
{
   ErrorCode err = ErrorNoError;
   
   if( _pagerFindContiguousPages( heap, numPages, base ) == ErrorNoError )
   {
      err = _pagerAllocPages( heap, numPages, *base );
   }
   else
   {
      err = ErrorOutOfMemory;
   }
}

/**
 * Reserve a contiguous block, but do not assign it new pages yet
 * @TODO: this function doesn't work properly with expand down stacks!
 */
ErrorCode pagerReserve( PagerHeap heap, uint32 numPages, Pointer *base )
{
   ErrorCode err = ErrorNoError;
   
   if( _pagerFindContiguousPages( heap, numPages, base ) == ErrorNoError )
   {
      uint32 linearAddr = (uint32)(*base);
      uint32 *pageMap = (uint32 *)0xffc00000;
      uint32 *pageDirectory = (uint32 *)0xfffff000;
      uint32 allocPageOffset = (linearAddr / 4096);
      uint32 currentPageTable = linearAddr / (4096 * 1024);
      uint32 pointer, nextPageTablePointer;
   
      pointer = allocPageOffset;
      nextPageTablePointer = pointer;
      
      VPRINTF(("_pagerFindContiguousPages returned 0x%x pageTable %d\n",
         *base, currentPageTable));
   
      /**
       * shouldn't need to take care of page tables... _pagerFindContiguousPages 
       * should've taken care of that (NOT TRUE!)
       * @TODO: take care of page tables!
       */
      while( numPages-- )
      {
         if( pointer == nextPageTablePointer )
         {
            VPRINTF(("pagerReserve, checking page table %d: ", currentPageTable));
            if( pageDirectory[ currentPageTable ] == 0 )
            {
               VPRINTF(("creating\n"));
               /* create new page table */
               pagerCreateTable( currentPageTable );
            }
            else
            {
               VPRINTF(("exists\n"));
            }
            currentPageTable++;
            nextPageTablePointer = currentPageTable * 1024;
         }
         
         /*
          * just set one bit so that page is considered in use, despite not actually
          * pointing to a valid physical page (err... well... it'll end up pointing 
          * to 0x0, but should only be for a short period of time)
          */
         pageMap[pointer++] = PAGE_PRESENT;
      }
   }
   else
   {
      err = ErrorOutOfMemory;
   }
   
   return err;
}

ErrorCode pagerMap( PagerHeap heap, PhysicalAddress phys, Pointer *base )
{
   ErrorCode ec = ErrorOutOfMemory;
   
   if( _pagerFindContiguousPages( heap, 1, base ) == ErrorNoError )
   {
      ec = pagerMapPhysicalToLinear( phys, (LinearAddress)*base );
   }
   
   return ec;
}

ErrorCode pagerMapRange( PagerHeap heap, uint32 numPages, PhysicalAddress phys[], Pointer *base )
{
   ErrorCode ec = ErrorOutOfMemory;
   uint32 currentPos;
   uint32 i;
   
   if( _pagerFindContiguousPages( heap, numPages, base ) == ErrorNoError )
   {
      currentPos = (uint32)*base;
      for( i = 0; i < numPages; i++ )
      {
         ec = pagerMapPhysicalToLinear( phys[i], (LinearAddress)currentPos );
         currentPos += 4096;
         
         /* what to do if this fails? */
         assert( ec == ErrorNoError );
      }
   }
   
   return ec;
}

/** heap required here? I believe so... _pagerFindContiguousPages will return base at top of an 
 *  expand down heap, so _pagerAllocPages must allocate the correct way
 */
static ErrorCode _pagerAllocPages( PagerHeap heap, uint32 numPages, Pointer base )
{
   ErrorCode err = ErrorNoError;
   PhysicalAddress page;
   LinearAddress linearPage = base;
   int32 inc = (heap->direction == PagerHeapExpandUp) ? 4096 : -4096;
   
   while( (err == ErrorNoError) && (numPages--) )
   {
      if( pagerGetNewPhysical( &page ) == ErrorNoError )
      {
         err = pagerMapPhysicalToLinear( page, linearPage );
         linearPage += inc;
      }
   }
   
   return err;
}

/** 
 * @TODO: test lines JW: - added to support expand down heaps 
 */
static ErrorCode _pagerFindContiguousPages( PagerHeap heap, uint32 numPages, Pointer *base )
{
   ErrorCode err = ErrorNoError;
   uint32 *const InvalidBase = (uint32*)1;  /* since we're allocating pages, 
                                                a pointer of 1 isn't possible */
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 *pgDir = (uint32 *)0xfffff000;
   uint32 heapBase = (uint32)(heap->base);
   uint32 currentPageTable;
   uint32 currentIndex;
   uint32 nextPageTableIndex;
   uint32 lastIndex;
   uint32 *nextPageTableBase;
   uint32 *currentAddr;
   uint32 contiguousPages;
   uint32 *contiguousBase = InvalidBase;
   int32 inc, dirInc;
   
   /* page dir exists... */
   currentPageTable = heapBase / (4096 * 1024);
   currentIndex = heapBase / 4096;
   /* prime the pump by setting nextPageTableIndex to the current pageTableIndex */
   //nextPageTableIndex = currentPageTable * 1024; 
   nextPageTableIndex = currentIndex;  /* NOTE: can only do this if heap starts on a page */
   contiguousPages = 0;
   
   /** @TODO properly handle expand down stacks */
   if( heap->direction == PagerHeapExpandUp )
   {
      inc = 1;
      dirInc = 1024;
      lastIndex = currentIndex + heap->numPages;
      nextPageTableBase = (uint32*)( (currentPageTable + 1) * (4096 * 1024) );
   }
   else
   {
      inc = -1;
      dirInc = -1024;
      lastIndex = currentIndex - heap->numPages;
      nextPageTableBase = (uint32*)( (currentPageTable - 1) * (4096 * 1024) );
   }
   
   
   VPRINTF(("_pagerFindContiguousPages %d\n", numPages));

   while( (contiguousPages < numPages) && currentIndex != lastIndex )
   {
      /* if we've hit a new page table, check that it exists before indexing into it */
      /** NOTE: this doesn't work for expand down stacks without the first table mapped! */
      if( currentIndex == nextPageTableIndex )
      {
         // JW: nextPageTableIndex += 1024;
         nextPageTableIndex += dirInc;
         /* 
          * if the page table doesn't exit, it means we could map it in 
          * for 1024 contiguous pages
          */
         if( pgDir[ currentPageTable ] == 0 )
         {
            VPRINTF(("PageTable %d doesn't exist\n", currentPageTable));
            
            /* set base for expand up stacks */
            if( contiguousBase == InvalidBase )
            {
               contiguousBase = (uint32*)(currentIndex * 4096);
            }
            // JW: contiguousPages += 1024;
            // JW: currentIndex += 1024;
            // JW: currentPageTable++;
            contiguousPages += 1024;
            currentIndex += dirInc;
            currentPageTable += inc;

            /* always set base for expand down stacks... */
            if( heap->direction == PagerHeapExpandDown )
            {
               contiguousBase = (uint32*)(currentIndex * 4096);
            }
            continue;
         }

         //JW: currentPageTable++;
         currentPageTable += inc;
      }
      
      /* if this page exists, we've lost contigous blocks */
      //printf("*** pages %x currentIndex %x\n", pages, currentIndex);
      if( pages[ currentIndex ] )
      {
         contiguousBase = InvalidBase;
         contiguousPages = 0;
      }
      else
      {
         //JW: contiguousPages++;
         contiguousPages ++;
         if( contiguousBase == InvalidBase || heap->direction == PagerHeapExpandDown )
         {
            contiguousBase = (uint32*)(currentIndex * 4096);
         }
      }
   
      // JW: currentIndex++;
      currentIndex += inc;
   }
   
   if( contiguousBase != InvalidBase )
   {
      /* because of the way page tables are handled above, it's possible that we 
       * allocated too much space if using an expand down stack... adjust accordingly
       * (ie, move buffer up in memory so that it doesn't create fragmentation)
       */
      if( ( contiguousPages > numPages ) &&
           ( heap->direction == PagerHeapExpandDown ) )
      {
         contiguousBase = (uint32*)(
            (uint32)contiguousBase + (contiguousPages - numPages + 1) * PAGE_SIZE 
            );
      }
      *base = (Pointer)contiguousBase;
      err = ErrorNoError;
   }
   else
   {
      err = ErrorNotFound;
   }
   
   VPRINTF(("_pagerFindContiguousPages err=0x%x\n", err));
   
   return err;
}

ErrorCode pagerFree( PagerHeap heap, uint32 numPages, Pointer base )
{
   ErrorCode err = ErrorNoError;
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 allocBase = (uint32)base;
   uint32 currentIndex;
   uint32 phys;

   currentIndex = allocBase / 4096;
   
   /* for now, assume all pages truely are mapped (an exception will be thrown otherwise) */
   while( numPages-- )
   {
      if( pages[ currentIndex ] != 0 )
      {
         phys = ( pages[ currentIndex ] & 0xfffff000 );
         pages[ currentIndex ] = 0;
         invalidatePage( currentIndex * 4096 );
         
         /* be sure to put page back on the stack */
         // place the physical address onto the used stack, and
         // return it
         /**
          * @TODO @NOTE: for now, we just pop any page off the used stack... this means
          * the used stack will not be accurate.  We place the correct page on the available 
          * stack, however (do we even need to track the used pages?)
          */
#ifdef USED_STACK          
         stackPop(&pageStack.used);
#endif
         stackPush(&pageStack.avail, (long)phys );         
      }
      else
      {
         err = ErrorNotFound;
      } 
      currentIndex++;
   }
   
   return err;
}

ErrorCode pagerFreePhysical( PhysicalAddress addr )
{
   ErrorCode ec = ErrorNoError;

#ifdef USED_STACK   
   stackPop( &pageStack.used );
#endif
   stackPush( &pageStack.avail, (long)addr );
   
   return ec;
}

/**
 * This will create a fresh new page directory with nothing actually
 * mapped into it except the kernel, at 3GB
 */
ErrorCode pagerCreateNew(PhysicalAddress *addr)
{
   // allocate a physical page, map it somewhere
   // copy current page directory into this page
   // return the page
}

// PRIVATE...

void pagerCreatePageStack(void)
{
   module_t *mod;
   uint32 topOfKernelPhysical, topOfKernelLinear;
   uint32 i, c = 0;
   uint32 lastPageOfStack;
   uint32 pagesPerStack;

   /** multiboot should be in the first 1mb... which is identity mapped */
   VPRINTF(("multiboot info %x\n", multibootInfo));
   assert( (uint32)multibootInfo < (uint32)(1024*1024) );

   VPRINTF(("Usable memory: [Lower: %dkb, Upper %dkb]\n", multibootInfo->mem_lower,
          multibootInfo->mem_upper));

   // don't use mem_lower to calculate total memory... it'll return
   // 640k (ie, it doesn't include the bios).  Use 1024k (ie, 1MB)
   totalMemory = (multibootInfo->mem_upper + 1024) * 1024;
   totalPages = totalMemory / PAGE_SIZE;
   
   // find out how many pages will be required for the page stack (round up)
   pagesPerStack = totalPages * sizeof(Pointer);
   pagesPerStack += PAGE_SIZE - (pagesPerStack & PAGE_SIZE_MASK);
   pagesPerStack /= PAGE_SIZE;
   
   VPRINTF(("pagesPerStack %d\n", pagesPerStack ));

   // kernel is currently always fixed @ 1MB mark, but could be any length
   // Use bssEnd to calculate total space used by kernel, remember to round
   // kernelEnd _UP_ to the nearest page.
   // TODO: UNUSED: move to memory.c
   //kernelStart = (1024 * 1024) / PAGE_SIZE;
   //kernelEnd = ((long)&bssEnd + PAGE_SIZE - ((long)&bssEnd & (PAGE_SIZE-1))) / PAGE_SIZE;

   // get the end address of the last module (since its from GRUB, it'll be physical)
   mod = (module_t *) ( multibootInfo->mods_addr);
   topOfKernelPhysical = (long)mod[multibootInfo->mods_count - 1].mod_end;
   
   VPRINTF(("end of modules 0x%8x\n", topOfKernelPhysical));

   // make sure it's at least dword aligned (rounding up!)... page align, actually
   //topOfKernelPhysical += 4 - (tokPhys & 3);
   topOfKernelPhysical += PAGE_SIZE - (topOfKernelPhysical & PAGE_SIZE_MASK);
   topOfKernelLinear = topOfKernelPhysical
                     - propertyGetAsInteger(kernel_base_physical) 
                     + propertyGetAsInteger(kernel_base_linear);
   VPRINTF(("top of kernel, physical: 0x%8x, linear: 0x%8x\n", 
      topOfKernelPhysical, topOfKernelLinear));

   /** TODO: since the alloc base is at 3gb now, there will be issues when available memory 
    * is > 1GB (overflow).  The kernel needs to be capped off at 1GB memory usage
    */
   VPRINTF(("Creating page stack at: 0x%8x\n", topOfKernelLinear));
   /* the kernel, and page stack itself cannot be deallocated, so remove them from total pages */
   pageStack.total = totalPages - pagesPerStack - (topOfKernelPhysical / PAGE_SIZE);
   pageStack.avail.flags = STACK_EXPAND_DOWN;
   pageStack.avail.pointer = 0; //dmaStack.total - 1;
   pageStack.avail.base = topOfKernelLinear;
#ifdef USED_STACK
   pageStack.used.flags = STACK_EXPAND_UP;
   pageStack.used.base = (tokLin + 4 * totalPages);
   pageStack.used.pointer = 0;
#endif

   VPRINTF(("pageStack.total = %d\n", pageStack.total ));

   /* Add all memory to the page stack except the first MB (conventional
    * memory -> to be maintained by ndk itself) and the memory that's
    * already allocated by the kernel and the page stack
    */
   //lastPageOfStack = (pageStack.used.base / 4096) + 2;   // TODO: NOTE: off by one?
   lastPageOfStack = (topOfKernelPhysical / 4096) + pagesPerStack;
   VPRINTF(("lastPageOfStack %x pageStack.total %d\n", lastPageOfStack, pageStack.total));
   for(i = pageStack.total; i > lastPageOfStack; i--)
   //for(i = lastPageOfStack+1; i <= pageStack.total; i++)
   {
      //if(!pagerPageUsedByKernel(i))
      //printf("pushing page %x onto stack at %d\n", i * PAGE_SIZE, c);
      stackPush(&pageStack.avail, i * PAGE_SIZE);
      c++;
      
      /** 
       * @TODO: if a system has 4GB of RAM, a new page directory may need to be 
       * mapped into place in order to fill the entire page stack
       */ 
   }

/*
   while(1)
   {
      printf("fault, pausing\n");
      asm("stop: \n hlt \n jmp stop\n");
   }
//*/
}

Boolean pagerTableExists(unsigned long tableNum)
{
   uint32 *pgDir = (uint32 *)0xfffff000;

   return pgDir[(uint32)tableNum] == 0 ? False : True;
}

void pagerCreateTable(unsigned long tableNum)
{
   uint32 *pageDirectory = (uint32 *)0xfffff000;
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 *pageTablePhysical;
   uint32 i;

   VPRINTF(("pager: creating new table #%d\n", tableNum));
   // get a page from the kernel ( attempting to map potentially causes recursion! )
   //pageTable = (long *)memoryAllocUnmappedPage();
   pagerGetNewPhysical( (PhysicalAddress *)&pageTablePhysical );
   //pagerAllocate( pagerHeapKernel, 1, (LinearAddress*)&pageTableLinear );
   VPRINTF(("received page phys 0x%8x\n", pageTablePhysical));

   /* clear the page, and unmap it... */   
   //for(i = 0; i < 1024; i++) pageTableLinear[i] = 0;
   //pagerGetPhysical( pageTableLinear, (void*)&pageTablePhysical );
   //pagerUnmap( pageTableLinear );

   VPRINTF(("Adding to directory\n"));
   // and assign it to our page directory (must do before we write to it)
   pageDirectory[tableNum] = (long)pageTablePhysical | PAGE_PRESENT | PAGE_READ_WRITE | PAGE_P3_ACCESS;

   // zero it out
   VPRINTF(("And clearing\n"));
   for(i = 0; i < 1024; i++)
   {
      uint32 index = tableNum * 1024 + i; 
      pages[index] = 0x0;  //pageTable[i] = 0;
      //invalidatePage(index*4096); 
   }
   //invalidatePage( pageDirectory );
   //invalidatePage( (uint32)pages + tableNum * 4096 );
}

long pagerMapPage(unsigned long physAddr)
{
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 allocPageOffset = (4 * 1024 * 1024) / 4096;
   uint32 pointer;

   // simple way to find available slot
   pointer = allocPageOffset;
   while(pages[pointer++] != 0);

   // put physical page into linear space
   pages[pointer] = physAddr | PAGE_PRESENT | PAGE_READ_WRITE | PAGE_P3_ACCESS;
   return pointer * 4096;       //1024;
}

/*
void *pagerLinToPhys(void *linearAddr)
{
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 linAddr = (uint32)linearAddr;
   uint32 physAddr;
   uint32 bottom4k;
   uint32 pageTableNum;
   uint32 pageIndex;

   // save the bottom 4k, and remove it from the linear address
   bottom4k = linAddr & 4095;
   linAddr -= bottom4k;

   // debugging info...
   printf("page aligned linAddr: 0x%8x\n", linAddr);

   // calculate which page table to look into
   pageTableNum = (unsigned long)linAddr / (4096 * 1024);
   //printf("table %d, index %d\n", pageTableNum, pageTableIndex);
   // Check if this page table exists!
   if(!pagerTableExists(pageTableNum))
   {
      printf("Page table doesn't exist for linear address 0x%8x\n", linAddr);
      return 0;
   }

   // calculate the offset into the page tables, and get our physical address
   pageIndex = (unsigned long)linAddr >> 12;
   physAddr = pages[(unsigned long)pageIndex] & 0xfffff000;

   // add the bottom 4k
   physAddr += bottom4k;

   return (void *)physAddr;
}
*/

ErrorCode pagerDumpStats(void)
{
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 *pgDir = (uint32 *)0xfffff000;
   uint32 i;
   
#if 0
   printf("Page directory:\n");
   for(i = 0; i < 1024; i++)
   {
      if( ((i+1) % 8 ) == 0 ) 
      {
         printf("%8x\n", pgDir[i]);
      }
      else
      {
         printf("%8x  ", pgDir[i]);
      }
   }
   
   printf("Page tables:\n");
   for(i = 0; i < 1024; i++)
   {
      if( pgDir[i] )
      {
         uint32 pagesIndex = i * 1024, c;
         
         printf("0x%8x:\n", pgDir[i]);
         for( c = 0; c < 1024; c++ )
         {
            
            if( ((c+1) % 8 ) == 0 ) 
            {
               printf("%8x\n", pages[pagesIndex + c]);
            }
            else
            {
               printf("%8x  ", pages[pagesIndex + c]);
            }
         }
      }
   }
#endif

   printf("Duplicate page maps:\n");
   printf("physicalAddr: linearAddr(access) ...\n");
   for(i = 0; i < 1024; i++)
   {
      if( pgDir[i] )
      {
         uint32 pagesIndex = i * 1024, c;
         
         //printf("0x%8x:\n", pgDir[i]);
         for( c = 0; c < 1024; c++ )
         {
            if( pages[pagesIndex + c] )
            {
               printf("0x%8x: ", pages[pagesIndex + c]);
               _pagerShowMappingsFor( pages[pagesIndex + c] & 0xfffff000 );
               printf("\n");
            }
         }
      }
   }

   
   /*
   printf("pageStack.total %d\n", pageStack.total);
   printf("pageStack.avail.flags %d\n", pageStack.avail.flags );
   printf("pageStack.avail.pointer %d\n", pageStack.avail.pointer ); 
   printf("pageStack.avail.base 0x%x\n", pageStack.avail.base );
#ifdef USED_STACK 
   printf("pageStack.used.flags %d\n", pageStack.used.flags );
   printf("pageStack.used.base 0x%x\n", pageStack.used.base );
   printf("pageStack.used.pointer %d\n", pageStack.used.pointer );
   
   printf("Used stack:\n");
   stackDisplay( &pageStack.used );
#endif
   
   printf("Avail stack:\n");
   stackDisplay( &pageStack.avail );
   */
   
   return ErrorNoError;
}

void _pagerShowMappingsFor( uint32 addr )
{
   uint32 *pages = (uint32 *)0xffc00000;
   uint32 *pgDir = (uint32 *)0xfffff000;
   uint32 i, c;

   for(i = 0; i < 1024; i++)
   {
      if( pgDir[i] )
      {
         uint32 pagesIndex = i * 1024, c;
         
         //printf("0x%8x:\n", pgDir[i]);
         for( c = 0; c < 1024; c++ )
         {
            
            if( ( pages[pagesIndex + c] & 0xfffff000 ) == addr )
            {
               printf( "0x%8x(%x) ", i*4096*1024 + c*4096, (pages[pagesIndex + c] & 0x00000fff) );
            }
         }
      }
   }
}
