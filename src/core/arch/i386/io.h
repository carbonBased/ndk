/* ndk - [ io.h ]
 *
 * Basic port IO functions
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup io io.h: IDT functions
* @ingroup i386
*
 * functions for writting to io ports
*/
/** @{ */

#ifndef __ndk_io_h__
#define __ndk_io_h__

// TODO: move to "io*" style functions?

unsigned char inportb(unsigned int port);
void outportb(unsigned int port, unsigned char value);
void outportw(unsigned int port, unsigned int value);

#endif

/** @} */
