/* ndk - [ idt.c ]
 *
 * Contains code to initialize the idt, and the C
 * part of the general purpose exception handlers.
 *
 * Please see:
 *   /src/array.c
 *   /src/isr.asm
 *
 * (c)2002-2008 dcipher / neuraldk
 *                www.neuraldk.org
 */

#include "idt.h"
#include <pmode.h>
#include <task.h>

/* defined in start.asm */
extern uint32 idtBase;

/* IDT @ idtBase, 256 entries, 8 bytes long */
Array IDT = { 0, 256, 8 };

char *exceptionNames[19] = {
   "Divide by Zero",
   "Debug",
   "NMI",
   "Breakpoint",
   "Overflow",
   "Out of Bounds",
   "Invalid Opcode",
   "FPU Not Found",
   "Double-Fault",
   "CoProcessor Segment Overrun",
   "Invalid TSS",
   "Segment Not Present",
   "Stack",
   "General Protection",
   "Page Fault",
   "Reserved",
   "FPU Error",
   "Alignment Check",
   "Machine Check"
};

int32 currentInterrupt = -1;
Task keyboardTask = NULL;

void idtInit(void)
{
   printf("idt base address is %x\n", &idtBase);
   IDT.baseAddress = &idtBase;
   
   /* All the below wrappers are created in isr.asm
    *
    * They simply push all registers to the stack, and
    * call stdHandler, which, in turn prints them out and
    * halts.
    */
   createGate(&IDT, 0x00, (long)int00h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x01, (long)int01h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x02, (long)int02h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x03, (long)int03h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x04, (long)int04h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x05, (long)int05h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x06, (long)int06h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x07, (long)int07h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x08, (long)int08h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x09, (long)int09h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x0A, (long)int0Ah_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x0B, (long)int0Bh_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x0C, (long)int0Ch_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x0D, (long)int0Dh_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x0E, (long)int0Eh_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x0F, (long)int0Fh_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x10, (long)int10h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x11, (long)int11h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x12, (long)int12h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x13, (long)int13h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x14, (long)int14h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x15, (long)int15h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x16, (long)int16h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x17, (long)int17h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x18, (long)int18h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x19, (long)int19h_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x1A, (long)int1Ah_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x1B, (long)int1Bh_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x1C, (long)int1Ch_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x1D, (long)int1Dh_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x1E, (long)int1Eh_wrapper, SEL_P0_CODE, D_INT);
   createGate(&IDT, 0x1F, (long)int1Fh_wrapper, SEL_P0_CODE, D_INT);
   
   // for now, the keyboard gate is created here...
   createGate(&IDT, 0x21, (long)int21h_wrapper, SEL_P0_CODE, D_TRAP);
   
   return;
}

ErrorCode idtGetTaskHandle( uint32 interrupt, Task *task )
{
   if( interrupt == 0x21 && keyboardTask )
   {
      *task = keyboardTask;
   }
   else
   {
      /* for now they're all the same, and are the kernel handler... */
      return taskGetSystemTask( task );
   }

   return ErrorNoError;
}

/* obv. a hack for now... */
ErrorCode idtSetTaskHandle( uint32 interrupt, Task task )
{
   keyboardTask = task;
}

ErrorCode idtGetCurrentHandler( uint32 *interrupt )
{
   ErrorCode err;

   if( currentInterrupt != -1 )
   {
      *interrupt = currentInterrupt;
      err = ErrorNoError;
   }
   else
   {
      err = ErrorOutOfState;
   }

   return err;
}

void stdHandler(short exceptionNum, short cs, long ip, short error, short taskReg, long eflags,
                long edi, long esi, long ebp, long esp, long ebx, long edx, long ecx, long eax,
                short ss, short gs, short fs, short es, short ds)
{
   Task task;
   uint8 *instructions;
   int32 intr = currentInterrupt;

   currentInterrupt = exceptionNum;

   if(exceptionNum < 19)
   {
      printf("%s Exception\n", exceptionNames[exceptionNum]);
   }
   
   //asm("jmp stopHere\n");
#if 1
   printf("Exception num: 0x%2x\n", (long)exceptionNum);
   printf("   at address: 0x%2x:0x%8x\n", (long)cs, ip);

   if(exceptionHasErrorCode[exceptionNum])
   {
      printf("   Error Code: 0x%x [ Index: 0x%x, Type:%d ]\n", error, error >> 3, error & 7);
   }
   printf("Task Register: 0x%x\n", (long)taskReg);
   printf("       EFlags: 0x%8x\n", (long)eflags);
   printf("          CR2: 0x%8x\n\n", getCR2() );

   printf("ds: 0x%2x \teax: 0x%8x \tedi: 0x%8x\n", (long)ds, eax, edi);
   printf("es: 0x%2x \tebx: 0x%8x \tesi: 0x%8x\n", (long)es, ebx, esi);
   printf("fs: 0x%2x \tecx: 0x%8x \tebp: 0x%8x\n", (long)fs, ecx, ebp);
   printf("gs: 0x%2x \tedx: 0x%8x \tesp: 0x%8x\n", (long)gs, edx, esp);
   printf("ss: 0x%2x \n", (long)ss);
#endif
   
   //*
   instructions = (uint32*)ip;
   printf("Code snapshot:\n");
   printf("ip[-2]      ip[-1]      ip[0]       ip[1]       ip[2]\n");
   printf("%02x %02x %02x %02x "
          "%02x %02x %02x %02x "
          "(%02x) %02x %02x %02x "
          "%02x %02x %02x %02x "
          "%02x %02x %02x %02x\n",
          instructions[-8], instructions[-7], instructions[-6], instructions[-5],
          instructions[-4], instructions[-3], instructions[-2], instructions[-1],
          instructions[0] , instructions[1] , instructions[2] , instructions[3],
          instructions[4] , instructions[5] , instructions[6] , instructions[7],
          instructions[8] , instructions[9] , instructions[10], instructions[11]);
   //*/
   printf("Interrupt %d, ip 0x%08x\n", intr, ip );
      
   pagerDumpStats();
   taskGetCurrent(&task);
   taskPrintStackTrace(task);

   currentInterrupt = -1;

   asm("stopHere:\n hlt\n jmp stopHere\n");
   //exit (0);
}
