/* ndk - [ taskSpecificHandle.h ]
 *
 * Interface for creating handles which change per task
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup i386TaskHandle taskSpecificHandle.h: Task specific data implementation
 * @ingroup i386
 *
 * This file defines an interface for managing handles which change from task to 
 * task
 */
/** @{ */

#ifndef __ndk_taskSpecificHandle_i386_h__
#define __ndk_taskSpecificHandle_i386_h__

#include <core/types.h>
#include <core/errorCodes.h>

typedef PointerType TaskSpecificHandle;

ErrorCode taskSpecificHandleCreate( TaskSpecificHandle *handle );
ErrorCode taskSpecificHandleSet( TaskSpecificHandle handle, Pointer data );
ErrorCode taskSpecificHandleGet( TaskSpecificHandle handle, Pointer *data );
ErrorCode taskSpecificHandleGetAddr( TaskSpecificHandle handle, Pointer *addr );
ErrorCode taskSpecificHandleDestroy( TaskSpecificHandle handle );

#endif

/** @} */
