/* ndk - [ taskSpecificHandle.c ]
 *
 * Interface for creating handles which change per task
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "taskSpecificHandle.h"
#include <core/pageBasedArray.h>
#include <core/task.h>
#include <core/arch/i386/task.i386.h>
#include <core/arch/i386/pager.h>

/* this is currently a byte array... use a bit array in time? */
static PageArray handleAllocation = NULL;
static uint32 handleAllocationSize = 0;
static Pointer initialAddr;

ErrorCode taskSpecificHandleInit(void)
{
   ErrorCode ec = ErrorNoError;
   
   /* 
    * setup the infrastructure to use task base handles... effectively, just 
    * initialize the handleAllocation list
    */
   pageArrayCreate( &handleAllocation, sizeof( uint8 ), 0, False );
   pagerGetPhysical( handleAllocation, &initialAddr );
   
   printf("created handleAllocation array 0x%8x for taskSpecificHandle module\n", handleAllocation);
   
   return ec; 
}

ErrorCode taskSpecificHandleCreate( TaskSpecificHandle *handle )
{
   ErrorCode ec = ErrorUnknown;
   PointerType i;
   uint32 isAllocated;
   Pointer currentAddr;
   
   printf("handleAllocation 0x%8x size %d\n", handleAllocation, handleAllocationSize );
   pagerGetPhysical( handleAllocation, &currentAddr );
   printf("initialAddr 0x%8x, currentAddr 0x%8x\n", initialAddr, currentAddr);
   
   for( i = 0; i < handleAllocationSize; i++ )
   {
      
      pageArrayGetItem( handleAllocation, i, &isAllocated );
      if( !isAllocated )
      {
         isAllocated = -1;
         ec = pageArraySetItem( handleAllocation, (uint32)i, (Pointer)&isAllocated );
         
         break;
      }
   }
   
   /* 
    * if we haven't found a free'd handle, then handleAllocation must be 
    * expanded to create a new one
    */
   if( i == handleAllocationSize )
   {
      isAllocated = -1;
      
      printf("couldn't find free'd handle, creating new at %d\n", i);
      
      handleAllocationSize++;
      ec = pageArraySetItem( handleAllocation, (uint32)i, (Pointer)&isAllocated );
   }

   /* 0 shouldn't be a valid task specific handle */
   *handle = i + 1;
   
   return ec;
}

ErrorCode taskSpecificHandleSet( TaskSpecificHandle tsHandle, Pointer data )
{
   ErrorCode ec = ErrorNoError;
   PageArray handles;
   Task task;
   PointerType handleNum = tsHandle - 1;
   
   taskGetCurrent( &task );
   taskGetOsDependantHandle( task, TaskSpecificHandles, (Pointer*)&handles );
   if( handles == NULL )
   {
      // create new handles array
      printf("Creating new TaskSpecificHandle's pageArray for task 0x%8x due to set\n", task );
      pageArrayCreate( &handles, sizeof(Pointer), 0, False );
      taskSetOsDependantHandle( task, TaskSpecificHandles, handles );
   }
   
   pageArraySetItem( handles, handleNum, &data );
   
   return ec;
}

ErrorCode taskSpecificHandleGet( TaskSpecificHandle handleNum, Pointer *data )
{
   ErrorCode ec = ErrorNoError;
   Pointer location;
   
   taskSpecificHandleGetAddr( handleNum, &location );
   if( location )
   {
      *data = *((Pointer*)location);
   }
   else
   {
      *data = NULL;
   }
   
   return ec;
}

ErrorCode taskSpecificHandleGetAddr( TaskSpecificHandle tsHandle, Pointer *addr )
{
   ErrorCode ec = ErrorNoError;
   PageArray handles;
   Task task;
   PointerType handleNum = tsHandle - 1;
   uint32 interrupt;

   /* 
    * if we're in an interrupt, use the Task handle specified (a fudging, for now, until 
    * this module properly supports tasks and threads such that same address space tasks 
    * (such as in the PS2 keyboard driver) can share mapped handles)
    */
   if( idtGetCurrentHandler( &interrupt ) == ErrorNoError )
   {
      idtGetTaskHandle( interrupt, &task );
      printf("taskSpecificHandlerGetAddr says in int 0x%x, returning task 0x%08x\n", interrupt, task );
   }
   else
   {
      taskGetCurrent( &task );
   }

   taskGetOsDependantHandle( task, TaskSpecificHandles, (Pointer*)&handles );
   if( handles == NULL )
   {
      // create new handles array
      printf("Creating new TaskSpecificHandle's pageArray for task 0x%8x due to get\n", task );
      pageArrayCreate( &handles, sizeof(Pointer), 0, False );
      taskSetOsDependantHandle( task, TaskSpecificHandles, handles );
      *addr = NULL;
   }
   else
   {
      pageArrayGetItemPointer( handles, handleNum, addr );
   }
   
   return ec;
}

ErrorCode taskSpecificHandleDestroy( TaskSpecificHandle tsHandle )
{
   ErrorCode ec = ErrorNoError;
   uint32 isAllocated = 0;
   PointerType handleNum = tsHandle - 1;
   
   ec = pageArraySetItem( handleAllocation, handleNum, (Pointer)&isAllocated );
   
   return ec;
}
