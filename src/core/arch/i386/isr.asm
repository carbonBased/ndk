; ndk - [ isr.asm ]
;
; These are the definitions of all the general
; purpose exceptions, as well as some of the
; irqs.
;
; Please see:
;   /src/idt.{c,h}
;
; (c)2002 dcipher / neuraldk
;           www.neuraldk.org

; make sure all our wrappers are visible to gcc
global int00h_wrapper, int10h_wrapper
global int01h_wrapper, int11h_wrapper
global int02h_wrapper, int12h_wrapper
global int03h_wrapper, int13h_wrapper
global int04h_wrapper, int14h_wrapper
global int05h_wrapper, int15h_wrapper
global int06h_wrapper, int16h_wrapper
global int07h_wrapper, int17h_wrapper
global int08h_wrapper, int18h_wrapper
global int09h_wrapper, int19h_wrapper
global int0Ah_wrapper, int1Ah_wrapper
global int0Bh_wrapper, int1Bh_wrapper
global int0Ch_wrapper, int1Ch_wrapper
global int0Dh_wrapper, int1Dh_wrapper
global int0Eh_wrapper, int1Eh_wrapper
global int0Fh_wrapper, int1Fh_wrapper

; custom handlers for the timer and keyboard
global int20h_wrapper
global int21h_wrapper

; a byte array defining which exceptions
; send an error code
global exceptionHasErrorCode

; defined in the C source
extern timerHandler
extern kbdHandler
extern stdHandler
extern handleKey
extern currentInterrupt

section .text
bits 32

; A macro used to create the general purpose
; wrappers
%macro NEW_ISR 2
%1:
        push 	eax		; save original eax
	mov  	eax, %2		; which exception?
	jmp	commonHandler	; jump to the common code
%endmacro

commonHandler:
        push    ebp
        mov     ebp, esp

	; push all registers (to be printed upon an exception)
	; NOTE: should I push flags!?
	push ds
        push es
	push fs
	push gs
	push ss
        pushad

	; NOTE: new... eflags!
	pushfd

	; correct eax to value before "exceptionNum" overwrote it
	; back in the NEW_ISR wrapper
	add     esp, 32		; goto location of eax on stack
	push dword [ss:ebp+4]	; overwrite it
	sub	esp, 28		; return back to previous position on stack

	; store task register
	str     bx
	push    bx
	push word 0

	; make sure ds and es have valid selectors
	mov     bx, 08h
        mov     ds, bx
        mov     es, bx

	; check if this exception provides an error code
	mov     ebx, eax
        cmp     byte [exceptionHasErrorCode + ebx], 0
        je      .1

	; if so, push it onto the stack along with cs:ip
	push    word 0
        push    word  [ss:ebp+8]         ; error code
	push    dword [ss:ebp+12]         ; ip
	push    word 0
        push    word  [ss:ebp+16]         ; cs
	jmp     .2

.1:
	; if not, push "0" as error code, and cs:ip
	push    dword 0                  ; error code
	push    dword [ss:ebp+8]        ; ip
        push    dword [ss:ebp+12]        ; cs
.2:
	push    word 0
        push    ax                      ; exception no

	; call the standard error handler (C portion),
	; which will examine the stack (which contains all
	; the registers) and print them out
	call    stdHandler

	; get rid of the error code, cs:ip stuff
	add     esp, 10
	; and restore all the regs back to their previous settings
	popfd
        popad
	pop     ss
	pop     gs
	pop     fs
        pop     es
        pop     ds
        pop    ebp
        pop    eax
        iretd

int20h_wrapper:
        push    ds
        push    es                      ; saving segment registers and
        pushad                          ; other regs because it's an ISR
        mov     bx, 08h
        mov     ds, bx
        mov     es, bx                  ; load ds and es with valid selector
        call    timerHandler            ; call actual ISR code
        popad                           ; restoring the regs
        pop     es
        pop     ds
        iretd

int21h_wrapper:
        push    ds
        push    es                      ; saving segment registers and
        pushad                          ; other regs because it's an ISR
        mov     dword [currentInterrupt], 0x21
        mov     bx, 08h
        mov     ds, bx
        mov     es, bx                  ; load ds and es with valid selector
        mov     eax, [handleKey]
        cmp     eax, 0
        je      no_handler
        call    eax                     ; call actual ISR code
        jmp     done
   no_handler:
   		in      al, 0x60				; uber-simple kbd driver
   		mov     al, 0x20
   		out     0x20, al
   done:
        mov     dword [currentInterrupt], -1
        popad                           ; restoring the regs
        pop     es
        pop     ds
        iretd

NEW_ISR int00h_wrapper, 00h
NEW_ISR int01h_wrapper, 01h
NEW_ISR int02h_wrapper, 02h
NEW_ISR int03h_wrapper, 03h
NEW_ISR int04h_wrapper, 04h
NEW_ISR int05h_wrapper, 05h
NEW_ISR int06h_wrapper, 06h
NEW_ISR int07h_wrapper, 07h
NEW_ISR int08h_wrapper, 08h
NEW_ISR int09h_wrapper, 09h
NEW_ISR int0Ah_wrapper, 0Ah
NEW_ISR int0Bh_wrapper, 0Bh
NEW_ISR int0Ch_wrapper, 0Ch
NEW_ISR int0Dh_wrapper, 0Dh
NEW_ISR int0Eh_wrapper, 0Eh
NEW_ISR int0Fh_wrapper, 0Fh
NEW_ISR int10h_wrapper, 10h
NEW_ISR int11h_wrapper, 11h
NEW_ISR int12h_wrapper, 12h
NEW_ISR int13h_wrapper, 13h
NEW_ISR int14h_wrapper, 14h
NEW_ISR int15h_wrapper, 15h
NEW_ISR int16h_wrapper, 16h
NEW_ISR int17h_wrapper, 17h
NEW_ISR int18h_wrapper, 18h
NEW_ISR int19h_wrapper, 19h
NEW_ISR int1Ah_wrapper, 1Ah
NEW_ISR int1Bh_wrapper, 1Bh
NEW_ISR int1Ch_wrapper, 1Ch
NEW_ISR int1Dh_wrapper, 1Dh
NEW_ISR int1Eh_wrapper, 1Eh
NEW_ISR int1Fh_wrapper, 1Fh

section .data

exceptionHasErrorCode db 0,0,0,0,0,0,0,0, 1,0,1,1,1,1,1,0
                      db 0,1,0,0,0,0,0,0, 0,0,0,0,0,0,0,0
