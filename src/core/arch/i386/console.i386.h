/* ndk - [ console.i386.h ]
 *
 * Basic console IO for kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup i386Console console.i386.h: Basic console IO
 * @ingroup i386
 *
 * This file defines a interface for working with multiple consoles
 */
/** @{ */

#ifndef __ndk_console_i386_h__
#define __ndk_console_i386_h__

#include <errorCodes.h>

ErrorCode consoleInit(void);

#endif

/** @} */
