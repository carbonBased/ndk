/* ndk - [ memory.c ]
 *
 * ndk maintains "page stacks" in order to manage memory in the physical
 * address space.  This code relies heavily on the paging hardware and
 * interface described in the pager source.
 *
 * If done correctly, it should be able to create an rZero implementation
 * of this memory management code, and the pager, to allow for
 * interchangeable methods of managing memory.  The two modules, however,
 * would probably have to be combined into one rZero module, due to
 * their reliance on one another.
 *
 * Please see:
 *   /doc/memoryManagement.kwd
 *   /doc/memoryMap.kwd
 *   /src/pager.{c,h}
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "memory.h"
#include <propDb.h>

extern long bssEnd;

ErrorCode memoryGetKernelBase(Pointer *base)
{
   // kernel is loaded at 1MB (NOTE: a change in the multiboot header will
   // corrupt this... might want to use textStart?
   //*base = (Pointer)(propertyGetAsInteger(kernel_base_linear));
   *base = propertyGetAsInteger(kernel_base_linear);
   return ErrorNoError;
}

ErrorCode memoryGetKernelSize(uint32 *size)
{
   uint32 kernelEnd;
   uint32 kernelBase;
   uint32 pageSize;

   // the end of the kernel is equivalent to the end of the bss section... we'll
   // also round up to the nearest page
   pagerGetPageSize(&pageSize);
   kernelEnd = ((long)&bssEnd + pageSize - ((long)&bssEnd & (pageSize - 1)));
   kernelBase = propertyGetAsInteger(kernel_base_linear);

   *size = (kernelEnd - kernelBase);
   return ErrorNoError;
}

ErrorCode memoryGetModulesBase(Pointer *modBase)
{
   Pointer base;
   uint32 size;

   memoryGetKernelBase(&base);
   memoryGetKernelSize(&size);

   *modBase = (Pointer)((uint32)base + size);

   return ErrorNoError;
}

ErrorCode memoryGetModulesSize(uint32 *size)
{
   module_t *mod;
   Pointer base;
   uint32 end;

   // get address of last module
   mod = (module_t *) multibootInfo->mods_addr;
   end = (long)mod[multibootInfo->mods_count - 1].mod_end;
   end = end - propertyGetAsInteger(kernel_base_physical) + propertyGetAsInteger(kernel_base_linear);

   // and subtract from base
   memoryGetModulesBase(&base);
   *size = end - (uint32)base;

   return ErrorNoError;
}

ErrorCode memoryGetModulesCount(uint32 *count)
{
   *count = multibootInfo->mods_count;
   return ErrorNoError;
}
