/* ndk - [ semaphore.c ]
 *
 * Routines for implementing semaphores
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <klibc/assert.h>
#include <core/semaphore.h>
#include <core/task.h>
#include <core/pageBasedList.h>

struct _Semaphore
{
   uint32 value;
   SemaphoreType type;
   //Mutex mutex;   // needed?  Or simply cli/sti these functions?
   //Task waitingTasks[ (4096 - sizeof(uint32)) / sizeof(Task) ];
};

ErrorCode semaphoreCreate(SemaphoreId *semaphoreId, SemaphoreType type, uint32 initial)
{
   ErrorCode ec = ErrorNoError;
   PageListId listId;
   PageList list;
   
   ec = pageListCreate( &listId );
   if( ec == ErrorNoError )
   {
      ec = pageListMap( listId, &list );
      if( ec == ErrorNoError )
      {
         struct _Semaphore semData;
         
         semData.value = initial;
         semData.type = type;
         
         ec = pageListEmbedPrivateData( list, &semData, sizeof( semData ) );
         
         pageListUnmap( list );
      }
   }
   
   *semaphoreId = (SemaphoreId)listId;
   
   return ec;
}

ErrorCode semaphoreMap( SemaphoreId semId, Semaphore *sem )
{
   return pageListMap( (PageListId)semId, (PageList*)sem );
}

ErrorCode semaphoreUnmap( Semaphore sem )
{
   return pageListUnmap( (PageList)sem );
}

ErrorCode semaphoreUp(Semaphore *semaphorePtr)
{
   ErrorCode ec;
   uint32 length;
   Semaphore semaphore;
   
   pageListGetPrivateData( (PageList)(*semaphorePtr), (Pointer*)&semaphore );

   semaphore->value++;
   //printf("semaphore 0x%8x up %d\n", (*semaphorePtr), semaphore->value);
   //mutexLock(semaphore->mutex, TimeoutInfinite);

   // semaphore has been raised... check to see if someone wants it!
   ec = pageListGetLength( (PageList)(*semaphorePtr), &length);
   if( (ec == ErrorNoError) && (length != 0) )
   {
      Task task;
      PageListNode node;
      
      //printf("%d waiting tasks\n", length );

      /* TODO: implement these?
         switch(mutex->type) {
         case MutexTypeFIFO:
         case MutexTypePriority:
         }
       */
      //if(listGetFirst(semaphore->waitingTasks, (void **)&task) == ErrorNoError)
      //ec = pageListRemove( (PageList*)semaphorePtr, task );
      ec = pageListGetFirst( (PageList)(*semaphorePtr), &node);
      if( ec  == ErrorNoError )
      {
         // remove the task from the waiting state, and activate it
         ec = pageListGetData( (PageList)(*semaphorePtr), node, (Pointer*)&task );
         assert( ec == ErrorNoError );
         
         pageListRemove( (PageList*)semaphorePtr, (Pointer)task);
         
         taskSetState(task, TaskStateAlive);
         //printf("marked task 0x%8x active\n", task);
      }
      else
      {
         printf("No one waiting on this semaphore? %x ec=0x%8x\n", semaphore, ec);
      }
   }
   else
   {
      printf("No waiting tasks ec=0x%8x\n", ec);
   }

   //mutexUnlock(semaphore->mutex);

   return ErrorNoError;
}

ErrorCode semaphoreDown(Semaphore *semaphorePtr, Timeout timeout)
{
   ErrorCode err = ErrorNoError;
   Task task;
   Semaphore semaphore;

   //printf("semaphore down: 0x%8x 0x%8x", semaphorePtr, *semaphorePtr);
   
   pageListGetPrivateData( (PageList)(*semaphorePtr), (Pointer*)&semaphore );

   //mutexLock(semaphore->mutex, TimeoutInfinite);

   if(semaphore->value)
   {
      semaphore->value--;
      //printf("down %d\n", semaphore->value);
      //mutexUnlock(semaphore->mutex);
   }
   else
   {
      TaskState state;

      taskGetCurrent(&task);
      //listAddBack(semaphore->waitingTasks, task);
      pageListAdd( (PageList*)semaphorePtr, task );

      /*
       * this will tell the task to wait the specified timeout...
       * if the other thread unlocks the mutex, and this task is
       * waiting on it, it will set the task into an Alive state,
       * otherwise, we'll enter the TimedOut state
       */
      taskSetTimeout(task, timeout);

      // unlock first, otherwise a waiting task holds this
      // semaphore's mutex, and will not allow anything to use it (ie, 'Up' it)
      //mutexUnlock(semaphore->mutex);

    reswitch:
      taskForceSwitch();

      //mutexLock(semaphore->mutex, TimeoutInfinite);
      //printf("Semaphore, back from switch\n");

      taskGetState(task, &state);
      if(state != TaskStateWaiting)
      {
         //listRemove(semaphore->waitingTasks, task);
         pageListRemove( (PageList*)semaphorePtr, task );
      }

      switch (state)
      {
      case TaskStateTimedOut:
         taskSetState(task, TaskStateAlive);
         //printf("semaphore timed out\n");
         err = ErrorTimedOut;
         break;
      case TaskStateAlive:
         semaphore->value--;
         //printf("down delayed %d\n", semaphore->value);
         break;
      default:
         printf("***************************unknown state %d!\n", state);
         goto reswitch;
      }
      //mutexUnlock(semaphore->mutex);
   }

   //mutexUnlock(semaphore->mutex);

   return err;   
}

ErrorCode semaphoreDestroy(Semaphore *semaphore)
{
}

ErrorCode semaphoreDestroyById(SemaphoreId *semaphore)
{
}
