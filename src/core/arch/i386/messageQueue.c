/* ndk - [ messageQueue.c ]
 *
 * Basic message queue class for IPC
 *
 * (c)1996-2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "messageQueue.h"
//#include "semaphore.h"  // @TODO NON-LOCAL
//#include "mutex.h"      // @TODO NON-LOCAL
#include "pager.h"
#include <task.h>
#include <console.h>
#include <stdarg.h>
#include <core/message.h> // only for Message
#include <core/pageBasedList.h>
#include <core/mutex.h>
#include <core/semaphore.h>
#include <core/arch/i386/taskSpecificHandle.h>
#include <klibc/assert.h>

struct _MessageQueue
{
   //uint32 length; // irrelevant, as it's in the semaphore?
   //uint32 remaining; // same as above? "
   
   /* 
    * can't place the actual Mutex in here... need some way to convert 
    * physical to linear quickly
    */
   MutexId queueSync;   /* probably don't even need a mutex, as semaphore can be used for protection */
   SemaphoreId queueSem;
   TaskSpecificHandle mutexHandle;
   TaskSpecificHandle semaphoreHandle;
};

ErrorCode messageQueueCreate(MessageQueueId *queueId, uint32 size)
{
   ErrorCode ec = ErrorNoError;
   PageListId listId;
   PageList list;
   
   ec = pageListCreate( &listId );
   if( ec == ErrorNoError )
   {
      ec = pageListMap( listId, &list );
      if( ec == ErrorNoError )
      {
         struct _MessageQueue queueData;

         taskSpecificHandleCreate( &queueData.mutexHandle );
         taskSpecificHandleCreate( &queueData.semaphoreHandle );
         
         mutexCreate( &queueData.queueSync, MutexTypeFIFO | MutexTypeRecursive );
         semaphoreCreate( &queueData.queueSem, SemaphoreTypeFIFO, 0 );
         
         ec = pageListEmbedPrivateData( list, &queueData, sizeof( queueData ) );
         
         pageListUnmap( list );
      }
   }
   
   *queueId = (MutexId)listId;   

   return ec;
}

ErrorCode messageQueueMap(MessageQueueId queueId, MessageQueue *msgq)
{
   ErrorCode ec;
   
   ec = pageListMap( (PageListId)queueId, (PageList*)msgq );
   if( ec == ErrorNoError )
   {
      Mutex mutex;
      Semaphore semaphore;
      MessageQueue queueData;
      
      pageListGetPrivateData( (PageList)(*msgq), (Pointer*)&queueData );
      
      mutexMap( queueData->queueSync, &mutex );
      semaphoreMap( queueData->queueSem, &semaphore );
      
      taskSpecificHandleSet( queueData->mutexHandle, mutex );
      taskSpecificHandleSet( queueData->semaphoreHandle, semaphore );
   }
   
   return ec;
}

ErrorCode messageQueueUnmap(MessageQueue queue)
{
   ErrorCode ec = ErrorNoError;
   MessageQueue queueData;
   
   ec = pageListGetPrivateData( (PageList)queue, (Pointer*)&queueData );
   if( ec == ErrorNoError )
   {
      Mutex mutex;
      Semaphore semaphore;
      
      taskSpecificHandleGet( queueData->mutexHandle, (Pointer*)&mutex );
      taskSpecificHandleGet( queueData->semaphoreHandle, (Pointer*)&semaphore );
      
      mutexUnmap( mutex );
      semaphoreUnmap( semaphore );

      taskSpecificHandleSet( queueData->mutexHandle, NULL );
      taskSpecificHandleSet( queueData->semaphoreHandle, NULL );
   }
   
   return ec;
}

ErrorCode messageQueueSend(MessageQueue *queuePtr, Pointer message)
{
   ErrorCode ec = ErrorNoError;
   MessageQueue queueData;
   
   //printf("%s(0x%8x,0x%8x)\n", __func__, queuePtr, message);

   ec = pageListAdd( (PageList*)queuePtr, message );
   if( ec == ErrorNoError )
   {
      Semaphore *semaphore;
      
      ec = pageListGetPrivateData( (PageList)(*queuePtr), (Pointer*)&queueData );
      if( ec == ErrorNoError )
      {
         taskSpecificHandleGetAddr( queueData->semaphoreHandle, (Pointer*)&semaphore );
         
         //printf("messageQueue upping sem 0x%8x id=0x%8x\n", (*semaphore), queueData->queueSem );
         semaphoreUp( semaphore );
      }
   }
   
   return ec;
}

/*
ErrorCode messageQueueSendAndReceive( MessageQueue *sendTo, Message message, Message *result )
{
   ErrorCode ec;
   MessageId sendId, recvId;
   
   ec = messageGetId( message, &sendId );
   assert( ec == ErrorNoError );
   
   ec = messageQueueSend( sendTo, sendId );
   if( ec == ErrorNoError )
   {
      MessageQueue recvFrom;
      MessageReturnPath returnPath;
      
      messageGetReturnPath( msg, &returnPath, &recvFrom );
      assert( returnPath == MessageReturnRemoteQueue );
      
      ec = messageQueueReceive( recvFrom, TimeoutInfinite, result ); 
   }
   
   return ec;
}
*/

ErrorCode messageQueuePeek(MessageQueue mq, Pointer *message)
{
   ErrorCode ec = ErrorNoError;
   PageListNode first;
   
   ec = pageListGetFirst( (PageList)mq, &first );
   if( ec == ErrorNoError )
   {
      ec = pageListGetData( (PageList)mq, first, message );
   }
   
   if( !ec ) printf("messageQueuePeek returning message 0x%8x\n", *message );
   
   return ec;
}

ErrorCode messageQueueReceive(MessageQueue *queuePtr, Timeout to, Pointer *message)
{
   ErrorCode ec = ErrorNoError;
   MessageQueue queueData;
   Semaphore *semaphore;
   Mutex *mutex;
   PageListNode first;
   PointerType numItems;
   
   pageListGetLength( (PageList)(*queuePtr), &numItems );
   //printf("Receive on queue with %d items\n", numItems );
      
   ec = pageListGetPrivateData( (PageList)(*queuePtr), (Pointer*)&queueData );
   if( ec == ErrorNoError )
   {
      taskSpecificHandleGetAddr( queueData->semaphoreHandle, (Pointer*)&semaphore );
      taskSpecificHandleGetAddr( queueData->mutexHandle, (Pointer*)&mutex );
      
      //ec = mutexLock( mutex, to );
      if( ec == ErrorNoError )
      {
         /*
         if( TimeoutIsMs(to) )
         {
            // adjust 'to' by the ms used to lock mutex?
         }
         */
         
         //printf("___waiting for message via semaphore\n");
         ec = semaphoreDown( semaphore, to );
         if( ec == ErrorNoError )
         {
            //printf("___acquired semaphore\n");
            ec = pageListRemoveFirst( (PageList)(*queuePtr), message );
         }
         else
         {
            assert(0);
            //printf("semaphore error 0x%8x\n", ec);
         }
         
         
         //mutexUnlock( mutex );
      }
   }
   
   pageListGetLength( (PageList)(*queuePtr), &numItems );
   //printf("Queue now has %d items, ret=0x%8x\n", numItems, ec );
   
   return ec;
}

ErrorCode messageQueueDestroy(MessageQueue *mq)
{
}
