/* ndk - [ physicalMemoryDescriptor.h ]
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Utilities physicalMemoryDescriptor.h: Describes a collection of physical memory pages.
* @ingroup core
*
* This file defines an iterface for handling collections of physical pages such that they can be 
* easily mapped in and out of separate address spaces (eg. a message from a queue) 
*/
/** @{ */

#ifndef __ndk_physical_memory_descriptor__
#define __ndk_physical_memory_descriptor__

#include <errorCodes.h>
#include <types.h>
#include <arch/i386/pager.h>

typedef PhysicalAddress PhysicalMemoryDescriptor;

ErrorCode physicalMemoryDescriptorCreate( PhysicalMemoryDescriptor *desc, LinearAddress base, uint32 size );
ErrorCode physicalMemoryDescriptorDestroy( PhysicalMemoryDescriptor desc );
ErrorCode physicalMemoryDescriptorGetSize( PhysicalMemoryDescriptor desc, uint32 *size );
ErrorCode physicalMemoryDescriptorMap( PhysicalMemoryDescriptor desc, LinearAddress *base );
ErrorCode physicalMemoryDescriptorUnmap( PhysicalMemoryDescriptor desc, LinearAddress base );

#endif

/** @} */
