/* ndk - [ debug.h ]
 *
 * Basic debugging functions
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup Debug debug.h: Debugging helper functions
 * @ingroup core
 *
 * This file defines a interface to assist in debugging the kernel
 */
/** @{ */

#ifndef __ndk_debug_h__
#define __ndk_debug_h__

#include "console.h"

#define DEBUG_ENTER_LEAVE

void debugEnter(char *func, char *pattern, ...);
void debugLeave(char *func, char *pattern, ...);

#endif

/** @} */
