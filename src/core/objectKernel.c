/* ndk - [ objectKernel.h ]
 *
 * Implements an 'Object' class for the kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "objectKernel.h"
#include "object.h"

// for the export declarations
#include <console.h>
#include <memory.h>
#include <brink/localMutex.h>
#include <timer.h>
#include <brink/localMessageQueue.h>
#include <driver.h>
#include <klibc/string.h>
#include <klibc/printf.h>
#include <klibc/assert.h>
#include <klibc/malloc.h>
#include <klibc/sprintf.h>
#include <core/messageQueue.h>
#include <core/message.h>
#include <core/mutex.h>
#include <core/task.h>
#include <core/propDb.h>
// TODO: remove i386 dep!!!
#include <arch/i386/io.h>
#include <arch/i386/idt.h>
#include <arch/i386/memory.h>
#include <arch/i386/pic.h>

#define addExport(sym)	\
	{ #sym, (void*)sym }

#define addImport(sym)	\
	{ #sym, (void*)&sym }

Object objectKernel;

typedef struct _SymbolMapping
{
   char *name;
   void *location;
} SymbolMapping;

// TODO: get rid of these externs!
extern void *textEnd;
extern void *dataEnd;
extern void *bssEnd;

// TODO: find a better method to define these imports!
void (*splashScreen) (void *argument) = 0;
void (*handleKey)(void) = 0;   /** @TODO: resource manager to allow interrupt hook */

static const SymbolMapping symExports[] = {
   addExport(_assert),
   addExport(consoleClear),
   addExport(consoleEnableCharacterListener),
   addExport(consoleDisableCharacterListener),
   addExport(consoleLockScrollWindow),
   
   /* new for keyboard driver */
   addExport(picEnableIRQ),

   /** TODO: alphabetize these driver funcs, and ensure all that are needed are in the export list */
   addExport(driverCreate),
   addExport(driverItemCreateFunction),
   addExport(driverItemCreateParameter),
   addExport(driverItemCreateNamespace),
   addExport(driverItemAdd),
   addExport(driverManagerAdd),
   addExport(driverManagerRemove),
   addExport(driverParamCreate),
   addExport(driverParamDestroy),
   addExport(driverSetRoot),
   addExport(driverSetMetaData),
   addExport(driverGetMessageQueue),
   addExport(driverReserveIrq),
   addExport(driverReservePort),
   addExport(driverReserveMemory),
   addExport(driverReserveIrqRange),
   addExport(driverReservePortRange),
   addExport(driverReserveMemoryRange),
   
   addExport(driverContextCreate),
   addExport(driverContextExecute),
   addExport(driverContextTraverse),
   addExport(driverContextDestroy),

   /* temporary... */
   addExport(idtSetTaskHandle),

   addExport(printf),
   addExport(inportb),
   addExport(malloc),
   addExport(memcpy),
   // TODO: remove memory*
   //addExport(memoryAllocPage),
   //addExport(memoryFreePage),
   // malloc?
   addExport(memset),
   addExport(messageCreate), 
   addExport(messageCreateSimple),
   addExport(messageMap),
   addExport(messageSetResponseQueueId),
   addExport(messageGetResponseQueueId),
   addExport(messageGetId),
   addExport(messageGetSender),
//   addExport(messageGetSize),
   addExport(messageGetArgument),
   addExport(messageGetArgumentPointer),
   addExport(messageGetArgumentCount),
   addExport(messageGetCode),
   addExport(messageRespond),
   addExport(messageDestroy),   
   addExport(messageQueueCreate),
   addExport(messageQueueDestroy),
   //addExport(messageQueueDestroyById),   
   addExport(messageQueueMap),
   addExport(messageQueuePeek),
   addExport(messageQueueReceive),
   addExport(messageQueueSend),
   addExport(messageQueueUnmap),
   
   addExport(mutexCreate),
   addExport(mutexMap),
   addExport(mutexUnmap),
   addExport(mutexLock),
   addExport(mutexUnlock),
   addExport(mutexDestroy),
   
   addExport(listCreate),
   addExport(listDestroy),
   addExport(listAddFront),
   addExport(listAddBack),
   addExport(listRemove),
   addExport(listGetLength),
   addExport(listGetFirst),
   addExport(listGetLast),
   addExport(listFind),
   addExport(listIteratorCreate),
   addExport(listIteratorReset),
   addExport(listIteratorGetNext),
   addExport(listIteratorGetPrevious),
   addExport(listIteratorDestroy),
   
   addExport(localMessageQueueCreate),
   addExport(localMessageQueueDestroy),
   addExport(localMessageQueuePeek),
   addExport(localMessageQueueReceive),
   addExport(localMessageQueueSend),
   addExport(localMutexCreate),
   addExport(localMutexDestroy),
   addExport(localMutexLock),
   addExport(localMutexUnlock),
   addExport(outportb),
   addExport(outportw),
   addExport(printf),
   //{ "multibootInfo"  , (long)multibootInfo  , 0 },
   addExport(strcmp),
   addExport(sprintf),
   addExport(strncmp),
   addExport(taskGetCurrent),
   addExport(taskGetMessageQueueId),
   addExport(timerGetTicks),
   {(char *)NULL, NULL}
};

static const SymbolMapping symImports[] = {
   addImport(splashScreen),
   addImport(handleKey),
   {(char *)NULL, NULL}
};

ErrorCode objectKernelInit(void)
{
   uint32 i = 0;
   ObjectSection text, data, bss;
   ObjectRelocation reloc;
   ObjectSymbol symbol;
   PointerType textStart = propertyGetAsInteger(kernel_base_linear);

   objectCreateEmpty(&objectKernel);

   objectKernel->name = "ndk";
   objectKernel->base = (uint8 *)0x00000000;
   objectKernel->length = (long)bssEnd - textStart;

   // add .text and .data sections from textStart...End, etc, linker script defines
   // (remember, multiboot is not portable across arch's)
   printf("___%x %x %x\n", &textEnd, &dataEnd, &bssEnd);
   objectParserCreateSection(&text, ".text", 1, textStart, (long)&textEnd - textStart, 2);
   objectParserCreateSection(&data, ".data", 2, (long)&textEnd, (long)&dataEnd - (long)&textEnd, 2);
   objectParserCreateSection(&bss, ".bss", 3, (long)&dataEnd, (long)&bssEnd - (long)&dataEnd, 2);

   // create all the exports
   while(symExports[i].name)
   {
      // add symbol in .text section
      printf("___creating export %s at %x\n", symExports[i].name,
             (long)symExports[i].location - textStart);
      objectParserCreateSymbol(&symbol, symExports[i].name, i,
                               (long)symExports[i].location - textStart);

      objectParserAddExport(objectKernel, symbol, text);

      i++;
   }

   // and then all the imports
   i = 0;
   while(symImports[i].name)
   {
      // add symbol in .text section
      printf("___creating import %s at %x\n", symImports[i].name,
             (long)symImports[i].location - textStart);
      objectParserCreateSymbol(&symbol, symImports[i].name, i,
                               (long)symImports[i].location - textStart);
      objectParserAddImport(objectKernel, symbol);

      objectParserCreateSymbolRelocation(&reloc, text, (long)symImports[i].location - textStart,
                                         symbol, 4, False);
      objectParserAddRelocation(objectKernel, reloc);

      i++;
   }

   return ErrorNoError;
}

ErrorCode objectKernelFinal(void)
{
   printf(" --- %s %x\n", symImports[0].name, symImports[0].location);
   return ErrorNoError;
}
