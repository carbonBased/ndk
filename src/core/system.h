/* ndk - [ system.h ]
 *
 * Basic system routines for the kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup System system.h: Basic system routines
* @ingroup core
*
* This file defines a interface for working with the basic system
*/
/** @{ */

#ifndef __ndk_system_h__
#define __ndk_system_h__

#include <errorCodes.h>
#include <types.h>

ErrorCode systemAssert(uint32 lineNumber, String filename);
ErrorCode systemReset(void);
ErrorCode systemSleep(void);
ErrorCode systemResume(void);

#endif

/** @} */
