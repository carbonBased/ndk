/* ndk - [ objectKernel.h ]
 *
 * Implements an 'Object' class for the kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup ObjectKernel objectKernel.h: Kernel object
* @ingroup core
*
* This file defines an object for the kernel interface
*/
/** @{ */

#ifndef __ndk_object_kernel__
#define __ndk_object_kernel__

#include "object.h"

extern Object objectKernel;

ErrorCode objectKernelInit(void);
ErrorCode objectKernelFinal(void);

/**
 * The following define the function imports that the kernel
 * can expect.
 */
extern void (*splashScreen) (void *argument);
extern void (*handleKey)(void);

#endif

/** @} */
