/* ndk - [ objectRDOFF2.h ]
 *
 * Object Parser for RDOFF2 objects
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Object objectRDOFF2.h: RDOFF2 Object Parser
* @ingroup core
*
* This file defines an object parser for the RDOFF2 object format
*/
/** @{ */

#undef interface

#if Build_Rdoff2_internal
#define interface(n) rdoff2_##n
#else
#define interface(n) n
#endif

#ifndef __ndk_object_rdoff2__
#define __ndk_object_rdoff2__

//#include "buildProperties.h"

ErrorCode interface(driverInit) (void);
ErrorCode interface(driverFinal) (void);

#endif

/** @} */
