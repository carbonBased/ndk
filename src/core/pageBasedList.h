/* ndk - [ pageBasedList.h ]
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Utilities pageBasedList.h: Page based linked list
* @ingroup core
*
* This file defines an iterface for a linked list allocated in pages 
*/
/** @{ */

#ifndef __ndk_page_based_list__
#define __ndk_page_based_list__

#include <errorCodes.h>
#include <types.h>
#include <arch/i386/pager.h>

typedef PhysicalAddress PageListId;
typedef struct _PageList *PageList;
/* 
 * Potential Problem!  If page list is remapped via an add, its nodes will no longer 
 * be accurate.  One possible solution is to use an offset from the base of the 
 * page list... @TODO: why not just use an index rather then an offset?!
 */
typedef PointerType PageListNode;

/** 
 * due to the main consumers of PageList being already synchronized (mutex, semaphore, and 
 * messageQueue), and for reasons of simplicity, these routines will be developed without 
 * mutexes or any form of thread safety.
 */

ErrorCode pageListCreate(PageListId *listId);
ErrorCode pageListMap(PageListId id, PageList *handle);
ErrorCode pageListUnmap(PageList list);
ErrorCode pageListDestroy(PageList *handle);
ErrorCode pageListDestroyById(PageListId listId);
/** could remap handle, hense pointer to pointer */
/* data should be a physical mem descriptor, in order to me able to map into
 * another address space, but anything code be passed in (eg. even just an integer)
 */
ErrorCode pageListAdd(PageList *handle, Pointer data );
/** could remap handle, hense pointer to pointer */
ErrorCode pageListRemove(PageList *handle, Pointer data);   /* see below... is this one needed? */

ErrorCode pageListEmbedPrivateData( PageList handle, Pointer data, PointerType sizeOfData );
ErrorCode pageListGetPrivateData( PageList handle, Pointer *data );

/* functions to iterate through the list... */
ErrorCode pageListGetFirst( PageList list, PageListNode *first );
ErrorCode pageListGetNext( PageList list, PageListNode *next );
ErrorCode pageListGetData( PageList list, PageListNode node, Pointer *data );
ErrorCode pageListGetLength( PageList list, uint32 *length );

/* functions to remove from the list... */
ErrorCode pageListRemoveFirst( PageList list, Pointer *data );
ErrorCode pageListRemoveLast( PageList list, Pointer *data ); /* wont be easy with singly-linked list */
ErrorCode pageListRemoveNext( PageList list, PageListNode node, Pointer *data );

/* functions to insert into the middle of the list... ? */

/* debug function... */
ErrorCode pageListDump( PageList list );

#endif

/** @} */
