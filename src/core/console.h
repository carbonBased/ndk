/* ndk - [ console.h ]
 *
 * Basic console IO for kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Console console.h: Basic console IO
* @ingroup core
*
* This file defines a interface for working with multiple consoles
*/
/** @{ */

#ifndef __ndk_console_h__
#define __ndk_console_h__

#include <errorCodes.h>
#include <types.h>

typedef enum _ConsoleColourFormat
{
   ConsoleColour_TFgIBgI_844,
   ConsoleColour_TBgIFgI_844,
} ConsoleColourFormat;

/** TODO: types for Character?  CharacterWide, Character16? */
typedef ErrorCode(*ConsoleCharacterListener) (int c);

ErrorCode consoleClear(void);
ErrorCode consolePutChar(int c);
ErrorCode consoleGetFormat(ConsoleColourFormat * format);
ErrorCode consoleGetFrameBuffer(Pointer *buffer);
ErrorCode consoleGetSize(uint32 *rows, uint32 *cols);

/** TODO: the following could probably be done better... for now, they're just 
* here to prove that the serial driver works (they're also defined in arch, for now, 
* but can possible be moved to independant portion)
*/
ErrorCode consoleEnableCharacterListener(ConsoleCharacterListener charListener);
ErrorCode consoleDisableCharacterListener(ConsoleCharacterListener charListener);

// TODO:
ErrorCode consoleLockScrollWindowRect(uint32 row1, uint32 col1, uint32 row2, uint32 col2);
ErrorCode consoleLockScrollWindow(uint32 row1, uint32 row2);

#endif

/** @} */
