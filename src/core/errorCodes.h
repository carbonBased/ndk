/* ndk - [ errorCodes.h ]
 *
 * General error codes for ndk kerenl
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
�* @defgroup Error errorCodes.h: Error codes
�* @ingroup core
�*
�* This file defines all the possible error codes returned by kernel
 * functions
�*/
/** @{ */

#ifndef __ndk_errorcodes_h__
#define __ndk_errorcodes_h__

/**
 * \todo decide upon error codes... these are 32-bit, should be 16-bit to support
 * 16 bit platforms portably!
 */
/*! All possible error codes returned by ndk functions */
typedef enum _ErrorCode
{
   // no error...
   ErrorNoError = 0,
   // generic error codes
   ErrorNotSupported = 0x01000000,
   ErrorNotImplemented,
   ErrorNotAvailable,
   ErrorNotOwner,
   ErrorNotFound,
   ErrorBadObject,
   ErrorBadParam,
   ErrorResourceInUse,
   ErrorTimedOut,
   ErrorOutOfMemory,
   ErrorOutOfBounds,
   ErrorOutOfState,
   ErrorUnknown,
   // driver specific error codes
   ErrorDriverNotFound = 0x02000000,
   ErrorDriverIsExclusive,
} ErrorCode;

#endif

/** @} */
