/* ndk - [ stack.c ]
 *
 * This source file implements a basic 32-bit stack;  Only longs/dwords
 * can be pushed or poped from the stack.  The stack can expand upwards
 * or downwards (conforming to the Intel definitions).  These stacks are
 * used, primarily, for ndk's memory management.
 *
 * Please see:
 *   /doc/memoryManagement.kwd
 *   /doc/memoryMap.kwd
 *   /src/pager.{c,h}
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "stack.h"

// puts a new value onto the top of the stack, incrementing pointer
void stackPush(Stack *stack, long data)
{
   if(stack->flags & STACK_EXPAND_UP)
   {
      *(long *)(stack->base - (4 * stack->pointer++)) = data;
   }
   else
   {
      *(long *)(stack->base + (4 * stack->pointer++)) = data;
   }
}

// returns current value on top of stack, decrementing pointer
long stackPop(Stack *stack)
{
   if(stack->flags & STACK_EXPAND_UP)
   {
      return *(long *)(stack->base - (4 * --stack->pointer));
   }
   else
   {
      return *(long *)(stack->base + (4 * --stack->pointer));
   }
}

// return stack data without changing pointer
long stackPeek(Stack *stack, long offset)
{
   if(stack->flags & STACK_EXPAND_UP)
   {
      return *(long *)(stack->base - (4 * offset));
   }
   else
   {
      return *(long *)(stack->base + (4 * offset));
   }
}

// change data in stack without changing pointer
void stackPoke(Stack *stack, long offset, long data)
{
   if(stack->flags & STACK_EXPAND_UP)
   {
      *(long *)(stack->base - (4 * offset)) = data;
   }
   else
   {
      *(long *)(stack->base + (4 * offset)) = data;
   }
}

// remove a chunk of 'count' longs from anywhere withen the stack
void stackRemove(Stack *stack, long offset, long count)
{
   long src, dst, tot;

   debugEnter("stackRemove", "0x%8x, %d, %d", stack, offset, count);

   // error out, if trying to remove more than is available
   if(count >= stack->pointer)
   {
      printf("stackRemove > stack->pointer\n");
      return;
   }

   // if we're removing the top-most item, just adjust the stack pointer,
   // no memcpy's needed
   if(offset == stack->pointer - 1)
   {
      printf("stackRemove, stack->pointer == 1\n");
      stack->pointer -= count;
      return;
   }

   // calculate starting/ended address, and total # bytes to move,
   // based on the type of stack
   if(stack->flags & STACK_EXPAND_UP)
   {
      src = stack->base - (4 * (stack->pointer - 1));
      dst = src + (4 * count);
      tot = (stack->base - (4 * offset)) - src;
   }
   else
   {
      src = stack->base + (4 * (stack->pointer - 1));
      dst = src - (4 * count);
      tot = src - (stack->base + (4 * offset));
   }

   // adjust the counter
   stack->pointer -= count;

   //printf("Copying, dst (0x%8x), src (0x%8x), tot (%d)\n", dst, src, tot);
   // and perform the copy
   memcpy(dst, src, tot);

   debugLeave("stackRemove", 0, 0);
}

void stackDisplay(Stack *stack)
{
   long i;

   for(i = 0; i < stack->pointer; i++)
   {
      printf("%3d: 0x%8x\n", i, stackPeek(stack, i));
   }
}

// currently not used, and therefore not implemented...
/*
void stackInsert(Stack *stack, long offset, long *data, long count) {
}
*/
