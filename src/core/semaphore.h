/* ndk - [ semaphore.h ]
 *
 * Routines for implementing semaphores
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

#ifndef __ndk_semaphore__
#define __ndk_semaphore__

#include <brink/localSemaphore.h>
#include <errorCodes.h>
#include <types.h>

typedef struct _Semaphore *Semaphore;
typedef Pointer SemaphoreId;

/**
 * Create a new semaphore
 *
 * @param semaphore The semaphore to create
 * @param type The type of semaphore to create
 * @param initial The initial count of the semaphore
 */
ErrorCode semaphoreCreate(SemaphoreId *semaphoreId, SemaphoreType type, uint32 initial);

ErrorCode semaphoreMap( SemaphoreId semId, Semaphore *sem );

ErrorCode semaphoreUnmap( Semaphore sem );

/**
 * Increment the semaphore count
 *
 * @param semaphore The semaphore whose count will be incremented
 */
ErrorCode semaphoreUp(Semaphore *semaphore);

/**
 * Decrement the semaphore count
 *
 * @param semaphore The semaphore whose count will be decremented
 * @param timeout The ammount of time this function will wait to 
 *                succeed if the count is already 0
 */
ErrorCode semaphoreDown(Semaphore *semaphore, Timeout timeout);

/**
 * Destroy a semaphore
 *
 * @param semaphore The semaphore to destroy
 */
ErrorCode semaphoreDestroy(Semaphore *semaphore);

ErrorCode semaphoreDestroyById(SemaphoreId *semaphore);

#endif

/** @} */
