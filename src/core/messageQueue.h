/* ndk - [ messageQueue.h ]
 *
 * Basic message queue class for IPC
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup MessageQueue messageQueue.h: Basic message queue class
* @ingroup core
*
* This file defines a interface for working with message queues
*/
/** @{ */

#ifndef __ndk_messageQueue_h__
#define __ndk_messageQueue_h__

#include <errorCodes.h>
#include <types.h>

typedef struct _MessageQueue* MessageQueue;
typedef Pointer MessageQueueId;  /* physical page pointer */ 

ErrorCode messageQueueCreate(MessageQueueId *queueId, uint32 size);
ErrorCode messageQueueMap(MessageQueueId queueId, MessageQueue *msgq);
ErrorCode messageQueueUnmap(MessageQueue queue);
ErrorCode messageQueueSend(MessageQueue *mq, Pointer message);
//ErrorCode messageQueueSendAndReceive( MessageQueue *sendTo, MessageId message, MessageId *result );
ErrorCode messageQueuePeek(MessageQueue mq, Pointer *message);
ErrorCode messageQueueReceive(MessageQueue *mq, Timeout to, Pointer *message);
ErrorCode messageQueueDestroy(MessageQueue *mq);
//ErrorCode messageQueueDestroyById(MessageQueueId *mqid);

#endif

/** @} */
