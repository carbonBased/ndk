/* ndk - [ debug.c ]
 *
 * Code I use occasionally to debug the
 * ndk kernel.  Makes life a little
 * easier ;)
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "debug.h"

void debugEnter(char *func, char *pattern, ...)
{
#ifdef DEBUG_ENTER_LEAVE
   char **arg = (char **)&pattern;

   arg++;

   printf("+ %s: [", func);
   printf(pattern, arg);
   printf("]\n");
#endif
   return;
}

void debugLeave(char *func, char *pattern, ...)
{
#ifdef DEBUG_ENTER_LEAVE
   char **arg = (char **)&pattern;

   arg++;

   printf("- %s: [", func);

   if(pattern)
      printf(pattern, arg);
   else
      printf("void");

   printf("]\n");
#endif
   return;
}
