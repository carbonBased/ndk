/* ndk - [ stack.h ]
 *
 * Expand up, or down, stack class
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Stack stack.h: Abstract stack class
* @ingroup core
*
* This file defines a interface for working with abstract stacks
*/
/** @{ */

#ifndef __ndk_stack_h__
#define __ndk_stack_h__

#include "console.h"
#include <types.h>

/* Because I've maintained Intel's definitions for expand up
 * and expand down for the _real_ stack (ie, ss:esp), I've
 * continued to use them here.  Unfortunately, that means the
 * definitions are counter-intuitive... however, they
 * are consistent with the architecture...
 */
#define STACK_EXPAND_UP   (1)
#define STACK_EXPAND_DOWN (0)

/** @TODO: use pointer types! */
typedef struct
{
   uint32 flags;
   //long  total;
   uint32 pointer;
   uint32 base;
} Stack;

void stackPush(Stack *stack, long data);
long stackPop(Stack *stack);
long stackPeek(Stack *stack, long offset);
void stackPoke(Stack *stack, long offset, long data);
void stackRemove(Stack *stack, long offset, long count);
void stackInsert(Stack *stack, long offset, long *data, long count);
void stackDisplay(Stack *stack);

#endif

/** @} */
