#include "physicalMemoryDescriptor.h"

#include <assert.h>

#define PHYS_DESC_PAGE_PAGE_MASK (0xffffe000)
/** 
 * inlined has two meanings:
 * if( !lastElement ) { read_page_normally && rest_of_content_follows_descriptor }
 * else { read_page_as_raw_content }
 */
/* 
#define PHYS_DESC_PAGE_INLINED   (0x00000001)
#define PHYS_DESC_PAGE_IS_DESC   (0x00000002)
#define PHYS_DESC_PAGE_LENGTH    (0x00000ffc)   // length is dword granularity

#define PHYS_DESC_SIZE_INLINED   (0x80000000) 
*/

#define PHYS_DESC_MAX_PAGES      (1023)

#define DESCRIPTOR_LENGTH        (PAGE_SIZE)
#define DESCRIPTOR_LENGTH_MASK   ((PAGE_SIZE)-1)
#define DESCRIPTOR_ADDR_MASK     (-1 - DESCRIPTOR_LENGTH_MASK)

#define DescriptorIsInline(d) (((uint32)d) & DESCRIPTOR_LENGTH_MASK)
#define DescriptorGetSize(d) ((((uint32)d) & DESCRIPTOR_LENGTH_MASK) + 1)
#define DescriptorSetSize(d,s) if( s > 0 ) d = (PhysicalAddress)( (uint32)(d) | ((s)-1) );
#define DescriptorAddress(d) (PhysicalAddress)(((uint32)d) & DESCRIPTOR_ADDR_MASK)

/* upon overflow 'size' refers to the physical memory pointer for this page */
typedef struct _PhysDesc
{
   uint32 size;
   PhysicalAddress page[ PHYS_DESC_MAX_PAGES ];
} PhysDesc;

ErrorCode physicalMemoryDescriptorCreate( PhysicalMemoryDescriptor *descId, LinearAddress base, uint32 size )
{
   ErrorCode ec = ErrorNoError;
   PhysDesc *desc;
   uint32 descSize = 0;
   
   printf("physicalMemoryDescriptorCreate(%x,%x,%d) PAGE_SIZE=%d\n", descId, base, size, PAGE_SIZE);
   
   if( descId )
   {
      /* initially allocate to the kernel heap */
      ec = pagerAllocate( pagerHeapKernel, 1, (Pointer*)&desc );
      if( ec == ErrorNoError )
      {
         /* descriptor is the physical page pointer of the desc struct */
         printf("__getting phys of %x\n", desc );
         pagerGetPhysical( desc, (Pointer*)descId );
         printf("__got %x\n", *descId );
         
         /* check if content can be inlined on one page */
         if( size <= PAGE_SIZE )
         {
            printf("creating inlined descriptor\n");
            /* if so, returned inline descriptor */
            descSize = size;
            
            memcpy( (Pointer)desc, base, size );
         }
         else
         {
            LinearAddress linearPage, sourcePointer = base;
            PhysicalAddress physicalPage;
            uint32 pageIndex = 0;

            printf("desc == 0x%8x\n", desc);
            desc->size = size;
            
            /* otherwise keep mapping pages until we're done */
            while( size )
            {
               /* allocate a new page to hold rest of content */
               ec = pagerAllocate( pagerHeapKernel, 1, (Pointer*)&linearPage );
               if( ec == ErrorNoError )
               {
                  uint32 toCopy = size > PAGE_SIZE ? PAGE_SIZE : size;

                  printf("copying %x to %x bytes %d\n", linearPage, sourcePointer, toCopy);
                  memcpy( linearPage, sourcePointer, toCopy );
                  size -= toCopy;
                  sourcePointer += toCopy;
                  
                  pagerGetPhysical( linearPage, (Pointer*)&physicalPage );
                  desc->page[pageIndex] = physicalPage;
                  
                  if( pageIndex == PHYS_DESC_MAX_PAGES - 1 )
                  {
                     /* if we just mapped the last page, then it must've been inlined
                      * (see below)
                      */
                      DescriptorSetSize( desc->page[pageIndex], toCopy );
                  }
                  
                  pageIndex++; 
                  
                  pagerUnmap( linearPage );
                  
                  if( (pageIndex == PHYS_DESC_MAX_PAGES - 1) && (size > PAGE_SIZE) )
                  {
                     PhysDesc *oldDesc;
                     
                     /* we're at the end of this list, need to map another descriptor */
                     oldDesc = desc;

                     ec = pagerAllocate( pagerHeapKernel, 1, (Pointer*)&desc );
                     if( ec == ErrorNoError )
                     {
                        pagerGetPhysical( desc, (Pointer*)&physicalPage );
                        oldDesc->page[pageIndex] = physicalPage;
                        
                        /* do we need a self pointer? */
                        desc->size = 0;
                        pageIndex = 0;
                     }
                     else
                     {
                        assert(0);
                     }
                     
                     pagerUnmap( oldDesc );
                  }
               }
               else
               {
                  assert(0);
               }
            }
         }
         
         /* free the linearly mapped page in the kernel heap, but don't 
          * deallocate it (it should still be removed from the pager 
          * free stack
          */
         pagerUnmap( desc );
         
         printf("___descId before %x, size = %d\n", *descId, descSize );
         /* OR in the size (indication of an inline descriptor) if it exists */
         DescriptorSetSize( *descId, descSize );
         printf("___descId after %x\n", *descId );
      }      
   }
   else
   {
      ec = ErrorBadObject;
   }
   
   printf("returning descriptor %x\n", *descId);
   
   return ec;
}

ErrorCode physicalMemoryDescriptorDestroy( PhysicalMemoryDescriptor desc )
{
   ErrorCode ec = ErrorNoError;
   
   printf("physicalMemoryDescriptorDestroy(%x)\n", desc);
   
   if( desc )
   {
      if( DescriptorIsInline( desc ) )
      {
         pagerFreePhysical( DescriptorAddress( desc ) );
      }
      else
      {
         /** @TODO... */
      }
   }
}

ErrorCode physicalMemoryDescriptorGetSize( PhysicalMemoryDescriptor desc, uint32 *size )
{
   ErrorCode ec = ErrorNoError;
   
   printf( "physicalMemoryDescriptorGetSize %x\n", desc );
   
   if( desc )
   {
      if( DescriptorIsInline( desc ) )
      {
         *size = DescriptorGetSize( desc );
         printf("inline get size %d\n", *size);
      }
      else
      {
         LinearAddress linearAddr;
         
         ec = pagerMap( pagerHeapKernel, DescriptorAddress( desc ), &linearAddr );
         if( ec == ErrorNoError )
         {
            PhysDesc *desc = (PhysDesc*)linearAddr;
            
            printf("found size of %d\n", desc->size );
            *size = desc->size;
            
            pagerUnmap( linearAddr );
         }
      }
   }
   else
   {
      ec = ErrorBadObject;
   }
   
   return ec;
}

ErrorCode physicalMemoryDescriptorMap( PhysicalMemoryDescriptor desc, LinearAddress *base )
{
   ErrorCode ec = ErrorNoError;
   LinearAddress linearAddr;
   
   printf("physicalMemoryDescriptorMap(%x,%x)\n", desc, base );
   
   if( desc )
   {
      /* single inline descriptor is easy... */
      if( DescriptorIsInline( desc ) )
      {
         printf("mapping inline descriptor %x, addr=%x\n", desc, DescriptorAddress( desc ) );
         /* map to shared heap of current process */
         ec = pagerMap( pagerHeapShared, DescriptorAddress( desc ), base );
         
         /** @TODO: remove */
         //pagerInvalidate();
      }
      else
      {
         printf("mapping non-inline descriptor\n");
         
         ec = pagerMap( pagerHeapKernel, DescriptorAddress( desc ), &linearAddr );
         if( ec == ErrorNoError )
         {
            PhysDesc *desc = (PhysDesc*)linearAddr;
            uint32 size, numPages, pagePtr = 0;
            
            size = desc->size;
            numPages = (size & PAGE_SIZE_MASK) ? 
                       (size + PAGE_SIZE) / PAGE_SIZE :
                       (size / PAGE_SIZE); 
            //( size + ~(size & PAGE_SIZE_MASK) ) / PAGE_SIZE;
            
            printf("size is %d pages %d\n", size, numPages );
            
            /* reserve enough contiguous pages in the shared heap... */
            pagerReserve( pagerHeapShared, numPages, &linearAddr );
            
            printf("reserved %d pages at %x\n", numPages, linearAddr );
            *base = linearAddr;
            
            /* and then set them to their correct physical addresses... */
            while( numPages-- )
            {
               pagerMapPhysicalToLinear( DescriptorAddress(desc->page[pagePtr]), 
                                         linearAddr);
               printf("mapped %x to %x\n", DescriptorAddress(desc->page[pagePtr]), linearAddr );
               
               pagePtr++;
               linearAddr = (LinearAddress)( (uint32)linearAddr + PAGE_SIZE );
               
               /* the last pointer points to a descriptor, unless it's inlined */
               if( (pagePtr == PHYS_DESC_MAX_PAGES - 1) &&
                    !DescriptorIsInline( desc->page[pagePtr] ) )
               {
                  PhysDesc *oldDesc;
                  
                  printf("Last pointer -> load new descriptor\n");
                  
                  /* must load the next descriptor */
                  oldDesc = desc;
                  
                  ec = pagerMap( pagerHeapKernel, DescriptorAddress( desc->page[pagePtr] ), (Pointer*)&desc );
                  if( ec == ErrorNoError )
                  {
                     pagePtr = 0;
                     pagerUnmap( oldDesc );
                  }
                  else
                  {
                     assert(0);
                  }
               }
               else if( pagePtr == PHYS_DESC_MAX_PAGES )
               {
                  printf("Last pointer -> inline descriptor\n");
               }
            }
         }
         else
         {
            assert(0);
         }
      }
   }
   
   return ec;
}

/** do we even need this?  Just use pagerUnmapRange( LinearAddress lin, uint32 numPages); */
ErrorCode physicalMemoryDescriptorUnmap( PhysicalMemoryDescriptor desc, LinearAddress base )
{
}
