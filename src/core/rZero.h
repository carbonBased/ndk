/* ndk - [ rZero.h ]
 *
 * Interface for loading/interacting with rZero modules
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup RZero rZero.h: rZero modules
 * @ingroup core
 *
 * Functions for loading and linking rZero modules into the kernel
 * at run time
 */
/** @{ */

#ifndef __ndk_rZero_h__
#define __ndk_rZero_h__

#include "multiboot.h"
#include "console.h"
#include "string.h"
#include "memory.h"
#include "timer.h"
#include "ndk.h"
#include "io.h"

typedef struct
{
   char *symbolName;
   long symbolLocation;
   long segmentNum;
} rZeroSymbolInfo;

typedef struct
{
   short type;
   short number;
   short reserved;
   long length;
   long offset;
} rZeroSegment;

typedef struct
{
   char *name;
   long version;
   long headerLength;
   long headerOffset;
   long length;
   long pos;
   char *mem;
   char *end;
   long numSegments;
   rZeroSegment segment[3];     // NOTE: temporary! Should be dynamically allocated!
   // or just 3 (max segs for RDOff?)
} rZeroObj;

extern rZeroSymbolInfo rZeroExports[];
extern rZeroSymbolInfo rZeroImports[];

void rZeroParse(rZeroObj * obj);
void rZeroParseHeader(rZeroObj * obj);
void rZeroParseSegments(rZeroObj * obj);
void rZeroClearSegments(void);
int rZeroFindSymbol(char *symbol, rZeroSymbolInfo * symInfo);
int rZeroFindSegment(short segment, rZeroSymbolInfo * symInfo);

long volatile readLong(rZeroObj * obj);
short volatile readShort(rZeroObj * obj);
char volatile readByte(rZeroObj * obj);
#endif

/** @} */
