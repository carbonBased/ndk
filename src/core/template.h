/* ndk - [ header.h ]
 *
 * description
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup HeaderCaps header.h: Description
 * @ingroup core
 *
 * More description
 */
/** @{ */

#ifndef __ndk_header_h__
#define __ndk_header_h__

#include <errorCodes.h>
#include <types.h>

typedef struct _Thing *Thing;

#endif

/** @} */
