/* ndk - [ queue.h ]
 *
 * Basic abstract queue class
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Queue queue.h: Abstract queue class
* @ingroup core
*
* This file defines a interface for working with simple queues
*/
/** @{ */

/** \TODO: cleanup this header!  Divide it into Object and ObjectParser,
    possibly also adding an ObjectSection and ObjectSymbol API */

#ifndef __ndk_object_h__
#define __ndk_object_h__

#include <brink/list.h>
#include <errorCodes.h>
#include <types.h>

typedef struct _Object *Object;
typedef struct _ObjectParser *ObjectParser;
typedef struct _ObjectSection *ObjectSection;

struct _Object
{
   String name;
   uint8 *base;
   uint32 length;
   ObjectParser parser;
   List sections;
   ListIterator sectionsIterator;
   List imports;
   ListIterator importsIterator;
   List exports;
   ListIterator exportsIterator;
   List relocations;
   ListIterator relocationsIterator;
   void *privateData;
};

typedef struct _ObjectSymbol
{
   String symbolName;
   uint32 symbolId;
   uint32 symbolLocation;
   uint32 sectionNumber;
   ObjectSection section;
} *ObjectSymbol;

typedef struct _ObjectRelocation
{
   ObjectSymbol symbol;         /* if this is a symbol relocation, this is the import symbol we want */
   ObjectSection section;       /* if this is a section relocation, this is the section we want */
   uint32 symbolId;             // remove?
   uint32 sectionId;            // remove?
   ObjectSection sourceSection; /* this is the section where the relocation exists */
   uint32 location;
   uint8 length;
   Boolean relative;
} *ObjectRelocation;

struct _ObjectSection
{
   String name;
   uint32 id;
   uint32 offset;
   uint32 length;
   uint8 alignment;             // byteAlignment = pow(2, alignment)
   List symbols;
   ListIterator symbolIterator;
};

// move these into an objectParser file?
typedef ErrorCode(*ObjectParserCreateFunction) (Object obj, void *buffer, uint32 length);
typedef ErrorCode(*ObjectParserDestroyFunction) (Object obj);

struct _ObjectParser
{
   String name;
   ObjectParserCreateFunction create;
   ObjectParserDestroyFunction destroy;
};

/**
 * objectCreate() will take the C object code pointed to by 'buffer' and
 * gather all the appropriate information from it (symbols, sections, etc)
 */
ErrorCode objectCreate(Object *obj, String name, void *buffer, uint32 length);
ErrorCode objectCreateEmpty(Object *obj);
ErrorCode objectDestroy(Object *obj);
ErrorCode objectGetImportList(Object obj, List *imports);
ErrorCode objectGetExportList(Object obj, List *exports);
ErrorCode objectGetSectionIterator(Object obj, ListIterator * iterator);
ErrorCode objectGetSectionList(Object obj, List *sections);
ErrorCode objectSetBase(Object obj, void *base);
ErrorCode objectLink(Object obj1, Object obj2);
ErrorCode objectDump(Object obj);

ErrorCode objectParserInit(void);
ErrorCode objectParserFinal(void);
ErrorCode objectParserAdd(String formatName, ObjectParserCreateFunction create,
                          ObjectParserDestroyFunction destroy);
ErrorCode objectParserRemove(ObjectParser parser);

ErrorCode objectParserCreateSection(ObjectSection *section, String name, uint32 id, uint32 offset,
                                    uint32 length, uint32 alignment);
ErrorCode objectParserAddSection(Object obj, ObjectSection section);
ErrorCode objectParserGetSection(Object obj, String name, ObjectSection *section);
ErrorCode objectParserGetSectionById(Object obj, uint32 id, ObjectSection *section);

ErrorCode objectParserCreateSymbol(ObjectSymbol *symbol, String name, uint32 id, uint32 location);

//ErrorCode objectParsetSymbolSetSection(ObjectSymbol symbol, ObjectSection section);
//ErrorCode objectParserAddSymbol(ObjectSection section, ObjectSymbol symbol);
// above, or below...
ErrorCode objectParserAddImport(Object obj, ObjectSymbol import);
ErrorCode objectParserAddExport(Object obj, ObjectSymbol export, ObjectSection section);

ErrorCode objectParserGetImportById(Object obj, uint32 id, ObjectSymbol *symbol);
ErrorCode objectParserGetExportById(Object obj, uint32 id, ObjectSymbol *symbol);

ErrorCode objectParserGetImportByName(Object obj, String name, ObjectSymbol *symbol);
ErrorCode objectParserGetExportByName(Object obj, String name, ObjectSymbol *symbol);

/**
 * Create a relocation record for a symbol at a certain location and length.
 * This relocation can be relative, or absolute.
 *
 * @param reloc		The relocation to create
 * @param section		The section where this relocation exists
 * @param symbol		A symbol which this object has defined as an import
 * @param location	The location where this relocation should be, relative to section start
 * @param length		The length, in bytes, of the symbol relocation
 * @param relative	True if this relocation is relative to the current instruction, or
 *							false is absolute.
 */
 /* previous
    ErrorCode objectParserCreateRelocation(ObjectRelocation *reloc, ObjectSection section, ObjectSymbol symbol,
    uint32 location, uint8 length, Boolean relative);
  */
ErrorCode objectParserCreateSymbolRelocation(ObjectRelocation *reloc, ObjectSection sourceSection,
                                             uint32 offset, ObjectSymbol symbol, uint8 length,
                                             Boolean relative);
ErrorCode objectParserCreateSectionRelocation(ObjectRelocation *reloc, ObjectSection sourceSection,
                                              uint32 offset, ObjectSection section, uint8 length,
                                              Boolean relative);
ErrorCode objectParserAddRelocation(Object obj, ObjectRelocation rec);

ErrorCode objectParserSetPrivateData(Object obj, void *privateData);
ErrorCode objectParserGetPrivateData(Object obj, void **privateData);

#endif

/* @} */
