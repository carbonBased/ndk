/* ndk - [ propDb.h ]
 *
 * Database of run/build-time property
 *
 * (c)2006 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup PropDb propDb.h: Property Database
* @ingroup core
*
* This file defines an interface for querying build/run-time properties.
* These properties can be stored as either #defines or as an internal
* array.  The defines execute faster while array data can be easily
* changed in a hex editor.
*
*/
/** @{ */

#ifndef __ndk_propdb_h__
#define __ndk_propdb_h__

#include <generated/properties.h>
#include <types.h>

#if (Property_propdb_useArrays == 1)

/*
#define propertyGetAsString(x, v) \
   propertyGetStringFromArray(#x, v)

#define propertyGetAsInteger(x, v) \
   propertyGetIntegerFromArray(#x, v)
*/

#define propertyGetAsString(x) \
   propertyGetStringFromArray(#x)

#define propertyGetAsInteger(x) \
   propertyGetIntegerFromArray(#x)


#else

/*
#define propertyGetAsString(x, v) \
   (String)*v = (String)PropertyQuoted_##x

#define propertyGetAsInteger(x, v) \
{ \
   uint32 **u = (uint32)&v; \
   *u = (uint32)(Property_##x); \
   //*v = (uint32)(Property_##x) \
}
*/

#define propertyGetAsString(x) \
   PropertyQuoted_##x

#define propertyGetAsInteger(x) \
   Property_##x

#endif

/** TODO: string length? */
/*
ErrorCode propertyGetStringFromArray(String propName, String *value);
ErrorCode propertyGetIntegerFromArray(String propName, Pointer value);
*/
String propertyGetStringFromArray(String propName);
uint32 propertyGetIntegerFromArray(String propName);

#endif

/** @} */
