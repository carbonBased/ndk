/* ndk - [ mutex.h ]
 *
 * Routines for implementing mutual exclusion
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup mutex mutex.h: mutex routines
 * @ingroup core
 *
 * Defines interfaces for implementing mutual exclusion
 */
/** @{ */

#ifndef __ndk_mutex_h__
#define __ndk_mutex_h__

#include <errorCodes.h>
#include <types.h>
#include <brink/localMutex.h>

typedef struct _Mutex *Mutex;
typedef Pointer MutexId;

// TODO: implement all these correctly! (and rename to LocalMutexType, or keep?)
/*
typedef enum _MutexType
{
   MutexTypeRecursive = 0,
   MutexTypeNonRecursive = 1,
   MutexTypeFIFO = 0,
   MutexTypePriority = 2
} MutexType;
*/

/**
 * @param mutex Pointer to a mutex id handle, which will contain
 *              a valid mutex upon completion.
 * @param type  The type of mutex to be created
 */
ErrorCode mutexCreate(MutexId *mutex, MutexType type);

ErrorCode mutexMap(MutexId mutexId, Mutex *mutex);

/* id or pointer */
ErrorCode mutexUnmap(Mutex mutex);

/**
 * @param mutex   The mutex to lock
 * @param timeout The time to wait for the mutex to be available
 *                for locking.
 */
ErrorCode mutexLock(Mutex *mutex, Timeout timeout);

/**
 * @param mutex The mutex to unlock
 */
ErrorCode mutexUnlock(Mutex *mutex);

/**
 * @param mutex The mutex to destroy
 */
ErrorCode mutexDestroy(Mutex *mutex);

//ErrorCode mutexDestroyById(MutexId *mutex);

#endif

/** @} */
