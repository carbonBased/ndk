/* ndk - [ ndk.c ]
 *
 * Contains the main source file for the ndk
 * operating system.  This is where the system
 * comes alive!
 *
 * Please see:
 *   /doc/*
 *   /src/*
 *
 * (c)2002 dcipher / neuraldk
 *           www.neuraldk.org
 */

// TODO: these should only be API includes, not arch includes!
#include "multiboot.h"
#include "pic.h"
#include "idt.h"
#include "gdt.h"
#include "task.h"
//#include "vm86.h"
#include "brink/localMutex.h"
#include "rZero.h"
#include "pager.h"
#include "pmode.h"
#include "memory.h"
#include "system.h"
#include "console.h"
#include "descgate.h"
#include "brink/localMessageQueue.h"
#include "objectKernel.h"
#include <core/messageQueue.h>
#include <core/pageBasedList.h>
#include <stddef.h>
#include <object.h>
#include <driver.h>
#include <propDb.h>
//#include <driverManager.h>

#include <objectRDOFF2.h>

// search for VIDINC to make tasks increment video memory again

/* The following are for the multitasking test
 * I can't use local variables, because they're
 * assigned to the stack in C, and I haven't
 * defined separate stacks for each tasks yet ;)
 */
char *videoOne = (char *)0x000B8000 + 0xA0;
char *videoTwo = (char *)0x000B8002 + 0xA0;
char *videoThree = (char *)0x000B8004 + (0xA0 * 11);
char *videoFour = (char *)0x000b8004 + (0xA0 * 12);

#define TASK_STACK_SIZE 4096
#define MAX_TASKS	5

Task task[MAX_TASKS];

//TSS  tss[MAX_TASKS];
char *p0_stack[MAX_TASKS][TASK_STACK_SIZE];
char *p3_stack[MAX_TASKS][TASK_STACK_SIZE];

/*
char *stackOne[TASK_STACK_SIZE];
char *stackTwo[TASK_STACK_SIZE];
char *stackThree[TASK_STACK_SIZE];
Task taskOne, taskTwo, taskMain, taskGateTask;
TSS  tssOne,  tssTwo,  tssMain, tssGateTask;
*/

void jobOne(void);
void jobTwo(void);
void jobGateTask(void);

void temp(void);

int bssCheck;

//char *regPrint = "->   Reg: 0x%x   .\n";
void testTimerFunc(void *arg);

void testTimerFunc(void *arg)
{
   printf("Test Timer Function\n");
}

void idleEntry(void *arg);

void stackTraceTest(void);
void stackTest2(void);

void stackTraceTest(void)
{
   stackTest2();
}

void stackTest2(void)
{
   systemAssert(__LINE__, __FILE__);
}

typedef struct _MyStruct
{
   uint32 data1;
   uint8 data2, data3;
} MyStruct;

void manualStackCreation( uint32 one, uint32 two, MyStruct three, uint32 four )
{
   printf("manualStackCreation %d %d %d %d %d %d\n", one, two, three.data1, three.data2, three.data3, four);
}

extern multiboot_info_t *multibootInfo;

#define stackBegin()                                              \
{                                                                   \
   uint32 numDwords, dwordCount=1; /* remember the return address */

#define pushImm( val )                                            \
   asm("pushl %0\n"::"g" (val) );                                  \
   dwordCount++;
   
#define pushVal( base, size )                                    \
   numDwords = (size + 2) / 4;                                     \
   while( numDwords-- )                                           \
   {                                                               \
      asm("pushl %0\n"::"g" ( ((uint32*)base)[numDwords] ) );     \
      dwordCount++;                                                \
   }

#define call( func )                                             \
   asm("call *%0\n"::"r" (func) );
   
#define stackEnd()                                               \
   asm("add %0, %%esp\n"::"g" (dwordCount*4));                    \
}


void ndkStart(unsigned long magic, unsigned long addr)
{
   /* div by zero test! */
   void (*rZeroFunction) (void *);
   void (*rZeroSetProgress) (long);
   int x = 3, y = 0;
   short *rmIDT = (short *)0x00001000;
   short c;
   long cs, eip;
   long *pages = (long *)0xffc00000;
   MyStruct myStruct;

   int i;
   long progress = 0;

   long linearAddr;
   long physAddr;
   void *temp;

   Pointer mbase, kbase;
   uint32 msize, ksize;

   ErrorCode ec;
   Task splashTask;
   Task idleTask;
   Timer testTimer;
   LocalMutex testMutex;
   MessageQueueId splashQueueId;
   MessageQueue splashQueue;
   DriverContext drvCtx;
   module_t *mod;               // multiboot module structure

   /* for the time being, I'll keep this here */
   //mb_main(magic, addr);

   // soon enough, all the architecture dependant stuff will be in the
   // archetectureInit() function... there's x86 stuff all over the place right now!!!
   if(architectureInit(magic, addr) != ErrorNoError)
   {
      // what to do here?  No console IO yet... need a method of telling user of an error that
      // doesn't rely on initialization...
   }

   driverManagerInit();

   // NOTE: these are hardcoded, and should be changed!!!
   rZeroFunction = (void (*)(void *))rZeroImports[4].symbolLocation;
   rZeroSetProgress = (void (*)(long))rZeroImports[3].symbolLocation;

   localMutexCreate(&testMutex, MutexTypeRecursive | MutexTypeFIFO);
   printf("Creating mutex %x\n", testMutex);
   localMutexLock(testMutex, TimeoutInfinite);

   // create the idle task
   taskCreate(&idleTask, NULL, "idle", idleEntry, NULL, TaskTypeOS, 4096, 255);  // lowest priority

   printf("Message queue is %x %x\n", splashQueueId, splashQueue);

   printf("init parsers\n");
   objectParsersInit();
   printf("init rdoff2\n");
   rdoff2_driverInit();

   objectKernelInit();

   temp = splashScreen;

   /* loop through all the modules, and parse/link them
    * into the kernel
    */
   mod = (module_t *) multibootInfo->mods_addr;
   for(i = 0; i < multibootInfo->mods_count; i++)
   {
      Object obj;
      Driver drv;

      //obj.mem = (char *)mod[i].mod_start;
      //obj.end = (char *)mod[i].mod_end;
      //obj.name = (char *)mod[i].string;
      printf("Inspecting boot module %s\n", mod[i].string);
      ec =
         objectCreate(&obj, (char *)mod[i].string, (void *)mod[i].mod_start,
                      mod[i].mod_end - mod[i].mod_start);
      if(ec == ErrorNoError)
      {
         ObjectSymbol symbol;
         
         objectLink(objectKernel, obj);
         objectDump(obj);

         /* perhaps this is a driver object? */
         //ec = driverCreateFromObject(&drv, obj);
         if(objectParserGetExportByName(obj, "main", &symbol) == ErrorNoError)
         {
            Task newTask;
            void (*taskEntry) (void *) =
               (Pointer)((PointerType)obj->base + (PointerType)symbol->section->offset +
                         (PointerType)symbol->symbolLocation);
                         
            printf("Creating new task with entry 0x%8x (0x%8x + 0x%8x + 0x%8x)\n", 
               taskEntry, obj->base, symbol->section->offset, symbol->symbolLocation );
            
            printf("symbol 0x%8x symbol->section 0x%8x\n", symbol, symbol->section);
                         
            taskCreate( &newTask, NULL, "drv", taskEntry, NULL, TaskTypeOS, 8192, 0 );
            taskSetObject( newTask, obj );
         //if(ec == ErrorNoError)
         //{
            /* cool, we've got a driver, add it to the manager */
            //printf("___got a driver\n");
            //driverManagerAdd(drv);
            
            /* wait 15s for task to be created and run... */
            timerDelay( 7000 );
            
            
            printf("DriverManager tree is:\n");
            driverManagerShowTree();
            
            //timerDelay( 5000 );
         
            driverContextCreate(&drvCtx, "io.serial(0)");
            driverContextExecute(drvCtx, "trace.enable(1)");
         
            driverContextExecute(drvCtx, "put(65)");
            driverContextExecute(drvCtx, "put(66)");
            driverContextExecute(drvCtx, "put(67)");
         }
         else
         {
            printf("___no driver\n");
         }
         //printf("___mod %d %d\n", mod[i].mod_start, mod[i].mod_end);
      }
   }

   printf("Usable memory: [Lower: %dkb, Upper %dkb]\n", multibootInfo->mem_lower,
          multibootInfo->mem_upper);
   
   //*
   printf("splash screen is %x\n", splashScreen);
   printf("orig splash screen is -> %x\n", temp);

   // create a limited message queue, for now
   messageQueueCreate(&splashQueueId, 100);
   messageQueueMap( splashQueueId, &splashQueue );

   printf("++++ creating splash screen task!\n");
   taskCreate(&splashTask,
              NULL,
              "splashscreen",
              splashScreen,
              (void*)splashQueueId, //0x11223344,
              TaskTypeOS,
              4096, // stack size
              128); // priority
   // try to call directly...
   //splashScreen( splashQueueId );

   // fake initialization
   for(i = 0; i <= 100; i+=10)
   { 
      timerDelay(1000);
      printf("Sending message to queue of %d\n", i);
      messageQueueSend(&splashQueue, (Pointer)i);
   }
   //*/

   if(testsInit() == ErrorNotAvailable)
   {
      printf("Testing N/A\n");
   }
   else printf("Testing available\n");
   testsStart();
   
   for(i = 0; i < 10; i++)
   {
      printf("___testing %d \n", i);
   }
   
   //pagerDumpStats();
   
   /*
   i = 2;
   myStruct.data1 = 10;
   myStruct.data2 = 15;
   myStruct.data3 = 20;
   
   // remember to push in opposite order!
   stackBegin();
   pushImm( 55 );
   pushVal( &myStruct, sizeof(MyStruct) );
   pushImm( i );
   pushImm( 1 ); // first param!
   call(manualStackCreation); // line 3872
   stackEnd();
   
   
   i = 4;
   myStruct.data1 = 20;
   myStruct.data2 = 30;
   myStruct.data3 = 40;
   
   // remember to push in opposite order!
   stackBegin();
   pushImm( 110 );
   pushVal( &myStruct, sizeof(MyStruct) );
   pushImm( i );
   pushImm( 2 ); // first param!
   call(manualStackCreation); // line 3872
   stackEnd();
   */
   

   while(1)
   {
      printf("main task is finished\n");
      printf("hlt loop\n");
      asm("stop: \n hlt \n jmp stop\n");
   }
   return;
}

void idleEntry(void *arg)
{
   uint32 i = 0;

   while(1)
   {
      // tight inner loops aren't good for idle threads... this
      // should really do something... that's a TODO! :)
      i++;
      if((i % 10000) == 0)      /*i/=2;// */
         printf("idle %d\n", i);
      asm("hlt");
   }
}

/* multitasking test!!! */
void jobOne(void)
{
   // Should NOT execute, because of IOPL!
   //asm("cli");
   //printf("Task Gate Link: 0x%x, EFlags: 0x%x", tss[3].link, tss[3].eflags);
   //if(tss[3].eflags & 16384)
   //  printf("NT\n");
   //else printf("\n");

   printf("Task one\n");
   //asm("int $128\n");
 loopOne:
   //VIDINC (*videoOne)++;
   // for overflow flag... just a test
   /*
      asm("movl $0xffffffff, %%eax\n"
      "addl $0x1, %%eax\n"
      :
      :
      : "eax");
    */
   goto loopOne;
}

void jobTwo(void)
{
   //printf("Task Gate Link: 0x%x, EFlags: 0x%x", tss[3].link, tss[3].eflags);
   /*
      asm(
      //"pushl %0\n"
      "pushl %1\n"
      "pushl %0\n"
      "call printf\n"
      "addl $8, %%esp\n"
      :
      : "g" (regPrint), "g" (tss[3].eflags)
      );
      if(tss[3].eflags & 16384)
      printf("NT\n");
      else printf("\n");

    */
   //printf("task 1 eflags: 0x%x .\n", tss[1].eflags);
 loopTwo:
   // VIDINC (*videoTwo)++;
   //asm("int $128\n");
   goto loopTwo;
}

/*
asm(
  ".global temp\n"
  "temp:\n"
  "jmp temp\n"
  */
  /*
     "push %eax\n"
     "push %ds\n"
     "pusha\n"
     "mov $0x08, %ax\n"
     "mov %ax, %ds\n"
     "call jobGateTask\n"
     "popa\n"
     "pop %ds\n"
     "pop %ax\n"
   */
  /*
     "iretl\n"
     // following line needed because it's a task gate!!!
     "jmp temp\n");
   */

/* This is a task gate... somewhere here this gate must be
 * added to the list of on-going tasks...
 */
 /*
    void jobGateTask(void) {
    //int i;
    //for(i = 0; i < 1000; i++)
    //printf("->Task Gate Link: 0x%x, EFlags: 0x%x\n", tss[3].link, tss[3].eflags);

    asm(
    //"pushl %0\n"
    "pushf\n"
    "pushl %0\n"
    "call printf\n"
    "addl $8, %%esp\n"
    :
    : "g" (regPrint)
    );
    loopThree:
    (*videoThree)++;
    goto loopThree;
    } */
