/* ndk - [ array.h ]
 *
 * Basic/abstracted array class
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Array array.h: abstract array class
* @ingroup core
*
* This file defines a interface for working with abstract arrays
*/
/** @{ */

#ifndef __ndk_array_h__
#define __ndk_array_h__

typedef struct
{
   long baseAddress;
   long numElements;
   long sizeOfElement;
} Array;

void *arrayElementAt(Array *array, long eNum);
long arrayFindNthEmptyElement(Array *array, int n);
long arrayIsElementEmpty(Array *array, long eNum);

#endif

/** @} */
