/* ndk - [ message.h ]
 *
 * Basic message class for IPC
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup MessageQueue message.h: Basic message class
* @ingroup core
*
* This file defines a interface for a common message type
*/
/** @{ */

#ifndef __ndk_message_h__
#define __ndk_message_h__

#include <core/errorCodes.h>
#include <core/types.h>
#include <core/messageQueue.h>

typedef struct _Message *Message;

typedef struct _MessageParam
{
   Pointer address;
   PointerType size;
} MessageParam;

#define MessageParamInteger( x )   &x, sizeof(PointerType)
#define MessageParamPointer( x )   &x, sizeof(Pointer)
#define MessageParamType( x, t )   &x, sizeof(t)
#define MessageParamTypePtr( x, t ) x, sizeof(t)
#define MessageParamString( x )     x, strlen(x)
#define MessageParamBuffer( x, s )  x, s
#define MessageParamNull            0, 0

typedef ErrorCode(*MessageDestroyFunction) (Message msg);

#define MESSAGE_MAX_SIZE   (65536)

typedef PointerType MessageId;
/**
 * Messages are limited to 16kb (4 pages, on x86).  This should be more then enough 
 * room for a decent sized message! 
 */
/** @TODO: get rid of MessageReturnPath and support only MessageQueue's? */
/** @TODO: is a destroy function really useful in separate address space?  Must be implemented in shared mem */ 
ErrorCode messageCreate(MessageId *msgId, MessageDestroyFunction destroy, 
                        MessageQueueId responseQueueId, Message inResponseto,
                        uint32 messageSenderId, uint32 code, uint32 numArgs, ...);
ErrorCode messageCreateSimple( MessageId *msgId, uint32 senderId, uint32 code, uint32 numArgs, ... ); /** @TODO */
ErrorCode messageMap( MessageId msgId, Message *msg );
ErrorCode messageSetResponseQueueId( Message msg, MessageQueueId  responseQueueId );
ErrorCode messageGetResponseQueueId( Message msg, MessageQueueId *responseQueueId );
ErrorCode messageGetId( Message msg, MessageId *msgId );
ErrorCode messageGetSender(Message msg, uint32 *sender);
ErrorCode messageGetSize(Message msg, uint32 num, uint32 *arg);
ErrorCode messageGetArgument(Message msg, uint32 num, Pointer *arg);
ErrorCode messageGetArgumentPointer(Message msg, uint32 num, Pointer *arg);
ErrorCode messageGetArgumentCount(Message msg, uint32 *total);
//ErrorCode messageGetArgumentList(Message msg, Pointer *top, uint32 *total);
ErrorCode messageGetCode(Message msg, uint32 *code);
ErrorCode messageRespond(Message source, Pointer response);
ErrorCode messageDestroy(Message * msg);

#endif

/** @} */
