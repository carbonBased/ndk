#include "object.h"
#include <assert.h>
#include <core/arch/i386/pager.h>   /** @TODO remove */

static List objectParsers;
static ListIterator objectParserIterator;

static ErrorCode _objectLink(Object obj1, Object obj2);
static ErrorCode _objectParserCreateRelocation(ObjectRelocation *rec, ObjectSection sourceSection,
                                               uint32 offset, ObjectSymbol symbol,
                                               ObjectSection section, uint8 length,
                                               Boolean relative);

ErrorCode objectParsersInit(void)
{
   listCreate(&objectParsers);
   listIteratorCreate(&objectParserIterator, objectParsers);
}

ErrorCode objectParsersFinal(void)
{
   listDestroy(&objectParsers);
}

ErrorCode objectParserAdd(String formatName, ObjectParserCreateFunction create,
                          ObjectParserDestroyFunction destroy)
{
   ObjectParser parser = (ObjectParser)malloc(sizeof(struct _ObjectParser));

   parser->name = formatName;
   parser->create = create;
   parser->destroy = destroy;

   listAddBack(objectParsers, parser);
}

ErrorCode objectParserSetPrivateData(Object obj, void *privateData)
{
   obj->privateData = privateData;

   return ErrorNoError;
}

ErrorCode objectParserGetPrivateData(Object obj, void **privateData)
{
   *privateData = obj->privateData;

   return ErrorNoError;
}

ErrorCode objectParserCreateSection(ObjectSection *objSection, String name, uint32 id,
                                    uint32 offset, uint32 length, uint32 alignment)
{
   ObjectSection section = (ObjectSection)malloc(sizeof(struct _ObjectSection));

   section->name = name;
   section->id = id;
   section->offset = offset;
   section->length = length;
   section->alignment = alignment;

   listCreate(&section->symbols);
   listIteratorCreate(&section->symbolIterator, section->symbols);

   *objSection = section;

   return ErrorNoError;
}

ErrorCode objectParserAddSection(Object obj, ObjectSection section)
{
   listAddBack(obj->sections, section);

   return ErrorNoError;
}

ErrorCode objectParserGetSection(Object obj, String name, ObjectSection *section)
{
   ErrorCode err = ErrorNotFound;
   ObjectSection asection;

   listIteratorReset(obj->sectionsIterator);

   while(listIteratorGetNext(obj->sectionsIterator, (void **)&asection) == ErrorNoError)
   {
      if(strcmp(asection->name, name) == 0)
      {
         *section = asection;
         err = ErrorNoError;
         break;
      }
   }

   return err;
}

ErrorCode objectParserGetSectionById(Object obj, uint32 id, ObjectSection *section)
{
   ErrorCode err = ErrorNotFound;
   ObjectSection asection;

   listIteratorReset(obj->sectionsIterator);
   
   printf("objectParserGetSectionById %d\n", id);

   while(listIteratorGetNext(obj->sectionsIterator, (void **)&asection) == ErrorNoError)
   {
      printf("___compare %d %d\n", asection->id, id);
      if(asection->id == id)
      {
         *section = asection;
         err = ErrorNoError;
         break;
      }
   }

   printf("___returning ec == 0x%8x\n", err);
   return err;
}

ErrorCode objectCreate(Object *obj, String name, void *buffer, uint32 length)
{
   ObjectParser objectParser;
   Object object;
   ErrorCode ec;

   objectCreateEmpty(&object);

   listIteratorReset(objectParserIterator);
   
   printf("objectCreate(0x%8x, %s, 0x%8x, %d)\n",
      obj, name, buffer, length );

   /**
	 * Loop through all the object parsers and find one that'll be able to
	 * parse this format of object
	 */
   while((ec = listIteratorGetNext(objectParserIterator, (void **)&objectParser)) == ErrorNoError)
   {
      if((ec = objectParser->create(object, buffer, length)) == ErrorNoError)
      {
         printf("Object is of type %s\n", objectParser->name);
         break;
      }
   }

   object->name = name;

   *obj = object;

   return ec;
}

ErrorCode objectCreateEmpty(Object *obj)
{
   Object object;
   ErrorCode ec = ErrorNoError;

   object = (Object)malloc(sizeof(struct _Object));

   listCreate(&object->sections);
   listIteratorCreate(&object->sectionsIterator, object->sections);

   listCreate(&object->imports);
   listIteratorCreate(&object->importsIterator, object->imports);

   listCreate(&object->exports);
   listIteratorCreate(&object->exportsIterator, object->exports);

   listCreate(&object->relocations);
   listIteratorCreate(&object->relocationsIterator, object->relocations);

   *obj = object;

   return ec;
}

ErrorCode objectDestroy(Object *obj)
{
   ErrorCode ec = ErrorNoError;

   if(obj)
   {
      (*obj)->parser->destroy(*obj);
      free(*obj);
      *obj = NULL;
   }
   else
   {
      ec = ErrorBadParam;
   }

   return ec;
}

ErrorCode objectGetImportList(Object obj, List *imports)
{
   ErrorCode ec = ErrorNoError;

   if(obj)
   {
      //obj->parser->getImportList(obj, imports);
   }
   else
   {
      ec = ErrorBadParam;
   }

   return ec;
}

ErrorCode objectGetExportList(Object obj, List *exports)
{
   ErrorCode ec = ErrorNoError;

   if(obj)
   {
      //obj->parser->getExportList(obj, exports);
   }
   else
   {
      ec = ErrorBadParam;
   }

   return ec;
}

ErrorCode objectGetSectionIterator(Object obj, ListIterator * iterator)
{
   ErrorCode ec = ErrorNoError;

   if(obj)
   {
      *iterator = obj->sectionsIterator;
   }
   else
   {
      *iterator = NULL;
      ec = ErrorBadParam;
   }

   return ec;
}

ErrorCode objectGetSectionList(Object obj, List *sections)
{
   ErrorCode ec = ErrorNoError;

   if(obj)
   {
      //obj->parser->getSectionList(obj, sections);
   }
   else
   {
      ec = ErrorBadParam;
   }

   return ec;
}

ErrorCode objectSetBase(Object obj, void *base)
{
   ErrorCode ec = ErrorNoError;

   if(obj)
   {
      obj->base = (uint8 *)base;
   }
   else
   {
      ec = ErrorBadParam;
   }

   return ec;
}

ErrorCode objectLink(Object obj1, Object obj2)
{
   printf("Object Link - %s <-> %s\n", obj2->name, obj1->name);
   _objectLink(obj2, obj1);
   printf("Object Link - %s <-> %s\n", obj1->name, obj2->name);
   _objectLink(obj1, obj2);
}

ErrorCode _objectLink(Object obj1, Object obj2)
{
   ObjectRelocation reloc;
   ObjectSymbol import, export;
   ObjectSection section, sourceSection;
   char *segmentBase;
   long offset;
   PointerType relocation;

   /* loop through all the relocations of obj1, and see if obj2 can satisfy them */
   listIteratorReset(obj1->relocationsIterator);
   while(listIteratorGetNext(obj1->relocationsIterator, (void **)&reloc) == ErrorNoError)
   {
      import = reloc->symbol;
      section = reloc->section;
      sourceSection = reloc->sourceSection;

      if(import)
      {
         // symbol relocation
         printf("  symbol relocation for %s\n", import->symbolName);
         if(objectParserGetExportByName(obj2, import->symbolName, &export) == ErrorNoError)
         {
            relocation = (PointerType)export->symbolLocation;
            relocation += ((PointerType)obj2->base + (PointerType)export->section->offset);
            printf("  relocation = %x\n", relocation);
         }
         else
         {
            printf("  no matching symbol, continuing\n");
            continue;
         }
      }
      else
      {
         // section relocation
         relocation = (PointerType)obj1->base + (PointerType)section->offset;
         printf("  section relocation for %s %x\n", section->name, relocation);
      }

      if(reloc->relative)
      {
         /* TODO: confirm sourceSection in here... section? */
         relocation -= (PointerType)obj1->base + (PointerType)sourceSection->offset;
         if(import == NULL)
         {
            printf("relative segment reloc!?\n");
         }
      }

      segmentBase = (char *)obj1->base + sourceSection->offset;
      offset = reloc->location;

      /*
         if(import)
         {
         printf("___performing %s relocation %d %d %d %d [ %s ]\n",
         reloc->relative ? "relative" : "", segmentBase, offset,
         reloc->length, relocation,
         import ? (char*)import->symbolName : "");
         }
       */

      switch (reloc->length)
      {
      case 1:
         printf("    1 byte reloc at 0x%8x\n", &segmentBase[offset]); 
         segmentBase[offset] += (char)relocation;
         break;
      case 2:
         printf("    2 byte reloc at 0x%8x\n", (long *)(segmentBase + offset));
         *(short *)(segmentBase + offset) += (short)relocation;
         break;
      case 4:
         printf("    4 byte reloc at 0x%8x of %d\n", (long *)(segmentBase + offset), relocation);
         if(segmentBase[offset] == relocation)
         {
            printf("    New relocation == old %x\n", relocation);
            assert(0);
         }
         if(import == NULL)
            printf("    previous (%x %d) ", *(long *)(segmentBase + offset), *(long *)(segmentBase + offset) );
            //printf("    previous (%x %d) ", segmentBase[offset], segmentBase[offset]);
         *(long *)(segmentBase + offset) += (long)relocation;
         if(import == NULL)
            printf("    current  (%x %d)\n", *(long *)(segmentBase + offset), *(long *)(segmentBase + offset) );
            //printf("    current (%x %d)\n", segmentBase[offset], segmentBase[offset]);
         break;
      case 8:
         printf("    8 byte relocation not yet supported");
      default:
         printf("    unknown relocation size");
         assert(0);
      }

      //printf("   %d %d %d %d %d\n", reloc->symbolId, reloc->sectionId, reloc->location,
      // reloc->length, reloc->relative);
   }
}

ErrorCode objectDump(Object obj)
{
   ObjectSymbol symbol;
   ObjectSection section;
   ObjectRelocation reloc;

   printf("Object [%s] @ %x\n", obj->name, obj->base);

   printf("Sections:\n");
   listIteratorReset(obj->sectionsIterator);
   while(listIteratorGetNext(obj->sectionsIterator, (void **)&section) == ErrorNoError)
   {
      printf("   %s (%d) @ %d for %d bytes with %d**2 alignment\n", section->name, section->id,
             section->offset, section->length, section->alignment);
   }

   /*
      printf("Imports:\n");
      listIteratorReset(obj->importsIterator);
      while(listIteratorGetNext(obj->importsIterator, (void**)&symbol) == ErrorNoError)
      {
      printf("   %s (%d) @ %d %x\n",
      symbol->symbolName, symbol->symbolId, symbol->symbolLocation, symbol);
      }
    */

   /*
      printf("Exports:\n");
      listIteratorReset(obj->exportsIterator);
      while(listIteratorGetNext(obj->exportsIterator, (void**)&symbol) == ErrorNoError)
      {
      printf("   %s (%d) @ %d\t",
      symbol->symbolName, symbol->symbolId, symbol->symbolLocation);
      }
    */

   /*
      printf("Relocations:\n");
      listIteratorReset(obj->relocationsIterator);
      while(listIteratorGetNext(obj->relocationsIterator, (void**)&reloc) == ErrorNoError)
      {
      if(reloc->symbolId)
      {
      printf("   %d %d %d %d %d %x\n", reloc->symbolId, reloc->sectionId, reloc->location,
      reloc->length, reloc->relative, reloc->symbol);
      }
      }
    */
}

ErrorCode objectParserCreateSymbol(ObjectSymbol *symbol, String name, uint32 id, uint32 location)
{
   ObjectSymbol sym = (ObjectSymbol)malloc(sizeof(struct _ObjectSymbol));

   printf("objectParseCreateSymbol %s %d 0x%8x\n", name, id, location);
   sym->symbolName = name;
   sym->symbolId = id;
   sym->symbolLocation = location;

   *symbol = sym;
   return ErrorNoError;
}

ErrorCode objectParserAddImport(Object obj, ObjectSymbol import)
{
   import->section = NULL;
   listAddBack(obj->imports, import);
}

ErrorCode objectParserAddExport(Object obj, ObjectSymbol export, ObjectSection section)
{
   printf("objectParserAddExport 0x%8x 0x%8x 0x%8x\n",
      obj, export, section);
      
   export->section = section;
   listAddBack(obj->exports, export);
}

ErrorCode objectParserGetImportById(Object obj, uint32 id, ObjectSymbol *symbol)
{
   ErrorCode ec = ErrorNotFound;
   ObjectSymbol sym;

   *symbol = NULL;

   listIteratorReset(obj->importsIterator);
   while(listIteratorGetNext(obj->importsIterator, (void **)&sym) == ErrorNoError)
   {
      PhysicalAddress phys;
      pagerGetPhysical( (LinearAddress)sym, &phys);
      printf("sym 0x%8x phys 0x%8x\n", sym, phys);
      if(sym->symbolId == id)
      {
         *symbol = sym;
         ec = ErrorNoError;
         break;
      }
   }

   return ec;
}

ErrorCode objectParserGetExportById(Object obj, uint32 id, ObjectSymbol *symbol)
{
   ErrorCode ec = ErrorNotFound;
   ObjectSymbol sym;

   *symbol = NULL;

   listIteratorReset(obj->exportsIterator);
   while(listIteratorGetNext(obj->exportsIterator, (void **)&sym) == ErrorNoError)
   {
      if(sym->symbolId == id)
      {
         *symbol = sym;
         ec = ErrorNoError;
         break;
      }
   }

   return ec;
}

ErrorCode objectParserGetImportByName(Object obj, String name, ObjectSymbol *symbol)
{
   ErrorCode ec = ErrorNotFound;
   ObjectSymbol sym;

   *symbol = NULL;

   listIteratorReset(obj->importsIterator);
   while(listIteratorGetNext(obj->importsIterator, (void **)&sym) == ErrorNoError)
   {
      if(strcmp(sym->symbolName, name) == 0)
      {
         *symbol = sym;
         ec = ErrorNoError;
         break;
      }
   }

   return ec;
}

ErrorCode objectParserGetExportByName(Object obj, String name, ObjectSymbol *symbol)
{
   ErrorCode ec = ErrorNotFound;
   ObjectSymbol sym;

   *symbol = NULL;

   listIteratorReset(obj->exportsIterator);
   while(listIteratorGetNext(obj->exportsIterator, (void **)&sym) == ErrorNoError)
   {
      if(strcmp(sym->symbolName, name) == 0)
      {
         *symbol = sym;
         ec = ErrorNoError;
         break;
      }
   }

   return ec;
}

ErrorCode objectParserCreateSymbolRelocation(ObjectRelocation *rec, ObjectSection sourceSection,
                                             uint32 offset, ObjectSymbol symbol, uint8 length,
                                             Boolean relative)
{
   *rec = NULL;

   return _objectParserCreateRelocation(rec, sourceSection, offset, symbol, NULL, length, relative);
}

ErrorCode objectParserCreateSectionRelocation(ObjectRelocation *rec, ObjectSection sourceSection,
                                              uint32 offset, ObjectSection section, uint8 length,
                                              Boolean relative)
{
   *rec = NULL;

   return _objectParserCreateRelocation(rec, sourceSection, offset, NULL, section, length,
                                        relative);
}

ErrorCode _objectParserCreateRelocation(ObjectRelocation *rec, ObjectSection sourceSection,
                                        uint32 offset, ObjectSymbol symbol, ObjectSection section,
                                        uint8 length, Boolean relative)
{
   ObjectRelocation reloc = (ObjectRelocation)malloc(sizeof(struct _ObjectRelocation));

   // must have either a section or a symbol!
   if((symbol == NULL) && (section == NULL))
   {
      assert(0);
      return ErrorBadParam;
   }

   // TODO: have a relocation type enum?
   if(symbol)
   {
      // symbol relocation
      reloc->sectionId = 0;
      reloc->section = NULL;
      reloc->symbolId = symbol->symbolId;
      reloc->symbol = symbol;
   }
   else
   {
      // section relocation
      reloc->symbolId = 0;
      reloc->symbol = NULL;
      reloc->sectionId = section->id;
      reloc->section = section;
   }

   reloc->sourceSection = sourceSection;
   reloc->location = offset;
   reloc->length = length;
   reloc->relative = relative;

   *rec = reloc;

   return ErrorNoError;
}

ErrorCode objectParserAddRelocation(Object obj, ObjectRelocation rec)
{
   listAddBack(obj->relocations, rec);

   return ErrorNoError;
}
