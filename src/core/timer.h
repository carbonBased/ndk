/* ndk - [ timer.h ]
 *
 * Interface for controlling the x86 timer
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup Timer timer.h: Timer operations
 * @ingroup core
 *
 * This file defines the portable interface to the platforms timer hardware.
 */
/** @{ */

#ifndef __ndk_timer_h__
#define __ndk_timer_h__

#include <types.h>
#include <errorCodes.h>

typedef void (*TimerFunction) (void *data);
typedef struct _Timer *Timer;

/**
 * Set the timer base frequency
 *
 * @param freq The frequency, in x/s
 */
ErrorCode timerSetFreq(uint32 freq);

/**
 * Set the timer base frequency
 *
 * @param freq The frequency, in x/s
 */
ErrorCode timerGetFreq(uint32 *freq);

/**
 * Get the number of ticks since ndk started
 *
 * @param ticks Pointer to integer to fill with ticks
 */
ErrorCode timerGetTicks(uint32 *ticks);

/**
 * Delay for a period of x * (1/1000th of a second)
 * This is a fairly accurate blocking delay, but with
 * a tight inner loop.  Where accuracy is not a concern,
 * taskDelay would be a better option as it will perform
 * a voluntary task switch instead of polling, as this does.
 *
 * @param usec The ms to delay for
 */
ErrorCode timerDelay(uint32 usec);

/**
 * repeat True for repetitive timer
 * Create a new timer object
 *
 * @param t The timer handle for this timer
 * @param usec How often to call the callback function
 * @param func The actual callback function
 * @param data A parameter to pass to the callback function
 */
ErrorCode timerCreate(Timer *t, uint32 usec, TimerFunction func, void *data);

/**
 * Start a timer object
 *
 * @param t The timer to start
 */
ErrorCode timerStart(Timer t);

/**
 * Stop a timer object
 *
 * @param t The timer to stop
 */
ErrorCode timerStop(Timer t);

/**
 * Set's the timer's timeout (or time between each callback)
 *
 * @param t    The timer to use
 * @param usec The number of (1/1000th) of a second to wait before
 *             calling the callback again
 */
ErrorCode timerSetTimeout(Timer t, uint32 usec);

/**
 * Set whether this is a repetitive timer, or a one time timer
 *
 * @param t      The timer to use
 * @param repeat True for repetitive timer
 */
ErrorCode timerSetRepeat(Timer t, Boolean repeat);

/**
 * Destroy a timer
 *
 * @param t The timer to destroy
 */
ErrorCode timerDestroy(Timer *t);

/** @} */
#endif
