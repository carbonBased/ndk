/* ndk - [ rZero.c ]
 * (c)2002 neuraldk
 *    dcipher
 *    carbonBased
 *
 * rZero modules are ndk's method of externalizing functions of the
 * kernel which users might want to customize (such as the
 * scheduling method used, the start-up splash screen, memory
 * management, etc).
 *
 * In essence, the kernel will automatically load these modules and
 * run-time link them into the kernel (allowing the modules access
 * to a select few kernel functions, and publishing the required
 * symbols from the module, to the kernel).
 *
 * With such a system, it should also be able to change these modules
 * from withen the operating system.
 *
 * For more information, see doc/rZeroModules.kwd
 */

#include "rZero.h"
#include "rdoff.h"

#include <malloc.h>
#include <brink/localMessageQueue.h>
#include <brink/localMutex.h>
#include <klibc/printf.h>

#define CHECK_FLAG(flags,bit)   ((flags) & (1 << (bit)))

/* Listed here will be the _only_ symbols exported to
 * rZero modules.  As a security precaution, so they
 * can't take the whole system down (seeing as
 * though, by definition, rZero modules run at the
 * highest privelege level).
 */

// TODO: only OS API in here...!  klibc prob'ly shouldn't really be here...?
// TODO: alphabetize
rZeroSymbolInfo rZeroExports[] = {
   {"consoleClear", (long)consoleClear, 0},
   {"consoleLockScrollWindow", (long)consoleLockScrollWindow, 0},
   {"printf", (long)printf, 0},
   {"inportb", (long)inportb, 0},
   {"memcpy", (long)memcpy, 0},
//  { "memoryAllocPage", (long)memoryAllocPage, 0 },
//  { "memoryFreePage" , (long)memoryFreePage , 0 },
// TODO: remove the memory* entirely!
   {"memoryAllocPage", (long)0, 0},
   {"memoryFreePage", (long)0, 0},
//  { "malloc"         , (long)malloc         , 0 },
   {"memset", (long)memset, 0},
   {"messageQueueCreate", (long)localMessageQueueCreate, 0},
   {"messageQueueDestroy", (long)localMessageQueueDestroy, 0},
   {"messageQueuePeek", (long)localMessageQueuePeek, 0},
   {"messageQueueReceive", (long)localMessageQueueReceive, 0},
   {"messageQueueSend", (long)localMessageQueueSend, 0},
   {"mutexCreate", (long)localMutexCreate, 0},
   {"mutexDestroy", (long)localMutexDestroy, 0},
   {"mutexLock", (long)localMutexLock, 0},
   {"mutexUnlock", (long)localMutexUnlock, 0},
   {"outportb", (long)outportb, 0},
   {"outportw", (long)outportw, 0},
   {"printf", (long)printf, 0},
//{ "multibootInfo"  , (long)multibootInfo  , 0 },
   {"strcmp", (long)strcmp, 0},
   {"strncmp", (long)strncmp, 0},
   {"timerGetTicks", (long)timerGetTicks, 0},
   {(char *)NULL, NULL, 0}
};

/* And here are all the symbols we wanted imported from
 * the rZero modules.  Again, it's impossible for a rouge
 * module to replace any system code, as these symbols
 * will be the only ones changed.  An attempt to overwrite
 * an already defined symbol will also flag an error.
 */
rZeroSymbolInfo rZeroImports[] = {
   {"_main", NULL, 0},
   {"main", NULL, 0},
   {"splashPrint", NULL, 0},
   {"splashProgress", NULL, 0},
   {"splashScreen", NULL, 0},
   {"taskNew", NULL, 0},
   {"taskSwitch", NULL, 0},
   {(char *)NULL, NULL, 0}
};

void rZeroInit(void)
{
   module_t *mod;               // multiboot module structure
   rZeroObj obj;                // rZero module structure
   int i;

   return;

   /* Do we have ring zero modules? */
   if(!CHECK_FLAG(multibootInfo->flags, 3))
   {
      printf("No rZero Modules Found!!!\n");
      printf("The following modules:\n");
      printf("  scheduler.r0\n");
      printf("  progress.r0\n");
      printf("are required by ndk!\n");
      //exit(1);
   }

   printf("Total rZero Modules: %d, Starting at: 0x%8x\n", (int)multibootInfo->mods_count,
          (int)multibootInfo->mods_addr);

   /* loop through all the modules, and parse/link them
    * into the kernel
    */
   mod = (module_t *) multibootInfo->mods_addr;
   for(i = 0; i < multibootInfo->mods_count; i++)
   {
      obj.mem = (char *)mod[i].mod_start;
      obj.end = (char *)mod[i].mod_end;
      obj.name = (char *)mod[i].string;
      rZeroClearSegments();
      rZeroParse(&obj);
      //rZeroRelocate(&obj);
   }
   return;
}

/* Clear any linking references the previous module
 * may have left (ie, allow for more than one
 * module to use printf, for example)
 */
void rZeroClearSegments(void)
{
   int i = 0;

   while(rZeroExports[i].symbolName != NULL)
   {
      rZeroExports[i++].segmentNum = 0;
   }
}

/* Loop through the given symbol table, returning
 * an index to the provided symbol.  Usually used with rZeroImports
 */
int rZeroFindSymbol(char *symbol, rZeroSymbolInfo * symInfo)
{
   int i = 0;

   // NOTE: could also use a bsearch, if symbol names are alphabetical...
   while(strcmp(symbol, symInfo[i].symbolName) != 0)
   {
      if(symInfo[i].symbolName == 0)
      {
         printf("Could not find symbol: %s\n", symbol);
         return -1;
      }
      i++;
   }
   return i;
}

/* Loop through the given symbol table, returning
 * an index to the provided symbol.  Usually used with rZeroExports
 */
int rZeroFindSegment(short segment, rZeroSymbolInfo * symInfo)
{
   int i = 0;

   while(symInfo[i].segmentNum != segment)
      i++;

   return i;
}

/* Parse an rZero module.  This function will validate
 * that the module is, infact, in the rdoff format, record
 * information on all segments included in the file and
 * then call rZeroParseHeader (to parse the header information
 * and link it into the kernel)
 */
void rZeroParse(rZeroObj * obj)
{
   // Does the module have the rdoff signature?
   if(strncmp(obj->mem, "RDOFF", 5))
   {
      printf("rZero module (%s) not in RDOFF format\n", obj->name);
      return;
   }
   else
   {
      obj->version = obj->mem[5] - '0';
      printf("RDOFF Version %d.0\n", obj->version);
   }
   // skip past the signature
   obj->pos = 6;

   // this length read doesn't work properly!
   // However, everything else does!?
   if(obj->version > 1)
   {
      obj->length = readLong(obj);
      printf("Object content size: %d bytes\n", (int)obj->length);
   }

   // find the length of the header and record it
   obj->headerLength = readLong(obj);
   obj->headerOffset = obj->pos;

   // skip over the header for now...
   obj->pos += obj->headerLength;

   // because I need segment info first!
   rZeroParseSegments(obj);

   // skip back to the header
   obj->pos = obj->headerOffset;

   // and parse it!
   printf("Header (%d bytes):\n", obj->headerLength);
   rZeroParseHeader(obj);

   return;
}

/* This function will parse through an rZero/rdoff header
 * and link it.  It currently works in one step because, it
 * seems, Nasm will always include segment definitions before
 * actually using them.  This doesn't have to be the case with
 * rdoff, however, and so if Nasm does sometimes include segment
 * uses before definitions, this code will break!  I can't see
 * it happening though... but something to keep in mind :)
 */
void rZeroParseHeader(rZeroObj * obj)
{
   char buf[129], type, segment, length, flags;
   unsigned char recordLength;
   long offset, labelLength;
   short refSegment;
   long headerLength;

   char isRelative;
   long symbolIndex;
   long relocation;
   char *segReloc;

   // loop through the length of the header
   headerLength = obj->headerLength;
   while(headerLength > 0)
   {
      // read the segment type
      type = readByte(obj);

      // if this is rdoff2, also read the record length
      if(obj->version >= 2)
      {
         recordLength = readByte(obj);
      }

      // which type have we got?
      switch (type)
      {

      case 1:                  /* relocation record */
      case 6:                  /* segment relocation */
         printf("relocation (%d) :", type);
         segment = readByte(obj);
         offset = readLong(obj);
         length = readByte(obj);
         refSegment = readShort(obj);
         printf("seg: %x, offs %x, len %x, refSeg %x\n", segment, offset, length, refSegment);

         // if the referenced segment is less than three, we're
         // working with either the text, code or bss segment
         if(refSegment < 3)
         {
            relocation = (long)obj->mem + obj->segment[refSegment].offset;
         }
         // otherwise, it's a segment/symbol which must be imported
         else
         {
            // find the segment (as mentioned above, if this is defined in the
            // rdoff _after_ it's used (here), this code will break.  _Shouldn't_
            // ever happen, though...
            symbolIndex = rZeroFindSegment(refSegment, rZeroExports);
            //printf("rs: %d, symbolIndex: %d\n", rs, symbolIndex);
            // record the location of this segment
            relocation = rZeroExports[symbolIndex].symbolLocation;
            //printf("symbol location %x, actual %x\n", rel, printf);
         }

         isRelative = ((segment & 64) == 64);
         segment &= 63;

         // if this relocation is defined as relative, make sure our
         // relocation is defined as relative to the start of the
         // proper segment
         if(isRelative)
            relocation -= (long)obj->mem + obj->segment[segment].offset;

         // calculate the address of the segment we're going to patch
         if(segment == 0)
            segReloc = (char *)obj->mem + obj->segment[0].offset;
         else if(segment == 1)
            segReloc = (char *)obj->mem + obj->segment[1].offset;
         else
         {
            // ??? BSS relocation ever needed?
            printf("relocation not needed here!\n");
            continue;
         }

         // debugging info...
         printf("obj->mem: %x, obj->mem + seg[0].off %x\n", obj->mem,
                obj->mem + obj->segment[segment].offset);
         printf("patching area: %x + %x with %x\n", (long)segReloc, (long)offset, (long)relocation);

         // and, finally, perform the patch, based on the relocation size
         switch (length)
         {
         case 1:
            segReloc[offset] += (char)relocation;
            break;
         case 2:
            *(short *)(segReloc + offset) += (short)relocation;
            break;
         case 4:
            //printf("current mem: %x\n", *(long *)(seg + o));
            *(long *)(segReloc + offset) += (long)relocation;
            break;
         default:
            printf("unknown relocation size!\n");
            break;
         }

         //printf("  %s: location (%04x:%08x), length %d, "
         //     "referred seg %04x\n", t == 1 ? "relocation" : "seg relocation",
         //     (int)s, o,(int)l, rs);
         // perform some error checks...
         if(obj->version >= 2 && recordLength != 8)
            printf("warning: reclen != 8\n");
         if(obj->version == 1)
            headerLength -= 9;
         if(obj->version == 1 && type == 6)
            printf("warning: seg relocation not supported in RDOFF1\n");
         break;

      case 2:                  /* import record */
      case 7:                  /* import far symbol */
         printf("import: ");
         // get the referenced segment
         refSegment = readShort(obj);
         labelLength = 0;

         // read in the symbol name (different method, depending on
         // rdoff version!
         if(obj->version == 1)
         {                      // zero delimited
            do
            {
               buf[labelLength] = readByte(obj);
            }
            while(buf[labelLength++]);
         }
         else
         {                      // record length delimited (65536 - 4, max?)
            for(; labelLength < recordLength - 2; labelLength++)
               buf[labelLength] = readByte(obj);
         }
         printf("%s\n", buf);

         //printf("  %simport: segment %04x = %s\n",t == 7 ? "far " : "",
         //     rs,buf);
         // update our length (ie, skip over this record)
         if(obj->version == 1)
            headerLength -= labelLength + 3;

         // error checking...
         if(obj->version == 1 && type == 7)
            printf("    warning: far import not supported in RDOFF1\n");

         // find the exported symbol, and assign it the segment number
         symbolIndex = rZeroFindSymbol(buf, rZeroExports);
         if(symbolIndex == -1)
            printf("Cannot find!\n");
         rZeroExports[symbolIndex].segmentNum = refSegment;
         break;

      case 3:                  /* export record */
         printf("export: ");
         flags = readByte(obj);
         segment = readByte(obj);
         offset = readLong(obj);
         labelLength = 0;

         // read in the symbol name (different method depending on rdoff version)
         if(obj->version == 1)
         {
            do
            {
               buf[labelLength] = readByte(obj);
            }
            while(buf[labelLength++]);
         }
         else
         {
            for(; labelLength < recordLength - 6; labelLength++)
               buf[labelLength] = readByte(obj);
         }
         printf("%s\n", buf);

         /* what type of export?
            if (flags & SYM_GLOBAL)
            printf("  export");
            else
            printf("  global");
            if (flags & SYM_FUNCTION) printf(" proc");
            if (flags & SYM_DATA) printf(" data");
            printf(": (%04x:%08x) = %s\n",(int)s,o,buf);
          */
         // adjust the length for rdoff1 objects
         if(obj->version == 1)
            headerLength -= labelLength + 6;

         // import this symbol into the kernel
         symbolIndex = rZeroFindSymbol(buf, rZeroImports);
         if(symbolIndex != -1)
            rZeroImports[symbolIndex].symbolLocation =
               (long)obj->mem + obj->segment[segment].offset + offset;
         else
            printf("couldn't export\n");

         break;

      case 4:                  /* DLL and Module records */
      case 8:
         labelLength = 0;

         // read the module/dll name
         if(obj->version == 1)
         {
            do
            {
               buf[labelLength] = readByte(obj);
            }
            while(buf[labelLength++]);
         }
         else
         {
            for(; labelLength < recordLength; labelLength++)
               buf[labelLength] = readByte(obj);
         }

         // simply print which module/dll is required (this should never
         // happen with an rZero module)
         if(type == 4)
            printf("  requires dll: %s\n", buf);
         else
            printf("  requires module: %s\n", buf);

         // adjust the length for rdoff1
         if(obj->version == 1)
            headerLength -= labelLength + 1;
         break;
      case 5:                  /* BSS reservation */
         labelLength = readLong(obj);

         // do nothing for bss yet, just print it...
         printf("  bss reservation: %8x bytes\n", labelLength);

         // adjust length for rdoff1 and check for errors
         if(obj->version == 1)
            headerLength -= 5;
         if(obj->version > 1 && recordLength != 4)
            printf("    warning: reclen != 4\n");
         break;

      case 9:                  /* MultiBoot header record */
         printf("  MultiBoot header included!\n");
         printf("  Currently unsupported in rZero modules.\nSkipping over\n");
         // no need to adjust rdoff1 length, MB Headers only exist in rdoff2
         break;
      default:
         // print out the details of an unknown type...
         printf("  unrecognised record (type %d", (int)type);
         if(obj->version > 1)
         {
            printf(", length %d)\n", (int)recordLength);
            obj->pos += recordLength;
         }

         // adjust rdoff1 length
         if(obj->version == 1)
            headerLength--;
      }
      // adjust rdoff2+ length
      if(obj->version != 1)
         headerLength -= 2 + recordLength;
   }
}

/* Record all information from the segment info section of
 * the rdoff object
 */
void rZeroParseSegments(rZeroObj * obj)
{
   short segType;
   int i;

   // load in all the segment info
   obj->numSegments = 0;
   segType = readShort(obj);
   while(segType != 0 /*&& obj->numSegments < 6 */ )
   {
      obj->segment[obj->numSegments].type = segType;
      obj->segment[obj->numSegments].number = readShort(obj);
      obj->segment[obj->numSegments].reserved = readShort(obj);
      obj->segment[obj->numSegments].length = readLong(obj);
      obj->segment[obj->numSegments].offset = obj->pos;

      // seek past this segment
      obj->pos += obj->segment[obj->numSegments].length;
      obj->numSegments++;

      segType = readShort(obj);
   }

   // and print out a summary...
   printf("%d segments:\n", obj->numSegments);
   for(i = 0; i < obj->numSegments; i++)
   {
      printf("Type %d, Number %d, Reserved %d, Length %d, Offset %d\n", obj->segment[i].type,
             obj->segment[i].number, obj->segment[i].reserved, obj->segment[i].length,
             obj->segment[i].offset);
   }
   return;
}

/* The following read a long, short or byte from memory, converting it
 * from little-endian format to the processor's native format (unnecessary
 * for intel processors, but... it's portable, so I left it in :)
 */
long volatile readLong(rZeroObj * obj)
{
   char *start = &obj->mem[obj->pos];
   long r = 0;

   /*
      r = start[3];
      r = (r << 8) + start[2];
      r = (r << 8) + start[1];
      r = (r << 8) + start[0];
    */

   r = *(long *)start;

   obj->pos += 4;
   return r;
}

short volatile readShort(rZeroObj * obj)
{
   char *start = &obj->mem[obj->pos];
   short r = 0;

   //r = (start[1] << 8) + *start;
   r = *(short *)start;

   obj->pos += 2;
   return r;
}

char volatile readByte(rZeroObj * obj)
{
   return obj->mem[obj->pos++];
}
