/* ndk - [ pageBasedArray.h ]
 *
 * Simple page based allocator for a group of allocations
 * of the same size.  Acts like an array object.
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Utilities pageBasedArray.h: Page based page allocator
* @ingroup core
*
* This file defines an iterface for a dynamic array allocated in pages 
*/
/** @{ */

#ifndef __ndk_page_based_array__
#define __ndk_page_based_array__

#include <errorCodes.h>
#include <types.h>

typedef struct _PageArray *PageArray;

/**
 * Create a new page based array.
 * 
 * A page based array is an array of like structures, allocated using a page-granularity
 * allocator.  Ideally, structures will never span a page boundary such that its easy to 
 * unmap unused pages as required.  In reality, this can create a substantial ammount of 
 * wasted memory (up to PageSize/2 bytes).  Therefore, for maximum space efficiency, several 
 * contiguous linear pages will be used per allocation, and no structure will span the 
 * boundary of one of these 'runs.'  A 'run' can therefore be easily unmapped if no longer
 * required.  A 'run' is also unavoidable if the size of the fundamental element is 
 * greater then the size of a page.
 * 
 * @param handle The page array to create
 * @param sizeOfElement The size of the fundamental array element to be used
 * @param maxPagesPerRun The maximum number of consecutive pages to allocate at a time.
 * @param setFlag Should the array track which items are set?
 */
ErrorCode pageArrayCreate(PageArray *handle, uint32 sizeOfElement, uint32 maxPagesPerRun, Boolean setFlag);
ErrorCode pageArrayDestroy(PageArray *handle);
ErrorCode pageArrayGetItemPointer(PageArray handle, uint32 elementNum, Pointer *location);
ErrorCode pageArrayGetItem(PageArray handle, uint32 elementNum, Pointer location);
/** @TODO... elementNum starts at 1, now... element 0 doesn't work... is this okay? */
ErrorCode pageArraySetItem(PageArray handle, uint32 elementNum, Pointer data);
ErrorCode pageArrayGetRunSize(PageArray handle, uint32 *runSize);
ErrorCode pageArrayGetArraySize(PageArray handle, uint32 *arraySize);
ErrorCode pageArrayGetElementSize(PageArray handle, uint32 *elementSize);

#endif

/** @} */
