/* ndk - [ arch.h ]
 *
 * Architecture dependant initialization code.
 * Implementations will be in the respective
 * src/arch/{arch} folder
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * \defgroup i386 platform dependant portions (intel i386+)
 */
/** @{ */

#ifndef __ndk_arch_h__
#define __ndk_arch_h__

#include <errorCodes.h>

ErrorCode architectureInit(unsigned long magic, unsigned long addr);

#endif

/** @} */
