#include "pageBasedList.h"

#include <assert.h>
#include <core/arch/i386/pager.h>

/**
 * Singly linked for page based linked list.  Doubly would limit the 
 * number of items per page (thus increasing the number of reallocs required)
 * and increases complexity.  Users require only forward traversal (main 
 * users are mutex and semaphore) 
 */
typedef struct _PageListNodePrivate
{
   uint32 next;
   Pointer data;  /* points to a physical memory descriptor? */
} PageListNodePrivate;

/* 4096/sizeof(PageListNodePrivate) - sizeof_variables_above/sizeof(PageListNodePrivate) */
#define NODES_PER_FIRST_PAGE  (512 - 4)
/* 4096/sizeof(PageListNodePrivate) - sizeof_variables_above/sizeof(PageListNodePrivate) */
#define NODES_PER_PAGE        (4096 / sizeof(PageListNodePrivate) - 1)

#define PAGE_LIST_NO_NEXT     (0xffffffff)

/*
 * Layout in linear memory:
 * 
 * [ Page n          ] [ Page n + 1       ] [ Page n + 2       ]
 * [ PageList        ] [ PageListOverflow ] [ PageListOverflow ]
 *
 * Before iterating into any overflow page this code must check to see if 
 * that page is mapped, and if so, that the previous pages nextPage pointer 
 * equals the overflow page's physical address.  If not, then the entire 
 * PageList must be realloced (would indicate that another process has 
 * expanded the list, and so this process must now adapt to include the 
 * updated list. 
 */

struct _PageList
{
   PhysicalAddress nextPage;
   uint32 totalPages;
   uint32 head;
   uint32 tail;
   uint32 count;
   uint32 total;
   uint32 embeddedDataSize;
   uint32 padding;
   PageListNodePrivate nodes[ NODES_PER_FIRST_PAGE ];  
};
/* assert sizeof(struct _PageList) == PageSize */

typedef struct _PageListOverflow
{
   /* 
    * One could actually look at this as a complete page of nodes, but where nodes[0] is special 
    * and contains a pointer to the next physical page.  However, since I'd rather not special 
    * case nodes[0], I've actually inlined the nextPage pointer, make nodes[0] a regular node.
    */
   PhysicalAddress nextPage;
   uint32 padding;
   PageListNodePrivate nodes[ NODES_PER_PAGE ];
} PageListOverflow;

static ErrorCode _pageListEnsureConsistency(PageList *handle);
static ErrorCode _pageListGrow( PageList *list );

ErrorCode pageListCreate(PageListId *listId)
{
   ErrorCode ec = ErrorBadParam;
   PageList list;
   
   if( listId )
   {
      /* enter kernel mode? */
      /* initially allocate to the kernel heap */
      ec = pagerAllocate( pagerHeapKernel, 1, (Pointer*)&list );
      if( ec == ErrorNoError )
      {
         int i;
         
         /* initialize the page! */
         list->nextPage = NULL;  /* should this be PAGE_LIST_NO_NEXT as well? */
         list->head = 0;
         list->tail = 0;
         list->count = 0;
         list->total = NODES_PER_FIRST_PAGE;
         list->totalPages = 1;
         list->embeddedDataSize = 0;
         
         /* link all the existing nodes already */
         for( i = 0; i < list->total - 1; i++ )
         {
            list->nodes[i].next = i+1;
            list->nodes[i].data = NULL;
         }
         
         /* ensure the final one points to special value - will spawn a realloc once used */
         list->nodes[i].next = PAGE_LIST_NO_NEXT;
         list->nodes[i].data = NULL;
         
         printf("set final node %d to have no next\n", i);
                  
         /* and return the physical page pointer */         
         
         pagerGetPhysical( list, (Pointer*)listId );
         
         /* free the linearly mapped page in the kernel heap, but don't 
          * deallocate it (it should still be removed from the pager 
          * free stack
          */
         pagerUnmap( list );
      }
      else
      {
         assert(0);
      }
   }
   else
   {
      assert(0);
   }
   
   return ec;
}

ErrorCode pageListMap(PageListId id, PageList *handle)
{
   ErrorCode ec;
   PageList list;
   
   /* find enough contiguous pages for mapped(id)->totalPages & */
   /* map them into place, or just map the head for now? */
   /* return the linear pointer that begins the structure */
   
   ec = pagerMap( pagerHeapKernel, id, (Pointer*)&list );
   if( ec == ErrorNoError )
   {
      uint32 totalPages = list->totalPages;
      PhysicalAddress currentPhysicalPage;
      LinearAddress currentLinearPage;
      
      assert( pagerUnmap( list ) == ErrorNoError );
      
      ec = pagerReserve( pagerHeapShared, totalPages, (Pointer*)&list );
      if( ec == ErrorNoError )
      {
         currentPhysicalPage = (PhysicalAddress)id;
         currentLinearPage = (LinearAddress)list;
         
         printf("Reserved %d pages for PageList 0x%x at 0x%x\n",
            totalPages, id, list ); 
         
         while( totalPages-- )
         {
            //pagerMap( pagerHeapShared, currentPhysicalPage, currentLinearPage );
            pagerMapPhysicalToLinear( currentPhysicalPage, currentLinearPage ); 
            printf("pageListMap page %d 0x%x -> 0x%x\n",
               totalPages+1, currentPhysicalPage, currentLinearPage );
            
            // JJW currentPhysicalPage = ((PageListOverflow*)currentPhysicalPage)->nextPage;
            currentPhysicalPage = ((PageListOverflow*)currentLinearPage)->nextPage;
            currentLinearPage = (LinearAddress)((uint32)currentLinearPage + PAGE_SIZE);
         }
         *handle = list;
      } 
   }
   
   return ec;
}

ErrorCode pageListUnmap(PageList list)
{
   /* must first calculate how many pages are actually mapped of the list */
   /* _pageListEnsureConsistency() has an algorithm for this */
   ErrorCode ec = ErrorNoError;
   PhysicalAddress nextPageFromList, listId;
   LinearAddress currentAddress;
   uint32 numPages = 1;
   
   pagerGetPhysical( list, &listId );
   printf("_pageListUnmap on 0x%8x, id 0x%8x\n", list, listId);
   
   nextPageFromList = list->nextPage;
   currentAddress = (LinearAddress)list;
   pagerUnmap( list );
   
   while( nextPageFromList != NULL )
   {
      PhysicalAddress nextPageFromPager;
      
      currentAddress = (PhysicalAddress)( (uint32)currentAddress + PAGE_SIZE );
      pagerGetPhysical( currentAddress, &nextPageFromPager );
      
      if( nextPageFromPager != nextPageFromList )
      {
         /* the list doesn't extend this far, so exit */
         break;
      }

      nextPageFromList = ((PageListOverflow *)nextPageFromList)->nextPage;
      numPages++;
      
      pagerUnmap( currentAddress );
   }
   
   printf("Unmapped %d pages of list\n", numPages);
   
   return ec;
}

ErrorCode pageListDestroy(PageList *handle)
{
   return ErrorNotImplemented;
}

ErrorCode pageListDestroyById(PageListId listId)
{
   return ErrorNotImplemented;
}

/** could remap handle, hense pointer to pointer */
ErrorCode pageListAdd(PageList *handle, Pointer data)
{
   ErrorCode ec = ErrorNoError;
   
   if( handle )
   {
      uint32 nextAvailable;
      
      ec = _pageListEnsureConsistency( handle );
      if( ec == ErrorNoError )
      {
         if( (*handle)->nodes[ (*handle)->tail ].next == PAGE_LIST_NO_NEXT )
         {
            /* (*handle) may be updated/moved due to this call */
            ec = _pageListGrow( handle );
            
            /* not sure how to handle... */
            assert( ec == ErrorNoError );
         }
         
         if( (*handle)->count == 0 )
         {
            /* first addition, add as tail (and head) */
            nextAvailable = (*handle)->tail;
            //printf("first addition, head=%d tail=%d\n", (*handle)->head, (*handle)->tail);
         }
         else
         {
            /* map next available node to end, and update tail */
            nextAvailable = (*handle)->nodes[ (*handle)->tail ].next;
         }   

         (*handle)->nodes[ nextAvailable ].data = data;
         //printf("setting node %d to %x\n", nextAvailable, data);
         //(*handle)->nodes[ nextAvailable ].next = PAGE_LIST_NO_NEXT;
         (*handle)->tail = nextAvailable;
         
         (*handle)->count++;
      }
   }
   else
   {
      ec = ErrorBadObject;
   }
   
   return ec;
}

/** could remap handle, hense pointer to pointer */
ErrorCode pageListRemove(PageList *handle, Pointer data)
{
   ErrorCode ec = ErrorNotFound;
   PageListNodePrivate *node;
   PageList list = (*handle);
   PointerType nodenum, prevnode = -1;
   Pointer dummy;
  
   nodenum = list->head;

   do
   {
      node = &list->nodes[ nodenum ];
      if( node->data == data )
      {
         if( prevnode == -1 )
         {
            pageListRemoveFirst( (*handle), &dummy );
         }
         else
         {
            pageListRemoveNext( (*handle), prevnode, &dummy );
         }
         
         ec = ErrorNoError;
         break;
      }
      
      prevnode = nodenum;
      nodenum = node->next;
   }
   while( node->next != PAGE_LIST_NO_NEXT );
      
   return ec;
}

/* 
 * allow the user to embed some private data into this list at the expense of the number 
 * of nodes in the first page (in other words, use some nodes to hold private data if 
 * the user wishes.  Note that this function is only garaunteed to work on a 
 * fresh list, at the moment (no data must exist in this list, else it will be overwritten
 */
ErrorCode pageListEmbedPrivateData( PageList handle, Pointer data, PointerType sizeOfData )
{
   ErrorCode ec = ErrorNoError;
   uint32 numElements;
   
   numElements = sizeOfData / sizeof(PageListNodePrivate);
   if( sizeOfData % sizeof(PageListNodePrivate) )
   {
      numElements++;
   }
   
   if( numElements < handle->total )
   {
      /** @TODO: handle case whereby list already contains some data!*/
      assert( handle->count == 0 );
      
      handle->head += numElements;
      handle->tail += numElements;
      handle->embeddedDataSize = sizeOfData;
      memcpy( &handle->nodes[0], data, sizeOfData );
   }
   else
   {
      /** @TODO: increase size of list and embed?  So far nothing requires this */
      ec = ErrorBadParam;
   }
   
   return ec;
}

ErrorCode pageListGetPrivateData( PageList handle, Pointer *data )
{
   ErrorCode ec = ErrorNoError;
   
   if( handle->embeddedDataSize )
   {
      *data = (Pointer)&handle->nodes[0];
   }
   else
   {
      ec = ErrorNotFound;
   }
   
   return ec;
}

ErrorCode pageListGetFirst( PageList list, PageListNode *first )
{
   ErrorCode ec = ErrorNotFound;
   PageListNodePrivate *node;
   
   if( list->count )
   {
      /*
      node = &list->nodes[ list->head ];

      printf("list 0x%8x head is %d node is 0x%8x\n", list, list->head, node );
      
      *first = (PointerType)node - (PointerType)list;
      */
      *first = list->head;
      
      printf("returning pointer of %d\n", *first );
      
      ec = ErrorNoError;
   }
   
   return ec;
}

ErrorCode pageListGetNext( PageList list, PageListNode *next )
{
   ErrorCode ec = ErrorNotFound;
   //PointerType offset;
   PageListNodePrivate *node;
   
   if( next )
   {
      //offset = (PointerType)list + (PointerType)(*next);
      //node = (PageListNodePrivate*)offset;
      node = &list->nodes[ (*next) ];
      
      //printf("get next of 0x%8x\n", offset);
      
      if( /*node != &list->nodes[ list->tail ]*/ (*next) != list->tail )
      {
         printf("next is %d\n", node->next);

         //node = &list->nodes[ node->next ];
         //*next = (PointerType)node - (PointerType)list;
         *next = node->next;
      
         ec = ErrorNoError;
      }
   }   
   
   return ec;
}
   
ErrorCode pageListGetData( PageList list, PageListNode node, Pointer *data )
{
   ErrorCode ec = ErrorNotFound;
   //PointerType offset;
   PageListNodePrivate *privnode;
   
   if( node )
   {
      //offset = (PointerType)list + (PointerType)node;
      //privnode = (PageListNodePrivate*)offset;
      privnode = &list->nodes[ node ];
      
      //printf("getting data from node %d 0x%8x (0x%8x + 0x%8x)\n", node, privnode, list, node );
      *data = privnode->data;
      
      ec = ErrorNoError;
   }
   
   return ec;
}

ErrorCode pageListGetLength( PageList list, uint32 *length )
{
   ErrorCode ec = ErrorNoError;
   
   if( list )
   {
      *length = list->count;
   }
   else
   {
      ec = ErrorBadParam;
   }
   
   return ec;
}

static ErrorCode _pageListEnsureConsistency(PageList *handle)
{
   ErrorCode ec = ErrorNoError;
   PageList list = *handle;
   PhysicalAddress nextPageFromList, listId;
   LinearAddress currentAddress;
   uint32 numPages = 1;
   
   pagerGetPhysical( list, &listId );

   //printf("_pageListEnsureConsistency on 0x%8x, id 0x%8x\n", list, listId);
   
   /* ensure all the next pointers are valid, otherwise realloc */
   nextPageFromList = list->nextPage;
   currentAddress = (LinearAddress)list;
   
   //printf("nextPageFromList 0x%8x\n", nextPageFromList);
   while( nextPageFromList != NULL )
   {
      PhysicalAddress nextPageFromPager;
      
      currentAddress = (PhysicalAddress)( (uint32)currentAddress + PAGE_SIZE );
      pagerGetPhysical( currentAddress, &nextPageFromPager );
      
      if( nextPageFromPager != nextPageFromList )
      {
         printf("Noted inconsisency with pagerlist (id=%8x) at page %d expecting %8x got %8x\n",
            listId, numPages+1, nextPageFromList, nextPageFromPager );
         ec = pagerUnmapRange( list, numPages );
         if( ec == ErrorNoError )
         {
            ec = pageListMap( listId, handle );
            printf("Remapped listId %8x to %8x\n", listId, *handle );
         }
      }
      
      nextPageFromList = ((PageListOverflow *)nextPageFromList)->nextPage;
      numPages++;
   }
   
   /* 
    * just a quick check... if we iterated the whole list, check that the number of pages
    * we iterated is what the list header actually contains!
    */
   if( nextPageFromList == NULL )
   {
      assert( numPages == list->totalPages );
   }
   
   return ec;
}

/** @TODO: return usable errorcodes */
static ErrorCode _pageListGrow( PageList *list )
{
   PointerType nextPage;
   PageListOverflow *overflow;
   PhysicalAddress physicalAddr = NULL;
   PhysicalAddress listId;
   PageListNodePrivate *privnode;
   PointerType nodeptr;
   uint32 i;
   
   /* check if we can map a page onto the end to extend this list */
   nextPage = (PointerType)&(*list)[ (*list)->totalPages ];
   
   //printf("_pageListGrow: Checking if 0x%8x is already mapped\n", nextPage);
   
   /* 
    * don't really care if getPhysical returns an error, as physicalAddr is 
    * preset to NULL to indicate the page doesn't exist
    */
   pagerGetPhysical( (LinearAddress)nextPage, &physicalAddr );
   if( physicalAddr == 0 )
   {
      /* map a new page in... */
      
      //printf("_pageListGrow -> next page is available, using it!\n");
      
      pagerGetNewPhysical( &physicalAddr );
      pagerMapPhysicalToLinear( physicalAddr, (LinearAddress)nextPage );
      
      (*list)[ (*list)->totalPages - 1 ].nextPage = physicalAddr;
      (*list)->totalPages++;
      
      overflow = (PageListOverflow*)nextPage;
   }
   else
   {
      PageList oldlist;
      
      //printf("_pageListGrow -> must relocate!\n");
      
      /* first add a new physical page into the mix */
      pagerGetNewPhysical( &physicalAddr );
      /* 
       * because sizeof(struct _PageSize) == PAGE_SIZE the following will 
       * set the last pointer to the newly allocated page.  Shouldn't need 
       * to set the new pages nextPage pointer yet, it'll be initialized after 
       * it's remapped
       */
      assert( (*list)[ (*list)->totalPages - 1 ].nextPage == NULL );
      (*list)[ (*list)->totalPages - 1 ].nextPage = physicalAddr;
      (*list)->totalPages++;
      
      pagerGetPhysical( (*list), &listId );
      pageListUnmap( (*list) );
      oldlist = (*list);
      
      pageListMap( listId, list );
      //printf("remapping listid 0x%8x: 0x%8x -> 0x%8x\n", listId, oldlist, (*list) );
      
      overflow = (PageListOverflow*)&(*list)[ (*list)->totalPages - 1 ];
   }

   /* now initialized the final page... */
   overflow->nextPage = NULL;//PAGE_LIST_NO_NEXT;
   overflow->padding = PAGE_LIST_NO_NEXT; /* ensure this next pointer isn't valid, it may be 
                                             iterated over (eg. pageListRemoveLast) */

   /* we're expanding, therefore (*list)->tail is in use, and is the last available 
    * spot in the list... update it's next to point to this new node (remember that 
    * overflow->nextPage and overflow->padding act as a node, but we skip over it)
    */
   assert( (*list)->nodes[ (*list)->tail ].next == PAGE_LIST_NO_NEXT );
   (*list)->nodes[ (*list)->tail ].next = (*list)->total + 1; 
   
   privnode = &overflow->nodes[0];
   nodeptr = (*list)->total + 2;
   for( i = 0; i < NODES_PER_PAGE - 1; i++) 
   {
      //printf("setting %d (node %d -> 0x%8x) to %d\n", 
      //   i, nodeptr-1, privnode, nodeptr );

      privnode->next = nodeptr;
      privnode->data = NULL;
      
      privnode++;
      nodeptr++;
   }
   //printf("setting %d (node %d -> 0x%8x) to PAGE_LIST_NO_NEXT\n", 
   //      i, nodeptr-1, privnode, nodeptr );
   privnode->next = PAGE_LIST_NO_NEXT;
   privnode->data = NULL;
   
   (*list)->total += NODES_PER_PAGE + 1;
   
   return ErrorNoError;
}

/* 
 * should remove perform a potential remap if an empty page occurs, or perhaps 
 * add in a compaction function? 
 */
ErrorCode pageListRemoveFirst( PageList list, Pointer *data )
{
   ErrorCode ec = ErrorNoError;
   PageListNodePrivate *privnode, *tailnode;
   PointerType temp;
   
   if( list->count )
   {
      /* remove head, and place it after tail */
      privnode = &list->nodes[ list->head ];
      tailnode = &list->nodes[ list->tail ];
      
      temp = tailnode->next;
      tailnode->next = list->head;
      list->head = privnode->next;
      privnode->next = temp;
      
      /* return the data, and invalidate it in the node */
      *data = privnode->data;
      privnode->data = NULL;
      
      list->count--;
      /*
      if( list->count == 0 )
      {
         // @TODO: run through head... is this required? I don't think so
         list->head = list->tail;
      }
      */
   }
   else
   {
      ec = ErrorNotFound;
   }
   
   return ec;
}

ErrorCode pageListRemoveLast( PageList list, Pointer *data )
{
   ErrorCode ec = ErrorNoError;
   PageListNodePrivate *prevnode = NULL, *tailnode;
   PointerType i;

   if( list->count )
   {
      tailnode = &list->nodes[ list->tail ];

      if( list->count > 1 )
      {
         /*
          * Remember, when searching through for the previous node -- 
          * taversing the list is garaunteed to take the most ammount of time, 
          * however, traversing the array of nodes *may* find it in less time.
          */
         for( i = 0; i < list->total; i++ )
         {
            if( list->nodes[ i ].next == list->tail )
            {
               /* we've found the previous node to tail... */
               //prevnode = &list->nodes[i];
               break;
            }
         }
         
         
         /* 
          * removing the last node is easy... just update tail, tail->next is 
          * already linked, and then becomes unused
          */
         list->tail = i;
      }
      

      *data = tailnode->data;
      tailnode->data = NULL;
      
      list->count--;
      /*
      if( list->count == 0 )
      {
         list->head = list->tail;
      }
      */
   }
   else
   {
      ec = ErrorNotFound;
   }
   
   return ec;
}
ErrorCode pageListRemoveNext( PageList list, PageListNode node, Pointer *data )
{
   ErrorCode ec = ErrorNoError;
   PageListNodePrivate *privnode, *nextnode, *tailnode;
   PointerType temp;
   
   if( node != list->tail && list->count )
   {
      /* remove node->next, and place it after tail */
      privnode = &list->nodes[ node ];
      nextnode = &list->nodes[ privnode->next ];
      tailnode = &list->nodes[ list->tail ];

      temp = tailnode->next;
      tailnode->next = privnode->next;
      privnode->next = nextnode->next;
      nextnode->next = temp;
      
      /* return the data, and invalidate it in the node */
      *data = nextnode->data;
      nextnode->data = NULL;
      
      list->count--;
   }
   else
   {
      ec = ErrorNotFound;
   }

   return ec;
}

ErrorCode pageListDump( PageList list )
{
   PageListNodePrivate *node;
   PointerType nodenum, count = 0;
   
   printf("List 0x%8x total pages %d count %d/%d head %d tail %d\n", 
      list, list->totalPages, list->count, list->total, list->head, list->tail);
   
   nodenum = list->head;
   /*
    * remember, list->total also contains the "fake nodes" at the beginning 
    * of each new page which contains the physical page address of that 
    * page (used to ensure consistency when using the list
    */
   
   //while( count-- )
   do
   {
      node = &list->nodes[ nodenum ];
      
      printf("%d node %d next %d data 0x%8x %s\n", count++, 
         nodenum, node->next, node->data,
         (nodenum == list->tail) ? (nodenum == list->head) ? "is head & tail" : "is tail" : (nodenum == list->head) ? "is head " : "" );
      
      nodenum = node->next;
   }
   while( node->next != PAGE_LIST_NO_NEXT );
}
