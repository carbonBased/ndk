/* ndk - [ driver.h ]
 *
 * Interface for creating and using drivers
 *
 * (c)2005 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup Drivers driver.h: Driver interface
 * @ingroup core
 *
 * This file defines the interface for creating and using drivers
 *
 * This specification must be general enough to allow easy use of
 * the drivers both from a programmers perspective, and from a user's
 * perspective (from the command prompt).  Not only should the user have
 * full control over every aspect of the driver, but it should be
 * clean and easy to do so, like the following command lines:
 *
 * <pre>
 * echo < device.serial(0)                    <- read point from function call
 * cp myFile device.printer(1)                <- write point from function call
 * device.video.setResolution(320,240,32)     <- function call (noAccess)
 * echo < device.video.getWidth()             <- function call (noAccess)
 * </pre>
 */

/** @{ */

#ifndef __ndk_driver_h__
#define __ndk_driver_h__

#include <types.h>
#include <errorCodes.h>
#include <brink/localMessageQueue.h>
#include <brink/localSemaphore.h>
#include <object.h>
#include <task.h>

/**
 * The maximum length that a DriverItem's name can be (including null character)
 */
#define DRIVER_ITEM_MAX_LENGTH		(64)

typedef struct _DriverItem *DriverItem;
typedef struct _Driver *Driver;

//typedef DriverItem (*DriverItemQueryFunction)(Driver driver, void **retVal, uint32 numParams, ...);

typedef ErrorCode(*DriverInitFunction) (Driver drv);
typedef ErrorCode(*DriverFinalFunction) (Driver drv);

/* @TODO: enum! somewhere */
#define DRIVER_MESSAGE_SENDER_ID       (0x00000001)

typedef enum _DriverEvent
{
   DriverEventBase = 0,
   DriverEventExecuteFunction,
   DriverEventQueryParameter,
   DriverEventSetParameter,
   DriverEventHandlerResponse,
   DriverEventQuit
} DriverEvent;

typedef enum _DriverParamType
{
   DriverParamVoid,
   DriverParamCharacter,
   DriverParamString,
   DriverParamStringUtf,
   DriverParamInteger64,
   DriverParamInteger32,
   DriverParamInteger16,
   DriverParamInteger8,
   DriverParamUnsignedInteger64,
   DriverParamUnsignedInteger32,
   DriverParamUnsignedInteger16,
   DriverParamUnsignedInteger8,
   DriverParamBoolean,
   DriverParamPointer
} DriverParamType;

typedef union _DriverVariable
{
   char character;
   uint64 longInteger;  /* just uint32 for now... LCC doesn't support long long */
   uint32 integer;
   String string;
   Pointer pointer;
   Boolean boolean;
   ErrorCode errCode;
} DriverVariable;

typedef struct _DriverParam
{
   DriverParamType type;
   String name;
} *DriverParam;

//typedef DriverVariable (*DriverItemHandlerFunction) (Driver driver, DriverVariable vars[]);

typedef enum _DriverItemType
{
   DriverItemTypeFunction,
   DriverItemTypeParameter,
   DriverItemTypeNamespace
} DriverItemType;

struct _DriverItem
{
   String name;
   uint32 numParams;
   DriverParam *params;         /* contiguous list? */
   List subItems;
   ListIterator subItemsIterator;
   DriverParamType returnType;
   DriverItemType type;

   /**
	* When set, will contain the driver that defined this DriverItem.
	* This variable is only set on root items, for simplicity and speed.  As such, 
	* when traversing the list, the current driver in the hierarchy must be maintained 
	* by checking this variable.  If set, use this variable, otherwise, keep the 
	* previous.
	*/
   Driver driver;
   //DriverItemQueryFunction func;
   // TODO: union handler and get? (or set?  can set be NULL?)
   //DriverItemHandlerFunction handlerFunc;
   //DriverItemHandlerFunction getHandlerFunc;
   //DriverItemHandlerFunction setHandlerFunc;
   Pointer privateData;
};

struct _Driver
{
   DriverItem rootItem;
   String baseLoc;
   //LocalMessageQueue queue;
   DriverInitFunction init;
   DriverFinalFunction final;
   ErrorCode initReturnCode;
   //LocalSemaphore syncSemaphore;
   Task task;
   MessageQueueId queueId;
   MessageQueue queue;
};

// todo: define
typedef struct _DriverContext
{
   /**
	* used to hold the driver that currently implements this portion of the 
	* driver hierarchy
	*/
   Driver driver;
   DriverItem location;
   Pointer contextInfo;
} *DriverContext;

ErrorCode driverCreate(Driver *driver);
ErrorCode driverCreateFromObject(Driver *driver, Object object);
/** @TODO: allow multiple queues per driver?  aka, queue per driverItem? */
ErrorCode driverSetQueue( Driver driver, MessageQueue queue );
ErrorCode driverSetRoot(Driver driver, String loc, DriverItem root);
ErrorCode driverSetMetaData(Driver driver, String key, String value);
ErrorCode driverGetMessageQueue(Driver driver, MessageQueue *queue);
ErrorCode driverReserveIrq(Driver drv, uint32 irq);
ErrorCode driverReservePort(Driver drv, uint32 port);
ErrorCode driverReserveMemory(Driver drv, uint32 port);
ErrorCode driverReserveIrqRange(Driver drv, uint32 lowest, uint32 highest);
ErrorCode driverReservePortRange(Driver drv, uint32 lowest, uint32 highest);
ErrorCode driverReserveMemoryRange(Driver drv, uint32 lowest, uint32 highest);
ErrorCode driverDestroy(Driver *driver);

/** TODO: move to driverItem.[h,c] */
ErrorCode driverItemCreateFunction(DriverItem *dItem, String name, DriverParamType returnType,
                                   uint32 numParams, DriverParam params[],
                                   Pointer privateData);
ErrorCode driverItemCreateParameter(DriverItem *dItem, String name, DriverParamType type,
                                    Pointer privateData);
ErrorCode driverItemCreateNamespace(DriverItem *dItem, String name);
ErrorCode driverItemAdd(DriverItem root, DriverItem child);

/** TODO: mode to driverParam.[h,c] */
ErrorCode driverParamCreate(DriverParam *param, String name, DriverParamType type);
ErrorCode driverParamDestroy(DriverParam *param);

/** TODO: move to driverManager.[c,h] */
ErrorCode driverManagerInit(void);
ErrorCode driverManagerFinal(void);
ErrorCode driverManagerAdd(Driver driver);
ErrorCode driverManagerRemove(Driver driver);

/** Debug function... */
ErrorCode driverManagerShowTree(void);

/** TODO: move to driverContext.[c,h] */
ErrorCode driverContextCreate(DriverContext *dc, String url);

/** TODO: devise method of passing params in (other then just string?) and out (pointer to ret?) */
ErrorCode driverContextExecute(DriverContext dc, String url);
ErrorCode driverContextTraverse(DriverContext *to, DriverContext from, String url);
ErrorCode driverContextDestroy(DriverContext *dc);

/*
driverCreateItem(&root, "serial", DriverItemCapabilitiesNoAccess, serialFunction);
driverAddParam(root, "portNumber", DriverParamInteger16);

driverCreateItem(&baudItem, "baud", DriverItemCapabilitiesReadWrite, baudGetSetFunction);
driverAddParam(baudItem, "rate", DriverParamInteger32);

driverAddItem(root, baudItem);

serialFunction(Driver driver, DriverAccessType type, ...)
switch(access)
{
	case read:
		read from serial port
	case write:
		write to serial port
	case noAccess:
		set port in context (do for all?)
}

allows:
cat 57600 > driver.serial(0).baud
echo < driver.serial(0).baud
*/

#endif

/** @} */
