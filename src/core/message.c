/* ndk - [ message.c ]
 *
 * Basic message type for IPC
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <core/message.h>
#include <core/arch/i386/pager.h>
#include <klibc/stdarg.h>
#include <klibc/assert.h>

#define MessageParamSize( sizeAndIndex ) \
   ((sizeAndIndex) & 0xffff0000 ) >> 16
   
#define MessageParamIndex( sizeAndIndex ) \
   ((sizeAndIndex) & 0x0000ffff )
   
#define MessageParamSetSizeAndIndex( sizeAndIndex, size, index ) \
   sizeAndIndex = (size << 16) + (index)


struct _Message
{
   uint32 senderId;
   uint32 code;
   uint32 numArgs;
   MessageDestroyFunction destroy;
   MessageQueueId responseQueueId;
   Message responseFrom;
   
   uint32 numPages;
   PhysicalAddress next[3];   /* 3*4kb + this page == 16kb */

   /* paramN = &msg->args.data[ msg->args.indexOf[paramN] ] */
   union 
   {
      PointerType sizeAndIndex[1];
      uint8 data[ sizeof(Pointer) ];
   } args;
};

/* MessageId = PhysicalAddress | numPages */

#define MessageIdAddress( id ) \
   ((id) & ~PAGE_SIZE_MASK)
   
#define MessageIdNumPages( id ) \
   ((id) & PAGE_SIZE_MASK)

ErrorCode messageCreate(MessageId *msgId, MessageDestroyFunction destroy, 
                        MessageQueueId responseQueueId, Message inResponseTo,
                        uint32 senderId, uint32 code, uint32 numArgs, ...)
{
   ErrorCode err = ErrorNoError;
   PointerType i, paramSize = 0, currentOffset;
   PointerType messageSize, messageSizeInPages;
   PointerType linPointer;
   PhysicalAddress physAddr;
   Message m;
   va_list ap;
   
   /* first query how much space we'll need to account for the parameters */
   va_start(ap, numArgs);
   
   printf("num params %d\n", numArgs);
   for(i = 0; i < numArgs; i++)
   {
      Pointer *arg = va_arg(ap, Pointer);
      PointerType length = va_arg(ap, PointerType);
      
      printf("param %d 0x%8x %d bytes\n", i, arg, length );
      
      assert( length < MESSAGE_MAX_SIZE );
      
      Align( length, sizeof(Pointer) );
      
      paramSize += length;
   }
   
   va_end(ap)
   
   messageSize = paramSize + sizeof( struct _Message )
                           + ((numArgs - 1 ) * sizeof(Pointer));
   Align( messageSize, PAGE_SIZE );
   messageSizeInPages = messageSize / PAGE_SIZE;
   
   assert( messageSize < MESSAGE_MAX_SIZE );

   /* now actually construct the message */
   va_start(ap, numArgs);

   if(msgId)
   {
      //m = (Message) malloc(sizeof(struct _Message) + (numArgs - 1) * sizeof(Pointer));
      /*
      m = (Message) malloc( sizeof(struct _Message) +  
                            ( numArgs * sizeof(Pointer) ) + 
                            ( paramSize ) - 
                            sizeof(Pointer) );
      */
      /* there's really no reason to force this function to be executed in kernel 
       * mode... for that reason, I think there should be at least one temporary 
       * heap which can be used by applications (and possibly a separate temporary
       * heap for the kernel, to avoid possible corruptions)
       */
      err = pagerAllocate( pagerHeapKernel, messageSizeInPages, (Pointer*)&m );
      if( (err == ErrorNoError) && m)
      {
         m->senderId = senderId;
         m->code = code;
         m->numArgs = numArgs;
         m->destroy = destroy;
         m->responseQueueId = responseQueueId;
         m->responseFrom = NULL;
         m->numPages = messageSizeInPages;

         if( inResponseTo )
         {
            m->responseFrom = inResponseTo;
         }

         
         /* save off all the physical addresses for future mapping */
         linPointer = (PointerType)m;
         for( i = 0; i < m->numPages - 1; i++ )
         {
            linPointer += PAGE_SIZE;
            
            err = pagerGetPhysical( (LinearAddress)linPointer, &physAddr );
            assert( err = ErrorNoError ); 
            
            m->next[i] = physAddr;
         }
         
         currentOffset = numArgs * sizeof(Pointer);

         for(i = 0; i < numArgs; i++)
         {
            Pointer arg = va_arg(ap, Pointer);
            PointerType length = va_arg(ap, PointerType);
            
            MessageParamSetSizeAndIndex( m->args.sizeAndIndex[i], length, currentOffset );
            //m->args.indexOf[i] = currentOffset;
            // null pointer params take up no space
            if( length )
            {
               printf("memcpy %d item base 0x%8x to 0x%8x len %d\n", i, arg, &m->args.data[ currentOffset ], length );
               memcpy( &m->args.data[ currentOffset ], arg, length );
            }

            /*
            printf( "copy %8x to offset %d dataoffset 0x%8x readback 0x%8x\n", 
               arg, currentOffset, &m->args.data[ MessageParamIndex( m->args.sizeAndIndex[i] ) ], 
                *((Pointer*)(&m->args.data[ MessageParamIndex( m->args.sizeAndIndex[i] ) ])) );
            */
            
            Align( length, sizeof(Pointer) );
            currentOffset += length;
         }
      }
      else
      {
         err = ErrorOutOfMemory;
         printf("Error allocating %d pages, paramSize %d messageSize %d\n", 
            messageSizeInPages, paramSize, messageSize );
         assert(0);
      }
   }
   else
   {
      err = ErrorBadObject;
      assert(0);
   }

   va_end(ap);

   err = pagerGetPhysical( m, (PhysicalAddress*)msgId );
   *msgId = *msgId | messageSizeInPages;

   /* message has been constructed, unmap it from this address space */
   pagerUnmapRange( m, messageSizeInPages );
   
   assert( err == ErrorNoError );
   
   return err;
}

ErrorCode messageCreateSimple( MessageId *msgId, uint32 senderId, uint32 code, uint32 numArgs, ... )
{
   /* for all arguments, push? */
   return messageCreate( msgId, NULL, NULL, NULL,
                          senderId, code, numArgs /* how to handle the ... */ );
   /* for all arguments, pop? */
}

ErrorCode messageMap( MessageId msgId, Message *msg )
{
   ErrorCode ec;
   PointerType pages, i;
   PointerType currentPhysicalPage, currentLinearPage;
   
   if( msgId && msg )
   {
      pages = MessageIdNumPages( msgId );
      
      ec = pagerReserve( pagerHeapShared, pages, (Pointer*)msg );
      if( ec == ErrorNoError )
      {
         currentLinearPage = (PointerType)(*msg);
         currentPhysicalPage = MessageIdAddress( msgId );
         
         for( i = 0; i < pages; i++ )
         {
            pagerMapPhysicalToLinear( (PhysicalAddress)currentPhysicalPage, 
                                      (LinearAddress)currentLinearPage );
            //printf("mapped 0x%8x to 0x%8x\n", currentPhysicalPage, currentLinearPage);
            
            currentLinearPage += PAGE_SIZE;
            currentPhysicalPage = (PointerType) (*msg)->next[i];
         } 
      }
      else
      {
         printf("Couldn't reserve %d pages\n");
         assert(0);
      }
   }
   else
   {
      ec = ErrorBadObject;
   }
   
   return ec;
}

ErrorCode messageSetResponseQueueId( Message msg,MessageQueueId responseQueueId )
{
   ErrorCode ec = ErrorNoError;
   
   if( msg )
   {
      msg->responseQueueId = responseQueueId;
   }
   else
   {
      ec = ErrorBadObject;
   }
   
   return ec;
}

ErrorCode messageGetResponseQueueId( Message msg, MessageQueueId *responseQueueId )
{
   ErrorCode ec = ErrorNoError;
   
   if( msg )
   {
      *responseQueueId = msg->responseQueueId;
   }
   else
   {
      ec = ErrorBadObject;
   }
   
   return ec;
}

ErrorCode messageGetId( Message msg, MessageId *msgId )
{
   ErrorCode ec = ErrorNoError;
   PhysicalAddress addr;
   
   ec = pagerGetPhysical( msg, &addr );
   if( ec != ErrorNoError )
   {
      *msgId = (PointerType)addr | msg->numPages;
   }
   
   return ec;
}

ErrorCode messageGetArgumentSize(Message msg, uint32 num, uint32 *size)
{
   ErrorCode err = ErrorNoError;

   if(msg)
   {
      if(num <= msg->numArgs)
      {
         *size = MessageParamSize( msg->args.sizeAndIndex[ num - 1 ] );          
      }
      else
      {
         err = ErrorBadParam;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode messageGetArgument(Message msg, uint32 num, Pointer *arg)
{
   ErrorCode err = ErrorNoError;

   if(msg)
   {
      if(num <= msg->numArgs)
      {
         uint32 index = MessageParamIndex( msg->args.sizeAndIndex[ num - 1 ] );
         uint32 size  = MessageParamSize ( msg->args.sizeAndIndex[ num - 1 ] );
         
         //printf("get arg %d off %d data ptr 0x%8x size %d\n", num, index, 
         //   &msg->args.data[ index ], size );
         
         if( size )
         {
            //*arg = *((Pointer*)(&msg->args.data[ msg->args.indexOf[ num - 1 ] ]));
            memcpy( arg, &msg->args.data[ index ], size );
            //msg->arg[num - 1];
         }
         else
         {
            *arg = 0;
         }
      }
      else
      {
         err = ErrorBadParam;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode messageGetArgumentPointer(Message msg, uint32 num, Pointer *arg)
{
   ErrorCode err = ErrorNoError;

   if(msg)
   {
      if(num <= msg->numArgs)
      {
         uint32 index = MessageParamIndex( msg->args.sizeAndIndex[ num - 1 ] );
         uint32 size  = MessageParamSize ( msg->args.sizeAndIndex[ num - 1 ] );
         
         printf("get arg %d off %d data ptr 0x%8x size %d\n", num, index, 
            &msg->args.data[ index ], size );
         if( size )
         {
            *arg = &msg->args.data[ index ];
         }
         else
         {
            *arg = NULL;
         } 
      }
      else
      {
         err = ErrorBadParam;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode messageGetArgumentCount(Message msg, uint32 *total)
{
   ErrorCode err = ErrorNoError;

   if(msg)
   {
      *total = msg->numArgs;
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

/*
ErrorCode messageGetArgumentList(Message msg, Pointer *top, uint32 *total)
{
   ErrorCode err = ErrorNoError;

   if(msg)
   {
      *top = &msg->arg[0];
      *total = msg->numArgs;
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}
*/

ErrorCode messageGetSender(Message msg, uint32 *sender)
{
   ErrorCode err = ErrorNoError;

   if(msg)
   {
      *sender = msg->senderId;
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode messageGetCode(Message msg, uint32 *code)
{
   ErrorCode err = ErrorNoError;

   if(msg)
   {
      *code = msg->code;
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode messageRespond(Message source, Pointer response)
{
   ErrorCode err = ErrorNoError;
   MessageQueue queue;

   printf("messageRespond(0x%08x, 0x%08x)\n",
          source, response );
   if(source && source->responseQueueId)
   {
      printf("responding to message 0x%8x with 0x%8x through queueId 0x%8x\n", 
         source, response, source->responseQueueId );
         
      /* 
       * note that even though we can access the MessageQueue from another task (since there currently
       * is not separate address spaces) we can't actually make use of it, as the TaskSpecificHandles 
       * for the mutex and semaphore have not been created... therefore we must map
       */
      messageQueueMap( source->responseQueueId, &queue );
      
      err = messageQueueSend( &queue, response);
      
      /* annd... might as well unmap when we're done */
      messageQueueUnmap( queue );
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode messageDestroy(Message * msg)
{
   ErrorCode err = ErrorNoError;

   if(msg)
   {
      if((*msg)->destroy)
      {
         (*msg)->destroy((*msg));
      }
      //free((*msg));
      printf("freeing %d pages at 0x%8x\n", (*msg)->numPages, (*msg) );
      pagerFree( pagerHeapShared, (*msg)->numPages, (*msg) );
      //assert(0);
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}
