/* ndk - [ ndk.h ]
 *
 * Basic include for the C entry point of ndk
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * \defgroup core platform independant portions of code
 */
/** @{ */

#ifndef __ndk_ndk_h__
#define __ndk_ndk_h__

#endif

/** @} */
