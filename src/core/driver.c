/* ndk - [ driver.c ]
 *
 * Interface for creating and using drivers
 *
 * (c)2005 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <task.h>
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include <core/driver.h>
#include <core/messageQueue.h>
#include <core/message.h>
#include <core/mutex.h>
#include <core/task.h>

typedef enum _DriverContextTraversalType
{
   DriverContextTraversalTraverse = 1,
   DriverContextTraversalExecute = 2,
   DriverContextTraversalProperty = 4
} DriverContextTraversalType;

/** hierarchy of DriverItem's from driver.* */
//static List driverRoot = NULL;
//static ListIterator driverRootIterator = NULL;
static DriverItem driverRoot = NULL;
static List driverCollection = NULL;
static ListIterator driverCollectionIterator = NULL;
static DriverContext rootContext;
/**
 * For now, since there's only one queue, any functions that call SendAndReceive on it
 * should be mutex'd so that different thread requests don't get messed up.
 */
static MessageQueueId driverMgrQueueId;
static MessageQueue driverMgrQueue;

static MutexId driverMgrMutexId;
static Mutex driverMgrMutex;

static ErrorCode _driverManagerAdd(List children, DriverItem item);
static ErrorCode _driverManagerEnsure(String loc, DriverItem from, DriverItem *end);
static ErrorCode _driverManagerFindOrAdd(String loc, uint32 len, DriverItem from, DriverItem *end);
static ErrorCode _driverManagerShowTree(DriverItem item, uint32 level);
static ErrorCode _driverContextTraverse(DriverContext to, DriverContext from, String url,
                                        DriverContextTraversalType type);

static ErrorCode _driverUrlParseWhitespace(String *url);
static ErrorCode _driverUrlParseCharacter(char character, String *url);
static ErrorCode _driverUrlParseName(String name, String *url);
static ErrorCode _driverUrlParseType(DriverVariable *var, DriverParamType type, String *url);
static ErrorCode _driverUrlParseInteger(DriverVariable *var, Boolean isSigned, uint32 bitLength,
                                        String *url);
static ErrorCode _driverUrlParseDigit(uint8 *digit, String *url);
static ErrorCode _driverUrlParseAssignment(DriverVariable *var, DriverParamType type, String *url);
static ErrorCode _driverUrlParseParameterList(DriverVariable *vars, DriverItem item, String *url);
static ErrorCode _driverPerformFunction(DriverContext ctx, DriverVariable *input,
                                        DriverVariable *output);
static ErrorCode _driverPerformGet(DriverContext ctx, DriverVariable *output);
static ErrorCode _driverPerformSet(DriverContext ctx, DriverVariable *input);
static ErrorCode _driverProcessMessage( DriverContext ctx, MessageId msg, DriverVariable *result );
static void _driverCommonEntry(void *arg);
static Boolean _driverManagerListComparator(Pointer key, Pointer data);

ErrorCode driverItemCreateFunction(DriverItem *dItem, String name, DriverParamType returnType,
                                   uint32 numParams, DriverParam params[],
                                   Pointer privateData
                                   /*DriverItemHandlerFunction func*/)
{
   ErrorCode err = ErrorNoError;
   DriverItem item;

   if(dItem)
   {
      item = (DriverItem)malloc(sizeof(struct _DriverItem));

      if(item)
      {
         item->name = malloc( strlen( name ) );
         strcpy(item->name, name );
         
         item->numParams = numParams;
         if(numParams)
         {
            item->params = (DriverParam *)malloc(numParams * sizeof(DriverParam));
            memcpy(item->params, params, numParams * sizeof(DriverParam));
         }
         else
         {
            item->params = NULL;
         }
         item->returnType = returnType;

         //item->getHandlerFunc = NULL;
         //item->setHandlerFunc = NULL;
         //item->handlerFunc = func;
         item->type = DriverItemTypeFunction;
         item->privateData = privateData;
         item->driver = NULL;

         listCreate(&item->subItems);
         listIteratorCreate(&item->subItemsIterator, item->subItems);

         *dItem = item;
      }
      else
      {
         err = ErrorOutOfMemory;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   //printf("driverItemCreateFunction %x\n", err); 
   return err;
}

ErrorCode driverItemCreateParameter(DriverItem *dItem, String name, DriverParamType type,
                                    Pointer privateData)
                                    //DriverItemHandlerFunction getHandler,
                                    //DriverItemHandlerFunction setHandler)
{
   ErrorCode err = ErrorNoError;
   DriverItem item;

   if(dItem)
   {
      item = (DriverItem)malloc(sizeof(struct _DriverItem));

      if(item)
      {
         item->name = malloc( strlen(name) );
         strcpy(item->name, name );
         
         item->numParams = 0;
         item->params = NULL;
         item->returnType = type;
         item->type = DriverItemTypeParameter;

         //item->handlerFunc = NULL;
         //item->getHandlerFunc = getHandler;
         //item->setHandlerFunc = setHandler;
         item->privateData = privateData;
         item->driver = NULL;

         listCreate(&item->subItems);
         listIteratorCreate(&item->subItemsIterator, item->subItems);

         *dItem = item;
      }
      else
      {
         err = ErrorOutOfMemory;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   //printf("driverItemCreateParameter %x\n", err);
   return err;
}

ErrorCode driverItemCreateNamespace(DriverItem *dItem, String name)
{
   ErrorCode err = ErrorNoError;
   DriverItem item;

   if(dItem)
   {
      item = (DriverItem)malloc(sizeof(struct _DriverItem));

      if(item)
      {
         item->name = malloc( strlen(name) );
         strcpy( item->name, name );
         
         item->numParams = 0;
         item->params = NULL;
         item->returnType = DriverParamVoid;
         item->type = DriverItemTypeNamespace;

         //item->handlerFunc = NULL;
         //item->getHandlerFunc = NULL;
         //item->setHandlerFunc = NULL;
         item->privateData = NULL;
         item->driver = NULL;

         listCreate(&item->subItems);
         listIteratorCreate(&item->subItemsIterator, item->subItems);

         *dItem = item;
      }
      else
      {
         err = ErrorOutOfMemory;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   //printf("driverItemCreateNamespace %x\n", err);
   return err;
}

ErrorCode driverItemAdd(DriverItem root, DriverItem child)
{
   ErrorCode err = ErrorNoError;

   //printf("driverItemAdd %x %x\n", root, child);
   if(root)
   {
      if(child)
      {
         //printf("___adding %s\n", child->name);
         listAddFront(root->subItems, child);
      }
      else
      {
         err = ErrorBadParam;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   //printf("driverItemAdd %x\n", err);
   return err;
}

ErrorCode driverParamCreate(DriverParam *dParam, String name, DriverParamType type)
{
   ErrorCode err = ErrorNoError;
   DriverParam param;

   //printf("creating param %s\n", name);

   if(dParam)
   {
      param = (DriverParam)malloc(sizeof(struct _DriverParam));

      if(param)
      {
         /** TODO: copy, not set */
         param->name = name;
         param->type = type;

         *dParam = param;
      }
      else
      {
         err = ErrorOutOfMemory;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   //printf("driverParamCreate %x\n", err);
   return err;
}

ErrorCode driverParamDestroy(DriverParam *param)
{
   ErrorCode err = ErrorNoError;

   if(param)
   {
      free(*param);
      *param = NULL;
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode driverCreate(Driver *driver)
{
   ErrorCode err = ErrorNoError;
   Driver drv = (Driver)malloc(sizeof(struct _Driver));
   Task currentTask;
   MessageQueueId queueId;

   if(driver)
   {
      drv->rootItem = NULL;
      drv->baseLoc = NULL;
      drv->init = NULL;
      drv->final = NULL;
      //drv->syncSemaphore = NULL;
      drv->initReturnCode = ErrorNotImplemented;

      /* drivers created from objects should really just use the Task's queue... */
      /** @TODO: get queue from actively running task by default, or specify a queue? */
      //localMessageQueueCreate(&(drv->queue), 0);
      taskGetCurrent( &currentTask );
      taskGetMessageQueueId( currentTask, &drv->queueId );
      drv->queue = NULL; // map as needed, as in address space of driver manager (not caller)
      //messageQueueMap( queueId, &drv->queue );
   }
   else
   {
      err = ErrorBadObject;
   }

   *driver = drv;
   return err;
}

/** @TODO: this is effectively removed, as well, it would seem, as there's no special object format */
ErrorCode driverCreateFromObject(Driver *driver, Object object)
{
   ErrorCode err = ErrorNoError;
   ObjectSymbol symbol;
   Driver drv;
   Task task;

   if(driver)
   {
      driverCreate(driver);
      drv = *driver;

      if(object)
      {
         if(objectParserGetExportByName(object, "driverInit", &symbol) == ErrorNoError)
         {
            drv->init =
               (Pointer)((PointerType)object->base + (PointerType)symbol->section->offset +
                         (PointerType)symbol->symbolLocation);
         }

         if(objectParserGetExportByName(object, "driverFinal", &symbol) == ErrorNoError)
         {
            drv->final =
               (Pointer)((PointerType)object->base + (PointerType)symbol->section->offset +
                         (PointerType)symbol->symbolLocation);
         }

         /* check if we have the proper functions defined in this object (therefore making a valid 
          * driver object)
          */
         if(drv->init && drv->final)
         {
            /* create a semaphore to sync the driver task with this one.  We want the driver's init() function 
             * to be run within the driver's task, but we also don't want this function to wait until that's 
             * finished so that it can return a proper error code 
             */

            //localSemaphoreCreate(&drv->syncSemaphore, SemaphoreTypeFIFO, 0);

            printf("___creating task for driver %x\n", drv);
            taskCreate(&task, NULL, "driverTask", _driverCommonEntry, drv, TaskTypeOS,
                                    /** TODO: move to user space! */
                       4096 * 2, 128);
            printf("___got task %x, waiting on init\n", task);

            //localSemaphoreDown(drv->syncSemaphore, TimeoutInfinite);

            /* single use semaphore... no longer needed, so get rid of it */
            // gonna be used for execute/traverse now, as well
            //semaphoreDestroy(&drv->syncSemaphore);

            printf("___init done, errCode == %x\n", drv->initReturnCode);
            err = drv->initReturnCode;
         }
         else
         {
            err = ErrorBadParam;
         }
      }
      else
      {
         err = ErrorBadParam;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   *driver = drv;
   return err;
}

ErrorCode driverSetQueue( Driver driver, MessageQueue queue )
{
   ErrorCode ec = ErrorNoError;
   
   driver->queue = queue;
   
   return ec;
}

ErrorCode driverSetRoot(Driver drv, String loc, DriverItem root)
{
   ErrorCode err = ErrorNoError;

   printf("___DriverSetRoot %x %x %s\n", drv, root, loc);
   if(drv)
   {
      drv->rootItem = root;
      root->driver = drv;

      if(loc != NULL)
      {
         if(strcmp(loc, "driver") == 0)
         {
            /* a starting namespace of driver is implied, remove it if specified */
            loc = NULL;
         }
         else if(strncmp(loc, "driver.", strlen("driver.")) == 0)
         {
            /* else if the namespace begins with driver., remove it */
            loc += strlen("driver.");
         }
      }

      /** TODO: copy it in?  Copying pointer is not safe! */
      drv->baseLoc = loc;
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode driverSetMetaData(Driver driver, String key, String value)
{
   ErrorCode err = ErrorNotImplemented;

   // TODO: use hashtable to store meta data about driver

   return err;
}

ErrorCode driverGetMessageQueue(Driver drv, MessageQueue *queue)
{
   ErrorCode err = ErrorNoError;

   if(drv)
   {
      if(drv->queue)
      {
         /* queue is set in _driverCommonEntry */
         *queue = drv->queue;
      }
      else
      {
         /** TODO: better error code */
         err = ErrorBadParam;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode driverReserveIrq(Driver drv, uint32 irq)
{
   ErrorCode err = ErrorNoError;

   //printf("reserving irq %d\n", irq);

   return err;
}

ErrorCode driverReservePort(Driver drv, uint32 port)
{
   ErrorCode err = ErrorNoError;

   //printf("reserving port %d\n", port);

   return err;
}

ErrorCode driverReserveMemory(Driver drv, uint32 page)
{
   ErrorCode err = ErrorNoError;

   //printf("reserving memory page %d\n", page);

   return err;
}

ErrorCode driverReserveIrqRange(Driver drv, uint32 lowest, uint32 highest)
{
   ErrorCode err = ErrorNoError;

   //printf("reserving irq range %d to %d\n", lowest, highest);

   return err;
}

ErrorCode driverReservePortRange(Driver drv, uint32 lowest, uint32 highest)
{
   ErrorCode err = ErrorNoError;

   //printf("reserving port range %d to %d\n", lowest, highest);

   return err;
}

ErrorCode driverReserveMemoryRange(Driver drv, uint32 lowest, uint32 highest)
{
   ErrorCode err = ErrorNoError;

   //printf("reserving memory range %d to %d\n", lowest, highest);

   return err;
}

ErrorCode driverDestroy(Driver *driver)
{
   ErrorCode err = ErrorNoError;

   if(driver)
   {
      free(*driver);
      *driver = NULL;
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

ErrorCode driverManagerInit(void)
{
   /* create root driverItem */
   driverItemCreateNamespace(&driverRoot, "driver");

   /* create the driver object list */
   listCreate(&driverCollection);
   listIteratorCreate(&driverCollectionIterator, driverCollection);

   /* create root driverContext */
   rootContext = (DriverContext)malloc(sizeof(struct _DriverContext));
   rootContext->location = driverRoot;
   rootContext->contextInfo = NULL;
   
   messageQueueCreate( &driverMgrQueueId, 0 );
   messageQueueMap( driverMgrQueueId, &driverMgrQueue );

   mutexCreate( &driverMgrMutexId, MutexTypeRecursive | MutexTypeFIFO );
   mutexMap( driverMgrMutexId, &driverMgrMutex );

}

ErrorCode driverManagerFinal(void)
{
   /** TODO: destroy all drivers first */
   listDestroy(&driverCollection);
}

ErrorCode driverManagerAdd(Driver driver)
{
   ErrorCode err = ErrorNoError;
   DriverItem addTo;
   uint32 size;

   printf("driverManagerAdd %x %x\n", driver, driver ? driver->rootItem : (DriverItem)-1);

   mutexLock( &driverMgrMutex, TimeoutInfinite );

   if(driver && driver->rootItem)
   {
      if(driver->baseLoc)
      {
         printf("***Ensuring %s, root is %x\n", driver->baseLoc, driverRoot);
         _driverManagerEnsure(driver->baseLoc, driverRoot, &addTo);
      }
      else
      {
         printf("***Adding directly to root\n");
         addTo = driverRoot;
      }

      /* add this driver to the hierarchy */
      driverItemAdd(addTo, driver->rootItem);

      /* and add it to the collection of drivers that make up the hierarchy */
      listAddFront(driverCollection, driver);
   }
   else
   {
      err = ErrorBadObject;
   }

   mutexUnlock( &driverMgrMutex );

   return err;
}

/* ensure the url described by 'loc' exists, and return the ending DriverItem of it */
static ErrorCode _driverManagerEnsure(String loc, DriverItem from, DriverItem *end)
{
   ErrorCode err = ErrorNoError;
   DriverItem nextFrom;
   uint32 i, len;
   String endLoc;

   /* find the location of the next . or end-line */
   //printf("___ensure %s ->", loc);
   endLoc = (String)strchr(loc, '.');
   //printf(" %d\n", endLoc);
   if(endLoc == (String)NULL)
   {
      /* at the end of the url, so save off the result in end */
      err = _driverManagerFindOrAdd(loc, strlen(loc), from, end);
   }
   else
   {
      *endLoc = '\0';           // why can't I do this?
      _driverManagerFindOrAdd(loc, endLoc - loc, from, &nextFrom);
      err = _driverManagerEnsure(++endLoc, nextFrom, end);
   }

   return err;
}

static ErrorCode _driverManagerFindOrAdd(String name, uint32 len, DriverItem from, DriverItem *end)
{
   ErrorCode err = ErrorNoError;
   DriverItem match, namespace;

   /* loop through subItems until we find one coresponding to 'loc.'  If not found, 
    * add a new one
    */

   // does this item already exist?
   if(listFind(from->subItems, _driverManagerListComparator, name, (Pointer *)&match) ==
      ErrorNoError)
   {
      *end = match;
   }
   else
   {
      // create a new item (of type namespace) and add it into the train of items
      driverItemCreateNamespace(&namespace, name);
      driverItemAdd(from, namespace);

      *end = namespace;
   }

   return err;
}

Boolean _driverManagerListComparator(Pointer key, Pointer data)
{
   String one = (String)key;
   String two;

   two = (String)((DriverItem)data)->name;
   printf("_driverManagerListComparator %x(%s) %x(%s)\n", key, one, data, two);

   return (strcmp(one, two) == 0);
}

ErrorCode driverManagerRemove(Driver driver)
{
   ErrorCode err = ErrorNoError;

   // remove driver->rootItem from driverRoot (Ensure not to remove shared
   // DriverItem's

   listRemove(driverCollection, driver);

   return err;
}

ErrorCode driverManagerShowTree(void)
{
   DriverItem item;
   ErrorCode ec = ErrorNoError;

   mutexLock( &driverMgrMutex, TimeoutInfinite );

   ec = _driverManagerShowTree(driverRoot, 0);

   mutexUnlock( &driverMgrMutex );

   return ec;
}

ErrorCode _driverManagerShowTree(DriverItem rootItem, uint32 level)
{
   uint32 i;
   DriverItem item;

   for(i = 0; i < level; i++)
      printf(" ");
   printf("0x%8x 0x%8x %s\n", rootItem, rootItem->name, rootItem->name);

   listIteratorReset(rootItem->subItemsIterator);
   while(listIteratorGetNext(rootItem->subItemsIterator, (Pointer *)&item) == ErrorNoError)
   {
      _driverManagerShowTree(item, level + 1);
   }
}

ErrorCode driverContextCreate(DriverContext *dc, String url)
{
   DriverContext context;
   ErrorCode err;

   /* remember to remove prepended "driver." if necessary */
   if(strncmp(url, "driver.", strlen("driver.")) == 0)
   {
      /* else if the namespace begins with driver., remove it */
      url += strlen("driver.");
   }

   err = driverContextTraverse(&context, rootContext, url);
   *dc = context;

   return err;
}

ErrorCode driverContextExecute(DriverContext dc, String url)
{
   ErrorCode err = ErrorNoError;
   struct _DriverContext dcCopy;
   int i;
   static int test = 0;

   mutexLock( &driverMgrMutex, TimeoutInfinite );

   /**
   * Note that we've made a copy of dc on the stack due to the fact that 
   * _driverContextTraverse() will corrupt the value in its 'from' argument.
   * It uses 'from' to perform incremental updates and then eventually copy into 'to'
   */
   /*
      dcCopy.driver = dc->driver;
      dcCopy.location = dc->location;
      dcCopy.contextInfo = dc->contextInfo;
    */
   dcCopy = *dc;

   if(dc)
   {
      if(url)
      {
         printf("Executing to %s\n", url);
         /* to is NULL... we don't save off the resultant context in an execute */
         _driverContextTraverse(NULL, &dcCopy, url,
                                DriverContextTraversalTraverse | DriverContextTraversalExecute);
      }
      else
      {
         err = ErrorBadParam;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   mutexUnlock( &driverMgrMutex );

   return err;
}

ErrorCode driverContextTraverse(DriverContext *to, DriverContext from, String url)
{
   ErrorCode err = ErrorNoError;
   struct _DriverContext dcCopy; // = *from;
   DriverContext context;

   mutexLock( &driverMgrMutex, TimeoutInfinite );

   /**
	* For the same reasons stated in driverContextExecute(), we make a copy of 'from' 
	* before passing into _driverContextTraverse()
	*/
   memcpy(&dcCopy, from, sizeof(struct _DriverContext));

   context = (DriverContext)malloc(sizeof(struct _DriverContext));

   if(from && to)
   {
      if(url)
      {
         printf("Travsering to %s\n", url);
         _driverContextTraverse(context, &dcCopy, url,
                                DriverContextTraversalTraverse | DriverContextTraversalExecute);
         *to = context;
      }
      else
      {
         err = ErrorBadParam;
      }
   }
   else
   {
      err = ErrorBadObject;
   }

   mutexUnlock( &driverMgrMutex );

   return err;
}

/**
* This is the private handler function for execution and traversal of driver context's.
* @param to   A pointer of where to store the resultant context upon full traversal.  Specify NULL 
*             if the caller doesn't care
* @param from A DriverContext to start from.  This variable will be altered through execution so a copy 
*             should be passed if no changes are expected.
* @param url  The path to traverse/execute
* @param type Specifies the types of traveral that are allowed
*/
static ErrorCode _driverContextTraverse(DriverContext to, DriverContext from, String url,
                                        DriverContextTraversalType type)
{
   ErrorCode err = ErrorNoError;
   DriverItem newItem;
   char itemName[DRIVER_ITEM_MAX_LENGTH + 1];
   String endLoc;

   /* extract the next chunk and save off in itemName */
   endLoc = url;
   _driverUrlParseName(itemName, &endLoc);

   printf("___traverse %s %x %x\n", itemName, from, from->location);
   if( from->location && listFind
      (from->location->subItems, _driverManagerListComparator, itemName, (Pointer *)&newItem) == 0)
   {
      /* immediatly traverse to this point (ie, simply update the current location */
      from->location = newItem;
      //printf("___setting location to %x %s\n", newItem, newItem->name);

      if(newItem->driver)
      {
         from->driver = newItem->driver;
      }

      /* okay, we've found the correct item, perform any necesary actions to travserse to it */
      if(newItem->type == DriverItemTypeFunction)
      {
         DriverVariable *vars, returnVar;

         /* this is a function call */
         if(type & DriverContextTraversalExecute)
         {
            printf("___traverse function call, %d params\n", newItem->numParams);
            if(newItem->numParams)
            {
               vars = (DriverVariable *)malloc(sizeof(DriverVariable) * newItem->numParams);
            }
            else
            {
               vars = NULL;
            }

            if(_driverUrlParseParameterList(vars, newItem, &endLoc) == ErrorNoError)
            {
               if(_driverPerformFunction(from, vars, &returnVar) == ErrorNoError)
               {
                  /* function call succeeded! */
               }
               else
               {
                  /* function call failed */
               }
            }
         }
         else
         {
            err = ErrorNotAvailable;
         }
      }
      else if(newItem->type == DriverItemTypeParameter)
      {
         DriverVariable var;

         /* this is a property */
         if(type & DriverContextTraversalProperty)
         {
            if(_driverUrlParseCharacter('=', &endLoc) == ErrorNoError)
            {
               /* parse in the variable assignment */
               if(_driverPerformSet(from, &var) == ErrorNoError)
               {
                  // set succeeded!
               }
               else
               {
                  // set failed!
               }
            }
            else
            {
               /* no equals, therefore must be a get routine */
               if(_driverPerformGet(from, &var) == ErrorNoError)
               {
                  /* get succeeded */
               }
               else
               {
                  /* get failed */
               }
            }
         }
         else
         {
            /** TODO: new error? */
            err = ErrorNotAvailable;
         }
      }
      else
      {
         /* this is a namespace, never anything to do here */
      }

      //if(endLoc == NULL)
      if(endLoc[0] != '.')
      {
         if(to)
         {
            /* NEEDS TO BE A COPY... IS THERE A BETTER WAY OF DOING THIS? */
            *to = *from;
         }
      }
      else
      {
         /** TODO: update contextInfo by calling functions, if available */
         err = _driverContextTraverse(to, from, endLoc + 1, type);
      }
   }
   else
   {
      err = ErrorNotFound;
   }

   return err;
}

static ErrorCode _driverUrlParseWhitespace(String *url)
{
   ErrorCode err = ErrorNoError;
   String str = *url;
   uint32 pos = 0;

   /* skip over white space */
   while(isspace(str[pos]))
      pos++;

   *url = &str[pos];

   return err;
}

static ErrorCode _driverUrlParseCharacter(char character, String *url)
{
   ErrorCode err = ErrorNoError;
   String str;

   //printf("___parsing char %c %s\n", character, *url);
   _driverUrlParseWhitespace(url);

   str = *url;

   //printf("___char %c\n", str[0]);

   if(str[0] == character)
   {
      *url = &str[1];
   }
   else
   {
      err = ErrorNotFound;
   }

   return err;
}

/**
 * @param name is a caller allocate string of atleast DRIVER_ITEM_MAX_LENGTH characters
 * @param url is the string to parse.  The ending position is then returned in url.
 */
static ErrorCode _driverUrlParseName(String name, String *url)
{
   ErrorCode err = ErrorNoError;
   String str = *url;
   uint32 pos = 0, namePos = 0;

   /* skip over white space */
   while(str[pos] == ' ')
      pos++;

   /* copy identifier into name (must start with letter!) */
   if(isalpha(str[pos]))
   {
      name[namePos++] = str[pos++];

      /* but the rest can be alpha-numeric */
      while(isalnum(str[pos]))
      {
         name[namePos++] = str[pos++];

         /* ensure we haven't been supplied an invalid identifier (too long!) */
         if(namePos > DRIVER_ITEM_MAX_LENGTH)
         {
            err = ErrorBadParam;
            break;
         }
      }

      /* remember to null terminate, and update url */
      if(!err)
      {
         name[namePos] = '\0';
         *url = &str[pos];
      }
   }
   else
   {
      err = ErrorBadParam;
   }
   return err;
}

static ErrorCode _driverUrlParseType(DriverVariable *var, DriverParamType type, String *url)
{
   ErrorCode err = ErrorNoError;
   Boolean isSigned;
   String str = *url;
   uint32 pos = 0, namePos = 0, bitLength = 0;

   /* skip over white space */
   while(str[pos] == ' ')
      pos++;

   printf("_driverUrlParseType %d\n", type );

   switch (type)
   {
   case DriverParamVoid:
      memset(var, 0, sizeof(DriverVariable));
      break;

   case DriverParamCharacter:
      _driverUrlParseCharacter('\'', url);
      _driverUrlParseWhitespace(url);
      str = *url;
      var->character = str[pos];
      *url = &str[pos + 1];
      _driverUrlParseCharacter('\'', url);
      break;

   case DriverParamString:
      _driverUrlParseCharacter('"', url);
      _driverUrlParseCharacter('"', url);
      break;
   case DriverParamStringUtf:
      _driverUrlParseCharacter('"', url);
      _driverUrlParseCharacter('"', url);
      break;

   case DriverParamInteger64:
      bitLength = 64;
      /* fallthrough... */
   case DriverParamInteger32:
      if(!bitLength)
         bitLength = 32;
      /* fallthrough... */
   case DriverParamInteger16:
      if(!bitLength)
         bitLength = 16;
      /* fallthrough... */
   case DriverParamInteger8:
      isSigned = True;
      if(!bitLength)
         bitLength = 8;
      _driverUrlParseInteger(var, isSigned, bitLength, url);
      break;

   case DriverParamUnsignedInteger64:
      bitLength = 64;
      /* fallthrough... */
   case DriverParamUnsignedInteger32:
      if(!bitLength)
         bitLength = 32;
      /* fallthrough... */
   case DriverParamUnsignedInteger16:
      if(!bitLength)
         bitLength = 16;
      /* fallthrough... */
   case DriverParamUnsignedInteger8:
      isSigned = False;
      if(!bitLength)
         bitLength = 8;
      _driverUrlParseInteger(var, isSigned, bitLength, url);
      break;

   case DriverParamBoolean:
      break;

   case DriverParamPointer:
      isSigned = False;
      bitLength = sizeof(Pointer)*8;
      _driverUrlParseInteger( var, isSigned, bitLength, url);
      break;
   }

   return err;
}

static ErrorCode _driverUrlParseInteger(DriverVariable *var, Boolean isSigned, uint32 bitLength,
                                        String *url)
{
   ErrorCode err = ErrorNoError;
   Boolean isNegative = False;
   int64 number = 0;
   uint8 digit;

   //printf("___parse int\n");

   /* @TODO: parse 0xhex and 16_hex, 8_oct, etc formats */

   if(_driverUrlParseCharacter('-', url) == ErrorNoError)
   {
      if(isSigned)
      {
         isNegative = True;
      }
      else
      {
         err = ErrorBadParam; /** TODO: new err code for parsing urls? */
      }
   }

   while(_driverUrlParseDigit(&digit, url) == ErrorNoError)
   {
      //printf("___got digit %d\n", (int)digit);
      number *= 10;
      number += digit;
   }

   if(isSigned)
   {
      number = -number;
   }

   (*var).longInteger = number;

   //printf("___parsed integer %d\n", (uint32)number);

   return err;
}

static ErrorCode _driverUrlParseDigit(uint8 *digit, String *url)
{
   ErrorCode err;

   if(isdigit(*url[0]))
   {
      err = ErrorNoError;
      *digit = *url[0] - '0';

      (*url)++;
   }
   else
   {
      *digit = 0;
      err = ErrorNotFound;
   }

   return err;
}

static ErrorCode _driverUrlParseAssignment(DriverVariable *var, DriverParamType type, String *url)
{
   ErrorCode err = ErrorNoError;
   String str = *url;
   uint32 pos = 0, namePos = 0;

   /* skip over white space */
   while(str[pos] == ' ')
      pos++;

   if(str[pos] == '=')
   {
      *url = &str[pos];
      err = _driverUrlParseType(var, type, url);
   }
   else
   {
      err = ErrorBadParam;
   }

   return err;
}

static ErrorCode _driverUrlParseParameterList(DriverVariable *vars, DriverItem item, String *url)
{
   ErrorCode err = ErrorNoError;
   DriverParam param;
   int i;

   /** TODO: check error codes */

   //printf("___numParams %d %s\n", item->numParams, *url);

   _driverUrlParseCharacter('(', url);

   for(i = 0; i < item->numParams; i++)
   {
      param = item->params[i];

      printf("___parse param %d\n", i );

      //printf("___param %s type %d\n", param->name, param->type);
      _driverUrlParseType(&vars[i], param->type, url);

      if(i != item->numParams - 1)
      {
         _driverUrlParseCharacter(',', url);
      }
   }

   _driverUrlParseCharacter(')', url);
   /* parse ) */

   return err;
}

static ErrorCode _driverPerformFunction(DriverContext ctx, DriverVariable *input,
                                        DriverVariable *output)
{
   ErrorCode err = ErrorNoError;
   MessageId msg, outputMsg;

   //printf("___performing function call for %s\n", ctx->location->name);

   printf("___sending message to driver %x (%x,%x,%x)\n", ctx->driver, ctx, input, output);
   printf("___function call of %d arguments value=%d\n", ctx->location->numParams, input[0].integer );
   //printf("___func %x str %s\n", ctx->location->handlerFunc, ctx->location->name);
   /* should have to pass in the MessageQueueId!!! */
   if(messageCreate(&msg, NULL, driverMgrQueueId, NULL, DRIVER_MESSAGE_SENDER_ID, 
                    DriverEventExecuteFunction, 3, MessageParamPointer(ctx), 
                    MessageParamBuffer( &input[0], ctx->location->numParams * sizeof(DriverVariable) ),
                    MessageParamPointer( ctx->location->privateData ) ) == ErrorNoError)
   {
      //printf("___sending message to queue %x\n", ctx->driver->queue);
      /*
      if(localMessageQueueSend(ctx->driver->queue, (Pointer)msg) == ErrorNoError)
      {
         localSemaphoreDown(ctx->driver->syncSemaphore, TimeoutInfinite);
      }
      */
      /* @TODO ensure drivers return back a DriverVariable structure... */
      //messageQueueSendAndReceive( &ctx->driver->queue, msg, &outputMsg );
      _driverProcessMessage( ctx, msg, output );
   }
   else
   {
      printf("%s failed on line %d\n", __func__, __LINE__ );
      assert(0);
   }

   return err;
}

static ErrorCode _driverPerformGet(DriverContext ctx, DriverVariable *output)
{
   ErrorCode err = ErrorNoError;
   MessageId msg, outputMsg;

   //printf("___performing get on %s\n", ctx->location->name);

   if(messageCreate(&msg, NULL, driverMgrQueueId, NULL, DRIVER_MESSAGE_SENDER_ID, 
                     DriverEventQueryParameter, 3, MessageParamPointer(ctx), MessageParamNull, 
                     MessageParamPointer( ctx->location->privateData ) ) == ErrorNoError)
   {
      //printf("___sending message\n");
      //localMessageQueueSend(ctx->driver->queue, (Pointer)msg);
      //localSemaphoreDown(ctx->driver->syncSemaphore, TimeoutInfinite);
      //messageQueueSendAndReceive( &ctx->driver->queue, msg, &outputMsg );
      _driverProcessMessage( ctx, msg, output );
   }
   else
   {
      printf("%s failed on line %d\n", __func__, __LINE__ );
      assert(0);
   }

   return err;
}

static ErrorCode _driverPerformSet(DriverContext ctx, DriverVariable *input)
{
   ErrorCode err = ErrorNoError;
   DriverVariable output;

   MessageId msg, outputMsg;

   //printf("___performing set on %s\n", ctx->location->name);

   if(messageCreate(&msg, NULL, driverMgrQueueId, NULL, DRIVER_MESSAGE_SENDER_ID, 
                     DriverEventSetParameter, 3, MessageParamPointer(ctx), 
                     MessageParamTypePtr( input, DriverVariable ),
                     MessageParamPointer( ctx->location->privateData )) == ErrorNoError)
   {
      //printf("___sending message\n");
      //localMessageQueueSend(ctx->driver->queue, (Pointer)msg);
      //localSemaphoreDown(ctx->driver->syncSemaphore, TimeoutInfinite);
      //messageQueueSendAndReceive( &ctx->driver->queue, msg, &outputMsg );
      /** @TODO use output param?  Reflects value of the set operation? */
      _driverProcessMessage( ctx, msg, &output );
   }
   else
   {
      printf("%s failed on line %d\n", __func__, __LINE__ );
      assert(0);
   }

   return err;
}

static ErrorCode _driverProcessMessage( DriverContext ctx, MessageId msg, DriverVariable *result )
{
   ErrorCode ec;
   MessageId recvId;
   
   // map on demand...
   if( ctx->driver->queue == NULL )
   {
      messageQueueMap( ctx->driver->queueId, &ctx->driver->queue );
   }
   
   /* return path of msg is always driverMgrQueue */
   printf("sending msg 0x%8x to 0x%8x\n", msg, ctx->driver->queue);
   ec = messageQueueSend( &ctx->driver->queue, (Pointer)msg );
   if( ec == ErrorNoError )
   {
      printf("about to wait on driverMgrQueue 0x%8x\n", driverMgrQueue );
      ec = messageQueueReceive( &driverMgrQueue, TimeoutInfinite, (Pointer*)&recvId );
      if( ec == ErrorNoError )
      {
         Message recvMsg;
         
         printf("received msg 0x%8x from 0x%8x\n", recvId, driverMgrQueue);
         
         ec = messageMap( recvId, &recvMsg );
         if( ec == ErrorNoError )
         {
            DriverVariable var;
            
            ec = messageGetArgument( recvMsg, 1, (Pointer*)&var );
            if( ec == ErrorNoError )
            {
               printf("RESULT IS 0x%8x\n", var.integer);
               *result = var;
            }
         }
      }
   }
   
   return ec;   
}

ErrorCode driverContextDestroy(DriverContext *dc)
{
   ErrorCode err = ErrorNoError;

   if(dc)
   {
      free(*dc);

      *dc = NULL;
   }
   else
   {
      err = ErrorBadObject;
   }

   return err;
}

/** @TODO: this is, effectively, removed -- transitioned into driver space! */
static void _driverCommonEntry(void *arg)
{
   ErrorCode err = ErrorNoError;
   //DriverItemHandlerFunction function;
   Boolean running = True;
   Driver drv = (Driver)arg;
   DriverVariable *returnVar;
   DriverVariable *inputVars;
   DriverContext context;
   DriverItem drvItem;
   LocalMessageQueue queue;
   MessageId messageId;
   Message message;
   Pointer *list;
   uint32 numArgs;
   uint32 code;
   Task task;

   printf("In driverCommonEntry, driver is %x\n", drv);

   taskGetCurrent(&task);
   drv->task = task;

   /* call the init function */
   drv->initReturnCode = drv->init(drv);
   //localSemaphoreUp(drv->syncSemaphore);

   if(drv->initReturnCode == ErrorNoError)
   {
      /* note: the task's message queue is now useless...!!! */
      driverGetMessageQueue(drv, &queue);
      do
      {
         printf("Waiting for driver event\n");
         localMessageQueueReceive(queue, TimeoutInfinite, (Pointer *)&messageId);
         
         messageMap( messageId, &message );
         printf("mappedId 0x%8x to 0x%8x\n", messageId, message );
         //assert( 0 );

         messageGetCode(message, &code);
         switch (code)
         {
         case DriverEventExecuteFunction:
            printf("___got execute event\n");
            messageGetArgument(message, 1, (Pointer *)&context);
            messageGetArgument(message, 2, (Pointer *)&inputVars);
            messageGetArgument(message, 3, (Pointer *)&returnVar);
            //messageGetArgumentList(message, (Pointer*)&list, &numArgs);

            printf("___got %x %x %x\n", context, inputVars, returnVar);

            //printf("item: %s\n", context->location->name);

//            function = context->location->handlerFunc;
            //printf("___function is %x str %s\n", function, context->location->name);
//            *returnVar = function(context->driver, inputVars);
            printf("___got returnValue 0x%8x\n", *returnVar);
            //localSemaphoreUp(drv->syncSemaphore);

            /*
             * construct a response message
             * messageResponse(message, response);
             */
            break;
         case DriverEventQueryParameter:
            printf("___got get event\n");
            //semaphoreUp(drv->syncSemaphore);
            break;
         case DriverEventSetParameter:
            printf("___get set event\n");
            //semaphoreUp(drv->syncSemaphore);
            break;
         case DriverEventQuit:
            running = False;
            break;
         }
         messageDestroy(&message);

      }
      while(running);

      printf("DriverFinal: %x\n", drv->final);
      drv->final(drv);
   }
   else
   {
      err = ErrorNotFound;      // TODO: real error code!
   }

   assert(0);

   //return err;
   return;
}
