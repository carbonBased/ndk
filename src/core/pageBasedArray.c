/* ndk - [ pageBasedArray.c ]
 *
 * Page allocated array utility
 *
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include "pageBasedArray.h"
#include "arch/i386/pager.h"
#include <brink/localMutex.h>
#include <core/propDb.h>
#include <console.h>
#include <stdarg.h>

struct _PageArray
{
   uint32   sizeOfElement;
   uint32   sizeOfArray;
   uint32   sizeOfRun;
   uint32   numberOfRuns;
   uint32   elementsPerRun;
   Pointer *nextAvailableRunPointer;
   Pointer *endOfRunPointers;
   Boolean  useSetFlag;
   uint32   numInlineRuns;
   Pointer  run[1];
}; /* must be the size of one page */

typedef struct _PageArrayTOC
{
   Pointer run[1];
} PageArrayTOC;

typedef union _PageArrayEntry
{
   struct
   {
      Boolean isSet;
      uint8 payload[1];
   } withSetFlag;
   struct
   {
      uint8 payload[1];
   } withoutSetFlag;
} PageArrayEntry;

#if Property_trace_pageBasedArray_verbose == 1
#define VPRINTF(x) printf x
#else
#define VPRINTF(x)
#endif

static ErrorCode _pageArrayGetItem( PageArray handle, uint32 elementNum, Pointer location, Boolean pointerCopy );
static ErrorCode _pageArrayGetRun( PageArray handle, uint32 numRun, Pointer *runBase );
static ErrorCode _pageArrayEnsure( PageArray handle, uint32 size );
static ErrorCode _pageArrayAllocRun( PageArray handle );

ErrorCode pageArrayCreate(PageArray *handle, uint32 sizeOfElement, uint32 maxPagesPerRun, Boolean setFlag)
{
   ErrorCode err = ErrorNoError;
   uint32 pageSize, blockSize, runSize, wastage;
   PhysicalAddress physAddr;
   LinearAddress linAddr;
   struct _PageArray *pageArray;
   uint32 i;
   
   pagerGetPageSize(&pageSize);
   
   /* make sure we have space for the isSet boolean, if required */
   if( setFlag )
   {
      sizeOfElement += sizeof( Boolean );
   }
   
   if( sizeOfElement > pageSize )
   {
      /* round up to the nearest multiple of pageSize */
      blockSize = ((uint32)((sizeOfElement / pageSize) + 1)) * pageSize;
   }
   else
   {
      blockSize = pageSize;
   }
   
   /* 
    * calculate the ideal run size for least ammount of memory wastage
    * via sizeOfAllocation / remainderPerBlockSize
    */
   if( blockSize % sizeOfElement == 0 )
   {
      runSize = blockSize / pageSize;
   }
   else
   {
      uint32 remainderPerBlock = blockSize % sizeOfElement;
      uint32 requiredPerBlock = sizeOfElement - remainderPerBlock;
      
      /** @TODO: FIX THIS */
      runSize = ( sizeOfElement / requiredPerBlock );
   }
   
   if( maxPagesPerRun && (runSize > maxPagesPerRun) )
   {
      runSize = maxPagesPerRun;
   }
   
   wastage = (runSize * blockSize) % sizeOfElement;
   
   VPRINTF(("calculated optimal size of run for %d bytes is %d pages (%d bytes wastage)\n", sizeOfElement, runSize, wastage ));
   VPRINTF(("blockSize %d maxPagesPerRun %d\n", blockSize, maxPagesPerRun ));
   

   /* allocate first page for page array TOC */
   /*
   linAddr = todo_top_of_kernel_heap;
   pagerGetNewPhysical( &physAddr );
   pagerMapPhysicalToLinear( physAddr, linAddr );
   */
   pagerAllocate( pagerHeapKernel, 1, &linAddr );
   
   VPRINTF(("received 0x%x for PageArray\n", linAddr ));
   
   pageArray = linAddr;
   pageArray->sizeOfArray = 0;
   pageArray->numberOfRuns = 0;
   pageArray->sizeOfElement = sizeOfElement;
   pageArray->sizeOfRun = runSize;
   pageArray->elementsPerRun = (runSize * blockSize) / sizeOfElement;
   pageArray->nextAvailableRunPointer = (Pointer*)&pageArray->run[0];
   pageArray->endOfRunPointers = (Pointer*)((uint32)pageArray + pageSize - sizeof(Pointer));
   pageArray->useSetFlag = setFlag;
   pageArray->numInlineRuns = ((pageSize - sizeof( struct _PageArray ) ) / sizeof(Pointer)) + 1;
   /** @TODO use numInlineRuns */
   
   /* 
    * fill to remainder of page (the runs) with 0's -> initially empty array, no runs 
    * allocated
    */
   VPRINTF(("page array has %d inline items\n", pageArray->numInlineRuns ));
   for( i = 0; i < pageArray->numInlineRuns; i++ )
   {
      //printf("setting 0x%x + %i to 0\n", pageArray, i ); 
      pageArray->run[i] = 0;
      //printf("0x%x == 0x%x\n", &pageArray->run[i], pageArray->run[i] );
   }
   
   *handle = pageArray;
   
   return err;
}

ErrorCode pageArrayDestroy(PageArray *handle)
{
   ErrorCode err = ErrorNoError;
   
   return err;
}

/**
 * @TODO: Test!
 * Same as pageArrayGetItem, effectively, but returns the pointer to the item.
 * Can only be used for arrays which do not use the setFlag (in order to preserve the 
 * itegrity of the setFlag)
 */
ErrorCode pageArrayGetItemPointer(PageArray handle, uint32 elementNum, Pointer *location)
{
   ErrorCode err;
   
   if( handle->useSetFlag )
   {
      err = ErrorBadParam;
   }
   else
   {
      err = _pageArrayGetItem( handle, elementNum, (Pointer)location, True );
   }
   
   return err;
}

ErrorCode pageArrayGetItem(PageArray handle, uint32 elementNum, Pointer location)
{
   return _pageArrayGetItem( handle, elementNum, location, False );
}

ErrorCode pageArraySetItem(PageArray handle, uint32 elementNum, Pointer data )
{
   ErrorCode err = ErrorNoError;
   
   _pageArrayEnsure( handle, elementNum+1 );
   
   VPRINTF(("_pageArraySetItem(%d,0x%x)\n", elementNum, data)); 
   
   /* 
    * technically, the above call to ensure should allow the following condition
    * to always be met, but... just in case, it's checked...
    */
   if( elementNum <= handle->sizeOfArray )
   {
      uint32 runNum;
      uint8 *runBase;
      
      /* find out which run this element would be in */
      runNum = elementNum / handle->elementsPerRun;
      VPRINTF(("in run %d\n", runNum));
      if( _pageArrayGetRun( handle, runNum, (Pointer*)&runBase ) == ErrorNoError )
      {
         uint32 elementInRun = elementNum % handle->elementsPerRun;
         PageArrayEntry *element;
         Boolean isSet;
         
         element = (PageArrayEntry*) &runBase[ handle->sizeOfElement * elementInRun ];
         VPRINTF(("element is %d in run 0x%x, element == 0x%x\n", elementInRun, runBase, element));
         
         if( handle->useSetFlag )
         {
            element->withSetFlag.isSet = True;
            VPRINTF(("setting 0x%x to 0x%x\n", element->withSetFlag.payload, data));
            memcpy( element->withSetFlag.payload, data, handle->sizeOfElement - sizeof( Boolean ) );
         }
         else
         {
            VPRINTF(("setting 0x%x to 0x%x\n", element->withoutSetFlag.payload, data));
            memcpy( element->withoutSetFlag.payload, data, handle->sizeOfElement );
         }
      }
   }
   else
   {
      err = ErrorOutOfBounds;
   }
   
   return err;
}

ErrorCode pageArrayGetRunSize(PageArray handle, uint32 *runSize)
{
   ErrorCode err = ErrorNoError;
   
   *runSize = handle->sizeOfRun;
   
   return err;
}

ErrorCode pageArrayGetArraySize(PageArray handle, uint32 *arraySize)
{
   ErrorCode err = ErrorNoError;
   
   *arraySize = handle->sizeOfArray;
   
   return err;
}

ErrorCode pageArrayGetElementSize(PageArray handle, uint32 *elementSize)
{
   ErrorCode err = ErrorNoError;
   
   /* be sure to account for the isSet boolean */
   *elementSize = handle->sizeOfElement - sizeof( Boolean );
   
   return err;
}

static ErrorCode _pageArrayGetItem( PageArray handle, uint32 elementNum, Pointer location, Boolean pointerCopy )
{
   ErrorCode err = ErrorNotFound;
   
   if( elementNum < handle->sizeOfArray )
   {
      uint32 runNum;
      uint8 *runBase;
      
      /* find out which run this element would be in */
      runNum = elementNum / handle->elementsPerRun;
      VPRINTF(("pageArrayGetItem %d in run %d\n", elementNum, runNum ));
      if( _pageArrayGetRun( handle, runNum, (Pointer*)&runBase ) == ErrorNoError )
      {
         uint32 elementInRun = elementNum % handle->elementsPerRun;
         PageArrayEntry *element;
         Boolean isSet;
         
         VPRINTF(("got run\n"));
         
         element = (PageArrayEntry*) &runBase[ handle->sizeOfElement * elementInRun ];
         
         VPRINTF(("element 0x%x\n", element ));
         
         if( handle->useSetFlag )
         {
            VPRINTF(("setFlag %d\n", element->withSetFlag.isSet ));
            if( element->withSetFlag.isSet )
            {
               /* pointerCopy must be False! */
               memcpy( location, element->withSetFlag.payload, handle->sizeOfElement - sizeof(Boolean) );
               err = ErrorNoError;
            }
            else
            {
               VPRINTF(("element not set!\n"));
            }
         }
         else
         {
            if( pointerCopy )
            {
               Pointer *pLoc = (Pointer*)location;
               
               *pLoc = element->withoutSetFlag.payload;
            }
            else
            {
               memcpy( location, element->withoutSetFlag.payload, handle->sizeOfElement );
            }
            err = ErrorNoError;
         }
      }
   }
   else
   {
      err = ErrorOutOfBounds;
   }
   
   return err;   
}

static ErrorCode _pageArrayGetRun( PageArray handle, uint32 numRun, Pointer *runBase )
{
   ErrorCode err = ErrorNoError;
   uint32 pageSize, runsPerToc, i = 0;
   Pointer *tocBegin, *tocEnd;
   Boolean found = False;
   
   pagerGetPageSize( &pageSize );
   tocBegin = &handle->run[0];
   tocEnd = (Pointer)((uint32)handle + pageSize - sizeof(Pointer));
   
   VPRINTF(("_pageArrayGetRun %d totalRuns %d\n", numRun, handle->numberOfRuns ));
   
   if( numRun < handle->numberOfRuns )
   {
      while( !found )
      {
         runsPerToc = ((uint32)tocEnd - (uint32)tocBegin) / sizeof(Pointer);
         VPRINTF(("searching toc page %d (0x%x - 0x%x), runsPerTocPage %d\n", 
            i, tocBegin, tocEnd, runsPerToc));
         if( numRun < runsPerToc )
         {
            *runBase = tocBegin[numRun];
            found = True;
            VPRINTF(("Returning run of 0x%x\n", *runBase));
            break;
         }
         else
         {
            numRun -= runsPerToc;
            tocBegin = tocBegin[runsPerToc];
            tocEnd = tocBegin[ pageSize / sizeof(Pointer) - sizeof(Pointer) ];
         }
      }
   }
   else
   {
      err = ErrorNotFound;
   }

   return err;   
}
   

static ErrorCode _pageArrayEnsure( PageArray handle, uint32 size )
{
   ErrorCode err = ErrorNoError;
   
   VPRINTF(("_pageArrayEnsure size of %d, currently %d\n", size, handle->sizeOfArray));
   
   while( handle->sizeOfArray < size )
   {
      err = _pageArrayAllocRun( handle );
   }
   
   return err;
}

static ErrorCode _pageArrayAllocRun( PageArray handle )
{
   ErrorCode err = ErrorNoError;
   LinearAddress runAddr;
   PageArrayTOC *toc;
   uint32 pageSize;
   
   VPRINTF(("_pageArrayAllocRun\n"));
   
   pagerGetPageSize(&pageSize);

   if( pagerAllocate( pagerHeapKernel, handle->sizeOfRun, &runAddr ) == ErrorNoError )
   {
      /* allocate a run, and add it to the end of our array */
      *handle->nextAvailableRunPointer = (Pointer)runAddr;
      handle->nextAvailableRunPointer++;
      
      /* 
       * ensure the run is 0'd out, as we need to ensure the setFlag is False, 
       * and any user defined data which might double as a setFlag is also False
       */
      memset( runAddr, 0, handle->sizeOfRun * pageSize );

      handle->sizeOfArray += handle->elementsPerRun;
      handle->numberOfRuns++;
      
      /* if we're at the last item in the TOC list, allocate another page to the TOC */
      if( handle->nextAvailableRunPointer == handle->endOfRunPointers )
      {
         VPRINTF(("_pageArrayAllocRun, requires new TOC page\n"));
         
         pagerAllocate( pagerHeapKernel, 1, (Pointer*)&toc );
         
         *handle->nextAvailableRunPointer = (Pointer)toc;
         
         handle->nextAvailableRunPointer = (Pointer)&toc->run[0];
         handle->endOfRunPointers = (Pointer)&toc->run[pageSize / sizeof(Pointer) - 1];
         
         VPRINTF(("new nextAvailableRunPointer 0x%x endOfRunPointers 0x%x\n", 
            handle->nextAvailableRunPointer, handle->endOfRunPointers));
      }
   }
   else
   {
      err = ErrorOutOfMemory;
   }
   
   return err;
}
