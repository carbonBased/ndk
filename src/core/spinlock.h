/* ndk - [ spinlock.h ]
 *
 * Routines for implementing multiprocessor spinlocks
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
 * @defgroup spinlock spinlock.h: spinlock routines
 * @ingroup core
 *
 * Defines interfaces for implementing mutual exclusion on multiprocess
 * systems using spinlocks
 */
/** @{ */

typedef _Spinlock *Spinlock;

/**
 * Create a spinlock
 *
 * @param spin The spinlock to create
 */
ErrorCode spinlockCreate(Spinlock * spin);

/**
 * Destroy a spinlock
 *
 * @param spin The spinlock to delete
 */
ErrorCode spinlockDestroy(Spinlock * spin);

/**
 * Attempt to lock a spinlock
 *
 * @param spin The spinlock to lock
 * @param timeout The maximum time to wait to lock the spinlock
 */
ErrorCode spinlockLock(Spinlock spin, Timeout timeout);

/**
 * Unlock a spinlock
 *
 * @param spin The spinlock to unlock
 */
ErrorCode spinlockUnlock(Spinlock spin);

/** @} */
