
/* ndk - [ objectRDOFF2.h ]
 *
 * Object Parser for RDOFF2 objects
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Object ObjectRDOFF2.h: RDOFF2 Object Parser
* @ingroup core
*
* This file defines an object parser for the RDOFF2 object format
*/
/** @{ */

#include "object.h"
#include "objectRDOFF2.h"
#include "arch/i386/pager.h"
#include <generated/properties.h>
#include <assert.h>

// STUPID THING!  Shouldn't need to redefine... and can't redefine the whole #if ... else
// or else it doesn't work!
#undef interface
#define interface(n) rdoff2_##n

#if Property_trace_rdoff2_verbose == 1
#define VPRINTF(x) printf x
#else
#define VPRINTF(x)
#endif

/**
 */
#define	RDOFF2_SYMBOL_ID		(0x10000000)

// TODO: better method of this... shouldn't have access to
// the object structure?
typedef struct _ObjectRDOFF2
{
   uint32 version;
   uint32 headerLength;
   uint32 headerOffset;
   uint32 length;
   uint32 pos;
   uint8 *mem;
   uint8 *end;
} *ObjectRDOFF2;

static ErrorCode rdoff2Create(Object obj, void *buffer, uint32 length);
static ErrorCode rdoff2Destroy(Object obj);

static void rdoff2ParseHeader(Object obj, ObjectRDOFF2 rdoff2);
static void rdoff2ParseSegments(Object obj, ObjectRDOFF2 rdoff2);
static void rdoff2Relocate(Object obj, ObjectRDOFF2 rdoff2);
static long volatile readLong(ObjectRDOFF2 rdoff2);
static short volatile readShort(ObjectRDOFF2 rdoff2);
static char volatile readByte(ObjectRDOFF2 rdoff2);
static char *formatName = "RDOFF2";

static ErrorCode rdoff2Create(Object obj, void *buffer, uint32 length)
{
   ObjectRDOFF2 rdoff2;
   uint8 *data = (uint8 *)buffer;

   printf("rdoff2Create data=0x%08x\n", data );

   // Does the module have the rdoff signature?
   if(strncmp(data, "RDOFF", 5))
   {
      return ErrorBadParam;
   }
   else
   {
      rdoff2 = (ObjectRDOFF2) malloc(sizeof(struct _ObjectRDOFF2));
      objectParserSetPrivateData(obj, rdoff2);

      rdoff2->version = data[5] - '0';
      printf("RDOFF Version %d.0\n", rdoff2->version);
   }

   VPRINTF(( "Object base is 0x%8x", buffer ));
   objectSetBase(obj, buffer);
   obj->length = length;

   // skip past the signature
   rdoff2->mem = buffer;
   rdoff2->end = (uint8 *)((long)buffer + length);
   rdoff2->pos = 6;

   // this length read doesn't work properly!
   // However, everything else does!?
   if(rdoff2->version > 1)
   {
      rdoff2->length = readLong(rdoff2);
      printf("Object content size: %d bytes\n", (int)rdoff2->length);
   }

   // find the length of the header and record it
   rdoff2->headerLength = readLong(rdoff2);
   rdoff2->headerOffset = rdoff2->pos;

   // skip over the header for now...
   rdoff2->pos += rdoff2->headerLength;

   // because I need segment info first!
   rdoff2ParseSegments(obj, rdoff2);

   // skip back to the header
   rdoff2->pos = rdoff2->headerOffset;

   // and parse it!
   printf("Header (%d bytes):\n", rdoff2->headerLength);
   rdoff2ParseHeader(obj, rdoff2);

   rdoff2Relocate(obj, rdoff2);

   return ErrorNoError;
}

static ErrorCode rdoff2Destroy(Object obj)
{
}

static void rdoff2ParseHeader(Object obj, ObjectRDOFF2 rdoff2)
{
   ObjectSymbol symbol;
   ObjectSection section, sectionReloc;   // TODO: rename?
   ObjectRelocation reloc;
   char buf[129], type, segment, length, flags;
   char *symbolName;
   unsigned char recordLength;
   long offset, labelLength;
   short refSegment;
   long headerLength;

   char isRelative;
   long symbolIndex;
   long relocation;
   char *segReloc;

   // loop through the length of the header
   headerLength = rdoff2->headerLength;
   while(headerLength > 0)
   {
      // read the segment type
      type = readByte(rdoff2);

      // if this is rdoff2, also read the record length
      if(rdoff2->version >= 2)
      {
         recordLength = readByte(rdoff2);
      }

      // which type have we got?
      switch (type)
      {
      case 1:                  /* relocation record */
      case 6:                  /* segment relocation */
         VPRINTF(("relocation (%d) :", type));
         segment = readByte(rdoff2);
         offset = readLong(rdoff2);
         length = readByte(rdoff2);
         refSegment = readShort(rdoff2);
         VPRINTF(("seg: %x, offs %x, len %x, refSeg %x\n", segment, offset, length, refSegment));

         // if the referenced segment is less than three, we're
         // working with either the text, code or bss segment
         /* JWA
            if(refSegment < 3)
            {
            relocation = (long)rdoff2->mem + rdoff2->segment[refSegment].offset;
            }
            // otherwise, it's a segment/symbol which must be imported
            else
            {
            // find the segment (as mentioned above, if this is defined in the
            // rdoff _after_ it's used (here), this code will break.  _Shouldn't_
            // ever happen, though...
            symbolIndex = rZeroFindSegment(refSegment, rZeroExports);
            //printf("rs: %d, symbolIndex: %d\n", rs, symbolIndex);
            // record the location of this segment
            relocation = rZeroExports[symbolIndex].symbolLocation;
            //printf("symbol location %x, actual %x\n", rel, printf);
            }
          */

         isRelative = ((segment & 64) == 64);
         segment &= 63;

         // remember, our sectionId's are one off from segment numbers
         objectParserGetSectionById(obj, segment + 1, &section);

         /* If refSegment < 3 (should be maxSegment... rdoff2 states that there can be more then
          * 3 sections (ie, .text, .data, .bss, .comment, etc)
          * then it's a segment relocation, not a symbol relocation
          */
         if(refSegment >= 3)
         {
            objectParserGetImportById(obj, refSegment + RDOFF2_SYMBOL_ID, &symbol);
            objectParserCreateSymbolRelocation(&reloc, section, offset, symbol, length, isRelative);
         }
         else
         {
            // segment relocation... refSegment refers to the begining of a section!
            objectParserGetSectionById(obj, refSegment + 1, &sectionReloc);
            objectParserCreateSectionRelocation(&reloc, section, offset, sectionReloc, length,
                                                isRelative);
         }
         objectParserAddRelocation(obj, reloc);

         // if this relocation is defined as relative, make sure our
         // relocation is defined as relative to the start of the
         // proper segment
         // JWA if( isRelative )
         //JWA: relocation -= (long)rdoff2->mem + rdoff2->segment[ segment ].offset;

         // calculate the address of the segment we're going to patch
         /* JWA
            if(segment == 0)
            {
            segReloc = (char *)rdoff2->mem + rdoff2->segment[0].offset;
            }
            else if(segment == 1)
            {
            segReloc = (char *)rdoff2->mem + rdoff2->segment[1].offset;
            }
            else
            {
            // ??? BSS relocation ever needed?
            printf("relocation not needed here!\n");
            continue;
            }
          */

         // debugging info...
         /*
            printf("rdoff2->mem: %x, rdoff2->mem + seg[0].off %x\n",
            rdoff2->mem,
            rdoff2->mem + rdoff2->segment[ segment  ].offset);
          */
         /* JWA
            printf("patching area: %x + %x with %x\n", (long)segReloc,
            (long)offset,
            (long)relocation);
          */

         // and, finally, perform the patch, based on the relocation size
         /*
            switch(length)
            {
            case 1:
            segReloc[offset] += (char)relocation;
            break;
            case 2:
            *(short *)(segReloc + offset) += (short)relocation;
            break;
            case 4:
            //printf("current mem: %x\n", *(long *)(seg + o));
            *(long *)(segReloc + offset) += (long)relocation;
            break;
            default:
            printf("unknown relocation size!\n");
            break;
            }
          */

         //printf("  %s: location (%04x:%08x), length %d, "
         //     "referred seg %04x\n", t == 1 ? "relocation" : "seg relocation",
         //     (int)s, o,(int)l, rs);
         // perform some error checks...
         if(rdoff2->version >= 2 && recordLength != 8)
            printf("warning: reclen != 8\n");
         if(rdoff2->version == 1)
            headerLength -= 9;
         if(rdoff2->version == 1 && type == 6)
            printf("warning: seg relocation not supported in RDOFF1\n");
         break;

      case 2:                  /* import record */
      case 7:                  /* import far symbol */
         VPRINTF(("import: "));   // import from perspect of object -- get symbol from kernel
         flags = readByte(rdoff2);
         // get the referenced segment
         refSegment = readShort(rdoff2);
         labelLength = 0;

         // read in the symbol name (different method, depending on
         // rdoff version!
         if(rdoff2->version == 1)
         {                      // zero delimited
            do
            {
               buf[labelLength] = readByte(rdoff2);
            }
            while(buf[labelLength++]);
         }
         else
         {                      // record length delimited (65536 - 4, max?)
            for(; labelLength < recordLength - 3; labelLength++)
               buf[labelLength] = readByte(rdoff2);
         }
         VPRINTF(("%s %d\n", buf, refSegment));

         /* allocate memory for the symbol name (buf will be obliterated when this
          * function exists
          */
         // TODO: need way to free this memory
         symbolName = (char *)malloc(strlen(buf) + 1);
         strcpy(symbolName, buf);

         //printf("  %simport: segment %04x = %s\n",t == 7 ? "far " : "",
         //     rs,buf);
         // update our length (ie, skip over this record)
         if(rdoff2->version == 1)
            headerLength -= labelLength + 3;

         // error checking...
         if(rdoff2->version == 1 && type == 7)
            printf("    warning: far import not supported in RDOFF1\n");

         objectParserCreateSymbol(&symbol, symbolName, RDOFF2_SYMBOL_ID + refSegment, NULL);
         objectParserAddImport(obj, symbol);

         // find the exported symbol, and assign it the segment number
         /* JWA:
            symbolIndex = rZeroFindSymbol(buf, rZeroExports);
            if(symbolIndex == -1) printf("Cannot find!\n");
            rZeroExports[symbolIndex].segmentNum = refSegment;
          */
         break;

      case 3:                  /* export record */
         VPRINTF(("export: "));
         flags = readByte(rdoff2);
         segment = readByte(rdoff2);
         offset = readLong(rdoff2);
         labelLength = 0;

         // read in the symbol name (different method depending on rdoff version)
         if(rdoff2->version == 1)
         {
            do
            {
               buf[labelLength] = readByte(rdoff2);
            }
            while(buf[labelLength++]);
         }
         else
         {
            for(; labelLength < recordLength - 6; labelLength++)
               buf[labelLength] = readByte(rdoff2);
         }
         VPRINTF(("%s %d\n", buf, segment));

         // TODO: need elegant way to free this memory
         symbolName = (char *)malloc(strlen(buf) + 1);
         strcpy(symbolName, buf);

         objectParserCreateSymbol(&symbol, symbolName, RDOFF2_SYMBOL_ID + segment, offset);
         /* @TODO: check if this +1 is correct, and why it was working before!???!?!? */
         objectParserGetSectionById(obj, segment + 1, &section);
         objectParserAddExport(obj, symbol, section);

         /* what type of export?
            if (flags & SYM_GLOBAL)
            printf("  export");
            else
            printf("  global");
            if (flags & SYM_FUNCTION) printf(" proc");
            if (flags & SYM_DATA) printf(" data");
            printf(": (%04x:%08x) = %s\n",(int)s,o,buf);
          */
         // adjust the length for rdoff1 objects
         if(rdoff2->version == 1)
            headerLength -= labelLength + 6;

         // import this symbol into the kernel
         /*
            symbolIndex = rZeroFindSymbol(buf, rZeroImports);
            if(symbolIndex != -1)
            rZeroImports[symbolIndex].symbolLocation =
            (long)rdoff2->mem + rdoff2->segment[segment].offset + offset;
            else printf("couldn't export\n");
          */
         break;

      case 4:                  /* DLL and Module records */
      case 8:
         labelLength = 0;

         // read the module/dll name
         if(rdoff2->version == 1)
         {
            do
            {
               buf[labelLength] = readByte(rdoff2);
            }
            while(buf[labelLength++]);
         }
         else
         {
            for(; labelLength < recordLength; labelLength++)
            {
               buf[labelLength] = readByte(rdoff2);
            }
         }

         // simply print which module/dll is required (this should never
         // happen with an rZero module)
         if(type == 4)
            printf("  requires dll: %s\n", buf);
         else
            printf("  requires module: %s\n", buf);

         // adjust the length for rdoff1
         if(rdoff2->version == 1)
            headerLength -= labelLength + 1;
         break;
      case 5:                  /* BSS reservation */
         labelLength = readLong(rdoff2);

         // do nothing for bss yet, just print it...
         printf("  bss reservation: %8x bytes\n", labelLength);

         if(labelLength)
         {
            // TODO: fix hardcoded 3... should be max segment + 1?
            objectParserCreateSection(&section, ".bss", 3, 0 /*offset */ , labelLength, 2);
            objectParserAddSection(obj, section);
         }

         // adjust length for rdoff1 and check for errors
         if(rdoff2->version == 1)
            headerLength -= 5;
         if(rdoff2->version > 1 && recordLength != 4)
            printf("    warning: reclen != 4\n");
         break;

      case 9:                  /* MultiBoot header record */
         printf("  MultiBoot header included!\n");
         printf("  Currently unsupported in rZero modules.\nSkipping over\n");
         // no need to adjust rdoff1 length, MB Headers only exist in rdoff2
         break;
      default:
         // print out the details of an unknown type...
         printf("  unrecognised record (type %d", (int)type);
         if(rdoff2->version > 1)
         {
            printf(", length %d)\n", (int)recordLength);
            rdoff2->pos += recordLength;
         }

         // adjust rdoff1 length
         if(rdoff2->version == 1)
            headerLength--;
      }

      // adjust rdoff2+ length
      if(rdoff2->version != 1)
         headerLength -= 2 + recordLength;
   }
}

/* Record all information from the segment info section of
 * the rdoff object
 */
static void rdoff2ParseSegments(Object obj, ObjectRDOFF2 rdoff2)
{
   ObjectSection section;
   ObjectSymbol symbol;
   short segType;
   int i;

   // load in all the segment info
   //rdoff2->numSegments = 0;
   segType = readShort(rdoff2);
   while(segType != 0 /*&& rdoff2->numSegments < 6 */ )
   {
      uint16 number;
      uint16 reserved;
      uint32 length;
      uint32 offset;
      char *name;

      //segment = (ObjectSection)malloc(sizeof(struct _ObjectSection));

      // what is number?  segmentId?
      number = readShort(rdoff2);
      reserved = readShort(rdoff2);
      length = readLong(rdoff2);
      offset = rdoff2->pos;

      switch (segType)
      {
      case 1:
         name = ".text";
         break;
      case 2:
         name = ".data";
         break;
      case 3:
         name = ".bss";
         break;
      default:
         assert(0);
         break;
      }

      // segment numbers are 0 based, but we wish to be 1 based for sectionId's (0 is the
      // absense of a sectionId)
      objectParserCreateSection(&section, name, number + 1, offset, length, 2);
      objectParserAddSection(obj, section);

      // create a symbol that references the beginning of this section as well!
      // offset == 0, name is the same as the section!
      // JW_LATE_NIGHT:
      objectParserCreateSymbol(&symbol, name, number + 1, 0);
      objectParserAddExport(obj, symbol, section);

      printf("Section: Type %d, Number %d, Reserved %d, Length %d, Offset %d\n", segType, number,
             reserved, length, offset);

      // seek past this segment
      rdoff2->pos += length;
      //rdoff2->numSegments++;

      segType = readShort(rdoff2);
   }

   // and print out a summary...
   /*
      printf("%d segments:\n", rdoff2->numSegments);
      for(i = 0; i < rdoff2->numSegments; i++) {
      printf("Type %d, Number %d, Reserved %d, Length %d, Offset %d\n",
      rdoff2->segment[i].type,
      rdoff2->segment[i].number,
      rdoff2->segment[i].reserved,
      rdoff2->segment[i].length,
      rdoff2->segment[i].offset);
      }
    */
   return;
}

/* Grab all the sections from the file and align them all into the same
 * contiguous block of memory
 */
static void rdoff2Relocate(Object obj, ObjectRDOFF2 rdoff2)
{
   ObjectSection section;
   ListIterator it;
   uint32 totalLength = 0;
   void *newLoc;

   objectGetSectionIterator(obj, &it);

   listIteratorReset(it);
   while(listIteratorGetNext(it, (void **)&section) == ErrorNoError)
   {
      totalLength += section->length;
   }

   /** @TODO: test: usage of page allocator from apps heap (also round up potentially uses 1 too many pages) */
   //newLoc = (void *)malloc(totalLength);
   pagerAllocate( pagerHeapApps, (totalLength + 4096) / 4096, &newLoc );

   totalLength = 0;
   listIteratorReset(it);
   while(listIteratorGetNext(it, (void **)&section) == ErrorNoError)
   {
      /* copy the section to its new location */
      printf("___copying to 0x%8x from 0x%8x length %d\n", ((long)newLoc) + totalLength,
             ((long)obj->base) + section->offset, section->length);
      memcpy(((long)newLoc) + totalLength, ((long)obj->base) + section->offset, section->length);

      /* and update its location in the section object */
      section->offset = /*((long)newLoc) + */ totalLength;
      totalLength += section->length;
   }

   objectSetBase(obj, newLoc);
}

/* The following read a long, short or byte from memory, converting it
 * from little-endian format to the processor's native format (unnecessary
 * for intel processors, but... it's portable, so I left it in :)
 */
static long volatile readLong(ObjectRDOFF2 rdoff2)
{
   uint8 *start = &rdoff2->mem[rdoff2->pos];
   long r = 0;

   /*
      r = start[3];
      r = (r << 8) + start[2];
      r = (r << 8) + start[1];
      r = (r << 8) + start[0];
    */

   r = *(long *)start;

   rdoff2->pos += 4;
   return r;
}

static short volatile readShort(ObjectRDOFF2 rdoff2)
{
   uint8 *start = &rdoff2->mem[rdoff2->pos];
   short r = 0;

   //r = (start[1] << 8) + *start;
   r = *(short *)start;

   rdoff2->pos += 2;
   return r;
}

static char volatile readByte(ObjectRDOFF2 rdoff2)
{
   return rdoff2->mem[rdoff2->pos++];
}

ErrorCode interface(driverInit) (void)
{
   objectParserAdd(formatName, rdoff2Create, rdoff2Destroy);
}

ErrorCode interface(driverFinal) (void)
{
// objectParsersRemove(objectParserRDOFF2);
}

/** #} */
