/* ndk - [ errorCodes.h ]
 *
 * Basic types used in ndk kernel
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Types types.h: Basic data types
* @ingroup core
*
* Basic typedefs and defines used through the kernel
*/
/** @{ */

#ifndef __ndk_types_h__
#define __ndk_types_h__

typedef unsigned char uint8;
typedef signed char int8;
typedef unsigned short uint16;
typedef signed short int16;
typedef unsigned long uint32;
typedef signed long int32;

/** @todo check this define!  What to do for ansi 64-bit type?  Is there one? */
//#ifdef __GCC__
//typedef unsigned long long uint64;
//typedef signed long long int64;
//#else
typedef unsigned int uint64;
typedef signed int int64;
//#endif
typedef char *String;
typedef unsigned short *String16;
typedef char Character;
typedef short Character16;

/** Pointer should allow one to reference all types of objects */
typedef void *Pointer;

/** PointerType should be large enough to hold a pointer, and allow
    arithmetic to be done on it */
typedef uint32 PointerType;

/** @todo update all timers/timeouts to be 64-bit, with more then simple ms accuracy? */
typedef uint32 Timeout;

#define TimeoutInfinite (0xffffffff)
#define TimeoutNone     (0)

#define TimeoutIsMs(n) ( ( (n) != TimeoutNone ) && ( (n) != TimeoutInfinite ) )

typedef enum _Boolean
{
   True = 1,
   False = 0
} Boolean;

/*! standard NULL define */
#define NULL		0

#define Align( x, n ) \
   if( (x) & ( (n) - 1 ) ) (x) += ( ~(x) & ( (n) - 1 ) ) + 1

#endif

/** @} */
