/* ndk - [ task.h ]
 *
 * Interface for creating/controlling tasks
 *
 * (c)2004 dcipher / neuraldk
 *           www.neuraldk.org
 */

/**
* @defgroup Task task.h: Task/Process operations
* @ingroup core
*
* This file defines the portable interface to the platforms timer hardware.
*/
/** @{ */

#ifndef __ndk_tss_h__
#define __ndk_tss_h__

// TODO: all this crap has to be made portable... move to task.c in arch!!!

#include "ndk.h"
#include "array.h"
#include <errorCodes.h>
#include <types.h>
#include <core/messageQueue.h>
#include <core/object.h>

// new API!
typedef enum _TaskType
{
   TaskTypeOS = 0,
   TaskTypeUser
} TaskType;

typedef enum _TaskState
{
   TaskStateDead,
   TaskStateAlive,
   TaskStateWaiting,
   TaskStateTimedOut
} TaskState;

typedef enum _TaskWaitingOn
{
   TaskWaitingOnMutex,
   TaskWaitingOnSemaphore,
   TaskWaitingOnDelay
} TaskWaitingOn;

typedef struct _Task *Task;

//extern Task currentTask;
//extern Task firstTask;

/**
 * Create a new task/process
 *
 * @param task      The handle for the newly created task
 * @param parent    The parent for this task.  If NULL, a new task will be
 *                  created.  If non-null, a new thread will be created
 *                  using 'parent's address space.
 * @param name      The name to assign this thread, up to 64 bytes
 *                  (63 character NULL terminated string)
 * @param entry     The entry point of this new process
 * @param argument  The argument to pass to entry
 * @param type      The type of process to create
 * @param stackSize The size of stack to give this task
 * @param priority  The priority of this task
 * \TODO  Should entry return an ErrorCode?
 */
ErrorCode taskCreate(Task *task, Task parent, String name, void (*entry) (void *), void *argument,
                     TaskType type, uint32 stackSize, uint8 priority);

/**
 * Delete a task, pre-empting it if currently running
 *
 * @param task The handle for the task to delete
 */
ErrorCode taskDestroy(Task *task);

ErrorCode taskSetObject( Task task, Object object );

/**
 * Returns the task structure of the currently executing task
 *
 * @param task The task structure of the currently active task
 */
ErrorCode taskGetCurrent(Task *task);

/**
 * Return the state of a given task
 *
 * @param task   The task in question
 * @param state  The state of that task
 */
ErrorCode taskGetState(Task task, TaskState * state);

/**
 * Update the state of a given task
 *
 * @param task   The task which will have its state changed
 * @param state  The state to change it to
 */
ErrorCode taskSetState(Task task, TaskState state);

/**
 * Get this task's message queue
 *
 * @param task  The task to query
 * @param queue A pointer to where the queue will be returned
 */
ErrorCode taskGetMessageQueueId(Task task, MessageQueueId *queue);

/**
 * Returns the task structure of the first active task created
 *
 * @param task The first active task structure
 */
//ErrorCode taskFirst(Task *task);

/**
 * Returns the next task structure after the task structure
 * pointed to by 'this'.
 *
 * @param this The task structure to start from
 * @param next The task structure after this is returned in 'next'
 */
//ErrorCode taskNext(Task current, Task *next);

/**
 * Delay the currently active task a set number of milliseconds
 *
 * @param task The task to set a timeout to
 * @param to   The timeout to use
 */
ErrorCode taskSetTimeout(Task task, Timeout to);

/**
 * Force a task script operation to occur.  This will preempt the
 * current task and call the task scheduler to select a new task.
 */
ErrorCode taskForceSwitch(void);

/**
 * Will output (in decently readable terms) the current stack.
 * To be used to exception handlers to print a reset log.
 *
 * @param task  This task's stack will be printed
 */
ErrorCode taskPrintStackTrace(Task task);

/**
 * Get the current position on the stack for a specified
 * task.
 *
 * @param task  The task in question
 * @param ptr   A copy of the current stack pointer
 */
ErrorCode taskGetStackPointer(Task task, Pointer *ptr);

/**
 * Get the base of the stack segment for this task
 *
 * @param task  The task in question
 * @param ptr   The base address of the stack
 */
ErrorCode taskGetStackBase(Task task, Pointer *ptr);

/**
 * Get the size of the task.
 *
 * @param task   The task in question
 * @param size   The size of the task's stack
 */
ErrorCode taskGetStackSize(Task task, uint32 *size);

/**
 * Returns the direction that the stack increments.
 *
 * @param direction Will return -1 or +1
 */
ErrorCode taskGetStackDirection(int32 *direction);

#endif

/** @} */
