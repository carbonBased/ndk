#ifndef __ndk_fixed_h__
#define __ndk_fixed_h__

#define intToFixed(x) ((long)(x) * 65536)

typedef unsigned long Fixed32;

extern Fixed32 fxPi;
extern Fixed32 sinTable[1025];
extern Fixed32 radSinTable[1025];

void fixedInit();
Fixed32 sine(Fixed32 x);
Fixed32 cosine(Fixed32 x);
Fixed32 radSine(Fixed32 x);
Fixed32 radCosine(Fixed32 x);
Fixed32 hyp(Fixed32 x, Fixed32 y);

// defined in fixedasm.asm
extern Fixed32 fxSqrt  (Fixed32 x);
extern Fixed32 fxSqrtHP(Fixed32 x);
extern Fixed32 fxSquare(Fixed32 x);
extern Fixed32 fxMul(Fixed32 x, Fixed32 y);
extern Fixed32 fxDiv(Fixed32 x, Fixed32 y);

#endif
