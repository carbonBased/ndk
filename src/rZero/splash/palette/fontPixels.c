#include <stdio.h>
#include <stdlib.h>

#include "fontData.h"

//#define NO_DUPS	// define for no duplicates
#define START_CHAR 32
#define END_CHAR   126

unsigned short pixels[256];  // neads to be short for 0xffff (used) tag
unsigned char nearest[256];

void calcPixels(void);
void calcSkew(void);
void calcNearest(void);
void printNearest(void);
void printHeader(void);

#define BIT_SET(a,b) (( (a) & (1 << ( 7 - (b) ) )) ? 1 : 0)

int main(void) {
  calcPixels();
  calcSkew();
  calcNearest();
  //printNearest();
  
  printHeader();
  return 0;
}

void calcPixels(void) {
  int c;
  int tot;
  int x, y;

  for(c = START_CHAR; c <= END_CHAR; c++) {
    printf("Calculating Pixel %d: ", c);
    tot = 0;
    for(x = 0; x < 8; x++) {
      for(y = 0; y < 12; y++) {
        if(BIT_SET(asciiFontData[c * 12 + y], x)) tot++;
      }
    }
    pixels[c] = (unsigned char)tot; //(char)( (tot * 255l) / 96l );
    printf("%d\n", pixels[c]);
  }
}

void calcSkew(void) {
  int c;
  int highest = 0;

  for(c = START_CHAR; c <= END_CHAR; c++) {
    if(pixels[c] > highest)
      highest = pixels[c];
  }

  // skew numbers into 0 - 255 range
  for(c = 0; c < 256; c++) {
    pixels[c] = pixels[c] * 255 / highest;
    printf("%d: %d\n", c, pixels[c]);
  }
  return;
}

void calcNearest(void) {
  int near, diff, pixel;
  int colour, chr;

  //for(colour = 0; colour < 256; colour++) {
  for(colour = 255; colour >= 0; colour--) {
    printf("Finding nearest for %d\n", colour);
    near = 0xffff;
    pixel = 0;
    for(chr = START_CHAR; chr <= END_CHAR; chr++) {
      diff = abs(pixels[chr] - colour);
      if(diff < near) {
        near = diff;
        pixel = chr;
      }
    }
    nearest[colour] = pixel;
#ifdef NO_DUPS
    pixels[pixel] = 0xFFFF;
#endif
  }
  return;
}

void printNearest(void) {
  int c;

  for(c = 0; c < 256; c++)
    printf("%3d\t", nearest[c]);

  return;
}

void printHeader(void) {
  int c;

  printf("const unsigned char palette[256] = {");
  for(c = 0; c < 256; c++) {
    if( (c & 15) == 0) printf("\n");
    printf("%3d,", nearest[c]);
  }

  printf("};");
  return;
}

