/* some random fixed point routines I've either written, or found on my HD and
 * meshed to together in haste to port my, already ported to Linux, version of
 * Alex J. Champandard's plasma, to use fixed point math instead of floating
 * point.
 *
 * The reason behind the fixed point switch (I hate fixed point!  I wouldn't
 * do it unless I had to...) is that I intend to use this as an ndk rZero
 * startup module, and so I figured I might as well not assume every user
 * has an fpu... even though they probably do now-a-days... ;)
 */

#include "fixed.h"

//#define SINCOS
//#define CREATE_PRECALC  // default to create sineConsine.h with precalculated
                          // sine and cosine values

#ifdef CREATE_PRECALC
#include <math.h>
#include <stdio.h>
#endif


Fixed32 fxPi  = 205887;
Fixed32 fx2Pi = 411775;

#ifdef SINCOS
Fixed32 sineTable[5760]; // 360 * 16
Fixed32 cosineTable[5760];
#endif

//Fixed32 radSineTable[3216]; // fxPi / 64
//Fixed32 radCosineTable[3216];

#include "sineCosine.h"

// of note... fixed point numbers (at least 16.16's) suck for squares of
// numbers.  Try to find the square of 320 (smallest screen X resolution)...
// well over 64k, and therefore will not fit into the 16 bits assigned for
// the whole number, ergo this hyp function will fail also... keep that
// in mind!
Fixed32 hyp(Fixed32 x, Fixed32 y) {
  Fixed32 x2, y2, result;
  
  x = fxSquare(x);
  y = fxSquare(y);

  result = fxSqrt( (x + y) );
  
  return result;
}

void fixedInit() {
#ifdef CREATE_PRECALC
  double t;int i;
  FILE *preCalc = fopen("sineCosine.h", "w");
#endif

#ifdef SINCOS
  for(i = 0; i < 5760; i++) {
    t = M_PI/2880l * i;
    sineTable[i] = (Fixed32)(sin(t)*65536.0);
    cosineTable[i] = (Fixed32)(cos(t)*65536.0);
  }
#endif

#ifdef CREATE_PRECALC
  for(i = 0; i < 3216; i++) {
    t = (i * 64) / 65536.0;
    radSineTable[i] = (Fixed32)(sin(t) * 65536.0);
    radCosineTable[i] = (Fixed32)(cos(t) * 65536.0);
  }

  fprintf(preCalc, "Fixed32 radSineTable[] = {");
  for(i = 0; i < 3216; i++) {
    if( (i & 7) == 0) fprintf(preCalc, "\n");
    fprintf(preCalc, "0x%08x, ", radSineTable[i]);
  }

  fprintf(preCalc, "\n};\n");
  fprintf(preCalc, "Fixed32 radCosineTable[] = {");
  for(i = 0; i < 3216; i++) {
    if( (i & 7) == 0) fprintf(preCalc, "\n");
    fprintf(preCalc, "0x%08x, ", radCosineTable[i]);
  }
  fprintf(preCalc, "\n};\n");

  fclose(preCalc);
#endif
  return;
}

#ifdef SINCOS
Fixed32 sine(Fixed32 x) {
  //if(x < 0) x = intToFixed(360) - x;
  if(x < 0) x = -x;

  x >>= 12;
  while( x > 5760 ) x -= 5760;
  //x &= 5759;
  return sineTable[x];

}

Fixed32 cosine(Fixed32 x) {
  if(x < 0) x = -x;

  x >>= 12;
  while( x > 5760 ) x -= 5760;
  //x &= 5759;

  return cosineTable[x];
}
#endif

Fixed32 radSine(Fixed32 x) {
  int sign;

  // this isn't true for the sine wave... it doesn't mirror on y axis!!!
  //if(x < 0) x = -x;

  while((signed)x < 0) x = fx2Pi - x; // bring us into 0...2PI

  x >>= 6;
  while(x > 6432) x -= 6436; // bring us into 0...2PI

  // bring us into 0...PI
  if (x > 3216) {
    x -= 3216;
    sign = 1;
  } else sign = 0;

  // return the correctly signed "y" on sign wave
  if(sign) return - radSineTable[x];
  else return radSineTable[x];
}

Fixed32 radCosine(Fixed32 x) {
  // because the cosine wave is mirrored by the
  // y-axis, we solve for negatives, in positive
  // space
  if((signed)x < 0) x = -x;

  x >>= 6;
  while(x > 6432) x -= 6432; // now in 0...2PI

  if(x > 3216) x = 6432 - x;  // now in 0..PI

  return radCosineTable[x];
}

