/* ndk/rZero - [ rZeroSplash.c ]
 *
 * A lot of the following code was originally from Alex Champindard's Art of
 * Demo Making tutorials.  I've since ported this code to PETAL, my graphics library,
 * then to fixed point math, and finally to my own text mode graphics library.
 * Suffice to say, a lot of work went into this little puppy!
 *
 * This source is, more-or-less, useless for anyone who isn't using the ndk
 * operating system.  It is meant to be used as the default splashScreen rZero
 * module :)
 */

/* simple fixed point math routines */
#include "fixed.h"

/* Include only one of the following (defines how text pixels are drawn) */
#include "textPaletteNoDups.h"
/*#include "textPalettePureAscii.h"
 *#include "textPalette.h"
 */

#include <types.h>
#include <timer.h>
#include <mutex.h>
#include <core/messageQueue.h>
#include <errorCodes.h>
#include <malloc.h>
#include <console.h>

#define SMALL_BAND

/* function definitions */
void splashScreen(void *argument);
void openWindow(void);
void closeWindow(void);
void preCalculate(void);
void loadImage(void);
void setPalette(long seed);
void doPlasma(void);
void waitVRetrace(void);
void splashProgress(long progress);

/* defined by NDK! */
/*
extern long  timerGetTicks(long *);
extern void *memoryAllocPage(void);
*/

/* The following define the window size, and other geometry items
 * I've assigned them all a value at this point, because I don't want
 * to add BSS support to my rZero modules just yet ;)
 */
int          xRes = 0,      yRes = 0;
int         x2Res = 0,     y2Res = 0;
int      halfXRes = 0,  halfYRes = 0;
Fixed32 halfXResF = 0, halfYResF = 0;

Fixed32 one = intToFixed(1);

uint32 someBss, moreBss, lastBss;

long loadStatus = 1;
long loadPercent = 1;

/* the plasma sources */
char *plasma1 = 0x0;
char *plasma2 = 0x0;
char *textScreen = 0x0;

char ndkString[] = "ndk v0.05 | (c) dcipher, carbonBased / neuraldk";

/* Used to map "logical" colours (0-15) to their VGA/EGA mappings
 * Thanks to Stephane Martineau for pointing this out!
 */
char vgaColourMappings[] = { 0,1,2,3,4,5,20,7,56,57,58,59,60,61,62,63 };
static MessageQueueId splashQueueId = 0;
static MessageQueue splashQueue = 0;

void splashScreen(void *arg) {
  //Mutex test = (Mutex)arg;
  splashQueueId = (MessageQueue)arg;
  
  printf("received splashQueueId 0x%8x\n", splashQueueId);
  
  messageQueueMap( splashQueueId, &splashQueue );
  printf("mapped splashQueueId to 0x%8x\n", splashQueue);

  openWindow();
  preCalculate();
  loadImage();
  doPlasma();
  closeWindow();
}

void openWindow(void) {
  int i = 0;
  int bufferSize;

  /* define our window size */
  xRes = 80;
#ifdef SMALL_BAND
  yRes = 15;
#else
  yRes = 25;
#endif

  /* initialize the fixed point math engine */
  fixedInit();

  /* set up more screen dimensional stuff */
  x2Res = xRes * 2;
  y2Res = yRes * 2;
  halfXRes = xRes / 2;
  halfYRes = yRes / 2;
  halfXResF = intToFixed(halfXRes) - one;
  halfYResF = intToFixed(halfYRes) - one;
  bufferSize = x2Res * y2Res * 2;

  /* get memory for the plasma images (these should really be allocated!) */
  // TODO: check these sizes of allocations!  They shouldn't need to be this 
  // large (buffer overrun, probably).
  // OORRRR not... x2Res * y2Res * 2
  plasma1    = malloc(bufferSize);  /* 80 * 25 * 2 */
  plasma2    = malloc(bufferSize);  /* 80 * 25 * 2 */
  //plasma1    = (char *)0x00080000;
  //plasma2    = (char *)0x00085000;
  printf("plasma1 0x%8x plasma2 0x%8x bufferSize %d\n", plasma1, plasma2, bufferSize);
  /* the physical screen */
  textScreen = (char *)0x000b8000;
  
  memset( plasma1, 0, bufferSize );
  memset( plasma2, 0, bufferSize );

#ifdef SMALL_BAND
  /* clear the top line */
  memset(textScreen, 0x0, 80*2);

  /* print the ndk info string */
  while(ndkString[i]) {
    textScreen[i*2]   = ndkString[i];
    textScreen[i*2+1] = 15;
    i++;
  }

  for(i = 0; i < 80; i++) {
    /* the top line on the screen @ line 2 */
    textScreen[160 + i * 2]     = 196;
    textScreen[160 + i * 2 + 1] =  15;
    /* the bottom line on the screen @ line 17 */
    textScreen[2720 + i * 2]     = 196;
    textScreen[2720 + i * 2 + 1] =  15;
  }

  // skip past two lines
  textScreen += 320;
#endif

  consoleLockScrollWindow(19, 25);

  return;
}

void closeWindow(void) {
  consoleLockScrollWindow(1, 25);
  free(plasma1);
  free(plasma2);
  return;
}

void preCalculate(void) {
  unsigned char *buf1 = (unsigned char *)plasma1;
  unsigned char *buf2 = (unsigned char *)plasma2;
  int i, j, dst = 0;
  
  for (j=0; j<y2Res; j++) {
    for (i=0; i<x2Res; i++) {
      /* this will create a "circle" plasma (ie, circles, phazing, radiating 
       * from the centre)
       */
      buf1[dst] = (unsigned char)
                  (( intToFixed(64) + fxMul(intToFixed(63),
		  ( radSine(
		      fxDiv(
		        //fxSqrt( intToFixed(xDiff + yDiff) )
			hyp(intToFixed(yRes-j),intToFixed(xRes-i))
		        ,
		        intToFixed(16)
		        )
		    )
		  ) ) ) / 65536);

      /* and this will create some sweet ass blobular plasma :) */
      buf2[dst] = (unsigned char)
                  ( intToFixed(64) + fxMul(intToFixed(63),
		  fxMul(
		  radSine(
		    fxDiv(intToFixed(i),
		         (intToFixed(19) +
			 fxMul(intToFixed(8), radCosine( fxDiv(intToFixed(j),intToFixed(37))))
			 )
		        )
		  )
                  ,
		  radCosine(
		        fxDiv(intToFixed(j), (intToFixed(15)) +
			fxMul(intToFixed(6),
		             radSine( fxDiv(intToFixed(i), intToFixed(28)) )
			     )
			)
			)
		)
		)
		/ 65536 ) ;

      dst++;
    }
  }
}

void loadImage(void) {
  /* Another reminant from the old days, where an image was "superimposed" ontop
   * of the plasma... left here in case I decide to port those routines to 
   * text mode
   */
  return;
}

void setColour(char colour, char red, char green, char blue) {
  /* obtain the proper VGA colour mapping, and change its RGB components */
  outportb(0x3C8, vgaColourMappings[colour]);
  outportb(0x3C9, red);
  outportb(0x3C9, green);
  outportb(0x3C9, blue);
  return;
}

void setPalette(long seed) {
  int i;
  unsigned long r, g, b;
  unsigned long iPiDiv32;

  waitVRetrace();
  for (i=1; i<15; i++) {
    /* calculate i * Pi / 32, used in all the below equations */
    iPiDiv32 = fxDiv(
                 fxMul(
                   intToFixed(i),
                   fxPi
                 ),
                 intToFixed(32)
               );


    /* calculate flowing R, G, and B components (will phaze in and out of
     * each spectrum).
     */
    r = fxMul(
          intToFixed(31),
          radCosine(
            iPiDiv32 +
            fxDiv(
              intToFixed(seed),
              intToFixed(74)
            )
          )
        );

    g = fxMul(
          intToFixed(31),
          radSine(
            iPiDiv32 +
            fxDiv(
              intToFixed(seed),
              intToFixed(63)
            )
          )
        );

    b = fxMul(
          intToFixed(31),
          radCosine(
            iPiDiv32 +
            fxDiv(
              intToFixed(seed),
              intToFixed(81)
            )
          )
        );


    /* set the palette with our new colour */
    setColour(i, (char)(r/65536) + 32,
                 (char)(g/65536) + 32,
                 (char)(b/65536) + 32);
  }

  return;
}

void doPlasma(void) {
  /* define the plasma movement variables */
  uint32 x1, y1, x2, y2, x3, y3;
  uint32 dst, i, j, src1, src2, src3;
  uint32 currentTime;
  char pixel;
  uint32 value;

  /* until the OS is loaded (loadStatus = 100% or 255) */
  while (loadStatus < 100) {
    /* we get the time for this frame only once so that we are sure it stays
     * the same for all our calculations :)
     */
    //currentTime = timerGetTicks();
    timerGetTicks(&currentTime);
    currentTime << 4;

	if(messageQueuePeek(splashQueue, (Pointer*)&value) == ErrorNoError)
	{
		//printf("Peek from queue %d\n", value);
		messageQueueReceive(&splashQueue, TimeoutInfinite, (Pointer*)&value);
		//printf("Reading from queue %d\n", value);
		loadStatus = value;
		loadPercent = value;
	}
	else
	{
		//printf("Nothing in queue\n");
	}

    /* setup some nice colours, different every frame
     * this is a palette that wraps around itself, with different period sine
     * functions to prevent monotonous colours
     */
    setPalette(currentTime);

    /* move plasma with more sine functions :) */
    x1 = halfXRes +
         (int)( fxMul(halfXResF , radCosine( fxDiv(intToFixed( currentTime), intToFixed(97 )) )) ) / 65536;
    x2 = halfXRes +
         (int)( fxMul(halfXResF , radSine  ( fxDiv(intToFixed(-currentTime), intToFixed(114)) )) ) / 65536;
    x3 = halfXRes +
         (int)( fxMul(halfXResF , radSine  ( fxDiv(intToFixed(-currentTime), intToFixed(137)) )) ) / 65536;
    y1 = halfYRes +
         (int)( fxMul(halfYResF , radSine  ( fxDiv(intToFixed( currentTime), intToFixed(123)) )) ) / 65536;
    y2 = halfYRes +
         (int)( fxMul(halfYResF , radCosine( fxDiv(intToFixed(-currentTime), intToFixed(75 )) )) ) / 65536;
    y3 = halfYRes +
         (int)( fxMul(halfYResF , radCosine( fxDiv(intToFixed(-currentTime), intToFixed(108)) )) ) / 65536;

    /* we only select the part of the precalculated buffer that we need */
    src1 = y1 * x2Res + x1;
    src2 = y2 * x2Res + x2;
    src3 = y3 * x2Res + x3;

    // draw the plasma... this is where most of the time is spent.
    // a good idea would be to add 4 pixels at a time by type casting
    // the unsigned chars to longs. I'll leave that to you :)
    dst = 0;
    waitVRetrace();
    for (j=0; j<yRes; j++) {
      for (i=0; i<xRes; i++) {
        /* get the "colour" value - ie, the text character */
        pixel = (char)(plasma1[src1] +
                       plasma2[src2] +
	               plasma2[src3]) & 0xff;

        /* assign the text character */
        textScreen[dst++] = textPalette[pixel];
	/* calculate a colour from the text character, and assign it */
	textScreen[dst++] = (pixel >> 4) & 15 - 1;

        /* increment the plasma coordinates */
        src1++; src2++; src3++;
      }

      /* get the next line in the precalculated buffers */
      src1 += xRes; src2 += xRes; src3 += xRes;
    }

    /* display loading status */
    if(loadPercent >= 100)
      textScreen[2400 + 74 * 2] = '1';
    //else textScreen[] = ' ';
    textScreen[2400 + 75 * 2] = (loadPercent / 10) % 10 + '0';
    textScreen[2400 + 76 * 2] = loadPercent % 10 + '0';
    textScreen[2400 + 77 * 2] = '%';

    /* if using a virtual screen, this would be where to copy it... */
  }

  /* we've finished! */
  return;
}

void waitVRetrace(void) {
  /* wait until done with vertical retrace */
  while ( (inportb(0x03da) & 0x08));
  /* wait until done refreshing */
  while (!(inportb(0x03da) & 0x08));
}

void splashProgress(long progress) {
  loadStatus = progress;
  loadPercent = loadStatus * 100 / 255;
  return;
}
