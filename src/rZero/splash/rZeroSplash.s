[section .data]
[global $textPalette]
times ($-$$) & 0 nop
$textPalette:
db '', -4, -8, -15, -62
db '', -88, '}{s'
db 'c', -13, -14, -16
db '', -63, -3, -39, -64
db '', -1, ' ', 0, -10
db '', -6, '\', -7, '`'
db '.-', -60, '_'
db ',', 39, -86, -87
db '^=:', 28
db '/+"~'
db '', 27, 26, -51, '|'
db ';', -38, -65, '>'
db '<)(', -5
db '?%', 29, 7
db '', -25, 22, -43, -44
db '', -66, -72, -83, 'z'
db 'xvti'
db '][I!'
db '', -89, 'r1'
db '', -47, -49, -77, -81
db '', -82, -95, -115, 'o'
db 'lea*'
db '', 25, 24, -11, -12
db '', -27, -90, 'un'
db '', -18, -30, -116, -117
db '', -121, 'wfT'
db '', -45, -61, -67, -76
db '73', -20, -48
db '', -101, 'yjJ'
db '', 19, -118, '5', 17
db '', 16, -26, -28, -32
db '', -53, -54, -58, -59
db '', -75, -94, -96, -97
db '', -107, -123, -126, 'm'
db 'YLC9'
db '62$', 18
db '', -19, -42, -73, -93
db '', -98, -105, -119, 'S'
db '', -2, -9, -17, -21
db '', -40, -46, -55, -56
db '', -68, -69, -80, -108
db '', -114, -124, 'qp'
db 'kg4', 31
db '', 30, 13, 12, 9
db '', 4, -100, -111, -112
db '', -120, -127, 'hd'
db 'bPF', -109
db '', -125, 'XVO'
db 'G', -85, -92, -106
db 'ZE', -24, -102
db '', -103, -104, -128, 'U'
db 'A', 23, -31, 11
db '', -23, -99, 'WK'
db 'HD8', 3
db '', 1, -22, -29, -122
db 'R@&', 21
db '', 15, 5, 'QB'
db '', -110, -113, '#', -50
db '', -91, 6, 'NM'
db '0', -52, -71, -84
db '', 20, -33, -34, -35
db '', -36, -70, -79, 14
db '', -57, -74, -41, 2
db '', -78, 10, 8, -37
[global $xRes]
times ($-$$) & 3 nop
$xRes:
dd 0
[global $yRes]
times ($-$$) & 3 nop
$yRes:
dd 0
[global $x2Res]
times ($-$$) & 3 nop
$x2Res:
dd 0
[global $y2Res]
times ($-$$) & 3 nop
$y2Res:
dd 0
[global $halfXRes]
times ($-$$) & 3 nop
$halfXRes:
dd 0
[global $halfYRes]
times ($-$$) & 3 nop
$halfYRes:
dd 0
[global $halfXResF]
times ($-$$) & 3 nop
$halfXResF:
dd 00H
[global $halfYResF]
times ($-$$) & 3 nop
$halfYResF:
dd 00H
[global $one]
times ($-$$) & 3 nop
$one:
dd 010000H
[global $loadStatus]
times ($-$$) & 3 nop
$loadStatus:
dd 1
[global $loadPercent]
times ($-$$) & 3 nop
$loadPercent:
dd 1
[global $plasma1]
times ($-$$) & 3 nop
$plasma1:
dd 00H
[global $plasma2]
times ($-$$) & 3 nop
$plasma2:
dd 00H
[global $textScreen]
times ($-$$) & 3 nop
$textScreen:
dd 00H
[global $ndkString]
times ($-$$) & 0 nop
$ndkString:
db 'ndk v0.05 | (c) dcipher, carbonBased / neuraldk', 0
[global $vgaColourMappings]
times ($-$$) & 0 nop
$vgaColourMappings:
db '', 0, 1, 2, 3
db '', 4, 5, 20, 7
db '89:;'
db '<=>?'
[global $splashScreen]
[section .text]
$splashScreen:
push ebx
push esi
push edi
push ebp
mov ebp,esp
sub esp,4
mov edi,dword [ebp + 20]
mov dword [ebp + -4],edi
mov edi,dword [ebp + -4]
push dword edi
lea edi,[$L2]
push dword edi
call $consoleOut
add esp,8
call $openWindow
add esp,0
mov edi,dword [ebp + -4]
push dword edi
lea edi,[$L2]
push dword edi
call $consoleOut
add esp,8
call $preCalculate
add esp,0
call $loadImage
add esp,0
call $doPlasma
add esp,0
call $closeWindow
add esp,0
$L1:
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $openWindow]
$openWindow:
push ebx
push esi
push edi
push ebp
mov ebp,esp
sub esp,4
mov dword [ebp + -4],0
mov dword [$xRes],80
mov dword [$yRes],15
call $fixedInit
add esp,0
mov edi,dword [$xRes]
lea esi,[edi*2]
mov dword [$x2Res],esi
mov esi,dword [$yRes]
lea ebx,[esi*2]
mov dword [$y2Res],ebx
mov ebx,2
mov eax,edi
mov ecx,ebx
cdq
idiv ecx
mov dword [$halfXRes],eax
mov eax,esi
mov ecx,ebx
cdq
idiv ecx
mov dword [$halfYRes],eax
mov edi,dword [$one]
mov esi,dword [$halfXRes]
sal esi,16
sub esi,edi
mov dword [$halfXResF],esi
mov esi,dword [$halfYRes]
sal esi,16
sub esi,edi
mov dword [$halfYResF],esi
mov dword [$plasma1],080000H
mov dword [$plasma2],085000H
mov dword [$textScreen],0b8000H
push dword 160
push dword 0
mov edi,dword [$textScreen]
push dword edi
call $memset
add esp,12
jmp $L5
$L4:
mov edi,dword [ebp + -4]
mov esi,dword [$textScreen]
mov bl,byte [edi + ($ndkString)]
mov byte [esi + edi*2],bl
mov edi,1
mov esi,dword [ebp + -4]
lea edi,[edi + esi*2]
mov esi,dword [$textScreen]
mov byte [esi + edi],15
inc dword [ebp + -4]
$L5:
mov edi,dword [ebp + -4]
movsx edi,byte [edi + ($ndkString)]
cmp edi,0
jne near $L4
mov dword [ebp + -4],0
$L7:
mov edi,dword [ebp + -4]
lea edi,[edi*2]
lea edi,[edi + (160)]
mov esi,dword [$textScreen]
mov ebx,196
mov byte [esi + edi],bl
mov edi,dword [ebp + -4]
lea edi,[edi*2]
lea edi,[edi + (160)]
lea edi,[edi + (1)]
mov esi,dword [$textScreen]
mov byte [esi + edi],15
mov edi,dword [ebp + -4]
lea edi,[edi*2]
lea edi,[edi + (2720)]
mov esi,dword [$textScreen]
mov ebx,196
mov byte [esi + edi],bl
mov edi,dword [ebp + -4]
lea edi,[edi*2]
lea edi,[edi + (2720)]
lea edi,[edi + (1)]
mov esi,dword [$textScreen]
mov byte [esi + edi],15
$L8:
inc dword [ebp + -4]
cmp dword [ebp + -4],80
jl near $L7
mov edi,dword [$textScreen]
lea edi,[edi + (320)]
mov dword [$textScreen],edi
$L3:
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $closeWindow]
$closeWindow:
push ebx
push esi
push edi
push ebp
mov ebp,esp
$L25:
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $preCalculate]
$preCalculate:
push ebx
push esi
push edi
push ebp
mov ebp,esp
sub esp,20
mov edi,dword [$plasma1]
mov dword [ebp + -16],edi
mov edi,dword [$plasma2]
mov dword [ebp + -20],edi
mov dword [ebp + -12],0
mov dword [ebp + -8],0
jmp $L30
$L27:
mov dword [ebp + -4],0
jmp $L34
$L31:
mov edi,dword [$xRes]
sub edi,dword [ebp + -4]
sal edi,16
push dword edi
mov edi,dword [$yRes]
sub edi,dword [ebp + -8]
sal edi,16
push dword edi
call $hyp
add esp,8
mov edi,eax
push dword 0100000H
push dword edi
call $fxDiv
add esp,8
mov edi,eax
push dword edi
call $radSine
add esp,4
mov edi,eax
push dword edi
push dword 03f0000H
call $fxMul
add esp,8
mov esi,dword [ebp + -12]
mov ebx,dword [ebp + -16]
lea edi,[eax + (0400000H)]
shr edi,16
mov dx,di
mov byte [ebx + esi],dl
push dword 0250000H
mov edi,dword [ebp + -8]
sal edi,16
push dword edi
call $fxDiv
add esp,8
mov edi,eax
push dword edi
call $radCosine
add esp,4
mov edi,eax
push dword edi
push dword 080000H
call $fxMul
add esp,8
lea edi,[eax + (0130000H)]
push dword edi
mov edi,dword [ebp + -4]
sal edi,16
push dword edi
call $fxDiv
add esp,8
mov edi,eax
push dword edi
call $radSine
add esp,4
mov edi,eax
push dword 01c0000H
mov esi,dword [ebp + -4]
sal esi,16
push dword esi
call $fxDiv
add esp,8
mov esi,eax
push dword esi
call $radSine
add esp,4
mov esi,eax
push dword esi
push dword 060000H
call $fxMul
add esp,8
lea esi,[eax + (0f0000H)]
push dword esi
mov esi,dword [ebp + -8]
sal esi,16
push dword esi
call $fxDiv
add esp,8
mov esi,eax
push dword esi
call $radCosine
add esp,4
mov esi,eax
push dword esi
push dword edi
call $fxMul
add esp,8
mov edi,eax
push dword edi
push dword 03f0000H
call $fxMul
add esp,8
mov esi,dword [ebp + -12]
mov ebx,dword [ebp + -20]
mov edi,eax
shr edi,16
lea edi,[edi + (0400000H)]
mov dx,di
mov byte [ebx + esi],dl
inc dword [ebp + -12]
$L32:
inc dword [ebp + -4]
$L34:
mov edi,dword [$x2Res]
cmp dword [ebp + -4],edi
jl near $L31
$L28:
inc dword [ebp + -8]
$L30:
mov edi,dword [$y2Res]
cmp dword [ebp + -8],edi
jl near $L27
$L26:
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $loadImage]
$loadImage:
push ebx
push esi
push edi
push ebp
mov ebp,esp
$L52:
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $setColour]
$setColour:
push ebx
push esi
push edi
push ebp
mov ebp,esp
mov edi,dword [ebp + 20]
mov bx,di
mov byte [ebp + 20],bl
mov edi,dword [ebp + 24]
mov bx,di
mov byte [ebp + 24],bl
mov edi,dword [ebp + 28]
mov bx,di
mov byte [ebp + 28],bl
mov edi,dword [ebp + 32]
mov bx,di
mov byte [ebp + 32],bl
movsx edi,byte [ebp + 20]
movsx edi,byte [edi + ($vgaColourMappings)]
push dword edi
push dword 968
call $outportb
add esp,8
movsx edi,byte [ebp + 24]
push dword edi
push dword 969
call $outportb
add esp,8
movsx edi,byte [ebp + 28]
push dword edi
push dword 969
call $outportb
add esp,8
movsx edi,byte [ebp + 32]
push dword edi
push dword 969
call $outportb
add esp,8
$L53:
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $setPalette]
$setPalette:
push ebx
push esi
push edi
push ebp
mov ebp,esp
sub esp,20
call $waitVRetrace
add esp,0
mov dword [ebp + -4],1
$L55:
mov edi,dword [$fxPi]
push dword edi
mov edi,dword [ebp + -4]
sal edi,16
push dword edi
call $fxMul
add esp,8
mov edi,eax
push dword 0200000H
push dword edi
call $fxDiv
add esp,8
mov dword [ebp + -8],eax
push dword 04a0000H
mov edi,dword [ebp + 20]
sal edi,16
push dword edi
call $fxDiv
add esp,8
mov esi,dword [ebp + -8]
lea edi,[eax + esi]
push dword edi
call $radCosine
add esp,4
mov edi,eax
push dword edi
push dword 01f0000H
call $fxMul
add esp,8
mov dword [ebp + -12],eax
push dword 03f0000H
mov edi,dword [ebp + 20]
sal edi,16
push dword edi
call $fxDiv
add esp,8
mov esi,dword [ebp + -8]
lea edi,[eax + esi]
push dword edi
call $radSine
add esp,4
mov edi,eax
push dword edi
push dword 01f0000H
call $fxMul
add esp,8
mov dword [ebp + -16],eax
push dword 0510000H
mov edi,dword [ebp + 20]
sal edi,16
push dword edi
call $fxDiv
add esp,8
mov esi,dword [ebp + -8]
lea edi,[eax + esi]
push dword edi
call $radCosine
add esp,4
mov edi,eax
push dword edi
push dword 01f0000H
call $fxMul
add esp,8
mov dword [ebp + -20],eax
mov edi,dword [ebp + -20]
shr edi,16
mov bx,di
movsx edi,bl
lea edi,[edi + (32)]
mov bx,di
movsx edi,bl
push dword edi
mov edi,dword [ebp + -16]
shr edi,16
mov bx,di
movsx edi,bl
lea edi,[edi + (32)]
mov bx,di
movsx edi,bl
push dword edi
mov edi,dword [ebp + -12]
shr edi,16
mov bx,di
movsx edi,bl
lea edi,[edi + (32)]
mov bx,di
movsx edi,bl
push dword edi
mov edi,dword [ebp + -4]
mov bx,di
movsx edi,bl
push dword edi
call $setColour
add esp,16
$L56:
inc dword [ebp + -4]
cmp dword [ebp + -4],15
jl near $L55
$L54:
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $doPlasma]
$doPlasma:
push ebx
push esi
push edi
push ebp
mov ebp,esp
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $waitVRetrace]
$waitVRetrace:
push ebx
push esi
push edi
push ebp
mov ebp,esp
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[global $splashProgress]
$splashProgress:
push ebx
push esi
push edi
push ebp
mov ebp,esp
mov esp,ebp
pop ebp
pop edi
pop esi
pop ebx
ret
[extern $inportb]
[extern $timerGetTicks]
[extern $outportb]
[extern $memset]
[extern $consoleOut]
[extern $fxDiv]
[extern $fxMul]
[extern $hyp]
[extern $radCosine]
[extern $radSine]
[extern $fixedInit]
[extern $fxPi]
[section .data]
times ($-$$) & 0 nop
$L2:
db 'Value is %d', 10, 0
