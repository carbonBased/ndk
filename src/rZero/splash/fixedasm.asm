[bits 32]

global fxMul
global fxDiv
global fxSqrt
global fxSqrtHP
global fxSquare

segment .text

fxMul:
   push ebp
   mov ebp, esp
   push edx
   mov eax, [ss:ebp+12]
   mov edx, [ss:ebp+8]
   imul edx
   add eax, 0x8000
   adc edx, 0
   shrd eax, edx, 16
   pop edx
   pop ebp
   ret

fxDiv:
   push ebp
   mov ebp, esp
   push ebx
   push edx
   mov ebx, [ss:ebp+12]
   mov edx, [ss:ebp+8]
   xor eax, eax
   shrd eax, edx, 16
   sar edx, 16
   idiv ebx
   pop edx
   pop ebx
   pop ebp
   ret


fxSqrt:
   push ebp
   mov ebp, esp
   push ecx
   mov ecx, [ss:ebp+8]
   xor eax, eax
   mov ebx, 40000000h
sqrtLP1:
   mov edx, ecx
   sub edx, ebx
   jl  sqrtLP2
   sub edx, eax
   jl  sqrtLP2
   mov ecx,edx
   shr eax, 1
   or  eax, ebx
   shr ebx, 2
   jnz sqrtLP1
   shl eax, 8
   jmp sqrtLP3
sqrtLP2:
   shr eax, 1
   shr ebx, 2
   jnz sqrtLP1
   shl eax, 8
sqrtLP3:
   pop ecx
   pop ebp
sqrtdone:
   ret

fxSqrtHP:
   push ebp
   mov ebp, esp
   push eax
   push ebx
   push ecx
   push edx
   mov ecx, [ss:ebp+8]
   xor eax, eax
   mov ebx, 40000000h
sqrtHP1:
   mov edx, ecx
   sub edx, ebx
   jb  sqrtHP2
   sub edx, eax
   jb  sqrtHP2
   mov ecx,edx
   shr eax, 1
   or  eax, ebx
   shr ebx, 2
   jnz sqrtHP1
   jz  sqrtHP5
sqrtHP2:
   shr eax, 1
   shr ebx, 2
   jnz sqrtHP1
sqrtHP5:
   mov ebx, 00004000h
   shl eax, 16
   shl ecx, 16
sqrtHP3:
   mov edx, ecx
   sub edx, ebx
   jb  sqrtHP4
   sub edx, eax
   jb  sqrtHP4
   mov ecx, edx
   shr eax, 1
   or  eax, ebx
   shr ebx, 2
   jnz sqrtHP3
   jmp sqrtHP6
sqrtHP4: 
   shr eax, 1
   shr ebx, 2
   jnz sqrtHP3
sqrtHP6:
   pop edx
   pop ecx
   pop ebx
   pop eax
   pop ebp
   ret

fxSquare:
   push ebp
   mov ebp, esp
   push edx
   mov eax, [ss:ebp+8]
   imul eax
   add eax, 8000h
   adc edx, 0
   shrd eax, edx, 16
   pop edx
   pop ebp
   ret
