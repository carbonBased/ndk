#include <core/driver.h>
#include <klibc/assert.h>
#include <arch/i386/io.h>
#include <core/messageQueue.h>
#include <core/message.h>
#include <drivers/arch/i386/ps2keyboard.h>

static void handleKeyPress( Message keyMessage );

void func1(void)
{
}

void func2(void)
{
}

void func3(void)
{
}

ErrorCode main( /*int argc, char *argv[]*/ void *param )
{
   MessageQueueId mainQueueId;
   MessageQueue mainQueue;
   MessageId messageId;
   Message message;
   ErrorCode ec;
   DriverContext keyboardCtx;
   Task self;
   char url[256];

   printf("inside devConsole.c...\n");

   taskGetCurrent( &self );
   taskGetMessageQueueId( self, &mainQueueId );
   
   messageQueueMap( mainQueueId, &mainQueue );
   
   /* register as a key listener */
   if( driverContextCreate( &keyboardCtx, "driver.input.keyboard") == ErrorNoError )
   {
      /** @TODO: native execution method... also, ensure this works when keyboard driver isn't loaded!!! */
      sprintf( url, "ps2.listener.add(%d)", mainQueueId );
      printf("my url is '%s'\n", url);
      driverContextExecute( keyboardCtx, url );
   }
   
   /* 
    * this main loop only handles driver messages, as this app does nothing more then 
    * manage serial ports
    */
   while( messageQueueReceive( &mainQueue, TimeoutInfinite, (Pointer*)&messageId ) == ErrorNoError )
   {
      uint32 senderId;
      
      printf("devConsole got messageId 0x%8x\n", messageId );

      ec = messageMap( messageId, &message );
      assert( ec == ErrorNoError );
      
      printf("devConsole mapped messageId 0x%8x to 0x%8x\n", messageId, message );

      ec = messageGetSender( message, &senderId );
      assert( ec == ErrorNoError );

      switch( senderId )
      {
         case DRIVER_MESSAGE_SENDER_ID:
            printf("received a driver event...\n");
            break;
         case 0x1234ffee:
            printf("recieved a key event\n");
            handleKeyPress( message );
            break;
         default:
            printf("Unhandled message from 0x%8x\n", senderId);
            assert(0);
      }

      messageDestroy( &message );
   }
   
   return ec;
}

static void handleKeyPress( Message keyMessage )
{
   Key key;
   
   messageGetArgument( keyMessage, 1, (Pointer*)&key );
   
   printf("received key %c 0x%0x\n", key.keycode, key.keycode );
   
   return;
}
