#include <driver.h>

#include "serialInterface.h"

#include <klibc/assert.h>

// TODO: Remove for driver(In/Out)Port(8/16/32)();
#include <arch/i386/io.h>
#include <core/messageQueue.h>
#include <core/message.h>

#define REGISTER_OFFSET_RECV_BUFFER							(0)
#define REGISTER_OFFSET_SEND_BUFFER							(0)
#define REGISTER_OFFSET_INTERRUPT_ENABLE					(1)
#define REGISTER_OFFSET_INTERRUPT_IDENT					(2)
#define REGISTER_OFFSET_DATA_FORMAT							(3)
#define REGISTER_OFFSET_MODEM_CONTROL						(4)
#define REGISTER_OFFSET_LINE_STATUS							(5)
#define REGISTER_OFFSET_MODEM_STATUS						(6)
#define REGISTER_OFFSET_SCRATCH_PAD							(7)
#define REGISTER_OFFSET_DIVISOR_LATCH_LSB					(0)
#define REGISTER_OFFSET_DIVISOR_LATCH_MSB					(1)

/** DATA FORMAT REGISTER MASKS */
#define REGISTER_MASK_DATA_FORMAT_DLAB						(0x80)
#define REGISTER_MASK_DATA_FORMAT_NO_DLAB_ACCESS		(0x7F)
#define REGISTER_MASK_DATA_FORMAT_BRK						(0x40)
#define REGISTER_MASK_DATA_FORMAT_PARITY					(0x38)
#define REGISTER_MASK_DATA_FORMAT_STOP						(0x04)
#define REGISTER_MASK_DATA_FORMAT_DBIT						(0x03)

/** DATA FORMAT REGISTER VALUES */
#define REGISTER_VALUE_DATA_FORMAT_DLAB_ACCESS			(0x80)
#define REGISTER_VALUE_DATA_FORMAT_BREAK_ON				(0x40)
#define REGISTER_VALUE_DATA_FORMAT_PARITY_NONE			(0x00)
#define REGISTER_VALUE_DATA_FORMAT_PARITY_ODD			(0x08)
#define REGISTER_VALUE_DATA_FORMAT_PARITY_EVEN			(0x18)
#define REGISTER_VALUE_DATA_FORMAT_PARITY_MARK			(0x28)
#define REGISTER_VALUE_DATA_FORMAT_PARITY_SPACE			(0x38)
#define REGISTER_VALUE_DATA_FORMAT_STOP_2					(0x04)
#define REGISTER_VALUE_DATA_FORMAT_STOP_1					(0x00)
#define REGISTER_VALUE_DATA_FORMAT_DBIT_5					(0X00)
#define REGISTER_VALUE_DATA_FORMAT_DBIT_6					(0x01)
#define REGISTER_VALUE_DATA_FORMAT_DBIT_7					(0x02)
#define REGISTER_VALUE_DATA_FORMAT_DBIT_8					(0x03)

/** TODO: Need to figure out how to handle private context data generally, and also
* in this driver.  Private context data should probably be attached to a DriverContext, not
* a driver.  As such, these functions should accept DriverContext params, rather then Driver
* params (A driver can be returned from a DriverContext anyway!).
*
* In the case of this serial driver, however, it may almost make more sense to simply have a
* global array of 4 _SerialContext structures.
*/
typedef struct _SerialContext
{
   uint8 port;
   uint8 dataBits;
   uint8 stopBits;
   uint8 parity;
   uint32 baud;
} *SerialContext;

ErrorCode _consoleCharListener(int c);
void _serialPutChar(uint8 port, int c);
uint8 _serialGetDataBits(uint8 port);
uint8 _serialGetStopBits(uint8 port);
uint8 _serialGetParity(uint8 port);
uint32 _serialGetBaud(uint8 port);
void _serialAccessDivisorLatch(uint8 port, Boolean accessable);

uint16 _baseRegister[4] = { 0X3f8, 0X2f8, 0x3e8, 0x2e8 };

ErrorCode main( /*int argc, char *argv[]*/ void *param )
{
   MessageQueueId mainQueueId;
   MessageQueue mainQueue;
   MessageId messageId;
   Message message;
   ErrorCode ec;
   Driver drv;
   Task self;

   printf("inside serialImplementation.c...\n");

   // perform some serial port detection here?

   // init the driver ( driverInit() is automatically generated code )
   driverInit( &drv );
   
   printf("created driver %x\n", drv );
   
   driverManagerAdd( drv );

   taskGetCurrent( &self );
   taskGetMessageQueueId( self, &mainQueueId );
   
   messageQueueMap( mainQueueId, &mainQueue );
   
   testStack( &drv );
   
   printf("self == 0x%x, queue = 0x%x\n", self, mainQueue );

   /* 
    * this main loop only handles driver messages, as this app does nothing more then 
    * manage serial ports
    */
   while( messageQueueReceive( &mainQueue, TimeoutInfinite, (Pointer*)&messageId ) == ErrorNoError )
   {
      uint32 senderId;
      
      printf("serialImplemenation got messageId 0x%8x\n", messageId );

      ec = messageMap( messageId, &message );
      assert( ec == ErrorNoError );
      
      printf("serialImplemenation mapped messageId 0x%8x to 0x%8x\n", messageId, message );

      ec = messageGetSender( message, &senderId );
      assert( ec == ErrorNoError );

      switch( senderId )
      {
         case DRIVER_MESSAGE_SENDER_ID:
            driverHandleMessage( message );
            break;
         default:
            printf("Unhandled message from 0x%8x\n", senderId);
            assert(0);
      }

      messageDestroy( &message );
   }
   
   return ec;
}

ErrorCode serialDetect()
{
   static int times = 0;

   printf("serialDetect() called %d times\n", ++times);

   /* As a result of a strange object linking bug, an unlinked symbol call resulted
    * in calling this serialDetect() function instead.  The below code is a remanent
    * of that, and left as a reminder, for the time being
    */
   if(times == 2)
      assert(0);

   return ErrorNoError;
}

ErrorCode serialFinal(Driver drv)
{
   printf("serialFinal()\n");
   return ErrorNoError;
}

DriverVariable driverSerialRoot(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   SerialContext ctx;

   ret.errCode = ErrorNoError;
   
   /** TODO: should ports be 0 or 1 based? */
   printf("driverSerialRoot() -> serial port %d\n", vars[0].integer);

   /* this is our entry into the driver... create a context */
   ctx = (SerialContext) malloc(sizeof(struct _SerialContext));
   ctx->port = vars[0].integer;
   ctx->dataBits = _serialGetDataBits(ctx->port);
   ctx->stopBits = _serialGetStopBits(ctx->port);
   ctx->parity = _serialGetParity(ctx->port);
   ctx->baud = _serialGetBaud(ctx->port);

   printf("driver settings %d,%d%c%d\n", ctx->baud, (int)ctx->dataBits, (char)ctx->parity,
          (int)ctx->stopBits);

   // TODO: set context

   return ret;
}

DriverVariable driverSerialGet(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;
   uint8 port = 0;

   /** TODO: get context! */

   printf("driverSerialGet()\n");

   ret.character = _serialGetChar(port);

   return ret;
}

DriverVariable driverSerialPut(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;
   uint8 port = 0;

   /** TODO: get context! */
   
   ret.errCode = ErrorNoError;   

   printf("driverSerialPut('%c') on serial %d\n", vars[0].character, port);

   _serialPutChar(port, vars[0].character);

   return ret;
}

DriverVariable driverSerialRead(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;
   uint32 bufferSize, i;
   char *buffer;
   uint8 port = 0;

   /** TODO: get context! */

   printf("driverSerialRead()\n");

   bufferSize = vars[0].integer;
   buffer = (char *)malloc(bufferSize);
   if(buffer)
   {
      for(i = 0; i < bufferSize; i++)
      {
         buffer[i] = _serialGetChar(port);
      }
   }

   return ret;
}

DriverVariable driverSerialWrite(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;
   uint32 bufferSize, i;
   char *buffer;
   uint8 port = 0;

   /** TODO: get context! */

   printf("driverSerialWrite()\n");

   bufferSize = vars[0].integer;
   buffer = (char *)vars[1].pointer;
   if(bufferSize && buffer)
   {
      for(i = 0; i < bufferSize; i++)
      {
         _serialPutChar(port, buffer[i]);
      }
   }

   return ret;
}

DriverVariable driverSerialBaud(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;

   printf("driverSerialGetBaud()\n");

   return ret;
}

DriverVariable driverSerialDataBits(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;

   printf("driverSerialGetDataBits()\n");

   return ret;
}

DriverVariable driverSerialStopBits(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;

   printf("driverSerialGetStopBits()\n");

   return ret;
}

DriverVariable driverSerialParity(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;

   printf("driverSerialGetParity()\n");

   return ret;
}

DriverVariable driverSerialTraceEnable(DriverEvent event, DriverContext ctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;

   printf("driverSerialTraceEnable(%d)\n", vars[0].boolean);

   if(vars[0].boolean)
   {
      consoleEnableCharacterListener(_consoleCharListener);
   }
   else
   {
      consoleDisableCharacterListener(_consoleCharListener);
   }

   return ret;
}

ErrorCode _consoleCharListener(int c)
{
   /** TODO: save off character listener port in driverSerialTraceEnable */
   _serialPutChar(0, c);
   
   return ErrorNoError;
}

void _serialPutChar(uint8 port, int c)
{
   outportb(_baseRegister[port] + REGISTER_OFFSET_SEND_BUFFER, c);
}

uint8 _serialGetDataBits(uint8 port)
{
   uint8 value, dataBits;

   //driverInPort8(_baseRegister[port] + REGISTER_OFFSET_LINE_CONTROL, &value);
   value = inportb(_baseRegister[port] + REGISTER_OFFSET_DATA_FORMAT);

   switch (value & REGISTER_MASK_DATA_FORMAT_DBIT)
   {
   case REGISTER_VALUE_DATA_FORMAT_DBIT_5:
      dataBits = 5;
      break;
   case REGISTER_VALUE_DATA_FORMAT_DBIT_6:
      dataBits = 6;
      break;
   case REGISTER_VALUE_DATA_FORMAT_DBIT_7:
      dataBits = 7;
      break;
   case REGISTER_VALUE_DATA_FORMAT_DBIT_8:
      dataBits = 8;
      break;
   default:
      dataBits = 0;
   }

   return dataBits;
}

uint8 _serialGetStopBits(uint8 port)
{
   uint8 value, stopBits;

   //driverInPort8(_baseRegister[port] + REGISTER_OFFSET_LINE_CONTROL, &value);
   value = inportb(_baseRegister[port] + REGISTER_OFFSET_DATA_FORMAT);

   if(value & REGISTER_MASK_DATA_FORMAT_STOP)
   {
      stopBits = 2;
   }
   else
   {
      stopBits = 1;
   }

   return stopBits;
}

uint8 _serialGetParity(uint8 port)
{
   uint8 value;
   char parity;

   //driverInPort8(_baseRegister[port] + REGISTER_OFFSET_LINE_CONTROL, &value);
   value = inportb(_baseRegister[port] + REGISTER_OFFSET_DATA_FORMAT);

   switch (value & REGISTER_MASK_DATA_FORMAT_PARITY)
   {
   case REGISTER_VALUE_DATA_FORMAT_PARITY_NONE:
      parity = 'n';
      break;
   case REGISTER_VALUE_DATA_FORMAT_PARITY_ODD:
      parity = 'o';
      break;
   case REGISTER_VALUE_DATA_FORMAT_PARITY_EVEN:
      parity = 'e';
      break;
   case REGISTER_VALUE_DATA_FORMAT_PARITY_MARK:
      parity = 'm';
      break;
   case REGISTER_VALUE_DATA_FORMAT_PARITY_SPACE:
      parity = 's';
      break;
   default:
      parity = '0';
   }

   return (uint8)parity;
}

uint32 _serialGetBaud(uint8 port)
{
   uint8 divisorHigh, divisorLow;
   uint16 baudDivisor;
   uint32 baudRate;

   /* give access to the divisor latch */
   _serialAccessDivisorLatch(port, True);

   /* read the divisor halfs, and assemble them together */
   divisorLow = inportb(_baseRegister[port] + REGISTER_OFFSET_DIVISOR_LATCH_LSB);
   divisorHigh = inportb(_baseRegister[port] + REGISTER_OFFSET_DIVISOR_LATCH_MSB);
   baudDivisor = divisorLow + (((uint16)divisorHigh) << 8);

   if(baudDivisor == 0)
   {
      baudRate = 0;
   }
   else
   {
      baudRate = 115200 / baudDivisor;
   }

   /* remove access to divisor latch */
   _serialAccessDivisorLatch(port, False);

   return baudRate;
}

void _serialAccessDivisorLatch(uint8 port, Boolean accessable)
{
   uint8 dataFormat;

   dataFormat = inportb(_baseRegister[port] + REGISTER_OFFSET_DATA_FORMAT);
   if(accessable)
   {
      dataFormat |= REGISTER_VALUE_DATA_FORMAT_DLAB_ACCESS;
   }
   else
   {
      dataFormat &= REGISTER_MASK_DATA_FORMAT_NO_DLAB_ACCESS;
   }
   outportb(_baseRegister[port] + REGISTER_OFFSET_DATA_FORMAT, dataFormat);
}
