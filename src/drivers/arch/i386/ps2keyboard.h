#ifndef __drivers_i386_ps2keyboard__
#define __drivers_i386_ps2keyboard__

#include <core/types.h>

typedef enum _KeyEvents
{
   KeyPress,
   KeyRelease
} KeyEvents;

typedef enum _KeyModifier
{
   KeyModifierNone        = 0,
   KeyModifierShift       = 1,
   KeyModifierAlt         = 2,
   KeyModifierControl     = 4,
   KeyModifierMainMenu    = 8,
   KeyModifierContextMenu = 16,
   KeyModifierEscape      = 32,
} KeyModifier;

typedef struct _Key
{
   KeyModifier modifiers;
   Boolean keypress;
   uint32 keycode;
} Key;

#endif
