/* ndk - [ ps2keyboardImplementation.c ]
 *
 * Driver for a PS2 keyboard.  Portions of this code were derived from 
 * Chris Giese's code (such as the fundamental read/write command routines 
 * and the AT-to-XT translation disable) 
 * 
 * (c)2007 dcipher / neuraldk
 *           www.neuraldk.org
 */

#include <klibc/assert.h>
#include <arch/i386/io.h>
#include <brink/list.h>
#include <core/messageQueue.h>
#include <core/message.h>
#include <core/driver.h>
#include <core/pageBasedList.h>
#include <core/mutex.h>
// GCC deps here... #include <arch/i386/idt.h>

#include "ps2keyboardInterface.h"
#include "ps2keyboard.h"

extern ErrorCode idtSetTaskHandle( uint32 interrupt, Task task );

static ErrorCode driverPS2KeyboardInit();

/* I believe this is only relevant to scancode set 1, or 2 with translation
#define SCANCODE_BREAKCODE       (0x80)
#define SCANCODE_ESCAPED         (0xE0)
*/

/* this should be for scancode set 2 */
#define SCANCODE_BREAKCODE       (0xf0)
#define SCANCODE_ESCAPED         (0xe0)

#define KEYBOARD_PORT_READ      (0x60)
#define KEYBOARD_PORT_WRITE     (0x60)
#define KEYBOARD_PORT_CONTROL   (0x64)

#define KEYBOARD_DEFAULT_BUFFER_SIZE   (128)

/* non-ascii characters must be >= 0x80 */
#define KEY_F1                   (0x80)
#define KEY_F2                   (KEY_F1 + 1)
#define KEY_F3                   (KEY_F1 + 2)
#define KEY_F4                   (KEY_F1 + 3)
#define KEY_F5                   (KEY_F1 + 4)
#define KEY_F6                   (KEY_F1 + 5)
#define KEY_F7                   (KEY_F1 + 6)
#define KEY_F8                   (KEY_F1 + 7)
#define KEY_F9                   (KEY_F1 + 8)
#define KEY_F10                  (KEY_F1 + 9)
#define KEY_F11                  (KEY_F1 + 10)
#define KEY_F12                  (KEY_F1 + 11)
#define KEY_PRINT_SCREEN         (KEY_F1 + 12)
#define KEY_SCROLL_LOCK          (KEY_F1 + 13)   /* passed to applications? need to if in raw mode */
#define KEY_PAUSE                (KEY_F1 + 14)
#define KEY_INSERT               (KEY_F1 + 15)
#define KEY_HOME                 (KEY_F1 + 16)
#define KEY_PAGE_UP              (KEY_F1 + 17)
#define KEY_NUM_LOCK             (KEY_F1 + 18)
#define KEY_DELETE               (KEY_F1 + 19)
#define KEY_END                  (KEY_F1 + 20)
#define KEY_PAGE_DOWN            (KEY_F1 + 21)
#define KEY_CAPS_LOCK            (KEY_F1 + 22)
#define KEY_LEFT_SHIFT           (KEY_F1 + 23)
#define KEY_RIGHT_SHIFT          (KEY_F1 + 24)
#define KEY_LEFT_CONTROL         (KEY_F1 + 25)
#define KEY_LEFT_MAIN_MENU       (KEY_F1 + 26)
#define KEY_LEFT_ALT             (KEY_F1 + 27)
#define KEY_RIGHT_ALT            (KEY_F1 + 28)
#define KEY_RIGHT_MAIN_MENU      (KEY_F1 + 29)
#define KEY_CONTEXT_MENU         (KEY_F1 + 30)
#define KEY_RIGHT_CONTROL        (KEY_F1 + 31)
#define KEY_UP                   (KEY_F1 + 32)
#define KEY_LEFT                 (KEY_F1 + 33)
#define KEY_DOWN                 (KEY_F1 + 34)
#define KEY_RIGHT                (KEY_F1 + 35)

/* 
 * what to do about new/unknown keys, like 'sleep' or 'calculator'?  Simply set a high order bit and 
 * send the raw code through to the application?
 */

typedef enum _KeyMapType
{
   KeyMapNormal,
   KeyMapShifted,
   KeyMapEscaped,
   KeyMapTotal
} KeyMapType;

typedef enum _KeyLock
{
   KeyLockNum    = 1,
   KeyLockCaps   = 2,
   KeyLockScroll = 4
} KeyLock;

typedef struct _KeyMap
{
   uint32 startingCode, endingCode;
   uint32 bytesPerCharacter;
   uint8 *map;
} KeyMap;

static MessageQueueId mainQueueId;
static MessageQueue mainQueue = NULL;
static MutexId bufferMutexId;
static Mutex bufferMutex;
static Key *buffer;
static uint32 bufferSize = KEYBOARD_DEFAULT_BUFFER_SIZE, bufferStart = 0, bufferPos = 0, bufferUsage = 0;
static KeyMap keymaps[KeyMapTotal];
static KeyLock locks = 0;
static List listeners; /* list of message queues for each listener */
static ListIterator listenersIterator;

static uint8 defaultKeyMapMap[] =
{
        /* 0x00  0x01  0x02  0x03  0x04  0x05  0x06  0x07  0x08  0x09  0x0A  0x0B  0x0C  0x0D  0x0E  0x0F */
/* 0x00 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, '\t',  '`', 0x00, 
/* 0x10 */ 0x00, 0x00, 0x00, 0x00, 0x00,  'q',  '1', 0x00, 0x00, 0x00,  'z',  's',  'a',  'w',  '2', 0x00, 
/* 0x20 */ 0x00,  'c',  'x',  'd',  'e',  '4',  '3', 0x00, 0x00,  ' ',  'v',  'f',  't',  'r',  '5', 0x00, 
/* 0x30 */ 0x00,  'n',  'b',  'h',  'g',  'y',  '6', 0x00, 0x00, 0x00,  'm',  'j',  'u',  '7',  '8', 0x00, 
/* 0x40 */ 0x00, 0x00,  'k',  'i',  'o',  '0',  '9', 0x00, 0x00, 0x00, 0x00,  'l', 0x00,  'p',  '-', 0x00, 
/* 0x50 */ 0x00, 0x00, 0x00, 0x00, 0x00,  '=', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, '\\', 0x00, 0x00, 
/* 0x60 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, '\b', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0x70 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0x80 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0x90 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0xA0 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0xB0 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0xC0 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0xD0 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0xE0 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
/* 0xF0 */ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
};

static KeyMap defaultKeyMap =
{
   0, 255,
   1,
   defaultKeyMapMap
};

static uint8 typematic = 0x7f;
static uint8 scancodeSet = 2;

static void keyboardWaitInputBuffer(void);
static void keyboardWaitOutputBuffer(void);
static uint32 keyboardRead(void);
static void keyboardWrite(uint32 port, uint32 value);
static ErrorCode keyboardWriteWithAck(uint32 val);
static ErrorCode driverPS2KeyboardInit(void);

/** This handleKey implements basic functionality for scancode set 1, or 2 with translation
void handleKey(void)
{
   static KeyModifier modifier = KeyModifierNone;
   static KeyMapType keymapType = KeyMapNormal;
   static Boolean keypress;
    
   int keycode = inportb( KEYBOARD_PORT_READ );
   switch( keycode )
   {
      case SCANCODE_ESCAPED:
         keymapType = KeyMapEscaped;
         break;
      
      default:
         keymapType = KeyMapNormal;
         
         if( keycode & SCANCODE_BREAKCODE )
         {
            // break code, key released
            keycode & ~SCANCODE_BREAKCODE;
            keypress = False;
         }
         else
         {
            // make code, key pressed
            keypress = True;
         }
         // getKey( keycode, keymaps[keymapType] );
         // constructMessage();
         // deliverMessage( );
   }
   
   printf("received key %d escaped %d value=%c\n", keycode, keymapType == KeyMapEscaped, 
      defaultKeyMapMap[keycode] );
   
   outportb(0x20,0x20);
}
*/

/** this one should be good for scancode set 2 */
void handleKey(void)
{
   static KeyModifier modifiers = KeyModifierNone;
   static KeyMapType keymapType = KeyMapNormal;
   static Boolean keypress;
   int keycode;

   keycode = inportb( KEYBOARD_PORT_READ );

   printf( "raw key code - 0x%x, break code 0x%x\n", keycode, SCANCODE_BREAKCODE );
   printf( "next line\n" );
   //if( keycode != 0xfa )
   switch( keycode )
   {
      case 0xfa:  // ACK
         printf("ack");
         break;

      case SCANCODE_ESCAPED:
         keymapType = KeyMapEscaped;
         break;

      case SCANCODE_BREAKCODE:
         keypress = False;
         /* next will come the keycode for the released key */
         break;
         
      case KEY_LEFT_ALT:
      case KEY_RIGHT_ALT:
         if( keypress) modifiers |= KeyModifierAlt;
         else modifiers ^= KeyModifierAlt;
         break;
      
      case KEY_LEFT_CONTROL:
      case KEY_RIGHT_CONTROL:
         if( keypress) modifiers |= KeyModifierControl;
         else modifiers ^= KeyModifierControl;
         break;

      case KEY_LEFT_SHIFT:
      case KEY_RIGHT_SHIFT:
         if( keypress) modifiers |= KeyModifierShift;
         else modifiers ^= KeyModifierShift;
         break;

      case KEY_LEFT_MAIN_MENU:
      case KEY_RIGHT_MAIN_MENU:
         if( keypress) modifiers |= KeyModifierMainMenu;
         else modifiers ^= KeyModifierMainMenu;
         break;
         
      case KEY_CONTEXT_MENU:
         if( keypress) modifiers |= KeyModifierContextMenu;
         else modifiers ^= KeyModifierContextMenu;
         break;

      default:
         keymapType = KeyMapNormal;
         
         printf("received key from h/w %s 0x%2x escaped %d value=%c\n", 
            keypress ? "press" : "release", keycode, keymapType == KeyMapEscaped, 
            defaultKeyMapMap[keycode] );
         
         mutexLock( &bufferMutex, TimeoutInfinite );
         
         printf("bufferUsage %d bufferSize %d\n", bufferUsage, bufferSize);
         if( bufferUsage < bufferSize ) 
         {
            printf("adding key to buffer\n");

            buffer[bufferPos].keycode = keycode;
            buffer[bufferPos].keypress = keypress;
            buffer[bufferPos].modifiers = modifiers;
         
            // update ring buffer maintenance variables
            bufferPos++;
            if( bufferPos >= bufferSize) 
            {
               bufferPos = 0;
            }

            //if( bufferUsage == 0 )
            {
               /* 
                * send a wakeup call to main task as it's got new keys.  I can reuse the 
                * mainQueue pointer as this thread (even though its not one now, but it will be) 
                * and the main thread will share the same address space 
                */
               if( mainQueue )
               {
                  printf("PS2 keyboard driver, sending NULL message to queue\n");
                  messageQueueSend( &mainQueue, NULL );
               }
            }
            bufferUsage++;
         }
         else
         {
            printf("PS2 keyboard driver, buffer is full\n");
            /* buffer is full... should just drop keys, instead, maybe? */
            //bufferStart++;
            //if( bufferStart >= bufferSize ) bufferStart = 0;
         }

         mutexUnlock( &bufferMutex );

         keypress = True;
         keymapType = KeyMapNormal;
   }
   
   outportb(0x20,0x20);
   printf("--handleKey\n");
}

uint32 getKey( uint32 scancode, KeyMap *map )
{
   uint32 keycode = 0, i;
   uint8 *keycodeAddr;
   
   if( scancode >= map->startingCode &&
       scancode <= map->endingCode )
   {
      keycodeAddr = &map->map[ (scancode - map->startingCode) * map->bytesPerCharacter ];
      for( i = 0; i < map->bytesPerCharacter; i++ )
      {
         keycode <<= 8;
         keycode += keycodeAddr[i];
      }
   }
   
   return keycode;
}


ErrorCode main( /*int argc, char *argv[]*/ void *param )
{
   MessageId messageId;
   Message message;
   ErrorCode ec;
   Driver drv;
   Task self;

   printf("inside PS2 keyboard driver\n");

   if( (ec = driverPS2KeyboardInit()) == ErrorNoError )
   {
      driverInit( &drv );
      driverManagerAdd( drv );
   
      taskGetCurrent( &self );
      taskGetMessageQueueId( self, &mainQueueId );
      idtSetTaskHandle( 0x21, self );
      
      messageQueueMap( mainQueueId, &mainQueue );

      /* 
       * this main loop only handles driver messages, as this app does nothing more then 
       * manage serial ports
       */
      while( messageQueueReceive( &mainQueue, TimeoutInfinite, (Pointer*)&messageId ) == ErrorNoError )
      {
         uint32 senderId;

         printf("PS2 keyboard driver received message\n");
         
         if( messageId )
         {
            ec = messageMap( messageId, &message );
            assert( ec == ErrorNoError );
            
            ec = messageGetSender( message, &senderId );
            assert( ec == ErrorNoError );
      
            switch( senderId )
            {
               case DRIVER_MESSAGE_SENDER_ID:
                  driverHandleMessage( message );
                  break;
               default:
                  printf("Unhandled message from 0x%8x\n", senderId);
                  assert(0);
            }

            printf("PS2 keyboard driver, done with message\n");
      
            messageDestroy( &message );
         }
         else
         {
            printf("PS2 driver wakeup message\n");
            /* it's a wakeup message, dequeue a key and send it */
            while( bufferUsage )
            {
               Key key;
               MessageId keyMessage;
               MessageQueue listenerQueue;
               
               mutexLock( &bufferMutex, TimeoutInfinite );
               
               key = buffer[bufferStart];
               bufferStart++;
               if( bufferStart >= bufferSize )
               {
                  bufferStart = 0;
               }
               bufferUsage--;
               
               mutexUnlock( &bufferMutex );

               printf("PS2 keyboard driver task received key %s 0x%2x value=%c\n", 
                  key.keypress ? "press" : "release", key.keycode,
                  defaultKeyMapMap[key.keycode] );
               
               // this this key to all listeners... only one for now
               listIteratorReset( listenersIterator );
               while( listIteratorGetNext( listenersIterator, (Pointer*)&listenerQueue ) == ErrorNoError )
               {
                  // create a message for this keypress (one for each listener)
                  messageCreate( &keyMessage, NULL, NULL, NULL, 0x1234ffee, KeyPress, 1, MessageParamType(key, sizeof(Key)) );
                  
                  messageQueueSend( &listenerQueue, (Pointer)keyMessage );
               }
            }
         }
         printf("PS2 keyboard driver next iter of while loop\n");
      }
   }
   else
   {
      printf("Couldn't initialize/find PS2 keyboard\n");
   }

   printf("PS2 keyboard driver exiting\n");
   
   return ec;
}

/* wait for the input buffer to be empty */
static void keyboardWaitInputBuffer(void)
{
   uint8 status;

   do
   {
      status = inportb( 0x64 );
   } while( status & 2 );
}

/* wait for the output buffer to be full */
static void keyboardWaitOutputBuffer(void)
{
   uint8 status;

   do
   {
      status = inportb( 0x64 );
   } while( (status & 1) == 0 );
}


static uint32 keyboardRead(void)
{
   uint32 timeout;
   uint32 stat, data, ret = -1;

   for(timeout = 500000L; timeout != 0; timeout--)
   {
      stat = inportb(0x64);
      /* loop until 8042 output buffer full */
      if(stat & 0x01)
      {
         data = inportb(0x60);
         /* loop if parity error or receive timeout (need to reread status port?)*/
         //if((stat & 0xC0) == 0)
         {
            ret = data;
            break;
         }
      }
   }

   return ret;
}

static void keyboardWrite(uint32 port, uint32 value)
{
   uint32 timeout;
   uint32 stat;

   for(timeout = 500000L; timeout != 0; timeout--)
   {
      stat = inportb(0x64);
      /* loop until 8042 input buffer empty */
      if((stat & 0x02) == 0)
         break;
   }
   if(timeout == 0)
   {
      printf("write_kbd: timeout\n");
      return;
   }
   outportb(port, value);
}

static ErrorCode keyboardWriteWithAck(uint32 val)
{
   ErrorCode ec = ErrorNoError;
   uint32 ack;

   keyboardWrite(0x60, val);
   ack = keyboardRead();
   if(ack != 0xFA)
   {
      printf("Didn't ACK -> got %2x\n",ack ); 
      ec = ErrorUnknown;
   }
   
   return ec;
}

static ErrorCode driverPS2KeyboardInit(void)
{
   ErrorCode ec = ErrorNoError;
   uint8 ack;

   // do this in kernel?
   //printf("creating gate for kbd in IDT\n" );
   //createGate(&IDT, 0x21, (long)int21h_wrapper, SEL_P0_CODE, D_TRAP);
   
   /* from Chris Giese... */
   printf("flushing keyboard output\n");
   while(keyboardRead() != -1);

   /* disable keyboard before programming it */
   printf("disabling keyboard controller\n");
   if(keyboardWriteWithAck(0xF5) != 0) 
   {
      printf("couldn't disable keyboard\n");
      /* fall-through... still try to setup keyboard configuration */
   }

   /* disable PS/2 mouse, set SYS bit, and Enable Keyboard Interrupt... */
   keyboardWrite(0x64, 0x60);
   
   /* disable AT-to-XT keystroke conversion */
   keyboardWrite(0x60, 0x25); /* 0x65 to enable translation */
   
   if( keyboardWriteWithAck( 0xed ) == ErrorNoError )
   {
      if( keyboardWriteWithAck( 0x00 ) != ErrorNoError )
      {
         printf("Unable to set LED status\n");
      }
   }
   else
   {
      printf("Unable to send keyboard LED command\n");
   }

   /* select scancode set 2 */
   printf("Selecting scancode set %d\n", scancodeSet);
   if( keyboardWriteWithAck(0xf0) == ErrorNoError )
   {
      if( keyboardWriteWithAck(scancodeSet) != ErrorNoError )
      {
         printf("Unable to select scancode set %d\n", scancodeSet);
      }
   }
   else
   {
      printf("Unable to send scancode select command\n");
   }
   
   /* we want all keys to return both a make and break code */
   if( scancodeSet == 3 )
   {
      printf("making all keys make-break\n");
      if(keyboardWriteWithAck(0xfa) != ErrorNoError)
      {
         printf("Couldn't make all keys make-break\n");
      }
   }

   /* set typematic delay and rate */
   printf("setting typematic params\n");
   if(keyboardWriteWithAck(0xf3) == ErrorNoError )
   {
      if(keyboardWriteWithAck(typematic) != ErrorNoError)
      {
         printf("Couldn't set typematic repeat rate\n");
      }
   }
   else
   {
      printf("Unable to send typematic repeat rate command\n");
   }
   
   /* enable keyboard */
   printf("enabling keyboard controller\n");
   if(keyboardWriteWithAck(0xf4) != ErrorNoError)
   {
      printf("couldn't enable keyboard controller\n");
      return -1;
   }
   /* --Chris Giese */
   
   buffer = (Key*)malloc( sizeof(Key) * KEYBOARD_DEFAULT_BUFFER_SIZE );
   mutexCreate( &bufferMutexId, MutexTypePriority | MutexTypeRecursive );
   mutexMap( bufferMutexId, &bufferMutex );
   
   listCreate( &listeners );
   listIteratorCreate( &listenersIterator, listeners );

   /* 
    * be sure to setup the keyboard before enabling IRQ 1, else ACK's will be  
    * swallowed by keyboard ISR
    */
   printf("enable IRQ 1\n");
   // should, in time, call a picRegisterIRQ( 1, myMessageQueueId );
   picEnableIRQ( 1 );
   
   return ec;   
}

      
DriverVariable driverKeyboardBufferSize(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardBufferUsage(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardBufferPosition(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardBufferStart(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardBufferFirstKey(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardBufferLastKey(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardModifierAdd(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardModifierSet(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardModifierUnset(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardMappingAdd(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardMappingRemove(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   
   ret.errCode = ErrorNoError;
   
   return ret;
}

DriverVariable driverKeyboardAddListener(DriverEvent event, DriverContext dctx, DriverVariable vars[])
{
   DriverVariable ret;
   MessageQueue messageQueue;
   MessageQueueId queueId = (MessageQueueId)vars[0].pointer;
   
   ret.errCode = ErrorNoError;
   printf("Adding listener for 0x%8x\n", vars[0].integer);
   
   /* map into our address space... */
   messageQueueMap( queueId, &messageQueue );

   /* @TODO need a listeners mutex!  For now, don't type when console comes up! :) */   
   listAddBack( listeners, (Pointer)messageQueue );
   
   return ret;
}
