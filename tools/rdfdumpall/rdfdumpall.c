#include <stdio.h>

#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>


typedef unsigned char      uint8;
typedef   signed char       int8;
typedef unsigned short     uint16;
typedef   signed short      int16;
typedef unsigned int       uint32;
typedef   signed int        int32;
/** @todo check this define!  What to do for ansi 64-bit type?  Is there one? */
#ifdef __GCC__
typedef unsigned long long uint64;
typedef   signed long long  int64;
#else
typedef unsigned int       uint64;
typedef   signed int        int64;
#endif

struct Segment
{
   uint32 offset;
};

struct _ObjectRDOFF2
{
   uint32 version;
   uint32 headerLength;
   uint32 headerOffset;
   uint32 length;
   uint32 pos;
   uint8 *mem;
   uint8 *end;
   struct Segment segment[2];
};

typedef struct _ObjectRDOFF2* ObjectRDOFF2;

static void openObject(char *fname, char **location, int *length);
static void parseObject(char *location, int length);
static void closeObject(char *location, int length);
static void parseHeader(ObjectRDOFF2 rdoff2);
static void parseSegments(ObjectRDOFF2 rdoff2);
static uint32 volatile readLong (ObjectRDOFF2 rdoff2);
static short volatile readShort(ObjectRDOFF2 rdoff2);
static char  volatile readByte (ObjectRDOFF2 rdoff2);

int file;

int main(int argc, char **argv)
{
   char *fname = argv[1];
   char *location;
   int length;

   openObject(fname, &location, &length);
   parseObject(location, length);
   closeObject(location, length);
}

static void openObject(char *fname, char **location, int *length)
{
   int fileLength;
   struct stat statbuf;

   file = open(fname, 0);

   fstat(file, &statbuf);
   fileLength = statbuf.st_size;
   *location = (char*)mmap(0, fileLength, PROT_READ, MAP_PRIVATE, file, 0);
   *length = fileLength;

   printf("mmapped %s of size %d to %x\n", fname, fileLength, *location);
}

static void parseObject(char *location, int length)
{
   ObjectRDOFF2 rdoff2;
   uint8 *data = (uint8*)location;

   // Does the module have the rdoff signature?
   if(strncmp(data, "RDOFF", 5))
   {
      printf("Not an RDOFF object\n");
      return;
   }
   else
   {
      rdoff2 = (ObjectRDOFF2)malloc(sizeof(struct _ObjectRDOFF2));
      //objectParserSetPrivateData(obj, rdoff2);

      rdoff2->version = data[5] - '0';
      printf("RDOFF Version %d.0\n", rdoff2->version);
   }

   // skip past the signature
   rdoff2->mem = location;
   rdoff2->end = (char*) (location + length);
   rdoff2->pos = 6;

   // this length read doesn't work properly!
   // However, everything else does!?
   if(rdoff2->version > 1)
   {
      rdoff2->length = readLong(rdoff2);
      printf("Object content size: %d bytes\n", (int)rdoff2->length);
   }

   // find the length of the header and record it
   rdoff2->headerLength = readLong(rdoff2);
   printf("HeaderLength %d\n", rdoff2->headerLength );
   rdoff2->headerOffset = rdoff2->pos;

   // skip over the header for now...
   rdoff2->pos += rdoff2->headerLength;

   // because I need segment info first!
   parseSegments(rdoff2);

   // skip back to the header
   rdoff2->pos = rdoff2->headerOffset;

   // and parse it!
   printf("Header (%d bytes):\n", rdoff2->headerLength);
   parseHeader( rdoff2);

   return;
}

static void closeObject(char *location, int length)
{
   munmap(location, length);
   close(file);
}

static void parseHeader(ObjectRDOFF2 rdoff2)
{
   char buf[129], type, segment, length, flags;
   char *symbolName;
   unsigned char recordLength;
   uint32 offset, labelLength;
   short refSegment;
   uint32 headerLength;

   char isRelative;
   uint32 symbolIndex;
   uint32 relocation = 0;
   char *segReloc;

   // loop through the length of the header
   headerLength = rdoff2->headerLength;
   while (headerLength > 0)
   {
      // read the segment type
      type = readByte(rdoff2);

      // if this is rdoff2, also read the record length
      if (rdoff2->version >= 2)
      {
         recordLength = readByte(rdoff2);
      }

      // which type have we got?
      switch(type)
      {
         case 1:      /* relocation record */
         case 6:      /* segment relocation */
            // JWA: printf("relocation (%d) :", type);
            segment    = readByte(rdoff2);
            offset     = readLong(rdoff2);
            length     = readByte(rdoff2);
            refSegment = readShort(rdoff2);
            // JWA: printf("seg: %x, offs %x, len %x, refSeg %x\n", segment, offset, length, refSegment);


            // if the referenced segment is less than three, we're
            // working with either the text, code or bss segment
            /* JWA
            if(refSegment < 3)
            {
               relocation = (long)rdoff2->mem + rdoff2->segment[refSegment].offset;
            }
            // otherwise, it's a segment/symbol which must be imported
            else
            {
               // find the segment (as mentioned above, if this is defined in the
               // rdoff _after_ it's used (here), this code will break.  _Shouldn't_
               // ever happen, though...
               symbolIndex = rZeroFindSegment(refSegment, rZeroExports);
               //printf("rs: %d, symbolIndex: %d\n", rs, symbolIndex);
               // record the location of this segment
               relocation = rZeroExports[symbolIndex].symbolLocation;
               //printf("symbol location %x, actual %x\n", rel, printf);
            }
            */

            isRelative = ((segment & 64) == 64);
            segment &= 63;

            /* If refSegment < 3 (should be maxSegment... rdoff2 states that there can be more then
             * 3 sections (ie, .text, .data, .bss, .comment, etc)
             * then it's a segment relocation, not a symbol relocation
             */
            if(refSegment >= 3)
            {
               //objectParserGetImportById(obj, refSegment, &symbol);
               printf("symbol relocation ");
            }
            else
            {
               //symbol = NULL;
               printf("section relocation ");
            }
            // remember, our sectionId's are one off from segment numbers
            //objectParserGetSectionById(obj, segment+1, &section);
            //objectParserCreateRelocation(&reloc, section, symbol, offset, length, isRelative);
            //objectParserAddRelocation(obj, reloc);

            // if this relocation is defined as relative, make sure our
            // relocation is defined as relative to the start of the
            // proper segment
            // JWA if( isRelative )
            //JWA: relocation -= (long)rdoff2->mem + rdoff2->segment[ segment ].offset;


            // calculate the address of the segment we're going to patch
            if(segment == 0)
            {
               segReloc = (char *)rdoff2->mem + rdoff2->segment[0].offset;
            }
            else if(segment == 1)
            {
               segReloc = (char *)rdoff2->mem + rdoff2->segment[1].offset;
            }
            else
            {
               // ??? BSS relocation ever needed?
               printf("relocation not needed here!\n");
               continue;
            }

            // debugging info...
            /*
            printf("rdoff2->mem: %x, rdoff2->mem + seg[0].off %x\n",
                  rdoff2->mem,
                  rdoff2->mem + rdoff2->segment[ segment  ].offset);
            */
            printf("patching area: %x + %x (%d byte relocation) seg %d ref %d\n", segReloc,
                                                   offset,
                                                   length,
                                                   segment,
                                                   refSegment);

            printf("current value %x %d\n", segReloc[offset], segReloc[offset]);

            // and, finally, perform the patch, based on the relocation size
            /*
            switch(length)
            {
               case 1:
                  segReloc[offset] += (char)relocation;
                  break;
               case 2:
                  *(short *)(segReloc + offset) += (short)relocation;
                  break;
               case 4:
                  //printf("current mem: %x\n", *(long *)(seg + o));
                  *(long *)(segReloc + offset) += (long)relocation;
                  break;
               default:
               printf("unknown relocation size!\n");
               break;
            }
            */

            //printf("  %s: location (%04x:%08x), length %d, "
            //     "referred seg %04x\n", t == 1 ? "relocation" : "seg relocation",
            //     (int)s, o,(int)l, rs);
            // perform some error checks...
            if(rdoff2->version >= 2 && recordLength != 8)
               printf("warning: reclen != 8\n");
            if(rdoff2->version == 1)
               headerLength -= 9;
            if(rdoff2->version == 1 && type == 6)
               printf("warning: seg relocation not supported in RDOFF1\n");
            break;

         case 2:             /* import record */
         case 7:      /* import far symbol */
            //printf("import: ");   // import from perspect of object -- get symbol from kernel
            flags = readByte(rdoff2);
            // get the referenced segment
            refSegment  = readShort(rdoff2);
            labelLength = 0;

            // read in the symbol name (different method, depending on
            // rdoff version!
            if (rdoff2->version == 1)
            {  // zero delimited
               do
               {
                  buf[labelLength] = readByte(rdoff2);
               } while (buf[labelLength++]);
            }
            else
            {                  // record length delimited (65536 - 4, max?)
               for (;labelLength < recordLength - 2; labelLength++)
                  buf[labelLength] = readByte(rdoff2);
            }
            printf("%s %d\n", buf, refSegment);

            /* allocate memory for the symbol name (buf will be obliterated when this
             * function exists
             */
            // TODO: need way to free this memory
            //symbolName = (char*)malloc(strlen(buf) + 1);
            //strcpy(symbolName, buf);

            //printf("  %simport: segment %04x = %s\n",t == 7 ? "far " : "",
            //     rs,buf);
            // update our length (ie, skip over this record)
            if (rdoff2->version == 1) headerLength -= labelLength + 3;

            // error checking...
            if (rdoff2->version == 1 && type == 7)
            printf ("    warning: far import not supported in RDOFF1\n");

            //objectParserCreateSymbol(&symbol, symbolName, refSegment, NULL);
            //objectParserAddImport(obj, symbol);


            // find the exported symbol, and assign it the segment number
            /* JWA:
            symbolIndex = rZeroFindSymbol(buf, rZeroExports);
            if(symbolIndex == -1) printf("Cannot find!\n");
            rZeroExports[symbolIndex].segmentNum = refSegment;
            */
            break;

         case 3:             /* export record */
            //printf("export: ");
            flags       = readByte(rdoff2);
            segment     = readByte(rdoff2);
            offset      = readLong(rdoff2);
            labelLength = 0;

            // read in the symbol name (different method depending on rdoff version)
            if (rdoff2->version == 1)
            {
               do
               {
                  buf[labelLength] = readByte(rdoff2);
               } while (buf[labelLength++]);
            }
            else
            {
               for (; labelLength < recordLength - 6; labelLength++)
                  buf[labelLength] = readByte(rdoff2);
            }
            printf("export %s @ %d\n", buf, offset);

            // TODO: need elegant way to free this memory
            //symbolName = (char*)malloc(strlen(buf) + 1);
            //strcpy(symbolName, buf);

            //objectParserCreateSymbol(&symbol, symbolName, segment, offset);
            //objectParserGetSectionById(obj, segment, &section);
            //objectParserAddExport(obj, symbol, section);

            /* what type of export?
            if (flags & SYM_GLOBAL)
            printf("  export");
            else
            printf("  global");
            if (flags & SYM_FUNCTION) printf(" proc");
            if (flags & SYM_DATA) printf(" data");
            printf(": (%04x:%08x) = %s\n",(int)s,o,buf);
            */
            // adjust the length for rdoff1 objects
            if (rdoff2->version == 1) headerLength -= labelLength + 6;

            // import this symbol into the kernel
            /*
            symbolIndex = rZeroFindSymbol(buf, rZeroImports);
            if(symbolIndex != -1)
               rZeroImports[symbolIndex].symbolLocation =
                  (long)rdoff2->mem + rdoff2->segment[segment].offset + offset;
            else printf("couldn't export\n");
            */
            break;

         case 4:      /* DLL and Module records */
         case 8:
            labelLength = 0;

            // read the module/dll name
            if(rdoff2->version == 1)
            {
               do
               {
                  buf[labelLength] = readByte(rdoff2);
               } while (buf[labelLength++]);
            }
            else
            {
               for (; labelLength < recordLength; labelLength++)
               {
                  buf[labelLength] = readByte(rdoff2);
               }
            }

            // simply print which module/dll is required (this should never
            // happen with an rZero module)
            if (type == 4) printf("  requires dll: %s\n", buf);
            else printf("  requires module: %s\n", buf);

            // adjust the length for rdoff1
            if (rdoff2->version == 1) headerLength -= labelLength + 1;
            break;
         case 5:      /* BSS reservation */
            labelLength = readLong(rdoff2);

            // do nothing for bss yet, just print it...
            printf("  bss reservation: %8x bytes\n", labelLength);

            if(labelLength)
            {
               // TODO: fix hardcoded 3... should be max segment + 1?
               //objectParserCreateSection(&section, ".bss", 3, 0 /*offset*/, labelLength, 2);
               //objectParserAddSection(obj, section);
            }

            // adjust length for rdoff1 and check for errors
            if (rdoff2->version == 1)  headerLength -= 5;
            if (rdoff2->version > 1 && recordLength != 4)
               printf("    warning: reclen != 4\n");
            break;

         case 9:      /* MultiBoot header record */
            printf("  MultiBoot header included!\n");
            printf("  Currently unsupported in rZero modules.\nSkipping over\n");
            // no need to adjust rdoff1 length, MB Headers only exist in rdoff2
            break;
         default:
            // print out the details of an unknown type...
            printf("  unrecognised record (type %d", (int)type);
            if (rdoff2->version > 1)
            {
               printf(", length %d)\n",(int)recordLength);
               rdoff2->pos += recordLength;
            }

            // adjust rdoff1 length
            if (rdoff2->version == 1) headerLength --;
      }

      // adjust rdoff2+ length
      if (rdoff2->version != 1) headerLength -= 2 + recordLength;
   }
}

/* Record all information from the segment info section of
 * the rdoff object
 */
static void parseSegments(ObjectRDOFF2 rdoff2)
{
   short segType;
   int i;

   // load in all the segment info
   //rdoff2->numSegments = 0;
   segType = readShort(rdoff2);
   while(segType != 0 /*&& rdoff2->numSegments < 6*/)
   {
      uint16 number;
      uint16 reserved;
      uint32 length;
      uint32 offset;
      char *name;

      //segment = (ObjectSection)malloc(sizeof(struct _ObjectSection));

      // what is number?  segmentId?
      number = readShort(rdoff2);
      reserved = readShort(rdoff2);
      length = readLong(rdoff2);
      offset = rdoff2->pos;

      switch(segType)
      {
         case 1:
            name = ".text";
            break;
         case 2:
            name = ".data";
            break;
         case 3:
            name = ".bss";
            break;
         default:
            assert(0);
            break;
      }

      rdoff2->segment[number].offset = offset;

      // segment numbers are 0 based, but we wish to be 1 based for sectionId's (0 is the
      // absense of a sectionId)
      //objectParserCreateSection(&section, name, number+1, offset, length, 2);
      //objectParserAddSection(obj, section);

      printf("Section: Type %d, Number %d, Reserved %d, Length %d, Offset %d\n",
         segType,
         number,
         reserved,
         length,
         offset);

      // seek past this segment
      rdoff2->pos += length;
      //rdoff2->numSegments++;

      segType = readShort(rdoff2);
   }

   // and print out a summary...
   /*
   printf("%d segments:\n", rdoff2->numSegments);
   for(i = 0; i < rdoff2->numSegments; i++) {
      printf("Type %d, Number %d, Reserved %d, Length %d, Offset %d\n",
         rdoff2->segment[i].type,
         rdoff2->segment[i].number,
         rdoff2->segment[i].reserved,
         rdoff2->segment[i].length,
         rdoff2->segment[i].offset);
   }
   */
   return;
}

/* The following read a long, short or byte from memory, converting it
 * from little-endian format to the processor's native format (unnecessary
 * for intel processors, but... it's portable, so I left it in :)
 */
static uint32 volatile readLong(ObjectRDOFF2 rdoff2)
{
  char *start = &rdoff2->mem[rdoff2->pos];
  uint32 r = 0;

  /*
  r = start[3];
  r = (r << 8) + start[2];
  r = (r << 8) + start[1];
  r = (r << 8) + start[0];
  */

  r = *(uint32 *)start;

  rdoff2->pos += 4;
  return r;
}

static short volatile readShort(ObjectRDOFF2 rdoff2)
{
  char *start = &rdoff2->mem[rdoff2->pos];
  short r = 0;

  //r = (start[1] << 8) + *start;
  r = *(short *)start;

  rdoff2->pos += 2;
  return r;
}

static char volatile readByte(ObjectRDOFF2 rdoff2)
{
  return rdoff2->mem[rdoff2->pos++];
}
