/* x86 running linux and using nasm as RDOFF */

#include <string.h>

#ifndef LCCDIR
#define LCCDIR "/usr/local/lib/lcc/"
#endif

#define NASMPATH "/usr/bin/nasm"

char *suffixes[] = { ".c", ".i", ".asm", ".rdf", ".rdx", 0 };
char inputs[256] = "";
char *cpp[] = { LCCDIR "cpp", "-D__STDC__=1",
	"-D__RDOFF__", "-Di386", "-D__i386", "-D__i386__",
	"-Dlinux", "-D__linux", "-D__linux__",
	"$1", "$2", "$3", 0 };
char *include[] = { "-I" LCCDIR "include", "-I/include/ndk",
	"-I/include", 0 };
char *com[] = { LCCDIR "rcc", "-target=x86/nasm",
	"$1", "$2", "$3", 0 };
char *as[] = { NASMPATH, "-frdf", "-o", "$3", "$1", "$2", 0 };
char *ld[] = { "/usr/bin/ldrdf", "-L", "/lib/ndk",
	"-o", "$3", "$1", "/lib/ndk/start.rdf", "$2", "",
	"/lib/ndk/end.rdf", 0 };
/*static char *bbexit = LCCDIR "bbexit.rdf";*/

extern char *concat(char *, char *);
extern int access(const char *, int);

int option(char *arg) {
	if (strncmp(arg, "-lccdir=", 8) == 0) {
		cpp[0] = concat(&arg[8], "/cpp");
		include[0] = concat("-I", concat(&arg[8], "/include"));
		com[0] = concat(&arg[8], "/rcc");
		/*bbexit = concat(&arg[8], "/bbexit.rdf");*/
	} else if (strcmp(arg, "-g") == 0)
		;
	/*
        else if (strcmp(arg, "-b") == 0 && access(bbexit, 4) == 0)
		ld[13] = bbexit;
        */
	else
		return 0;
	return 1;
}



