package org.neuraldk.ddfc;
import java.util.*;

class SimpleListHandler implements SectionHandler
{
	String list;
	boolean parsing = true;
	
	public SimpleListHandler(String list)
	{
		this.list = list;
	}
	
	public void parse(DriverDefinition ddf)
	{
		Iterator it = ddf.getTokenIterator();
		IntegerGroup ig;
		
		while( parsing )
		{
			ig = parseList(it);
			if(ig != null)
			{
				ddf.addGrouping(list, ig);
			}
		}
	}
	
	private IntegerGroup parseList(Iterator it)
	{
		IntegerGroup ig = null;
		Double low, high;
		Object token = it.next();
		String symbol;
		
		if(token.toString().equals("{"))
		{
			// goto next token...
			token = it.next();
		}
		
		if(token.toString().equals("}"))
		{
			parsing = false;
			return null;
		}
		
		if(token instanceof Double)
		{
			low = (Double)token;
			
			symbol = it.next().toString();
			if(symbol.equals("-"))
			{
				token = it.next();
				
				if(token instanceof Double)
				{
					high = (Double)token;
					ig = new IntegerGroup(low.intValue(), high.intValue());
					
					// get the comma now
					symbol = it.next().toString();
					if(symbol.equals("}"))
					{
						parsing = false;
					}
				}
				else
				{
					System.out.println("Expected a number got " + token.toString());
				}
			}
			else if(symbol.equals(",") || symbol.equals("}"))
			{
				ig = new IntegerGroup(low.intValue());
				
				if(symbol.equals("}")) parsing = false;
			}
			else
			{
				System.out.println("Expected - or ,");
			}
		}
		else
		{
			System.out.println("Expected number got " + token.toString());
		}
		
		return ig;
	}
}