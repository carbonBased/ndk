package org.neuraldk.ddfc;
import java.util.*;
import java.io.File;

public class DriverDefinition
{
	boolean compiled = false;
	IntegerGroup irqs, memory, ports;
	Vector root;
	Hashtable sectionHandlers, groupings, strings, types;
	TokenIterator tokIt;
	String ddfContents;
	String basePath;
	boolean reuseStaticVars;
	
	public DriverDefinition(String fName)
	{
		types = new Hashtable();
		strings = new Hashtable();
		groupings = new Hashtable();
		sectionHandlers = new Hashtable();
		sectionHandlers.put("driver", new SimpleStringHandler("driver"));
		sectionHandlers.put("namespace", new SimpleStringHandler("namespace"));
		sectionHandlers.put("description", new SimpleStringHandler("description"));
		sectionHandlers.put("author", new SimpleStringHandler("author"));
		sectionHandlers.put("copyright", new SimpleStringHandler("copyright"));
		sectionHandlers.put("irqs", new SimpleListHandler("irqs"));
		sectionHandlers.put("ports", new SimpleListHandler("ports"));
		sectionHandlers.put("memory", new SimpleListHandler("memory"));
		sectionHandlers.put("hierarchy", new HierarchyHandler());
		reuseStaticVars = true;
		
		types.put("char",      "DriverParamCharacter");
		types.put("string",    "DriverParamString");
		types.put("stringUtf", "DriverParamStringUtf");
		types.put("int64",     "DriverParamInteger64");
		types.put("int32",     "DriverParamInteger32");
		types.put("int16",     "DriverParamInteger16");
		types.put("int8",      "DriverParamInteger8");
		types.put("uint64",    "DriverParamUnsignedInteger64");
		types.put("uint32",    "DriverParamUnsignedInteger32");
		types.put("uint16",    "DriverParamUnsignedInteger16");
		types.put("uint8",     "DriverParamUnsignedInteger8");
		types.put("boolean",   "DriverParamBoolean");
		types.put("pointer",   "DriverParamPointer");
		types.put("void",      "DriverParamVoid");
		
		try
		{
			ddfContents = TextFile.read(new File(fName));
			System.out.println(ddfContents);
		}
		catch(Exception e)
		{
			System.out.println("Couldn't open file -> " + e);
		}
		tokIt = new TokenIterator(ddfContents);
	}
	
	public void compile()
	{
		SectionHandler handler;
		
		while(tokIt.hasNext())
		{
			String token = (String)(tokIt.next()).toString();
			System.out.println("-> " + token);
			
			handler = (SectionHandler)sectionHandlers.get(token);
			if(handler != null)
			{
				System.out.println("Found section handler for: " + token);
				if(getExpected(":"))
				{
					handler.parse(this);
				}
			}
		}
		
		compiled = true;
	}
	
	public void write(String basePath)
	{
		if(compiled)
		{
			this.basePath = basePath;
			writeInterfaceDotC();
			writeInterfaceDotH();
		}
	}
	
	public Iterator getTokenIterator()
	{
		return tokIt;
	}
	
	public void setString(String name, String value)
	{
		System.out.println("Setting " + name + " to " + value );
		strings.put(name, value);
	}
	
	public String getString(String name)
	{
		return (String)strings.get(name);
	}
	
	public void addGrouping(String type, IntegerGroup values)
	{
		Vector group;
		
		group = (Vector)groupings.get(type);
		if(group == null)
		{
			group = new Vector();
			groupings.put(type, group);
		}
		
		System.out.println("Adding " + values + " group to " + type);
		group.add(values);
	}
	
	public void setRoot(Vector root)
	{
		this.root = root;
	}
	
	private boolean getExpected(String searchToken)
	{
		boolean gotExpected = false;
		if(tokIt.hasNext())
		{
			/* get the colon */
			String token = (String)(tokIt.next()).toString();
			if(!token.equals(searchToken))
			{
				System.out.println("Syntax error, looking for " + searchToken + 
				                   ", got " + token + " at line " + tokIt.getLineNum());
			}
			else
			{
				gotExpected = true;
			}
		}
		else
		{
			System.out.println("Unexpected end of file, looking for " + searchToken + " at line " + tokIt.getLineNum());
		}
		
		return gotExpected;
	}
	
	private void writeInterfaceDotC()
	{
		StringBuffer text = new StringBuffer();
		String driverName = getString("driver");
		File file = new File( basePath + driverName + "Interface.c" );
		
		
		writeHeader(text, "Interface.c");
		
		text.append("#include <types.h>\n");
		text.append("#include <driver.h>\n");
      text.append("#include <klibc/assert.h>\n");
      text.append("#include <core/message.h>\n");
		text.append("#include <" + driverName + "Interface.h>\n\n");
		
		Iterator keys = strings.keySet().iterator();
		while(keys.hasNext())
		{
			String key = (String)keys.next();
			String value = (String)strings.get(key);
			
			text.append("const String " + key + " = \"" + value + "\";\n");
		}
      
      writeDriverInit( text );
      writeDriverHandleMessage( text );
		
		try
		{
			TextFile.write(file, text.toString());
		}
		catch( Exception e )
		{
			System.out.println("Couldn't write interface code " + e.toString());
		}
	}
   
   private void writeDriverInit( StringBuffer text )
   {
      String driverName = getString("driver");
      
      text.append("\n/* the following is used to avoid a bunch of malloc calls per each \n" +
            " * function call.  Instead, they're all allocated from this static \n" + 
            " * pool. \n" +
            " */\n");

      text.append("\n");
      text.append("ErrorCode driverInit( Driver *ret )\n");
      text.append("{\n");
      text.append("\tErrorCode err = ErrorNoError;\n");
      text.append("\tDriverItem driverItem, driverRoot, currentHub;\n");
      text.append("\tDriver drv;\n");
      if(reuseStaticVars)
      {
         text.append("\tDriverParam params[" + getMostParameters(root, 0) + "];\n");
      }
      else
      {
         text.append("\tDriverParam params[" + countParameters(root) + "];\n");
      }
      text.append("\tDriverParam *paramPointer;\n\n");
      
      text.append("\t// create driver here\n");
      
      //text.append("\terr = " + driverName + "Detect();\n");
      //text.append("\tif(err == ErrorNoError)\n");
      text.append("\t{\n");
      
      text.append("\t\terr = driverCreate( &drv );\n");
      text.append("\t\tassert(err == ErrorNoError);\n");
      
      reserveResources(text, "irqs");
      reserveResources(text, "ports");
      reserveResources(text, "memory");
      
      text.append("\n\n");
      
      text.append("\t\tparamPointer = &params[0];\n");
      writeDriverItems(text, root, 0);
      
      text.append("\t\tif(!err)\n");
      text.append("\t\t{\n");
      text.append("\t\t\terr = driverSetRoot(drv, \"" + strings.get("namespace") + "\", driverRoot);\n");
      
      //text.append("\t\t\tprintf(\"after driver set root %x\\n\", *ret);\n");
      text.append("\t\t\t*ret = drv;\n");
      //text.append("\t\t\tprintf(\"after driver set root2 %x\\n\", *ret);\n");
      text.append("\t\t}\n");
      
      //text.append("printf(\"%d\\n\", __LINE__);\n");
      
      text.append("\t}\n");
      //text.append("printf(\"%d\\n\", __LINE__);\n");
      text.append("\treturn err;\n");
      text.append("}\n\n");
      
      // dunno if we need driverFinal yet...?
      //text.append("ErrorCode driverFinal(Driver drv)\n");
      //text.append("{\n");
      //text.append("\tErrorCode err = ErrorNoError;\n\n");
      //text.append("\terr = " + driverName + "Final(drv);\n\n");
      //text.append("\treturn err;\n");
      //text.append("}\n");
   }
   
   private void writeDriverHandleMessage( StringBuffer text )
   {
      text.append("ErrorCode driverHandleMessage( Message msg )\n");
      text.append("{\n");
      text.append("   DriverItemHandlerFunction func;\n");
      text.append("   DriverVariable returnVar, *input;\n");
      text.append("   DriverContext ctx;\n");
      text.append("   MessageId returnMsgId;");
      text.append("   ErrorCode ec;\n");
      text.append("   uint32 code;\n\n");
      
      text.append("   messageGetCode( msg, &code );\n");
      text.append("   messageGetArgument       ( msg, 1, (Pointer*)&ctx );\n");
      text.append("   messageGetArgumentPointer( msg, 2, (Pointer*)&input );\n");
      text.append("   messageGetArgument       ( msg, 3, (Pointer*)&func );\n"); /* private data is a function */
      
      // debug...
      text.append("   printf(\"ctx 0x%x input 0x%x func 0x%x\", ctx, input, func);\n\n");

      /** 
       * @TODO: do we even need an event type?  Just pass input/output params to 
       *         all types
       */
      //text.append("   switch( code )\n");
      //text.append("   {\n");
      //text.append("      case DriverEventExecuteFunction:\n");
      //text.append("      case DriverEventQueryParameter:\n");
      //text.append("      case DriverEventSetParameter:\n");
      text.append("         returnVar = func( code, ctx, input );\n\n");
      //text.append("         break;\n");
      //text.append("   }\n\n");
      
      text.append("   messageCreate( &returnMsgId, NULL, NULL, msg, \n" +
                  "                  0, code, 1, MessageParamType( returnVar, DriverVariable ) );\n");
      text.append("   messageRespond( msg, (Pointer)returnMsgId );\n\n");
      
      text.append("   return ec;\n");
      text.append("}\n");
   }
	
	private void writeInterfaceDotH()
	{
		StringBuffer text = new StringBuffer();
		String name = getString("driver");
		File file = new File( basePath + name + "Interface.h" );
		
		writeHeader(text, "Interface.h");

		text.append("#include <errorCodes.h>\n");
		text.append("#include <driver.h>\n");
		text.append("#include <types.h>\n\n");

      text.append("typedef DriverVariable (*DriverItemHandlerFunction)( DriverEvent event, DriverContext ctx, DriverVariable vars[]);\n\n");
		
      text.append("ErrorCode driverInit( Driver *drv );\n\n");
      
		text.append("\n/* driver node handler functions... */\n");
		writePrototypes(text, root);
		
		try
		{
			TextFile.write(file, text.toString());
		}
		catch( Exception e )
		{
			System.out.println("Couldn't write interface header " + e.toString());
		}
	}
	
	private void writeHeader(StringBuffer text, String fname)
	{
		text.append("/* ndk - [ " + getString("driver") + fname + " ] \n");
		text.append(" * \n");
		text.append(" * The interface for the " + getString("driver") + " driver\n");
		text.append(" * Automatically generated.  Do not edit!\n" );
		text.append(" * \n");
		text.append(" * (c) 2005 " + getString("author") + "\n");
		text.append(" * \n");
		text.append(" * For more information on ndk and device drivers, please visit\n");
		text.append(" * www.neuraldk.org\n");
		text.append(" */\n\n");
	}
	
	private void reserveResources(StringBuffer text, String resource)
	{
		Vector list = (Vector)groupings.get(resource);
		
		if(list != null)
		{
			Iterator it = list.iterator();
			while(it.hasNext())
			{
				IntegerGroup ig = (IntegerGroup)it.next();
				
				text.append("\t\tif(!err) err = ");
				if(ig.isRange())
				{
					text.append("driverReserve" + capitalizedResource(resource) + "Range(drv," + 
							ig.getLow() + ", " + ig.getHigh() + ");\n");
				}
				else
				{
					text.append("driverReserve" + capitalizedResource(resource) + "(drv," + 
							ig.getLow() + ");\n");
				}
			}
		}
	}
	
	private void writePrototypes(StringBuffer text, Vector items)
	{
		if(items != null)
		{
			Iterator it = items.iterator();
			
			while(it.hasNext())
			{
				DriverItem di = (DriverItem)it.next();
				
				writePrototype(text, di);
				
				Vector subItems = di.getSubItems();
				writePrototypes(text, subItems);
			}
		}
	}
	
	private void writePrototype(StringBuffer text, DriverItem di)
	{
      /** @todo: consolidate */
		if( di.isNamespace() == false )
		{
			text.append("DriverVariable " + di.getFunction() + "(DriverEvent event, DriverContext ctx, DriverVariable vars[]);\n");
      }
      /* nothing to do for namespaces... no function handlers */
	}
	
	private void writeDriverItems(StringBuffer text, Vector items, int indentationLevels)
	{
		if(items != null)
		{
			Iterator it = items.iterator();
			String indentation = "\t\t";	// start with two levels
			int i;
			
			for(i = 0; i < indentationLevels; i++) indentation = indentation + "\t";
			
			/* construct a list to hold these items? */
			while(it.hasNext())
			{
				DriverItem di = (DriverItem)it.next();
				
				writeDriverItem(text, di, indentation);
				if(items == root)
				{
					text.append(indentation + "driverRoot = driverItem;\n");
				}
				else
				{
					text.append(indentation + "if(!err) err = driverItemAdd(currentHub, driverItem);\n");
				}
				
				Vector subItems = di.getSubItems();
				if((subItems != null) && (subItems.size() > 0))
				{
					text.append(indentation + "{\n");
					text.append(indentation + "\tDriverItem currentHub = driverItem;\n\n");
					writeDriverItems(text, subItems, indentationLevels+1);
				}
				else
				{
					text.append("\n");
				}
			}
			
			if(items != root)
			{
				text.append(indentation.substring(0,indentation.length()-1) + "}\n");
			}
		}
	}
	
	private void writeDriverItem(StringBuffer text, DriverItem di, String indentation)
	{
		if(di.isParameter())
		{
			text.append(indentation + "if(!err) err = driverItemCreateParameter(" +
					"&driverItem, \"" +
					di.getName() + "\", " + 
					getParameterType(di.getType()) + ", " +
					"(Pointer)" + di.getParameterFunction() + ");\n");
		}
		else if(di.isFunction())
		{
			//text.append("\t\t/* add " + di.getName() + " handler */\n");
			if(di.getNumParams() != 0)
			{
				writeDriverParams(text, di, indentation);
			}
			
			text.append(indentation + "if(!err) err = driverItemCreateFunction(" +
					"&driverItem, \"" +
					di.getName() + "\", " + 
					getParameterType(di.getType()) + ", " +
					di.getNumParams() + ", " +
					"paramPointer, " + 
					"(Pointer)" + di.getHandlerFunction() + ");\n");
			
			if((di.getNumParams() != 0) && (!reuseStaticVars))
			{
				text.append(indentation + "paramPointer += " + di.getNumParams());
			}
		}
		else
		{
			text.append(indentation + "if(!err) err = driverItemCreateNamespace(" +
					"&driverItem, \"" +
					di.getName() + "\");\n");
		}
	}
	
	private void writeDriverParams(StringBuffer text, DriverItem di, String indentation)
	{
		int i;
		
		for(i = 0; i < di.getNumParams(); i++)
		{
			//text.append("\t\tparamPointer[" + i + "].type = " + getParameterType(di.getParamType(i)) + ";\n");
			//text.append("\t\tparamPointer[" + i + "].name = \"\"; /** TODO: fix */\n");
			text.append(indentation + "if(!err) err = driverParamCreate(&paramPointer[" + i + "], \"" + 
					di.getParamName(i) + "\", " + 
					getParameterType(di.getParamType(i)) + ");\n");
		}
	}
	
	private int getMostParameters(Vector list, int currentHighest)
	{
		if(list != null)
		{
			Iterator it = list.iterator();
			
			while(it.hasNext())
			{
				DriverItem di = (DriverItem)it.next();
				
				if(di.getNumParams() > currentHighest)
				{
					currentHighest = di.getNumParams();
				}
				
				currentHighest = getMostParameters(di.getSubItems(), currentHighest);
			}
		}
		
		return currentHighest;
	}
	
	private int countParameters(Vector list)
	{
		int numParams = 0;
		
		if(list != null)
		{
			Iterator it = list.iterator();
			
			while(it.hasNext())
			{
				DriverItem di = (DriverItem)it.next();
				
				numParams += di.getNumParams();
				numParams += countParameters(di.getSubItems());
			}
		}
		
		return numParams;
	}
	
	/* Returns a singular (not plural) resource name with only the first 
	 * first character capitalized
	 */
	private String capitalizedResource(String str)
	{
		String firstCap = str.substring(0,1).toUpperCase() + str.substring(1).toLowerCase();
		
		if(firstCap.charAt(firstCap.length()-1) == 's')
		{
			firstCap = firstCap.substring(0, firstCap.length()-1);
		}
		
		return firstCap;
	}
	
	private String getParameterType(String paramType)
	{
		String param;
		
		if(paramType != null)
		{
			param = (String)types.get(paramType);
		}
		else
		{
			param = "DriverParamVoid";
		}
		
		return param;
	}

}
