package org.neuraldk.ddfc;
import java.util.*;

class ddfc
{
	public static void main( String[] args )
	{
		String inputFilename;
		DriverDefinition ddf;
		
		if(args.length == 1)
		{
			inputFilename = args[0];
			
			ddf = new DriverDefinition(inputFilename);
			ddf.compile();
			ddf.write("./");
		}
		else
		{
			System.out.println("Usage:\n");
			System.out.println("  ddfc input.ddf\n\n");
		}
	}
}