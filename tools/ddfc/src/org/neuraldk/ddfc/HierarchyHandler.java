package org.neuraldk.ddfc;
import java.util.*;

class HierarchyHandler implements SectionHandler
{
	Vector rootNode, currentNode;
	
	public HierarchyHandler()
	{
		rootNode = new Vector();
		currentNode = rootNode;
	}
	
	public void parse(DriverDefinition ddf)
	{
		Iterator it = ddf.getTokenIterator();
		
		if(getExpected("{", it, false) != null)
		{
			parseGroup(rootNode, it);
			ddf.setRoot(rootNode);
		}
	}
	
	private void parseGroup(Vector node, Iterator it)
	{
		String functionName;
		
		do
		{
			functionName = (String)it.next().toString();
			
			if(functionName.equals("{"))
			{
				/* the last function/parameter defined a new level of heirarchy... */
				DriverItem di = (DriverItem)node.lastElement();
			
				parseGroup(di.getSubItems(), it);
			}
			else if(functionName.equals("}"))
			{
				// nothing to do here?
			}
			else
			{
				parseFunctionCall(functionName, node, it);
			}
		
		} while(!functionName.equals("}"));
		
	}
	
	private void parseFunctionCall(String name, Vector node, Iterator it)
	{
		Vector paramTypes = new Vector();
		Vector paramNames = new Vector();
		String symbol, returnType = null;
		String functionName;
		DriverItem di = null;
		
		if(peekAtNext(it).toString().equals("{"))
		{
			di = new DriverItem(name, null, null, null);
		}
		else
		{
			symbol = (String)getExpectedType(String.class, it, true);
			if(symbol.equals("("))
			{
				/* function call defintion... */
				parseParameters(paramNames, paramTypes, it);
				System.out.println("got parameters " + paramTypes);
				
				if(peekAtNext(it).toString().equals(":"))
				{
					getExpected(":", it, true);
					returnType = (String)getExpectedType(String.class, it, true);
				}
				
				di = new DriverItem(name, paramNames, paramTypes, returnType);
				
				functionName = (String)getExpectedType(String.class, it, true);
				di.setHandlerFunction(functionName);
			}
			else if(symbol.equals(":"))
			{
				/* parameter definition... */
				returnType = (String)getExpectedType(String.class, it, true);
				
				di = new DriverItem( name, returnType );
				
				functionName = (String)getExpectedType(String.class, it, true);
				di.setParameterFunction(functionName);
				/*
				functionName = (String)getExpectedType(String.class, it, true);
				di.setSetFunction(functionName);
            */
			}
			else
			{
				System.out.println("Unknown symbol - " + symbol);
				System.exit(-1);
			}
		}
		
		node.add(di);
	}
	
	private void parseParameters(Vector paramNames, Vector paramTypes, Iterator it)
	{
		String paramName, paramType, symbol;
		do
		{
			paramName = (String)getExpectedType(String.class, it, false);
			
			// be sure to handle functions with no parameters
			if(paramName.equals(")")) break;
			
			getExpected(":", it, true);
			
			paramType = (String)getExpectedType(String.class, it, false);
			// confirmType( paramType );
			
			paramNames.add(paramName);
			paramTypes.add(paramType);
			
			symbol = it.next().toString();
			
		} while(symbol.equals(","));
	}
	
	private String getExpected(String name, Iterator it, boolean fail)
	{
		String value = it.next().toString();
		String retVal = null;
		
		if(value.equals(name))
		{
			retVal = value;
		}
		else
		{
			System.out.println("Got " + value + " expected " + name);
			if(fail)
			{
				System.exit(-1);
			}
			else
			{
				retVal = null;
			}
		}
		
		return retVal;
	}
	
	private Object getExpectedType(Class which, Iterator it, boolean fail)
	{
		Object value = it.next().toString();
		Object retType = null;
		
		if(value.getClass() == which)
		{
			retType = value;
		}
		else
		{
			System.out.println("Got " + value.getClass() + " expected " + which);
			if(fail)
			{
				System.exit(-1);
			}
			else
			{
				retType = null;
			}
		}
		
		return retType;
	}
	
	/* This is, admitantly, a hack... I know this iterator is actually
	 * a token iterator, and so I'll cast it, and check the next token.
	 * It's unfortunate the java Iterator class doesn't have a peek() 
	 * call
	 */
	private Object peekAtNext(Iterator it)
	{
		TokenIterator ti = (TokenIterator)it;
		
		return ti.peek();
	}
}