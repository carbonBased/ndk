package org.neuraldk.ddfc;
import java.util.*;

public class IntegerGroup
{
	private int low, high;
	
	public IntegerGroup(int low, int high)
	{
		this.low = low;
		this.high = high;
	}
	
	public IntegerGroup(int num)
	{
		this.low = num;
		this.high = num;
	}
	
	public boolean isRange()
	{
		return !(low == high);
	}
	
	public int getLow()
	{
		return low;
	}
	
	public int getHigh()
	{
		return high;
	}
	
	public String toString()
	{
		return "(" + low + " - " + high + ")";
	}
}
