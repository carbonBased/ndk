package org.neuraldk.ddfc;
import java.util.*;
import java.io.StreamTokenizer;
import java.io.StringReader;

class TokenIterator implements Iterator
{
	StreamTokenizer strTok;
	boolean hasNextToken;
	Object currentToken;
	int currentTokenType;
	
	public TokenIterator(String ddfContents)
	{
		strTok = new StreamTokenizer( new StringReader( ddfContents ) );
		strTok.ordinaryChar('{');
		strTok.ordinaryChar('}');
		strTok.ordinaryChar(':');
		strTok.parseNumbers();
		strTok.quoteChar('\'');
		strTok.quoteChar('"');
		strTok.slashSlashComments( true );
		strTok.slashStarComments( true );
		strTok.eolIsSignificant( false );
		
		hasNextToken = getToken();
	}
	
	public boolean hasNext()
	{
		return hasNextToken;
	}
		
	// Returns the next token
	public Object next()
	{
		Object previousToken = currentToken;
		int previousTokenType = currentTokenType;
		
		getToken();
		
		// if result == 0 and nextString starts with 'x', concatenate into a 
		// hex string and parse as a number (ie, 0x3f8)
		if( (previousTokenType == StreamTokenizer.TT_NUMBER) &&
			 (((Double)(previousToken)).intValue() == 0) &&
			 (currentTokenType == StreamTokenizer.TT_WORD) &&
		    (Character.toLowerCase( ((String)currentToken).charAt(0) ) == 'x'))
		{
			//previousToken = "0" + ((String)currentToken).toLowerCase();
			previousToken = new Double( Integer.parseInt( ((String)currentToken).substring(1), 16) );
			getToken();
		}
		
		return previousToken;
	}
	
	// Not supported.
	public void remove()
	{
		throw new UnsupportedOperationException();
	}
	
	public Object peek()
	{
		return currentToken;
	}
	
	public int getLineNum()
	{
		return strTok.lineno();
	}
	
	private boolean getToken()
	{
		try
		{
			hasNextToken = (strTok.nextToken() != StreamTokenizer.TT_EOF);
			
			currentTokenType = strTok.ttype;
			switch(currentTokenType)
			{
				case StreamTokenizer.TT_NUMBER:
					currentToken = new Double(strTok.nval);
					break;
				case StreamTokenizer.TT_WORD:
					currentToken = strTok.sval;
					break;
					/* string and character handling */
				case '\'':
					currentToken = new Character(strTok.sval.charAt(0));
					break;
				case '"':
					currentToken = strTok.sval;
					break;
				default:
					currentToken = new Character((char)currentTokenType);
			}
			
			return hasNextToken;
		}
		catch(Exception e)
		{
			return false;
		}
		
	}
	
	private boolean isAlpha(char c)
	{
		return( ((c >= 'a') && (c <= 'z')) ||
				  ((c >= 'A') && (c <= 'Z')) );
	}
	
	private boolean isDigit(char c)
	{
		return( (c >= '0') && (c <= '9'));
	}
	
}
