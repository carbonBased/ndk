package org.neuraldk.ddfc;
import java.io.*;

public class TextFile
{
	static boolean REPLACE_TABS = true;
	static int     NUMBER_OF_SPACES_IN_TAB = 3;
	
	static public String read(File aFile)
		throws FileNotFoundException, IOException
	{
		checkFile(aFile);
		StringBuffer contents = new StringBuffer();
	
		BufferedReader input = null;
		try
		{
			input = new BufferedReader( new FileReader(aFile) );
			String line = null; //not declared within while loop
			while (( line = input.readLine()) != null)
			{
				contents.append(line);
				contents.append(System.getProperty("line.separator"));
			}
		}
		catch (FileNotFoundException ex)
		{
			ex.printStackTrace();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			try
			{
				if (input!= null)
				{
					//flush and close both "input" and its underlying FileReader
					input.close();
				}
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}
		}
		
		return contents.toString();
	}

	/**
	* Change the contents of text file in its entirety, overwriting any
	* existing text.
	*
	* This style of implementation throws all exceptions to the caller.
	*
	* @param aFile is an existing file which can be written to.
	* @throws IllegalArgumentException if param does not comply.
	* @throws FileNotFoundException if the file does not exist.
	* @throws IOException if problem encountered during write.
	*/
	static public void write(File aFile, String aContents)
		throws FileNotFoundException, IOException
	{
		//checkFile(aFile);
		String tab = "        ";	// 8 spaces (note that this will break if there's more then 8 spaces in a tab)
		
		// replace all tabs with spaces if requested
		if(REPLACE_TABS)
		{
			aContents = aContents.replaceAll("\t", tab.substring(0,NUMBER_OF_SPACES_IN_TAB));
		}
	
		//declared here only to make visible to finally clause; generic reference
		Writer output = null;
		try {
			//use buffering
			output = new BufferedWriter( new FileWriter(aFile) );
			output.write( aContents );
		}
		finally
		{
			//flush and close both "output" and its underlying FileWriter
			if (output != null) output.close();
		}
	}

	/*
	public static void main ( String[] aArguments ) throws IOException {
		File testFile = new File("C:\\Temp\\blah.txt");
		setContents(testFile, "blah blah blah");
	
		System.out.println( "File contents: " + getContents(testFile) );
	}
	*/
	
	static public void checkFile(File aFile)
		throws FileNotFoundException, IOException
	{
		if (aFile == null)
		{
			throw new IllegalArgumentException("file handle null");
		}
		/*
		if (!aFile.exists())
		{
			throw new FileNotFoundException (aFile.toString() + " does not exit");
		}
		*/
		if (!aFile.isFile())
		{
			throw new IllegalArgumentException(aFile.toString() + " is a directory");
		}
		/*
		if (!aFile.canWrite())
		{
			throw new IllegalArgumentException(aFile.toString() + " isn't writeable");
		}
		*/
	}
} 
