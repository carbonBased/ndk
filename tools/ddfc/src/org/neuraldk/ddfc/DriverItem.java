package org.neuraldk.ddfc;
import java.util.*;

class DriverItem
{
	Vector paramTypes, paramNames;
	String name;
	String type;
   String paramFunction;            // for parameters (@TODO better method of handling this)
	String handlerFunction;				// for functions
	Vector subItems = null;
	
	public DriverItem( String functionName, Vector parameterNames, Vector parameterTypes, String returnType)
	{
		name = functionName;
		paramTypes = parameterTypes;
		paramNames = parameterNames;
		type = returnType;
		
		System.out.println(this);
	}
	
	public DriverItem( String paramName, String paramType )
	{
		name = paramName;
		type = paramType;
	}
	
	public void setParameterFunction(String paramFunction)
	{
		this.paramFunction = paramFunction;
	}
	
	public void setHandlerFunction( String function )
	{
		handlerFunction = function;
	}
	
	public boolean isParameter()
	{
		return (paramFunction != null);
	}
	
	public boolean isFunction()
	{
		return (handlerFunction != null);
	}
	
	public boolean isNamespace()
	{
		return (handlerFunction == null) && (paramFunction == null);
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getType()
	{
		return type;
	}
	
	public int getNumParams()
	{
		int numParams = 0;
		
		if(paramTypes != null)
		{
			numParams = paramTypes.size();
		}
		
		return numParams;
	}
	
	public String getParamType(int paramNum)
	{
		return (String)paramTypes.get(paramNum);
	}
	
	public String getParamName(int paramNum)
	{
		return (String)paramNames.get(paramNum);
	}	
	
	public String getParameterFunction()
	{
		return paramFunction;
	}
	
	public String getHandlerFunction()
	{
		return handlerFunction;
	}
   
   public String getFunction()
   {
      if( handlerFunction != null )
      {
         return handlerFunction;
      }
      else
      {
         return paramFunction;
      }
   }
	
	public Vector getSubItems()
	{
		if(subItems == null)
		{
			/* only create the vector when we actually need it */
			subItems = new Vector();
		}
		
		return subItems;
	}
	
	public String toString()
	{
		return name + " ( " + paramTypes + " ): " + type;
	}
}