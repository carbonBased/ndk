package org.neuraldk.ddfc;
import java.util.*;

class SimpleStringHandler implements SectionHandler
{
	String item;
	
	public SimpleStringHandler(String item)
	{
		this.item = item;
	}
	
	public void parse(DriverDefinition ddf)
	{
		Iterator it = ddf.getTokenIterator();
		
		String value = it.next().toString();
		ddf.setString(item, value);
	}
}